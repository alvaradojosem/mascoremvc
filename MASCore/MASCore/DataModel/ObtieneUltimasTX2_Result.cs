﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MASCore.DataModel
{
    public partial class ObtieneUltimasTX2_Result
    {
        public string Matricula { get; set; }
        public string Embarcacion { get; set; }
        public string RNP { get; set; }
        public string RazonSocial { get; set; }
        public string TipoPermiso { get; set; }
        public string PuertoBase { get; set; }
        public string UltimaTx { get; set; }
        public string Latitud { get; set; }
        public string Longitud { get; set; }
        public string Antena { get; set; }
        public string EnPuerto { get; set; }
        public string Tx { get; set; }
    }


}
