﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel;
using MASCore.ParaBorrar;

namespace MASCore.Models
{
    public class CargaImagenesModel
    {
        [DisplayName("Fecha:")]
        public DateTime Fecha { get; set; }
        [DisplayName("Dependencias:")]
        public string tipo { get; set; }
        [DisplayName("Tipo:")]
        public string subtipo1 { get; set; }
        [DisplayName("Subtipo:")]
        public string subtipo2 { get; set; }

        [DisplayName("Dependencias:")]
        public string tipobus { get; set; }
        [DisplayName("Tipo:")]
        public string subtipo1bus { get; set; }
        [DisplayName("Subtipo:")]
        public string subtipo2bus { get; set; }

        [DisplayName("Tipo:")]
        public string TiposDoc { get; set; }

        //[DisplayName("Identificador:")]
        //public string NombreIdentificador { get; set; }

        [DisplayName("Identificador:")]
        public string NumeroIdentificador { get; set; }

        public int EXISTE { get; set;}

        [DisplayName("RNP : ")]
        public string RNP { get; set; }

        [DisplayName("Rango de Fecha")]
        public Boolean UsaFechas { get; set; }

        [DisplayName("Fecha Inicial:")]
        public DateTime FechaInicio { get; set; }
        [DisplayName("Fecha Final:")]
        public DateTime FechaFin { get; set; }

        public List<BuscaCargaImagenes_Result> ListaImagenes { get; set; }
    }

    public class MonitoreoEmbarcPesqModel
    {
        [DisplayName("Tipo:")]
        public string Tipo { get; set; }

        [DisplayName("Descripción:")]
        public string nArchivo { get; set; }

        [DisplayName("Fecha de Registro:")]
        public DateTime fRegistro { get; set; }

        [DisplayName("Archivo:")]
        public DateTime sArchivo { get; set; }

    }
}