﻿using MASCore.ModelReference;
using System;
//V16+ para descarga de todos los oficios



namespace MASCore.Models
{
    public class FuncionesProcedimientos
    {
        //private BDMasContextEnt db = new BDMasContextEnt();

        public string SplitCadenaAplicaTrim(string Cadena, char CaracterSplit)
        {
            string RegresaCadena = "";
            string DatosTrim = "";
            Cadena = Cadena.Trim().Replace("'", "");
            string[] TokensBarco = Cadena.Split(',');
            if (TokensBarco.Length > 0)
            {
                foreach (string Token in TokensBarco)
                {
                    if (Token.Trim().Length > 0) DatosTrim += Token.Trim() + ',';
                }
                if (DatosTrim.Substring(DatosTrim.Length - 1, 1) == ",") DatosTrim = DatosTrim.Substring(0, DatosTrim.Length - 1);
                RegresaCadena = DatosTrim;
                //RegresaCadena = " AND V.Nombre in ('" + DatosTrim.Replace(",", "','") + "') ";
            }
            else
            {
                RegresaCadena = Cadena;
            }

            return RegresaCadena;
        }

        public string ValidaPermisoUsuario7X24(string UserName)
        {
            string Valida = "0";
            //ValidaPermisoUsuario7X24_Result Res = db.ValidaPermisoUsuario7X24(UserName).FirstOrDefault();
            //if (Res != null)
            //    Valida = Res.Valido.ToString();
            return Valida;
        }

        //public List<SelectListItem> LlenarListaFija(string busca)
        //{
        //    List<SelectListItem> items = new List<SelectListItem>();
        //    if (busca == "MOSTRARINFORMACION")
        //    {
        //        items.Add(new SelectListItem { Text = "Todo", Value = "Ver Todo" });
        //        items.Add(new SelectListItem { Text = "Sin Cedula", Value = "Sin Cedula" });
        //        items.Add(new SelectListItem { Text = "Con Cedula", Value = "Sin Cedula" }); 
        //    }          
        //    return items;
        //}

        //public List<ReporteTxIrregularModelo> LlenarTxIrregular( Guid UserId)
        //{
        //    //Guid UserId;
        //    //ObtieneIdUsuario_Result IdUsuario = db.ObtieneIdUsuario(User).FirstOrDefault();
        //    ////if (IdUsuario != null)
        //    ////{
        //    //    UserId = IdUsuario.UserId;
        //    ////}

        //   List<ReporteTxIrregularModelo> ListaFinal = new List<ReporteTxIrregularModelo>();
        //   List<TxIrregularReporte_Result> ListaTmp = db.TxIrregularReporte(UserId).ToList();
        //   if (ListaTmp != null)
        //   {
        //       foreach (TxIrregularReporte_Result Res in ListaTmp)
        //       {
        //           ReporteTxIrregularModelo Nuevo = new ReporteTxIrregularModelo();
        //           Nuevo.IdMensaje = Res.IdMensajeM.ToString();
        //           Nuevo.RNP = Res.RNP;
        //           Nuevo.Matricula = Res.Matricula;
        //           Nuevo.Embarcacion = Res.Embarcacion;
        //           Nuevo.NoSerie = Res.NoSerie;
        //           Nuevo.RazonSocial = Res.RazonSocial;
        //           Nuevo.Localidad = Res.Localidad;
        //           Nuevo.Zona = Res.Zona;
        //           Nuevo.UltimoReporte = Res.UltimoReporte.ToString();

        //           ListaFinal.Add(Nuevo);
        //       }
        //   }

        //   return ListaFinal;
        //}

        //public List<BuscaResultadoGenerico_Result> LlenarResultadoGenerico(string busca)
        //{
        //    //db = new BDMasContextEnt();  
        //    // Usando SP    SPTMasLitoralesSeleccionar
        //    List<BuscaResultadoGenerico_Result> listatmp = db.BuscaResultadoGenericoSP(busca).ToList();
        //    //Agregado por SZ el 24/11/2016
        //    //Se agregaron para funcionalidad del bucle.
        //    List<BuscaResultadoGenerico_Result> listatmp2 = db.BuscaResultadoGenericoSP(busca).ToList();
        //    List<BuscaResultadoGenerico_Result> listax = new List<BuscaResultadoGenerico_Result>();

        //    if (busca == "nombre_rnp_matriculabarco")
        //    {

        //        for (int i = 0; i < listatmp2.Count; i++)
        //        {
        //            string cadena = listatmp2[i].Resultado.ToString();
        //            char[] splitchar = { '|' };
        //            string[] strArr = cadena.Split(splitchar);
        //            //listax.Add(new BuscaResultadoGenerico_Result { Resultado = strArr[1] });
        //            listax.Add(new BuscaResultadoGenerico_Result { Resultado = cadena });
        //        }

        //        listatmp = new List<BuscaResultadoGenerico_Result>();
        //        listatmp = listax;

        //    }
        //    //Permite traer los datos del UniqueIdentifier de cada embarcación para posteriormente usarlas de parametros.
        //    if (busca == "nombre_rnp_matriculabarcoUi")
        //    {

        //        for (int i = 0; i < listatmp2.Count; i++)
        //        {
        //            string cadena = listatmp2[i].Resultado.ToString();
        //            char[] splitchar = { '|' };
        //            string[] strArr = cadena.Split(splitchar);
        //            listax.Add(new BuscaResultadoGenerico_Result { Resultado = strArr[0] });
        //        }

        //        listatmp = new List<BuscaResultadoGenerico_Result>();
        //        listatmp = listax;

        //    }
        //    //Fin Agreado por mi
        //    return listatmp;
        //}

        //public List<BuscaResultadoGenericoConParametros_Result> ObtieneTexto5(string embarcacion)
        //{
        //    List<BuscaResultadoGenericoConParametros_Result> listatexto5 = db.BuscaResultadoGenericoConParametros("Texto5","",embarcacion,0,0).ToList();
        //    return listatexto5;
        //}

        //public ICollection<BuscaResultadoGenericoConParametros_Result> BuscaResultadoGenericoConParametrosLista(string Busca_ = "", string Matricula_ = "", string ParametroNvarchar = "", int ParametroInt = 0, Decimal ParametroDecimal = 0)
        //{

        //    ICollection<BuscaResultadoGenericoConParametros_Result> ResultadoGenerico = db.BuscaResultadoGenericoConParametros(Busca_, Matricula_, ParametroNvarchar, ParametroInt, ParametroDecimal).ToList();

        //    //foreach (BuscaResultadoGenericoConParametros_Result Resultado in ResultadoGenerico)
        //    //{
        //    //    ResultadoFinal = ResultadoFinal + "," + Resultado.Resultado;
        //    //}
        //    //if (ResultadoFinal.Length > 1)
        //    //    ResultadoFinal = ResultadoFinal.Substring(1, ResultadoFinal.Length - 1); ;
        //    return ResultadoGenerico;

        //}

        //public string BuscaResultadoGenericoConParametros(string Busca_ = "", string Matricula_ = "", string ParametroNvarchar = "", int ParametroInt = 0, Decimal ParametroDecimal = 0)
        //{
        //    string ResultadoFinal = "";
        //    ICollection<BuscaResultadoGenericoConParametros_Result> ResultadoGenerico = db.BuscaResultadoGenericoConParametros(Busca_, Matricula_, ParametroNvarchar, ParametroInt, ParametroDecimal).ToList();
        //    foreach (BuscaResultadoGenericoConParametros_Result Resultado in ResultadoGenerico)
        //    {
        //        ResultadoFinal = ResultadoFinal + "," + Resultado.Resultado;
        //    }
        //    if (ResultadoFinal.Length > 1)
        //        ResultadoFinal = ResultadoFinal.Substring(1, ResultadoFinal.Length - 1); ;
        //    return ResultadoFinal;
        //}

        //public string BuscaResultadoGenericoConParametrosPermisos(string Busca_ = "", string Matricula_ = "", string ParametroNvarchar = "", int ParametroInt = 0, Decimal ParametroDecimal = 0)
        //{
        //    string ResultadoFinal = "";
        //    ICollection<BuscaResultadoGenericoConParametros_Result> ResultadoGenerico = db.BuscaResultadoGenericoConParametros(Busca_, Matricula_, ParametroNvarchar, ParametroInt, ParametroDecimal).ToList();
        //    foreach (BuscaResultadoGenericoConParametros_Result Resultado in ResultadoGenerico)
        //    {
        //        ResultadoFinal = ResultadoFinal + "|" + Resultado.Resultado;
        //    }
        //    if (ResultadoFinal.Length > 1)
        //        ResultadoFinal = ResultadoFinal.Substring(1, ResultadoFinal.Length - 1); ;
        //    return ResultadoFinal;
        //}

        //public GetPasswordSISMEP_Result BuscaDatosDeUsuario(string Usuario)
        //{
        //    // Se cambió el 19/10/2017 actualización de entity framework 5
        //    //db.CommandTimeout = 200;
        //    ((IObjectContextAdapter)this.db).ObjectContext.CommandTimeout = 200;
        //    return db.GetPasswordSISMEP(Usuario).FirstOrDefault();
        //}

        public bool ValidarColumna(System.Data.SqlClient.SqlDataReader reader, string nombreColumna)
        {

            reader.GetSchemaTable().DefaultView.RowFilter =

              "ColumnName= '" + nombreColumna + "'";

            return (reader.GetSchemaTable().DefaultView.Count > 0);

        }

        private string ConvierteFechaInicialFinal(DateTime Fecha, bool EsFechaFinal = false)
        {
            string FechaConvertida = "";
            if (EsFechaFinal == true)
            {
                FechaConvertida = Fecha.Year.ToString() + '-' + Fecha.Month.ToString().PadLeft(2, '0') + '-' + Fecha.Day.ToString().PadLeft(2, '0') + " 00:00:00";
            }
            else
            {
                FechaConvertida = Fecha.Year.ToString() + '-' + Fecha.Month.ToString().PadLeft(2, '0') + '-' + Fecha.Day.ToString().PadLeft(2, '0') + " 23:59:59";
            }
            return FechaConvertida;
        }

        public DateTime ConvierteFechaInicialFinalTipoFecha(DateTime Fecha, bool EsFechaFinal = false)
        {
            string FechaConvertida = "";
            if (EsFechaFinal == true)
            {
                FechaConvertida = Fecha.Year.ToString() + '-' + Fecha.Month.ToString().PadLeft(2, '0') + '-' + Fecha.Day.ToString().PadLeft(2, '0') + " 00:00:00";
            }
            else
            {
                FechaConvertida = Fecha.Year.ToString() + '-' + Fecha.Month.ToString().PadLeft(2, '0') + '-' + Fecha.Day.ToString().PadLeft(2, '0') + " 23:59:59";
            }

            return Convert.ToDateTime(FechaConvertida);
        }

        public string QuitaAcentos(string CadenaDeTexto)
        {
            string SinAcentos = CadenaDeTexto.ToUpper();
            SinAcentos = SinAcentos.Replace('Á', 'A');
            SinAcentos = SinAcentos.Replace('É', 'E');
            SinAcentos = SinAcentos.Replace('Í', 'I');
            SinAcentos = SinAcentos.Replace('Ó', 'O');
            SinAcentos = SinAcentos.Replace('Ú', 'U');

            return SinAcentos;
        }

        public string ObtieneIdentificadorFechaHora()
        {
            return DateTime.Now.ToString("dd-MM-yyyy") + " " + DateTime.Now.Hour.ToString() + DateTime.Now.Minute.ToString() + DateTime.Now.Second.ToString();
        }

        //#region "REPORTES AVANZADOS"

        #region "REPORTE DE TX IRREGULAR"
        //      public List<Evidencias> RegresaEvidencias ( string Usuario, string Matricula)
        //      {
        //          //ModeloTmp.ListaEvidencias = db.SPMasTxIrregularEvidencias("xxx", Matricula).ToList();
        //          List<Evidencias> ListaFinal = new List<Evidencias>();
        //          List<SPMasTxIrregularEvidencias_Result> Lista = new List<SPMasTxIrregularEvidencias_Result>();
        //          Lista = db.SPMasTxIrregularEvidencias("xxx", Matricula).ToList();
        //          foreach (SPMasTxIrregularEvidencias_Result listaObtenida in Lista)
        //          {
        //              Evidencias NuevoElemento = new Evidencias();
        //              NuevoElemento.Orden = listaObtenida.Orden.ToString();
        //              NuevoElemento.Fecha = listaObtenida.Fecha;
        //              NuevoElemento.Latitud = ConvertirCoordenada(listaObtenida.Latitud.ToString());
        //              NuevoElemento.Longitud = ConvertirCoordenada(listaObtenida.Longitud.ToString());
        //              NuevoElemento.Rumbo = listaObtenida.Rumbo.ToString();
        //              NuevoElemento.Velocidad = listaObtenida.Velocidad.ToString();
        //              ListaFinal.Add(NuevoElemento);
        //          }
        //          return ListaFinal;
        //      }
        //      #endregion

        //      public List<ObtieneMarcadoresDetalle_Result> ObtieneMarcadoresDetalleTabular(MarcadoresEarthModel ModeloTmp)
        //      {
        //          bool TotalPorEmarcaciones = false;
        //          List<MarcadoresEarth> Lista = new List<MarcadoresEarth>();
        //          List<ObtieneMarcadoresDetalle_Result> ListaMarcadores = new List<ObtieneMarcadoresDetalle_Result>();
        //          string Permiso = ModeloTmp.Permiso;
        //          #region "SE CONCATENAN LOS PERMISOS"
        //          switch (ModeloTmp.Permiso)
        //          {
        //              case "Fomento":
        //                  Permiso = "FOMENTO";
        //                  break;
        //              case "Túnidos":
        //                  Permiso = "TUNIDOS";
        //                  break;
        //              case "Camarón de altamar":
        //                  Permiso = "CAMARON DE ALTAMAR";
        //                  break;
        //              case "Calamar":
        //                  Permiso = "CALAMAR";
        //                  break;
        //              case "Langosta":
        //                  Permiso = "LANGOSTA";
        //                  break;
        //              case "Pulpo":
        //                  Permiso = "PULPO";
        //                  break;
        //              case "Tiburón":
        //                  Permiso = "TIBURON";
        //                  break;
        //              case "Picudos":
        //                  Permiso = "PEZ ESPADA";
        //                  break;
        //              case "Escama marina":
        //                  Permiso = "ESCAMA MARINA";
        //                  break;
        //              case "Pelágicos menores":
        //                  Permiso = "PELAGICOS MENORES','ANCHOVETA','SARDINA";
        //                  break;
        //              case "Otros":
        //                  Permiso = "CANGREJO','JAIBA','CARNADA VIVA";
        //                  break;
        //              case "Total":
        //                  Permiso = "FOMENTO','TUNIDOS','CAMARON DE ALTAMAR','CALAMAR','LANGOSTA','PULPO','TIBURON','PEZ ESPADA','ESCAMA MARINA','PELAGICOS MENORES','ANCHOVETA','SARDINA','CANGREJO','JAIBA','CARNADA VIVA','TIBURON MENORES', 'JAIBA MENORES', 'ESCAMA MARINA MENORES', 'CAMARON DE ALTAMAR MENORES";
        //                  break;
        //              //Total de permisos en el calculo
        //              case "Total de permisos en el calculo": // Antes Total
        //                  Permiso = "FOMENTO','TUNIDOS','CAMARON DE ALTAMAR','CALAMAR','LANGOSTA','PULPO','TIBURON','PEZ ESPADA','ESCAMA MARINA','PELAGICOS MENORES','ANCHOVETA','SARDINA','CANGREJO','JAIBA','CARNADA VIVA','TIBURON MENORES', 'JAIBA MENORES', 'ESCAMA MARINA MENORES', 'CAMARON DE ALTAMAR MENORES";
        //                  break;
        //              case "Total de embarcaciones":
        //                  TotalPorEmarcaciones = true;
        //                  Permiso = "FOMENTO','TUNIDOS','CAMARON DE ALTAMAR','CALAMAR','LANGOSTA','PULPO','TIBURON','PEZ ESPADA','ESCAMA MARINA','PELAGICOS MENORES','ANCHOVETA','SARDINA','CANGREJO','JAIBA','CARNADA VIVA','TIBURON MENORES', 'JAIBA MENORES', 'ESCAMA MARINA MENORES', 'CAMARON DE ALTAMAR MENORES";
        //                  break;


        //              case "²Total de embarcaciones operando, en puerto y transmitiendo con permiso registrado":
        //                  TotalPorEmarcaciones = true;
        //                  Permiso = "FOMENTO','TUNIDOS','CAMARON DE ALTAMAR','CALAMAR','LANGOSTA','PULPO','TIBURON','PEZ ESPADA','ESCAMA MARINA','PELAGICOS MENORES','ANCHOVETA','SARDINA','CANGREJO','JAIBA','CARNADA VIVA','TIBURON MENORES', 'JAIBA MENORES', 'ESCAMA MARINA MENORES', 'CAMARON DE ALTAMAR MENORES";
        //                  break;
        //              case "²Total de embarcaciones operando y transmitiendo con permiso registrado":
        //                  TotalPorEmarcaciones = true;
        //                  Permiso = "FOMENTO','TUNIDOS','CAMARON DE ALTAMAR','CALAMAR','LANGOSTA','PULPO','TIBURON','PEZ ESPADA','ESCAMA MARINA','PELAGICOS MENORES','ANCHOVETA','SARDINA','CANGREJO','JAIBA','CARNADA VIVA','TIBURON MENORES', 'JAIBA MENORES', 'ESCAMA MARINA MENORES', 'CAMARON DE ALTAMAR MENORES";
        //                  break;
        //              case "²Total de embarcaciones operando, en la mar y transmitiendo con permiso registrado":
        //                  TotalPorEmarcaciones = true;
        //                  Permiso = "FOMENTO','TUNIDOS','CAMARON DE ALTAMAR','CALAMAR','LANGOSTA','PULPO','TIBURON','PEZ ESPADA','ESCAMA MARINA','PELAGICOS MENORES','ANCHOVETA','SARDINA','CANGREJO','JAIBA','CARNADA VIVA','TIBURON MENORES', 'JAIBA MENORES', 'ESCAMA MARINA MENORES', 'CAMARON DE ALTAMAR MENORES";
        //                  break;
        //              case "¹Total de permisos vigentes registrados en embarcaciones":
        //                  Permiso = "FOMENTO','TUNIDOS','CAMARON DE ALTAMAR','CALAMAR','LANGOSTA','PULPO','TIBURON','PEZ ESPADA','ESCAMA MARINA','PELAGICOS MENORES','ANCHOVETA','SARDINA','CANGREJO','JAIBA','CARNADA VIVA','TIBURON MENORES', 'JAIBA MENORES', 'ESCAMA MARINA MENORES', 'CAMARON DE ALTAMAR MENORES";
        //                  break;
        //              //PermisosPangas ini
        //              case "Tiburón menores":
        //                  Permiso = "TIBURON MENORES";
        //                  break;
        //              case "Jaiba menores":
        //                  Permiso = "JAIBA MENORES";
        //                  break;
        //              case "Escama marina menores":
        //                  Permiso = "ESCAMA MARINA MENORES";
        //                  break;
        //              case "Camarón de altamar menores":
        //                  Permiso = "CAMARON DE ALTAMAR MENORES";
        //                  break;
        //              //PermisosPangas fin
        //              default:
        //                  // You can use the default case.
        //                  break;
        //          }
        //          #endregion

        //          ListaMarcadores = db.ObtieneMarcadoresDetalle(ModeloTmp.Reporte, ModeloTmp.Litoral, Permiso, TotalPorEmarcaciones).ToList();
        //          //if (ListaMarcadores.Count > 0)
        //          //{
        //          //    foreach (ObtieneMarcadoresDetalle_Result Fila in ListaMarcadores)
        //          //    {
        //          //        MarcadoresEarth NuevoMarcador = new MarcadoresEarth();
        //          //        NuevoMarcador.Latitud = Fila.Latitud;
        //          //        NuevoMarcador.Longitud = Fila.Longitud;
        //          //        NuevoMarcador.NombreSitio = Fila.Embarcacion;
        //          //        NuevoMarcador.Info = Fila.TipoPermiso;
        //          //        Lista.Add(NuevoMarcador);
        //          //    }
        //          //}

        //          return ListaMarcadores;
        //      }
        //public List<ObtieneMarcadoresDetalleAlertasCedulas_Result> ObtieneMarcadoresDetalleTabularAlertasCedulas(MarcadoresEarthModelRepEstadistico ModeloTmp)
        //      {
        //          bool TotalPorEmarcaciones = false;
        //          List<MarcadoresEarth> Lista = new List<MarcadoresEarth>();
        //          List<ObtieneMarcadoresDetalleAlertasCedulas_Result> ListaMarcadores = new List<ObtieneMarcadoresDetalleAlertasCedulas_Result>();
        //          string Permiso = ModeloTmp.Permiso;
        //          #region "SE CONCATENAN LOS PERMISOS"
        //          switch (ModeloTmp.Permiso)
        //          {
        //              case "Fomento":
        //                  Permiso = "FOMENTO";
        //                  break;
        //              case "Túnidos":
        //                  Permiso = "TUNIDOS";
        //                  break;
        //              case "Camarón de altamar":
        //                  Permiso = "CAMARON DE ALTAMAR";
        //                  break;
        //              case "Calamar":
        //                  Permiso = "CALAMAR";
        //                  break;
        //              case "Langosta":
        //                  Permiso = "LANGOSTA";
        //                  break;
        //              case "Pulpo":
        //                  Permiso = "PULPO";
        //                  break;
        //              case "Tiburón":
        //                  Permiso = "TIBURON";
        //                  break;
        //              case "Picudos":
        //                  Permiso = "PEZ ESPADA";
        //                  break;
        //              case "Escama marina":
        //                  Permiso = "ESCAMA MARINA";
        //                  break;
        //              case "Pelágicos menores":
        //                  Permiso = "PELAGICOS MENORES','ANCHOVETA','SARDINA";
        //                  break;
        //              case "Otros":
        //                  Permiso = "CANGREJO','JAIBA','CARNADA VIVA";
        //                  break;
        //              case "Total":
        //                  Permiso = "FOMENTO','TUNIDOS','CAMARON DE ALTAMAR','CALAMAR','LANGOSTA','PULPO','TIBURON','PEZ ESPADA','ESCAMA MARINA','PELAGICOS MENORES','ANCHOVETA','SARDINA','CANGREJO','JAIBA','CARNADA VIVA";
        //                  break;
        //              //Total de permisos en el calculo
        //              case "Total de permisos en el calculo": // Antes Total
        //                  Permiso = "FOMENTO','TUNIDOS','CAMARON DE ALTAMAR','CALAMAR','LANGOSTA','PULPO','TIBURON','PEZ ESPADA','ESCAMA MARINA','PELAGICOS MENORES','ANCHOVETA','SARDINA','CANGREJO','JAIBA','CARNADA VIVA";
        //                  break;
        //              case "Total de embarcaciones":
        //                  TotalPorEmarcaciones = true;
        //                  Permiso = "FOMENTO','TUNIDOS','CAMARON DE ALTAMAR','CALAMAR','LANGOSTA','PULPO','TIBURON','PEZ ESPADA','ESCAMA MARINA','PELAGICOS MENORES','ANCHOVETA','SARDINA','CANGREJO','JAIBA','CARNADA VIVA";
        //                  break;


        //              case "²Total de embarcaciones operando, en puerto y transmitiendo con permiso registrado":
        //                  TotalPorEmarcaciones = true;
        //                  Permiso = "FOMENTO','TUNIDOS','CAMARON DE ALTAMAR','CALAMAR','LANGOSTA','PULPO','TIBURON','PEZ ESPADA','ESCAMA MARINA','PELAGICOS MENORES','ANCHOVETA','SARDINA','CANGREJO','JAIBA','CARNADA VIVA";
        //                  break;
        //              case "²Total de embarcaciones operando y transmitiendo con permiso registrado":
        //                  TotalPorEmarcaciones = true;
        //                  Permiso = "FOMENTO','TUNIDOS','CAMARON DE ALTAMAR','CALAMAR','LANGOSTA','PULPO','TIBURON','PEZ ESPADA','ESCAMA MARINA','PELAGICOS MENORES','ANCHOVETA','SARDINA','CANGREJO','JAIBA','CARNADA VIVA";
        //                  break;
        //              case "²Total de embarcaciones operando, en la mar y transmitiendo con permiso registrado":
        //                  TotalPorEmarcaciones = true;
        //                  Permiso = "FOMENTO','TUNIDOS','CAMARON DE ALTAMAR','CALAMAR','LANGOSTA','PULPO','TIBURON','PEZ ESPADA','ESCAMA MARINA','PELAGICOS MENORES','ANCHOVETA','SARDINA','CANGREJO','JAIBA','CARNADA VIVA";
        //                  break;


        //              case "¹Total de permisos vigentes registrados en embarcaciones":
        //                  Permiso = "FOMENTO','TUNIDOS','CAMARON DE ALTAMAR','CALAMAR','LANGOSTA','PULPO','TIBURON','PEZ ESPADA','ESCAMA MARINA','PELAGICOS MENORES','ANCHOVETA','SARDINA','CANGREJO','JAIBA','CARNADA VIVA";
        //                  break;
        //              default:
        //                  // You can use the default case.
        //                  break;
        //          }
        //          #endregion

        //          ListaMarcadores = db.ObtieneMarcadoresDetalleAlertasCedulas(ModeloTmp.Reporte, ModeloTmp.Litoral, Permiso, TotalPorEmarcaciones).ToList();          

        //          return ListaMarcadores;
        //      }

        //      public List<ObtieneMarcadoresDetalleAlertasCedulas_Result> ObtieneMarcadoresDetalleTabularAlertasCedulasPorZona(MarcadoresEarthModelRepEstadistico ModeloTmp, string idZ)
        //      {
        //          bool TotalPorEmarcaciones = false;
        //          List<MarcadoresEarth> Lista = new List<MarcadoresEarth>();
        //          List<ObtieneMarcadoresDetalleAlertasCedulas_Result> ListaMarcadores = new List<ObtieneMarcadoresDetalleAlertasCedulas_Result>();
        //          string Permiso = ModeloTmp.Permiso;
        //          #region "SE CONCATENAN LOS PERMISOS"
        //          switch (ModeloTmp.Permiso)
        //          {
        //              case "Fomento":
        //                  Permiso = "FOMENTO";
        //                  break;
        //              case "Túnidos":
        //                  Permiso = "TUNIDOS";
        //                  break;
        //              case "Camarón de altamar":
        //                  Permiso = "CAMARON DE ALTAMAR";
        //                  break;
        //              case "Calamar":
        //                  Permiso = "CALAMAR";
        //                  break;
        //              case "Langosta":
        //                  Permiso = "LANGOSTA";
        //                  break;
        //              case "Pulpo":
        //                  Permiso = "PULPO";
        //                  break;
        //              case "Tiburón":
        //                  Permiso = "TIBURON";
        //                  break;
        //              case "Picudos":
        //                  Permiso = "PEZ ESPADA";
        //                  break;
        //              case "Escama marina":
        //                  Permiso = "ESCAMA MARINA";
        //                  break;
        //              case "Pelágicos menores":
        //                  Permiso = "PELAGICOS MENORES','ANCHOVETA','SARDINA";
        //                  break;
        //              case "Otros":
        //                  Permiso = "CANGREJO','JAIBA','CARNADA VIVA";
        //                  break;
        //              case "Total":
        //                  Permiso = "FOMENTO','TUNIDOS','CAMARON DE ALTAMAR','CALAMAR','LANGOSTA','PULPO','TIBURON','PEZ ESPADA','ESCAMA MARINA','PELAGICOS MENORES','ANCHOVETA','SARDINA','CANGREJO','JAIBA','CARNADA VIVA";
        //                  break;
        //              //Total de permisos en el calculo
        //              case "Total de permisos en el calculo": // Antes Total
        //                  Permiso = "FOMENTO','TUNIDOS','CAMARON DE ALTAMAR','CALAMAR','LANGOSTA','PULPO','TIBURON','PEZ ESPADA','ESCAMA MARINA','PELAGICOS MENORES','ANCHOVETA','SARDINA','CANGREJO','JAIBA','CARNADA VIVA";
        //                  break;
        //              case "Total de embarcaciones":
        //                  TotalPorEmarcaciones = true;
        //                  Permiso = "FOMENTO','TUNIDOS','CAMARON DE ALTAMAR','CALAMAR','LANGOSTA','PULPO','TIBURON','PEZ ESPADA','ESCAMA MARINA','PELAGICOS MENORES','ANCHOVETA','SARDINA','CANGREJO','JAIBA','CARNADA VIVA";
        //                  break;


        //              case "²Total de embarcaciones operando, en puerto y transmitiendo con permiso registrado":
        //                  TotalPorEmarcaciones = true;
        //                  Permiso = "FOMENTO','TUNIDOS','CAMARON DE ALTAMAR','CALAMAR','LANGOSTA','PULPO','TIBURON','PEZ ESPADA','ESCAMA MARINA','PELAGICOS MENORES','ANCHOVETA','SARDINA','CANGREJO','JAIBA','CARNADA VIVA";
        //                  break;
        //              case "²Total de embarcaciones operando y transmitiendo con permiso registrado":
        //                  TotalPorEmarcaciones = true;
        //                  Permiso = "FOMENTO','TUNIDOS','CAMARON DE ALTAMAR','CALAMAR','LANGOSTA','PULPO','TIBURON','PEZ ESPADA','ESCAMA MARINA','PELAGICOS MENORES','ANCHOVETA','SARDINA','CANGREJO','JAIBA','CARNADA VIVA";
        //                  break;
        //              case "²Total de embarcaciones operando, en la mar y transmitiendo con permiso registrado":
        //                  TotalPorEmarcaciones = true;
        //                  Permiso = "FOMENTO','TUNIDOS','CAMARON DE ALTAMAR','CALAMAR','LANGOSTA','PULPO','TIBURON','PEZ ESPADA','ESCAMA MARINA','PELAGICOS MENORES','ANCHOVETA','SARDINA','CANGREJO','JAIBA','CARNADA VIVA";
        //                  break;


        //              case "¹Total de permisos vigentes registrados en embarcaciones":
        //                  Permiso = "FOMENTO','TUNIDOS','CAMARON DE ALTAMAR','CALAMAR','LANGOSTA','PULPO','TIBURON','PEZ ESPADA','ESCAMA MARINA','PELAGICOS MENORES','ANCHOVETA','SARDINA','CANGREJO','JAIBA','CARNADA VIVA";
        //                  break;
        //              default:
        //                  // You can use the default case.
        //                  break;
        //          }
        //          #endregion

        //          Guid idZona = Guid.Parse(idZ);
        //          ListaMarcadores = db.ObtieneMarcadoresDetalleAlertasCedulasPorZona(ModeloTmp.Reporte, ModeloTmp.Litoral, Permiso, TotalPorEmarcaciones, idZona).ToList();

        //          return ListaMarcadores;
        //      }

        //public List<MarcadoresEarth> ObtieneMarcadoresDetalleAlertasCedulas(MarcadoresEarthModel ModeloTmp)
        //      {
        //          bool TotalPorEmarcaciones = false;
        //          List<MarcadoresEarth> Lista = new List<MarcadoresEarth>();

        //          List<ObtieneMarcadoresDetalleAlertasCedulas_Result> ListaMarcadores = new List<ObtieneMarcadoresDetalleAlertasCedulas_Result>();

        //          string Permiso = ModeloTmp.Permiso;
        //          #region "SE CONCATENAN LOS PERMISOS"
        //          switch (ModeloTmp.Permiso)
        //          {
        //              case "Fomento":
        //                  Permiso = "FOMENTO";
        //                  break;
        //              case "Túnidos":
        //                  Permiso = "TUNIDOS";
        //                  break;
        //              case "Camarón de altamar":
        //                  Permiso = "CAMARON DE ALTAMAR";
        //                  break;
        //              case "Calamar":
        //                  Permiso = "CALAMAR";
        //                  break;
        //              case "Langosta":
        //                  Permiso = "LANGOSTA";
        //                  break;
        //              case "Pulpo":
        //                  Permiso = "PULPO";
        //                  break;
        //              case "Tiburón":
        //                  Permiso = "TIBURON";
        //                  break;
        //              case "Picudos":
        //                  Permiso = "PEZ ESPADA";
        //                  break;
        //              case "Escama marina":
        //                  Permiso = "ESCAMA MARINA";
        //                  break;
        //              case "Pelágicos menores":
        //                  Permiso = "PELAGICOS MENORES','ANCHOVETA','SARDINA";
        //                  break;
        //              case "Otros":
        //                  Permiso = "CANGREJO','JAIBA','CARNADA VIVA";
        //                  break;
        //              case "Total":
        //                  Permiso = "FOMENTO','TUNIDOS','CAMARON DE ALTAMAR','CALAMAR','LANGOSTA','PULPO','TIBURON','PEZ ESPADA','ESCAMA MARINA','PELAGICOS MENORES','ANCHOVETA','SARDINA','CANGREJO','JAIBA','CARNADA VIVA";
        //                  break;
        //              //Total de permisos en el calculo
        //              case "Total de permisos en el calculo": // Antes Total
        //                  Permiso = "FOMENTO','TUNIDOS','CAMARON DE ALTAMAR','CALAMAR','LANGOSTA','PULPO','TIBURON','PEZ ESPADA','ESCAMA MARINA','PELAGICOS MENORES','ANCHOVETA','SARDINA','CANGREJO','JAIBA','CARNADA VIVA";
        //                  break;
        //              case "Total de embarcaciones":
        //                  TotalPorEmarcaciones = true;
        //                  Permiso = "FOMENTO','TUNIDOS','CAMARON DE ALTAMAR','CALAMAR','LANGOSTA','PULPO','TIBURON','PEZ ESPADA','ESCAMA MARINA','PELAGICOS MENORES','ANCHOVETA','SARDINA','CANGREJO','JAIBA','CARNADA VIVA";
        //                  break;
        //              case "²Total de embarcaciones operando, en puerto y transmitiendo con permiso registrado":
        //                  TotalPorEmarcaciones = true;
        //                  Permiso = "FOMENTO','TUNIDOS','CAMARON DE ALTAMAR','CALAMAR','LANGOSTA','PULPO','TIBURON','PEZ ESPADA','ESCAMA MARINA','PELAGICOS MENORES','ANCHOVETA','SARDINA','CANGREJO','JAIBA','CARNADA VIVA";
        //                  break;
        //              case "²Total de embarcaciones operando y transmitiendo con permiso registrado":

        //                  TotalPorEmarcaciones = true;
        //                  Permiso = "FOMENTO','TUNIDOS','CAMARON DE ALTAMAR','CALAMAR','LANGOSTA','PULPO','TIBURON','PEZ ESPADA','ESCAMA MARINA','PELAGICOS MENORES','ANCHOVETA','SARDINA','CANGREJO','JAIBA','CARNADA VIVA";
        //                  break;
        //              case "²Total de embarcaciones operando, en la mar y transmitiendo con permiso registrado":

        //                  TotalPorEmarcaciones = true;
        //                  Permiso = "FOMENTO','TUNIDOS','CAMARON DE ALTAMAR','CALAMAR','LANGOSTA','PULPO','TIBURON','PEZ ESPADA','ESCAMA MARINA','PELAGICOS MENORES','ANCHOVETA','SARDINA','CANGREJO','JAIBA','CARNADA VIVA";
        //                  break;




        //              case "¹Total de permisos vigentes registrados en embarcaciones":
        //                  Permiso = "FOMENTO','TUNIDOS','CAMARON DE ALTAMAR','CALAMAR','LANGOSTA','PULPO','TIBURON','PEZ ESPADA','ESCAMA MARINA','PELAGICOS MENORES','ANCHOVETA','SARDINA','CANGREJO','JAIBA','CARNADA VIVA";
        //                  break;
        //              //case "¹Total de permisos vigentes registrados en embarcaciones":
        //              //    Permiso = "FOMENTO','TUNIDOS','CAMARON DE ALTAMAR','CALAMAR','LANGOSTA','PULPO','TIBURON','PEZ ESPADA','ESCAMA MARINA','PELAGICOS MENORES','ANCHOVETA','SARDINA','CANGREJO','JAIBA','CARNADA VIVA";
        //              //    break;
        //              default:
        //                  // You can use the default case.
        //                  break;
        //          }
        //          #endregion



        //          ListaMarcadores = db.ObtieneMarcadoresDetalleAlertasCedulas(ModeloTmp.Reporte, ModeloTmp.Litoral, Permiso, TotalPorEmarcaciones).ToList();

        //          if (ListaMarcadores.Count > 0)
        //          {
        //              foreach (ObtieneMarcadoresDetalleAlertasCedulas_Result Fila in ListaMarcadores)

        //              {
        //                  MarcadoresEarth NuevoMarcador = new MarcadoresEarth();
        //                  NuevoMarcador.Latitud = Fila.Latitud; //LatLongADecimal(Fila.Latitud).ToString();
        //                  NuevoMarcador.Longitud = Fila.Longitud; // LatLongADecimal(Fila.Longitud).ToString();
        //                  NuevoMarcador.NombreSitio = Fila.Embarcacion;
        //                  NuevoMarcador.Info = Fila.TipoPermiso;
        //                  Lista.Add(NuevoMarcador);
        //              }
        //          }

        //          return Lista;
        //      }

        public static double LatLongADecimal(string Latitud)
        {
            double Valor = 0;//grados + (minutos/60) + (segundos/3600)
            Latitud = Latitud.Replace("..", ".").Replace(" ", "").Replace("N", "").Replace("W", "").Replace("E", "").Replace("S", "");
            int Grados = 0;
            int Minutos = 0;
            double Segundos = 0.0;
            string[] splGrados;
            string[] splMinutos;
            if (Latitud.Contains("°"))
            {
                splGrados = Latitud.Split('°');
                Int32.TryParse(splGrados[0].Replace(" ", ""), out Grados);
                //Grados = Convert.ToInt32(splGrados[0].Replace(" ", ""));
                if (splGrados[1].Contains("´"))
                {
                    splMinutos = splGrados[1].Split('´');
                    Minutos = Convert.ToInt32(splMinutos[0]);
                    if (splMinutos[1].Replace(" ", "").Replace("N", "").Replace("W", "").Replace("E", "").Replace("S", "") != "")
                    {
                        Segundos = Convert.ToDouble(splMinutos[1].Replace(" ", "").Replace("N", "").Replace("W", "").Replace("E", "").Replace("S", ""));
                    }
                    else
                    {
                        Segundos = 0.0;
                    }

                }
                if (Latitud.Contains("-"))
                {
                    Valor = -(Segundos / 3600.0) - (Minutos / 60.0) + Grados;
                }
                else
                {
                    Valor = Grados + (Minutos / 60.0) + (Segundos / 3600.0);
                }

            }
            else
            {
                Valor = 0;//grados + (minutos/60) + (segundos/3600)
            }


            return Valor;
        }
        private static double LongitudatitudaDecimal(decimal long1InDegrees, decimal lat1InDegrees, decimal long2InDegrees, decimal lat2InDegrees)
        {
            double lats = (double)(lat1InDegrees - lat2InDegrees);
            double lngs = (double)(long1InDegrees - long2InDegrees);

            //Paso a metros
            double latm = lats * 60 * 1852;
            double lngm = (lngs * Math.Cos((double)lat1InDegrees * Math.PI / 180)) * 60 * 1852;
            double distInMeters = Math.Sqrt(Math.Pow(latm, 2) + Math.Pow(lngm, 2));
            return distInMeters;
        }

        //public List<MarcadoresEarth> ObtieneMarcadoresDetalle(MarcadoresEarthModel ModeloTmp)
        //{
        //    bool TotalPorEmarcaciones = false;
        //    List<MarcadoresEarth> Lista = new List<MarcadoresEarth>();
        //    List<ObtieneMarcadoresDetalle_Result> ListaMarcadores = new List<ObtieneMarcadoresDetalle_Result>();
        //    string Permiso = ModeloTmp.Permiso;
        //    #region "SE CONCATENAN LOS PERMISOS"
        //    switch (ModeloTmp.Permiso)
        //    {
        //        case "Fomento":
        //            Permiso = "FOMENTO";
        //            break;
        //        case "Túnidos":
        //            Permiso = "TUNIDOS";
        //            break;
        //        case "Camarón de altamar":
        //            Permiso = "CAMARON DE ALTAMAR";
        //            break;
        //        case "Calamar":
        //            Permiso = "CALAMAR";
        //            break;
        //        case "Langosta":
        //            Permiso = "LANGOSTA";
        //            break;
        //        case "Pulpo":
        //            Permiso = "PULPO";
        //            break;
        //        case "Tiburón":
        //            Permiso = "TIBURON";
        //            break;
        //        case "Picudos":
        //            Permiso = "PEZ ESPADA";
        //            break;
        //        case "Escama marina":
        //            Permiso = "ESCAMA MARINA";
        //            break;
        //        case "Pelágicos menores":
        //            Permiso = "PELAGICOS MENORES','ANCHOVETA','SARDINA";
        //            break;
        //        case "Otros":
        //            Permiso = "CANGREJO','JAIBA','CARNADA VIVA";
        //            break;
        //        case "Total":
        //            Permiso = "FOMENTO','TUNIDOS','CAMARON DE ALTAMAR','CALAMAR','LANGOSTA','PULPO','TIBURON','PEZ ESPADA','ESCAMA MARINA','PELAGICOS MENORES','ANCHOVETA','SARDINA','CANGREJO','JAIBA','CARNADA VIVA','TIBURON MENORES', 'JAIBA MENORES', 'ESCAMA MARINA MENORES', 'CAMARON DE ALTAMAR MENORES";
        //            break;
        //        //Total de permisos en el calculo
        //        case "Total de permisos en el calculo": // Antes Total
        //            Permiso = "FOMENTO','TUNIDOS','CAMARON DE ALTAMAR','CALAMAR','LANGOSTA','PULPO','TIBURON','PEZ ESPADA','ESCAMA MARINA','PELAGICOS MENORES','ANCHOVETA','SARDINA','CANGREJO','JAIBA','CARNADA VIVA','TIBURON MENORES', 'JAIBA MENORES', 'ESCAMA MARINA MENORES', 'CAMARON DE ALTAMAR MENORES";
        //            break;
        //        case "Total de embarcaciones":
        //            TotalPorEmarcaciones = true;
        //            Permiso = "FOMENTO','TUNIDOS','CAMARON DE ALTAMAR','CALAMAR','LANGOSTA','PULPO','TIBURON','PEZ ESPADA','ESCAMA MARINA','PELAGICOS MENORES','ANCHOVETA','SARDINA','CANGREJO','JAIBA','CARNADA VIVA','TIBURON MENORES', 'JAIBA MENORES', 'ESCAMA MARINA MENORES', 'CAMARON DE ALTAMAR MENORES";
        //            break;
        //        case "²Total de embarcaciones operando, en puerto y transmitiendo con permiso registrado":
        //            TotalPorEmarcaciones = true;
        //            Permiso = "FOMENTO','TUNIDOS','CAMARON DE ALTAMAR','CALAMAR','LANGOSTA','PULPO','TIBURON','PEZ ESPADA','ESCAMA MARINA','PELAGICOS MENORES','ANCHOVETA','SARDINA','CANGREJO','JAIBA','CARNADA VIVA','TIBURON MENORES', 'JAIBA MENORES', 'ESCAMA MARINA MENORES', 'CAMARON DE ALTAMAR MENORES";
        //            break;
        //         case "²Total de embarcaciones operando y transmitiendo con permiso registrado":
        //            TotalPorEmarcaciones = true;
        //            Permiso = "FOMENTO','TUNIDOS','CAMARON DE ALTAMAR','CALAMAR','LANGOSTA','PULPO','TIBURON','PEZ ESPADA','ESCAMA MARINA','PELAGICOS MENORES','ANCHOVETA','SARDINA','CANGREJO','JAIBA','CARNADA VIVA','TIBURON MENORES', 'JAIBA MENORES', 'ESCAMA MARINA MENORES', 'CAMARON DE ALTAMAR MENORES";
        //            break;
        //         case "²Total de embarcaciones operando, en la mar y transmitiendo con permiso registrado":
        //            TotalPorEmarcaciones = true;
        //            Permiso = "FOMENTO','TUNIDOS','CAMARON DE ALTAMAR','CALAMAR','LANGOSTA','PULPO','TIBURON','PEZ ESPADA','ESCAMA MARINA','PELAGICOS MENORES','ANCHOVETA','SARDINA','CANGREJO','JAIBA','CARNADA VIVA','TIBURON MENORES', 'JAIBA MENORES', 'ESCAMA MARINA MENORES', 'CAMARON DE ALTAMAR MENORES";
        //            break;  
        //        case "¹Total de permisos vigentes registrados en embarcaciones":
        //            Permiso = "FOMENTO','TUNIDOS','CAMARON DE ALTAMAR','CALAMAR','LANGOSTA','PULPO','TIBURON','PEZ ESPADA','ESCAMA MARINA','PELAGICOS MENORES','ANCHOVETA','SARDINA','CANGREJO','JAIBA','CARNADA VIVA','TIBURON MENORES', 'JAIBA MENORES', 'ESCAMA MARINA MENORES', 'CAMARON DE ALTAMAR MENORES";
        //            break;
        //        //case "¹Total de permisos vigentes registrados en embarcaciones":
        //        //    Permiso = "FOMENTO','TUNIDOS','CAMARON DE ALTAMAR','CALAMAR','LANGOSTA','PULPO','TIBURON','PEZ ESPADA','ESCAMA MARINA','PELAGICOS MENORES','ANCHOVETA','SARDINA','CANGREJO','JAIBA','CARNADA VIVA";
        //        //    break;
        //        //PermisosPangas ini
        //        case "Tiburón menores":
        //            Permiso = "TIBURON MENORES";
        //            break;
        //        case "Jaiba menores":
        //            Permiso = "JAIBA MENORES";
        //            break;
        //        case "Escama marina menores":
        //            Permiso = "ESCAMA MARINA MENORES";
        //            break;
        //        case "Camarón de altamar menores":
        //            Permiso = "CAMARON DE ALTAMAR MENORES";
        //            break;
        //        //PermisosPangas fin
        //        default:
        //            // You can use the default case.
        //            break;
        //    }
        //    #endregion



        //    ListaMarcadores = db.ObtieneMarcadoresDetalle(ModeloTmp.Reporte, ModeloTmp.Litoral, Permiso, TotalPorEmarcaciones).ToList();
        //    if (ListaMarcadores.Count > 0)
        //    {
        //        foreach (ObtieneMarcadoresDetalle_Result Fila in ListaMarcadores)
        //        {
        //            MarcadoresEarth NuevoMarcador = new MarcadoresEarth();
        //            NuevoMarcador.Latitud = Fila.Latitud;
        //            NuevoMarcador.Longitud = Fila.Longitud;
        //            NuevoMarcador.NombreSitio = Fila.Embarcacion;
        //            NuevoMarcador.Info = Fila.TipoPermiso;
        //            Lista.Add(NuevoMarcador);
        //        }
        //    }

        //    return Lista;
        //}
        //public List<MarcadoresEarth> ObtieneMarcadoresDetalleAsyncAlertasCedulas(string Litoral, string Reporte, string PermisoX, string SessionIdZonaFiltro)
        //{
        //    bool TotalPorEmarcaciones = false;
        //    List<MarcadoresEarth> Lista = new List<MarcadoresEarth>();

        //    List<ObtieneMarcadoresDetalleAlertasCedulas_Result> ListaMarcadores = new List<ObtieneMarcadoresDetalleAlertasCedulas_Result>();

        //    if (SessionIdZonaFiltro == "")
        //        ListaMarcadores = db.ObtieneMarcadoresDetalleAlertasCedulas(Reporte, Litoral, PermisoX, TotalPorEmarcaciones).ToList();
        //    else
        //    {
        //        Guid idZona = Guid.Parse(SessionIdZonaFiltro);
        //        ListaMarcadores = db.ObtieneMarcadoresDetalleAlertasCedulasPorZona(Reporte, Litoral, PermisoX, TotalPorEmarcaciones, idZona).ToList();
        //    }


        //    if (ListaMarcadores.Count > 0)
        //    {
        //        var listCedulas = db.SPMasCedulaConsultarUltimosRNP().ToList();
        //        foreach (ObtieneMarcadoresDetalleAlertasCedulas_Result Fila in ListaMarcadores)
        //        {
        //            MarcadoresEarth NuevoMarcador = new MarcadoresEarth();
        //            if (Fila.Latitud != null && Fila.Longitud != null)
        //            {

        //                string EstatusCedula, NumeroCedula, TipoAlertaCedula = "";
        //                //Editado por Martin Martinez 29/12/2016   Se agrega Estatus, numero y Tipo de alerta 
        //                EstatusCedula = getDatosCedula(Fila.RNP, "EstatusCedula", listCedulas);
        //                NumeroCedula = getDatosCedula(Fila.RNP, "NumeroCedula", listCedulas);
        //                TipoAlertaCedula = getDatosCedula(Fila.RNP, "TipoAlertaCedula", listCedulas);

        //                NuevoMarcador.Latitud = Fila.Latitud != null ? Fila.Latitud : ""; // LatLongADecimal(Fila.Latitud).ToString();
        //                NuevoMarcador.Longitud = Fila.Longitud != null ? Fila.Longitud : ""; // LatLongADecimal(Fila.Longitud).ToString();
        //                NuevoMarcador.NombreSitio = Fila.Embarcacion;

        //                NuevoMarcador.Situacion = Reporte.Replace("CAUSAS", "");
        //                NuevoMarcador.Causas = Fila.Causas;
        //                NuevoMarcador.Fecha = Fila.Fecha.Value.ToShortDateString();
        //                NuevoMarcador.RNP = Fila.RNP;
        //                NuevoMarcador.Matricula = Fila.Matricula;
        //                NuevoMarcador.EstatusCedula = EstatusCedula;
        //                NuevoMarcador.NumeroCedula = NumeroCedula;
        //                NuevoMarcador.TipoAlerta = TipoAlertaCedula;

        //                    NuevoMarcador.Info = "<p>Situación : " + Reporte.Replace("CAUSAS", "") + " | Causa :" + Fila.Causas + "</p> <p>  Fecha: " + Fila.Fecha.Value.ToShortDateString() + " </p> <p>  Latitud: " + Fila.Latitud + " </p> <p>  Longitud: " + Fila.Longitud + " </p>    <p>  RNP: " + Fila.RNP + " </p> <p> Matricula: " + Fila.Matricula + "</p><p> Estatus: " + EstatusCedula + "</p><p> #Cedula: " + NumeroCedula + "</p><p> Tipo de Alerta: " + TipoAlertaCedula;
        //                if (Reporte == "EMERGENCIACAUSAS")
        //                {
        //                    NuevoMarcador.Info = "<p>Situación : " + Reporte.Replace("CAUSAS", "") + " | Causa :" + Fila.Causas + "</p> <p>  Fecha: " + Fila.Fecha.Value.ToShortDateString() + " </p> <p>  Latitud: " + Fila.Latitud + " </p> <p>  Longitud: " + Fila.Longitud + " </p>    <p>  RNP: " + Fila.RNP + " </p> <p> Matricula: " + Fila.Matricula + "</p><p> Estatus: " + EstatusCedula + "</p><p> #Cedula: " + NumeroCedula + "</p><p> Tipo de Alerta: " + TipoAlertaCedula;
        //                }


        //                if (Reporte == "EMERGENCIACAUSAS" && Litoral.ToLower() == "todas")
        //                    NuevoMarcador.Info = "<p>Situación : " + Reporte.Replace("CAUSAS", "") + " | Causa :" + Fila.Causas + "</p> <p>  Fecha: " + Fila.Fecha.Value.ToShortDateString() + " </p> <p>  Latitud: " + Fila.Latitud + " </p> <p>  Longitud: " + Fila.Longitud + " </p>    <p>  RNP: " + Fila.RNP + " </p> <p> Matricula: " + Fila.Matricula + "</p><p> Estatus: " + EstatusCedula + "</p><p> #Cedula: " + NumeroCedula + "</p><p> Tipo de Alerta: " + TipoAlertaCedula;



        //                //NuevoMarcador.Info = "<p>Situación : " + Reporte.Replace("CAUSAS", "") + " | Causa :" + Fila.Causas + "</p> <p>  Fecha: " + Fila.Fecha.Value.ToShortDateString() + " </p> <p>  Latitud: " + Fila.Latitud + " </p> <p>  Longitud: " + Fila.Longitud + " </p>    <p>  RNP: " + Fila.RNP + " </p> <p> Matricula: " + Fila.Matricula + "</p><p> Estatus: " + EstatusCedula + "</p><p> #Cedula: " + NumeroCedula + "</p><p> Tipo de Alerta: " + TipoAlertaCedula;
        //                //if (Reporte == "EMERGENCIACAUSAS")
        //                //    NuevoMarcador.Info = "<p>Situación : " + Reporte.Replace("CAUSAS", "") + " | Causa :" + Fila.Causas + "</p> <p>  Fecha: " + Fila.Fecha.Value.ToShortDateString() + " </p> <p>  Latitud: " + Fila.Latitud + " </p> <p>  Longitud: " + Fila.Longitud + " </p>    <p>  RNP: " + Fila.RNP + " </p> <p> Matricula: " + Fila.Matricula + "</p><p> Estatus: " + EstatusCedula + "</p><p> #Cedula: " + NumeroCedula + "</p><p> Tipo de Alerta: " + TipoAlertaCedula;

        //                //if (Reporte == "EMERGENCIACAUSAS" && Litoral.ToLower() == "todas")
        //                //    NuevoMarcador.Info = "<p>Situación : " + Reporte.Replace("CAUSAS", "") + " | Causa :" + Fila.Causas + "</p> <p>  Fecha: " + Fila.Fecha.Value.ToShortDateString() + " </p> <p>  Latitud: " + Fila.Latitud + " </p> <p>  Longitud: " + Fila.Longitud + " </p>    <p>  RNP: " + Fila.RNP + " </p> <p> Matricula: " + Fila.Matricula + "</p><p> Estatus: " + EstatusCedula + "</p><p> #Cedula: " + NumeroCedula + "</p><p> Tipo de Alerta: " + TipoAlertaCedula;


        //                Lista.Add(NuevoMarcador);
        //            }
        //        }
        //    }

        //    return Lista;
        //}

        /// <summary>
        /// Obtiene los datos de la cedula y devuelve un valor
        /// </summary>
        /// <param name="RNP"></param>
        /// <param name="campoRecuperar"></param>
        /// <returns>Valor en string</returns>
        //private string getDatosCedula(string RNP, string campoRecuperar, List<SPMasCedulaConsultarUltimosRNP_Result> listaCedula)
        //{
        //    Creado por Martin Martinez 29 / 12 / 2016
        //    string val = "";
        //    SPMasCedulaConsultarUltimosRNP_Result tmascedula = listaCedula.Where(t => t.RNP == RNP).OrderByDescending(t => t.FechaCedula).ToList()[0];

        //    if (tmascedula != null)
        //    {
        //        if (campoRecuperar == "EstatusCedula")
        //            val = tmascedula.Estatus;

        //        if (campoRecuperar == "NumeroCedula")
        //            val = tmascedula.NoCedula;

        //        if (campoRecuperar == "TipoAlertaCedula")
        //            val = tmascedula.TipoAlerta;
        //    }
        //    return val;
        //}


        //public List<MarcadoresEarth> ObtieneMarcadoresDetallePrioridad(MarcadoresEarthModel ModeloTmp)
        //{
        //    bool TotalPorEmarcaciones = false;
        //    List<MarcadoresEarth> Lista = new List<MarcadoresEarth>();
        //    List<ObtieneMarcadoresDetallePrioridad_Result> ListaMarcadores = new List<ObtieneMarcadoresDetallePrioridad_Result>();
        //    string Permiso = ModeloTmp.Permiso;
        //    #region "SE CONCATENAN LOS PERMISOS"
        //    switch (ModeloTmp.Permiso)
        //    {
        //        case "Fomento":
        //            Permiso = "FOMENTO";
        //            break;
        //        case "Túnidos":
        //            Permiso = "TUNIDOS";
        //            break;
        //        case "Camarón de altamar":
        //            Permiso = "CAMARON DE ALTAMAR";
        //            break;
        //        case "Calamar":
        //            Permiso = "CALAMAR";
        //            break;
        //        case "Langosta":
        //            Permiso = "LANGOSTA";
        //            break;
        //        case "Pulpo":
        //            Permiso = "PULPO";
        //            break;
        //        case "Tiburón":
        //            Permiso = "TIBURON";
        //            break;
        //        case "Picudos":
        //            Permiso = "PEZ ESPADA";
        //            break;
        //        case "Escama marina":
        //            Permiso = "ESCAMA MARINA";
        //            break;
        //        case "Pelágicos menores":
        //            Permiso = "PELAGICOS MENORES','ANCHOVETA','SARDINA";
        //            break;
        //        case "Otros":
        //            Permiso = "CANGREJO','JAIBA','CARNADA VIVA";
        //            break;
        //        case "Total":
        //            Permiso = "FOMENTO','TUNIDOS','CAMARON DE ALTAMAR','CALAMAR','LANGOSTA','PULPO','TIBURON','PEZ ESPADA','ESCAMA MARINA','PELAGICOS MENORES','ANCHOVETA','SARDINA','CANGREJO','JAIBA','CARNADA VIVA";
        //            break;
        //            Total de permisos en el calculo
        //        case "Total de permisos en el calculo": // Antes Total
        //            Permiso = "FOMENTO','TUNIDOS','CAMARON DE ALTAMAR','CALAMAR','LANGOSTA','PULPO','TIBURON','PEZ ESPADA','ESCAMA MARINA','PELAGICOS MENORES','ANCHOVETA','SARDINA','CANGREJO','JAIBA','CARNADA VIVA";
        //            break;
        //        case "Total de embarcaciones":
        //            TotalPorEmarcaciones = true;
        //            Permiso = "FOMENTO','TUNIDOS','CAMARON DE ALTAMAR','CALAMAR','LANGOSTA','PULPO','TIBURON','PEZ ESPADA','ESCAMA MARINA','PELAGICOS MENORES','ANCHOVETA','SARDINA','CANGREJO','JAIBA','CARNADA VIVA";
        //            break;
        //        case "²Total de embarcaciones operando, en puerto y transmitiendo con permiso registrado":
        //            TotalPorEmarcaciones = true;
        //            Permiso = "FOMENTO','TUNIDOS','CAMARON DE ALTAMAR','CALAMAR','LANGOSTA','PULPO','TIBURON','PEZ ESPADA','ESCAMA MARINA','PELAGICOS MENORES','ANCHOVETA','SARDINA','CANGREJO','JAIBA','CARNADA VIVA";
        //            break;
        //        case "²Total de embarcaciones operando y transmitiendo con permiso registrado":
        //            TotalPorEmarcaciones = true;
        //            Permiso = "FOMENTO','TUNIDOS','CAMARON DE ALTAMAR','CALAMAR','LANGOSTA','PULPO','TIBURON','PEZ ESPADA','ESCAMA MARINA','PELAGICOS MENORES','ANCHOVETA','SARDINA','CANGREJO','JAIBA','CARNADA VIVA";
        //            break;
        //        case "²Total de embarcaciones operando, en la mar y transmitiendo con permiso registrado":
        //            TotalPorEmarcaciones = true;
        //            Permiso = "FOMENTO','TUNIDOS','CAMARON DE ALTAMAR','CALAMAR','LANGOSTA','PULPO','TIBURON','PEZ ESPADA','ESCAMA MARINA','PELAGICOS MENORES','ANCHOVETA','SARDINA','CANGREJO','JAIBA','CARNADA VIVA";
        //            break;


        //        case "¹Total de permisos vigentes registrados en embarcaciones":
        //            Permiso = "FOMENTO','TUNIDOS','CAMARON DE ALTAMAR','CALAMAR','LANGOSTA','PULPO','TIBURON','PEZ ESPADA','ESCAMA MARINA','PELAGICOS MENORES','ANCHOVETA','SARDINA','CANGREJO','JAIBA','CARNADA VIVA";
        //            break;
        //        case "¹Total de permisos vigentes registrados en embarcaciones":
        //            Permiso = "FOMENTO','TUNIDOS','CAMARON DE ALTAMAR','CALAMAR','LANGOSTA','PULPO','TIBURON','PEZ ESPADA','ESCAMA MARINA','PELAGICOS MENORES','ANCHOVETA','SARDINA','CANGREJO','JAIBA','CARNADA VIVA";
        //            break;
        //        default:
        //            You can use the default case.
        //            break;
        //    }
        //    #endregion



        //    ListaMarcadores = db.ObtieneMarcadoresDetallePrioridad(ModeloTmp.Reporte, ModeloTmp.Litoral, Permiso, TotalPorEmarcaciones).ToList();
        //    if (ListaMarcadores.Count > 0)
        //    {
        //        foreach (ObtieneMarcadoresDetallePrioridad_Result Fila in ListaMarcadores)
        //        {
        //            MarcadoresEarth NuevoMarcador = new MarcadoresEarth();
        //            NuevoMarcador.Latitud = Fila.Latitud;
        //            NuevoMarcador.Longitud = Fila.Longitud;
        //            NuevoMarcador.NombreSitio = Fila.Embarcacion;
        //            NuevoMarcador.Info = Fila.TipoPermiso;
        //            Lista.Add(NuevoMarcador);
        //        }
        //    }

        //    return Lista;
        //}


        //public List<MarcadoresEarth> ObtieneMarcadoresDetalleAsync(string Litoral, string Reporte, string PermisoX)
        //{
        //    bool TotalPorEmarcaciones = false;
        //    List<MarcadoresEarth> Lista = new List<MarcadoresEarth>();
        //    List<ObtieneMarcadoresDetalle_Result> ListaMarcadores = new List<ObtieneMarcadoresDetalle_Result>();
        //    string Permiso = PermisoX;
        //    #region "SE CONCATENAN LOS PERMISOS"
        //    switch (PermisoX)
        //    {
        //        case "Fomento":
        //            Permiso = "FOMENTO";
        //            break;
        //        case "Túnidos":
        //            Permiso = "TUNIDOS";
        //            break;
        //        case "Camarón de altamar":
        //            Permiso = "CAMARON DE ALTAMAR";
        //            break;
        //        case "Calamar":
        //            Permiso = "CALAMAR";
        //            break;
        //        case "Langosta":
        //            Permiso = "LANGOSTA";
        //            break;
        //        case "Pulpo":
        //            Permiso = "PULPO";
        //            break;
        //        case "Tiburón":
        //            Permiso = "TIBURON";
        //            break;
        //        case "Picudos":
        //            Permiso = "PEZ ESPADA";
        //            break;
        //        case "Escama marina":
        //            Permiso = "ESCAMA MARINA";
        //            break;
        //        case "Pelágicos menores":
        //            Permiso = "PELAGICOS MENORES','ANCHOVETA','SARDINA";
        //            break;
        //        case "Otros":
        //            Permiso = "CANGREJO','JAIBA','CARNADA VIVA";
        //            break;
        //        case "Total":
        //            Permiso = "FOMENTO','TUNIDOS','CAMARON DE ALTAMAR','CALAMAR','LANGOSTA','PULPO','TIBURON','PEZ ESPADA','ESCAMA MARINA','PELAGICOS MENORES','ANCHOVETA','SARDINA','CANGREJO','JAIBA','CARNADA VIVA";
        //            break;
        //            Total de permisos en el calculo
        //        case "Total de permisos en el calculo": // Antes Total
        //            Permiso = "FOMENTO','TUNIDOS','CAMARON DE ALTAMAR','CALAMAR','LANGOSTA','PULPO','TIBURON','PEZ ESPADA','ESCAMA MARINA','PELAGICOS MENORES','ANCHOVETA','SARDINA','CANGREJO','JAIBA','CARNADA VIVA";
        //            break;
        //        case "Total de embarcaciones":
        //            TotalPorEmarcaciones = true;
        //            Permiso = "FOMENTO','TUNIDOS','CAMARON DE ALTAMAR','CALAMAR','LANGOSTA','PULPO','TIBURON','PEZ ESPADA','ESCAMA MARINA','PELAGICOS MENORES','ANCHOVETA','SARDINA','CANGREJO','JAIBA','CARNADA VIVA";
        //            break;
        //        case "²Total de embarcaciones operando, en puerto y transmitiendo con permiso registrado":
        //            TotalPorEmarcaciones = true;
        //            Permiso = "FOMENTO','TUNIDOS','CAMARON DE ALTAMAR','CALAMAR','LANGOSTA','PULPO','TIBURON','PEZ ESPADA','ESCAMA MARINA','PELAGICOS MENORES','ANCHOVETA','SARDINA','CANGREJO','JAIBA','CARNADA VIVA";
        //            break;
        //        case "²Total de embarcaciones operando y transmitiendo con permiso registrado":
        //            TotalPorEmarcaciones = true;
        //            Permiso = "FOMENTO','TUNIDOS','CAMARON DE ALTAMAR','CALAMAR','LANGOSTA','PULPO','TIBURON','PEZ ESPADA','ESCAMA MARINA','PELAGICOS MENORES','ANCHOVETA','SARDINA','CANGREJO','JAIBA','CARNADA VIVA";
        //            break;
        //        case "²Total de embarcaciones operando, en la mar y transmitiendo con permiso registrado":
        //            TotalPorEmarcaciones = true;
        //            Permiso = "FOMENTO','TUNIDOS','CAMARON DE ALTAMAR','CALAMAR','LANGOSTA','PULPO','TIBURON','PEZ ESPADA','ESCAMA MARINA','PELAGICOS MENORES','ANCHOVETA','SARDINA','CANGREJO','JAIBA','CARNADA VIVA";
        //            break;


        //        case "¹Total de permisos vigentes registrados en embarcaciones":
        //            Permiso = "FOMENTO','TUNIDOS','CAMARON DE ALTAMAR','CALAMAR','LANGOSTA','PULPO','TIBURON','PEZ ESPADA','ESCAMA MARINA','PELAGICOS MENORES','ANCHOVETA','SARDINA','CANGREJO','JAIBA','CARNADA VIVA";
        //            break;
        //        default:
        //            You can use the default case.
        //            break;
        //    }
        //    #endregion

        //    ListaMarcadores = db.ObtieneMarcadoresDetalle(Reporte, Litoral, Permiso, TotalPorEmarcaciones).ToList();
        //    if (ListaMarcadores.Count > 0)
        //    {
        //        foreach (ObtieneMarcadoresDetalle_Result Fila in ListaMarcadores)
        //        {
        //            MarcadoresEarth NuevoMarcador = new MarcadoresEarth();
        //            NuevoMarcador.Latitud = Fila.Latitud;
        //            NuevoMarcador.Longitud = Fila.Longitud;
        //            NuevoMarcador.NombreSitio = Fila.Embarcacion;
        //            NuevoMarcador.Info = Fila.TipoPermiso;
        //            Lista.Add(NuevoMarcador);
        //        }
        //    }

        //    return Lista;
        //}


        //public void PreparaInfoTMasRepUltimaTx()
        //{
        //    db.ReportesATMasRepUltimaTxBorrar();
        //    string matAct = "";
        //    string matAnt = "";
        //    List<ReportesATMasReporteUltimaTxSeleccionar_Result> ListaMatriculas = db.ReportesATMasReporteUltimaTxSeleccionar().ToList();
        //    foreach (ReportesATMasReporteUltimaTxSeleccionar_Result RegistroUnico in ListaMatriculas)
        //    {
        //        matAct = RegistroUnico.matricula;
        //        if (matAct != matAnt)
        //        {
        //            inserta
        //            db.ReportesATMasRepUltimaTxInsertar(RegistroUnico.matricula, RegistroUnico.embarcacion, RegistroUnico.tipopermiso, RegistroUnico.litoral, RegistroUnico.estado, RegistroUnico.EnPuerto, RegistroUnico.Tx);
        //        }
        //        matAnt = RegistroUnico.matricula;
        //    }
        //    borra o que hay en TMasRepUltimaTx
        //    recorre las matriculas resultantes de SELECT matricula, embarcacion, tipopermiso, litoral, estado, EnPuerto, Tx FROM MAS_ReporteUltimaTx WHERE litoral in ('pacifico', 'golfo') ORDER BY matricula
        //    inserta en TMasRepUltimaTx
        //}

        //public void PreparaInfoSinVeda(string UserName_, int opcion)
        //{
        //    //Borra la informacion de tmpRepUltimaTxSF

        //    //Ejecuta el procedimiento SPMasReportesAvanzadosPreparaInfo1

        //    if (opcion == 1)
        //    {
        //        List<ReportesAvanzadosPreparaInfo1_Result> ListaMatriculas = db.ReportesAvanzadosPreparaInfo1(UserName_).ToList();
        //    }
        //    else if(opcion==2)
        //    {
        //        List<ReportesAvanzadosPreparaInfoPangas_Result> ListaMatriculas = db.ReportesAvanzadosPreparaInfoPangas(UserName_).ToList();
        //        var x = 1;
        //    }
        //    else
        //    {
        //        List<ReportesAvanzadosPreparaInfo1_Result> ListaMatriculas = db.ReportesAvanzadosPreparaInfo1(UserName_).ToList();
        //    }
        //    #region Comentado
        //    //Recorre la lista de Matriculas resultantes de la ejecucion del procedimiento anterior
        //    //////foreach (ReportesAvanzadosPreparaInfo1_Result Matricula in ListaMatriculas)
        //    //////{
        //    //////    //  DENTRO DEL CICLO EJECUTA LA SELECCION DE PERMISOS POR MATRICULA
        //    //////    List<ReportesAvanzadosPermisosMatricula_Result> ListaPermisosMatricula = db.ReportesAvanzadosPermisosMatricula(Matricula.matricula).ToList();
        //    //////    Boolean enVedaEmb = false;
        //    //////    foreach (ReportesAvanzadosPermisosMatricula_Result PermisoMatricula in ListaPermisosMatricula)
        //    //////    {
        //    //////        //DENTRO DEL CICLO DE LOS PERMISOS LOS REVISA SI ESTAN EN VEDA O NO E INSERTA EN LA TABLA TMasRepUltimaTxSF
        //    //////        enVedaEmb = enVeda(PermisoMatricula.TipoPermiso, PermisoMatricula.Litoral, ModeloTmp_);
        //    //////        if (enVedaEmb == false)
        //    //////        {
        //    //////            //inserta en tmpRepUltimaTxSF   enveda = 0
        //    //////            db.ReportesAvanzadosPreparaInfo2(PermisoMatricula.Matricula, PermisoMatricula.Embarcacion, PermisoMatricula.RNP, PermisoMatricula.RazonSocial, PermisoMatricula.TipoPermiso, PermisoMatricula.PuertoBase, PermisoMatricula.Litoral, PermisoMatricula.Estado, PermisoMatricula.EnPuerto, PermisoMatricula.Tx, "0");
        //    //////            ////////string strInserta = "INSERT INTO ##tmpRepUltimaTxSF VALUES ('" + PermisoMatricula.Matricula + "', '" + PermisoMatricula.Embarcacion + "', '" + PermisoMatricula.RNP + "', '" + PermisoMatricula.RazonSocial + "', '" + PermisoMatricula.TipoPermiso + "', '" + PermisoMatricula.PuertoBase + "', '" + PermisoMatricula.Litoral + "', '" +PermisoMatricula.Estado + "', '" + PermisoMatricula.EnPuerto + "', '" + PermisoMatricula.Tx + "', '0')";
        //    //////            //Sale del foreach
        //    //////            break;
        //    //////        }
        //    //////    }
        //    //////    if (enVedaEmb == true)
        //    //////    {

        //    //////        //inserta en tmpRepUltimaTxSF     enveda = 1
        //    //////        db.ReportesAvanzadosPreparaInfo2(ListaPermisosMatricula[0].Matricula, ListaPermisosMatricula[0].Embarcacion, ListaPermisosMatricula[0].RNP, ListaPermisosMatricula[0].RazonSocial, ListaPermisosMatricula[0].TipoPermiso, ListaPermisosMatricula[0].PuertoBase, ListaPermisosMatricula[0].Litoral, ListaPermisosMatricula[0].Estado, ListaPermisosMatricula[0].EnPuerto, ListaPermisosMatricula[0].Tx, "1");
        //    //////        ////////string strInserta = "INSERT INTO ##tmpRepUltimaTxSF VALUES ('" + ListaPermisosMatricula[0].Matricula + "', '" + ListaPermisosMatricula[0].Embarcacion + "', '" + ListaPermisosMatricula[0].RNP + "', '" + ListaPermisosMatricula[0].RazonSocial + "', '" + ListaPermisosMatricula[0].TipoPermiso + "', '" + ListaPermisosMatricula[0].PuertoBase + "', '" + ListaPermisosMatricula[0].Litoral + "', '" + ListaPermisosMatricula[0].Estado + "', '" + ListaPermisosMatricula[0].EnPuerto + "', '" + ListaPermisosMatricula[0].Tx + "', '1')";
        //    //////    }


        //    //////}//FINALIZA EL CICLO DE LA LISTA DE MATRICULAS 
        //    #endregion

        //}
        ////ListaOrdenadaDePermisos = RegresaListaOrdenada(List < ReportesAvanzadosPermisosMatricula_Result >  ListaPermisosMatricula);z
        //public List<ReportesAvanzadosPermisosMatricula_Result> RegresaListaOrdenadaEnPuerto(List<ReportesAvanzadosPermisosMatricula_Result> ListaPermisos, ReporteSituacionFlotaPesquera ModeloTmp)
        //{
        //    List<ReportesAvanzadosPermisosMatricula_Result> ListaPermisosOrdenada = new List<ReportesAvanzadosPermisosMatricula_Result>();

        //    ReportesAvanzadosPermisosMatricula_Result PermisoFomento = new ReportesAvanzadosPermisosMatricula_Result();
        //    ReportesAvanzadosPermisosMatricula_Result PermisoCamaron = new ReportesAvanzadosPermisosMatricula_Result();
        //    ReportesAvanzadosPermisosMatricula_Result PermisoTiburon = new ReportesAvanzadosPermisosMatricula_Result();
        //    ReportesAvanzadosPermisosMatricula_Result PermisoEscama = new ReportesAvanzadosPermisosMatricula_Result();
        //    ReportesAvanzadosPermisosMatricula_Result PermisoTunidos = new ReportesAvanzadosPermisosMatricula_Result();
        //    ReportesAvanzadosPermisosMatricula_Result PermisoCalamar = new ReportesAvanzadosPermisosMatricula_Result();
        //    ReportesAvanzadosPermisosMatricula_Result PermisoLangosta = new ReportesAvanzadosPermisosMatricula_Result();
        //    ReportesAvanzadosPermisosMatricula_Result PermisoPulpo = new ReportesAvanzadosPermisosMatricula_Result();
        //    ReportesAvanzadosPermisosMatricula_Result PermisoPezEspada = new ReportesAvanzadosPermisosMatricula_Result();
        //    ReportesAvanzadosPermisosMatricula_Result PermisoPelagicosMenores = new ReportesAvanzadosPermisosMatricula_Result();
        //    ReportesAvanzadosPermisosMatricula_Result PermisoAnchoveta = new ReportesAvanzadosPermisosMatricula_Result();
        //    ReportesAvanzadosPermisosMatricula_Result PermisoCangrejo = new ReportesAvanzadosPermisosMatricula_Result();
        //    ReportesAvanzadosPermisosMatricula_Result PermisoJaiba = new ReportesAvanzadosPermisosMatricula_Result();
        //    ReportesAvanzadosPermisosMatricula_Result PermisoSardina = new ReportesAvanzadosPermisosMatricula_Result();
        //    ReportesAvanzadosPermisosMatricula_Result PermisoCarnadaViva = new ReportesAvanzadosPermisosMatricula_Result();
        //    ReportesAvanzadosPermisosMatricula_Result PermisoPescaDidactica = new ReportesAvanzadosPermisosMatricula_Result();
        //    //PermisosPangas
        //    ReportesAvanzadosPermisosMatricula_Result PermisoTiburonMenores = new ReportesAvanzadosPermisosMatricula_Result();
        //    ReportesAvanzadosPermisosMatricula_Result PermisoJaibaMenores = new ReportesAvanzadosPermisosMatricula_Result();
        //    ReportesAvanzadosPermisosMatricula_Result PermisoEscamaMMenores = new ReportesAvanzadosPermisosMatricula_Result();
        //    ReportesAvanzadosPermisosMatricula_Result PermisoCamaronAMMenores = new ReportesAvanzadosPermisosMatricula_Result();


        //    foreach (ReportesAvanzadosPermisosMatricula_Result PermisoMatricula in ListaPermisos)
        //    {
        //        ReportesAvanzadosPermisosMatricula_Result PermisoMatriculaNueva  = new ReportesAvanzadosPermisosMatricula_Result();
        //        //if (PermisoMatricula.TipoPermiso.ToUpper() == "GOLFO")
        //        //{
        //            switch (PermisoMatricula.TipoPermiso.ToUpper())
        //            {

        //                case "CAMARON DE ALTAMAR":
        //                    PermisoCamaron = PermisoMatricula;
        //                    break;
        //                case "TIBURON":
        //                    PermisoTiburon = PermisoMatricula;
        //                    break;
        //                case "ESCAMA MARINA":
        //                    PermisoEscama = PermisoMatricula;
        //                    break;
        //                case "TUNIDOS":
        //                    PermisoTunidos = PermisoMatricula;
        //                    break;
        //                case "CALAMAR":
        //                    PermisoCalamar = PermisoMatricula;
        //                    break;

        //                case "LANGOSTA":
        //                    PermisoLangosta = PermisoMatricula;
        //                    break;
        //                case "PULPO":
        //                    PermisoPulpo = PermisoMatricula;
        //                    break;                        
        //                case "PEZ ESPADA":
        //                    PermisoPezEspada = PermisoMatricula;
        //                    break;                        
        //                case "PELAGICOS MENORES":
        //                    PermisoPelagicosMenores = PermisoMatricula;
        //                    break;
        //                case "ANCHOVETA":
        //                    PermisoAnchoveta = PermisoMatricula;
        //                    break;
        //                case "CANGREJO":
        //                    PermisoCangrejo = PermisoMatricula;
        //                    break;
        //                case "JAIBA":
        //                    PermisoJaiba = PermisoMatricula;
        //                    break;
        //                case "SARDINA":
        //                    PermisoSardina = PermisoMatricula;
        //                    break;
        //                case "CARNADA VIVA":
        //                    PermisoCarnadaViva = PermisoMatricula;
        //                    break;
        //                case "PESCA DIDACTICA":
        //                    PermisoPescaDidactica = PermisoMatricula;
        //                    break;
        //                case "FOMENTO":
        //                    PermisoFomento = PermisoMatricula;
        //                    break;
        //                //PermisosPangas
        //                case "TIBURON MENORES":
        //                    PermisoPelagicosMenores = PermisoMatricula;
        //                    break;
        //                case "JAIBA MENORES":
        //                    PermisoPelagicosMenores = PermisoMatricula;
        //                    break;
        //                case "ESCAMA MARINA MENORES":
        //                    PermisoPelagicosMenores = PermisoMatricula;
        //                    break;
        //                case "CAMARON DE ALTAMAR MENORES":
        //                    PermisoPelagicosMenores = PermisoMatricula;
        //                    break;
        //                default:
        //                    // You can use the default case.
        //                    break;
        //            }
        //        //}
        //    }



        //    if (PermisoCamaron.TipoPermiso != null)
        //        ListaPermisosOrdenada.Add(PermisoCamaron);
        //    if (PermisoTiburon.TipoPermiso != null)
        //        ListaPermisosOrdenada.Add(PermisoTiburon);
        //    if (PermisoEscama.TipoPermiso != null)
        //        ListaPermisosOrdenada.Add(PermisoEscama);
        //    if (PermisoTunidos.TipoPermiso != null)
        //        ListaPermisosOrdenada.Add(PermisoTunidos);
        //    if (PermisoCalamar.TipoPermiso != null)
        //        ListaPermisosOrdenada.Add(PermisoCalamar);
        //    if (PermisoLangosta.TipoPermiso != null)
        //        ListaPermisosOrdenada.Add(PermisoLangosta);
        //    if (PermisoPulpo.TipoPermiso != null)
        //        ListaPermisosOrdenada.Add(PermisoPulpo);
        //    if (PermisoPezEspada.TipoPermiso != null)
        //        ListaPermisosOrdenada.Add(PermisoPezEspada);
        //    if (PermisoPelagicosMenores.TipoPermiso != null)
        //        ListaPermisosOrdenada.Add(PermisoPelagicosMenores);
        //    if (PermisoAnchoveta.TipoPermiso != null)
        //        ListaPermisosOrdenada.Add(PermisoAnchoveta);
        //    if (PermisoCangrejo.TipoPermiso != null)
        //        ListaPermisosOrdenada.Add(PermisoCangrejo);
        //    if (PermisoJaiba.TipoPermiso != null)
        //        ListaPermisosOrdenada.Add(PermisoJaiba);
        //    if (PermisoSardina.TipoPermiso != null)
        //        ListaPermisosOrdenada.Add(PermisoSardina);
        //    if (PermisoCarnadaViva.TipoPermiso != null)
        //        ListaPermisosOrdenada.Add(PermisoCarnadaViva);

        //    if (PermisoPescaDidactica.TipoPermiso != null)
        //        ListaPermisosOrdenada.Add(PermisoPescaDidactica);
        //    if (PermisoFomento.TipoPermiso != null)
        //        ListaPermisosOrdenada.Add(PermisoFomento);
        //    //PermisosPangas
        //    if (PermisoTiburonMenores.TipoPermiso != null)
        //        ListaPermisosOrdenada.Add(PermisoTiburonMenores);
        //    if (PermisoJaibaMenores.TipoPermiso != null)
        //        ListaPermisosOrdenada.Add(PermisoJaibaMenores);
        //    if (PermisoEscamaMMenores.TipoPermiso != null)
        //        ListaPermisosOrdenada.Add(PermisoEscamaMMenores);
        //    if (PermisoCamaronAMMenores.TipoPermiso != null)
        //        ListaPermisosOrdenada.Add(PermisoCamaronAMMenores);

        //    //if (ListaPermisos[0].Litoral.ToUpper() == "GOLFO")
        //    //{
        //    //    //if(ModeloTmp.CamaronGolfo || ModeloTmp.CalamarGolfo || ModeloTmp.EscamaGolfo || ModeloTmp.LangostaGolfo || ModeloTmp.PulpoGolfo || ModeloTmp.TiburonGolfo || ModeloTmp.TunidosGolfo)
        //    //VUELTA:
        //    //    if (ModeloTmp.CamaronGolfo == false)
        //    //    {
        //    //        if (PermisoCamaron.TipoPermiso != null)
        //    //            ListaPermisosOrdenada.Add(PermisoCamaron);
        //    //    }


        //    //    goto VUELTA;
        //    //}
        //    //if (ListaPermisos[0].Litoral .ToUpper() == "PACIFICO")
        //    //{

        //    //}

        //    return ListaPermisosOrdenada;
        //}

        //public List<ReportesAvanzadosPermisosMatricula_Result> RegresaListaOrdenadaFueraPuerto(List<ReportesAvanzadosPermisosMatricula_Result> ListaPermisos, ReporteSituacionFlotaPesquera ModeloTmp)
        //{
        //    List<ReportesAvanzadosPermisosMatricula_Result> ListaPermisosOrdenada = new List<ReportesAvanzadosPermisosMatricula_Result>();

        //    Boolean PermisoFomentoBool = false;

        //    Boolean PermisoCamaronBool = false;
        //    Boolean PermisoTiburonBool = false;
        //    Boolean PermisoEscamaBool = false;
        //    Boolean PermisoTunidosBool = false;
        //    Boolean PermisoCalamarBool = false;
        //    Boolean PermisoLangostaBool = false;
        //    Boolean PermisoPulpoBool = false;
        //    Boolean PermisoPezEspadaBool = false;
        //    Boolean PermisoPelagicosMenoresBool = false;
        //    Boolean PermisoAnchovetaBool = false;
        //    Boolean PermisoCangrejoBool = false;
        //    Boolean PermisoJaibaBool = false;
        //    Boolean PermisoSardinaBool = false;
        //    Boolean PermisoCarnadaVivaBool = false;
        //    Boolean PermisoPescaDidacticaBool = false;


        //    ReportesAvanzadosPermisosMatricula_Result PermisoFomento = new ReportesAvanzadosPermisosMatricula_Result();

        //    ReportesAvanzadosPermisosMatricula_Result PermisoCamaron = new ReportesAvanzadosPermisosMatricula_Result();
        //    ReportesAvanzadosPermisosMatricula_Result PermisoTiburon = new ReportesAvanzadosPermisosMatricula_Result();
        //    ReportesAvanzadosPermisosMatricula_Result PermisoEscama = new ReportesAvanzadosPermisosMatricula_Result();
        //    ReportesAvanzadosPermisosMatricula_Result PermisoTunidos = new ReportesAvanzadosPermisosMatricula_Result();
        //    ReportesAvanzadosPermisosMatricula_Result PermisoCalamar = new ReportesAvanzadosPermisosMatricula_Result();
        //    ReportesAvanzadosPermisosMatricula_Result PermisoLangosta = new ReportesAvanzadosPermisosMatricula_Result();
        //    ReportesAvanzadosPermisosMatricula_Result PermisoPulpo = new ReportesAvanzadosPermisosMatricula_Result();
        //    ReportesAvanzadosPermisosMatricula_Result PermisoPezEspada = new ReportesAvanzadosPermisosMatricula_Result();
        //    ReportesAvanzadosPermisosMatricula_Result PermisoPelagicosMenores = new ReportesAvanzadosPermisosMatricula_Result();
        //    ReportesAvanzadosPermisosMatricula_Result PermisoAnchoveta = new ReportesAvanzadosPermisosMatricula_Result();
        //    ReportesAvanzadosPermisosMatricula_Result PermisoCangrejo = new ReportesAvanzadosPermisosMatricula_Result();
        //    ReportesAvanzadosPermisosMatricula_Result PermisoJaiba = new ReportesAvanzadosPermisosMatricula_Result();
        //    ReportesAvanzadosPermisosMatricula_Result PermisoSardina = new ReportesAvanzadosPermisosMatricula_Result();
        //    ReportesAvanzadosPermisosMatricula_Result PermisoCarnadaViva = new ReportesAvanzadosPermisosMatricula_Result();
        //    ReportesAvanzadosPermisosMatricula_Result PermisoPescaDidactica = new ReportesAvanzadosPermisosMatricula_Result();
        //    //PermisosPangas
        //    ReportesAvanzadosPermisosMatricula_Result PermisoTiburonMenores = new ReportesAvanzadosPermisosMatricula_Result();
        //    ReportesAvanzadosPermisosMatricula_Result PermisoJaibaMenores = new ReportesAvanzadosPermisosMatricula_Result();
        //    ReportesAvanzadosPermisosMatricula_Result PermisoEscamaMMenores = new ReportesAvanzadosPermisosMatricula_Result();
        //    ReportesAvanzadosPermisosMatricula_Result PermisoCamaronAMMenores = new ReportesAvanzadosPermisosMatricula_Result();

        //    foreach (ReportesAvanzadosPermisosMatricula_Result PermisoMatricula in ListaPermisos)
        //    {
        //        ReportesAvanzadosPermisosMatricula_Result PermisoMatriculaNueva = new ReportesAvanzadosPermisosMatricula_Result();
        //        //if (PermisoMatricula.TipoPermiso.ToUpper() == "GOLFO")
        //        //{
        //        switch (PermisoMatricula.TipoPermiso.ToUpper())
        //        {

        //            case "CAMARON DE ALTAMAR":
        //                PermisoCamaron = PermisoMatricula;
        //                PermisoCamaronBool = true;
        //                break;
        //            case "TIBURON":
        //                PermisoTiburon = PermisoMatricula;
        //                PermisoTiburonBool = true;
        //                break;
        //            case "ESCAMA MARINA":
        //                PermisoEscama = PermisoMatricula;
        //                PermisoEscamaBool = true;
        //                break;
        //            case "TUNIDOS":
        //                PermisoTunidos = PermisoMatricula;
        //                PermisoTunidosBool = true;
        //                break;
        //            case "CALAMAR":
        //                PermisoCalamar = PermisoMatricula;
        //                PermisoCalamarBool = true;
        //                break;

        //            case "LANGOSTA":
        //                PermisoLangosta = PermisoMatricula;
        //                PermisoLangostaBool = true;
        //                break;
        //            case "PULPO":
        //                PermisoPulpoBool = true;
        //                PermisoPulpo = PermisoMatricula;
        //                break;
        //            case "PEZ ESPADA":
        //                PermisoPezEspadaBool = true;
        //                PermisoPezEspada = PermisoMatricula;
        //                break;
        //            case "PELAGICOS MENORES":
        //                PermisoPelagicosMenoresBool = true;
        //                PermisoPelagicosMenores = PermisoMatricula;
        //                break;
        //            case "ANCHOVETA":
        //                PermisoAnchovetaBool = true;
        //                PermisoAnchoveta = PermisoMatricula;
        //                break;
        //            case "CANGREJO":
        //                PermisoCangrejoBool = true;
        //                PermisoCangrejo = PermisoMatricula;
        //                break;
        //            case "JAIBA":
        //                PermisoJaibaBool = true;
        //                PermisoJaiba = PermisoMatricula;
        //                break;
        //            case "SARDINA":
        //                PermisoSardinaBool = true;
        //                PermisoSardina = PermisoMatricula;
        //                break;
        //            case "CARNADA VIVA":
        //                PermisoCarnadaVivaBool = true;
        //                PermisoCarnadaViva = PermisoMatricula;
        //                break;
        //            case "PESCA DIDACTICA":
        //                PermisoPescaDidacticaBool = true;
        //                PermisoPescaDidactica = PermisoMatricula;
        //                break;
        //            case "FOMENTO":
        //                PermisoFomento = PermisoMatricula;
        //                PermisoFomentoBool = true;
        //                break;
        //            //PermisosPangas
        //            case "TIBURON MENORES":
        //                PermisoPelagicosMenores = PermisoMatricula;
        //                break;
        //            case "JAIBA MENORES":
        //                PermisoPelagicosMenores = PermisoMatricula;
        //                break;
        //            case "ESCAMA MARINA MENORES":
        //                PermisoPelagicosMenores = PermisoMatricula;
        //                break;
        //            case "CAMARON DE ALTAMAR MENORES":
        //                PermisoPelagicosMenores = PermisoMatricula;
        //                break;
        //            default:
        //                // You can use the default case.
        //                break;
        //        }
        //        //}
        //    }


        //    //if (PermisoCamaron.TipoPermiso != null)
        //    //    ListaPermisosOrdenada.Add(PermisoCamaron);
        //    //if (PermisoTiburon.TipoPermiso != null)
        //    //    ListaPermisosOrdenada.Add(PermisoTiburon);
        //    //if (PermisoEscama.TipoPermiso != null)
        //    //    ListaPermisosOrdenada.Add(PermisoEscama);
        //    //if (PermisoTunidos.TipoPermiso != null)
        //    //    ListaPermisosOrdenada.Add(PermisoTunidos);
        //    //if (PermisoCalamar.TipoPermiso != null)
        //    //    ListaPermisosOrdenada.Add(PermisoCalamar);
        //    //if (PermisoLangosta.TipoPermiso != null)
        //    //    ListaPermisosOrdenada.Add(PermisoLangosta);
        //    //if (PermisoPulpo.TipoPermiso != null)
        //    //    ListaPermisosOrdenada.Add(PermisoPulpo);
        //    //if (PermisoPezEspada.TipoPermiso != null)
        //    //    ListaPermisosOrdenada.Add(PermisoPezEspada);
        //    //if (PermisoPelagicosMenores.TipoPermiso != null)
        //    //    ListaPermisosOrdenada.Add(PermisoPelagicosMenores);
        //    //if (PermisoAnchoveta.TipoPermiso != null)
        //    //    ListaPermisosOrdenada.Add(PermisoAnchoveta);
        //    //if (PermisoCangrejo.TipoPermiso != null)
        //    //    ListaPermisosOrdenada.Add(PermisoCangrejo);
        //    //if (PermisoJaiba.TipoPermiso != null)
        //    //    ListaPermisosOrdenada.Add(PermisoJaiba);
        //    //if (PermisoSardina.TipoPermiso != null)
        //    //    ListaPermisosOrdenada.Add(PermisoSardina);
        //    //if (PermisoCarnadaViva.TipoPermiso != null)
        //    //    ListaPermisosOrdenada.Add(PermisoCarnadaViva);


        //    if (ListaPermisos[0].Litoral.ToUpper() == "GOLFO")
        //    {
        //        #region "AGREGA PERMISOS A LA LISTA DE GOLFO"
        //        //if(ModeloTmp.CamaronGolfo || ModeloTmp.CalamarGolfo || ModeloTmp.EscamaGolfo || ModeloTmp.LangostaGolfo || ModeloTmp.PulpoGolfo || ModeloTmp.TiburonGolfo || ModeloTmp.TunidosGolfo)
        //        //VUELTA:
        //        //Si no se selecciono veda para el permiso entonces se agrega en el orden


        //        if (ModeloTmp.CamaronGolfo == false)
        //        {
        //            if (PermisoCamaron.TipoPermiso != null)
        //                ListaPermisosOrdenada.Add(PermisoCamaron);
        //        }
        //        if (ModeloTmp.TiburonGolfo == false)
        //        {
        //            if (PermisoTiburon.TipoPermiso != null)
        //                ListaPermisosOrdenada.Add(PermisoTiburon);
        //        }
        //        if (ModeloTmp.EscamaGolfo == false)
        //        {
        //            if (PermisoEscama.TipoPermiso != null)
        //                ListaPermisosOrdenada.Add(PermisoEscama);
        //        }
        //        //PermisoTunidos
        //        if (ModeloTmp.TunidosGolfo == false)
        //        {
        //            if (PermisoTunidos.TipoPermiso != null)
        //                ListaPermisosOrdenada.Add(PermisoTunidos);
        //        }
        //        //PermisoCalamar
        //        if (ModeloTmp.CalamarGolfo == false)
        //        {
        //            if (PermisoCalamar.TipoPermiso != null)
        //                ListaPermisosOrdenada.Add(PermisoCalamar);
        //        }
        //        //PermisoLangosta
        //        if (ModeloTmp.LangostaGolfo == false)
        //        {
        //            if (PermisoLangosta.TipoPermiso != null)
        //                ListaPermisosOrdenada.Add(PermisoLangosta);
        //        }
        //        //PermisoPulpo
        //        if (ModeloTmp.PulpoGolfo == false)
        //        {
        //            if (PermisoPulpo.TipoPermiso != null)
        //                ListaPermisosOrdenada.Add(PermisoPulpo);
        //        }

        //        if (PermisoPezEspada.TipoPermiso != null)
        //            ListaPermisosOrdenada.Add(PermisoPezEspada);
        //        if (PermisoPelagicosMenores.TipoPermiso != null)
        //            ListaPermisosOrdenada.Add(PermisoPelagicosMenores);
        //        if (PermisoAnchoveta.TipoPermiso != null)
        //            ListaPermisosOrdenada.Add(PermisoAnchoveta);
        //        if (PermisoCangrejo.TipoPermiso != null)
        //            ListaPermisosOrdenada.Add(PermisoCangrejo);
        //        if (PermisoJaiba.TipoPermiso != null)
        //            ListaPermisosOrdenada.Add(PermisoJaiba);
        //        if (PermisoSardina.TipoPermiso != null)
        //            ListaPermisosOrdenada.Add(PermisoSardina);
        //        if (PermisoCarnadaViva.TipoPermiso != null)
        //            ListaPermisosOrdenada.Add(PermisoCarnadaViva);

        //        if (PermisoPescaDidactica.TipoPermiso != null)
        //            ListaPermisosOrdenada.Add(PermisoPescaDidactica);
        //        //PermisosPangas
        //        if (PermisoTiburonMenores.TipoPermiso != null)
        //            ListaPermisosOrdenada.Add(PermisoTiburonMenores);
        //        if (PermisoJaibaMenores.TipoPermiso != null)
        //            ListaPermisosOrdenada.Add(PermisoJaibaMenores);
        //        if (PermisoEscamaMMenores.TipoPermiso != null)
        //            ListaPermisosOrdenada.Add(PermisoEscamaMMenores);
        //        if (PermisoCamaronAMMenores.TipoPermiso != null)
        //            ListaPermisosOrdenada.Add(PermisoCamaronAMMenores);

        //        if (ModeloTmp.FomentoGolfo == false)
        //        {
        //            if (PermisoFomento.TipoPermiso != null)
        //                ListaPermisosOrdenada.Add(PermisoFomento);
        //        }
        //        //Si no se agrego ningun permiso por que se seleccionaron las vedas, entonces se agrega
        //        //if (ListaPermisosOrdenada.Count == 0)
        //        //{
        //        if (ModeloTmp.CamaronGolfo == true && PermisoCamaron.TipoPermiso != null)
        //        {
        //            ListaPermisosOrdenada.Add(PermisoCamaron);
        //        }
        //        if (ModeloTmp.TiburonGolfo == true && PermisoTiburon.TipoPermiso != null)
        //        {
        //            ListaPermisosOrdenada.Add(PermisoTiburon);
        //        }
        //        if (ModeloTmp.EscamaGolfo == true && PermisoEscama.TipoPermiso != null)
        //        {
        //            ListaPermisosOrdenada.Add(PermisoEscama);
        //        }
        //        //PermisoTunidos
        //        if (ModeloTmp.TunidosGolfo == true && PermisoTunidos.TipoPermiso != null)
        //        {
        //            ListaPermisosOrdenada.Add(PermisoTunidos);
        //        }
        //        //PermisoCalamar
        //        if (ModeloTmp.CalamarGolfo == true && PermisoCalamar.TipoPermiso != null)
        //        {
        //            ListaPermisosOrdenada.Add(PermisoCalamar);
        //        }
        //        //PermisoLangosta
        //        if (ModeloTmp.LangostaGolfo == true && PermisoLangosta.TipoPermiso != null)
        //        {
        //            ListaPermisosOrdenada.Add(PermisoLangosta);
        //        }
        //        //PermisoPulpo
        //        if (ModeloTmp.PulpoGolfo == true && PermisoPulpo.TipoPermiso != null)
        //        {
        //            ListaPermisosOrdenada.Add(PermisoPulpo);
        //        }
        //        //}

        //        //goto VUELTA;
        //    #endregion
        //    }

        //    if (ListaPermisos[0].Litoral.ToUpper() == "PACIFICO")
        //    {
        //        #region "AGREGA PERMISOS A LA LISTA DE PACIFICO"
        //        //Si no se selecciono veda para el permiso entonces se agrega en el orden

        //        if (ModeloTmp.CamaronPacifico == false)
        //        {
        //            if (PermisoCamaron.TipoPermiso != null)
        //                ListaPermisosOrdenada.Add(PermisoCamaron);
        //        }
        //        if (ModeloTmp.TiburonPacifico == false)
        //        {
        //            if (PermisoTiburon.TipoPermiso != null)
        //                ListaPermisosOrdenada.Add(PermisoTiburon);
        //        }
        //        if (ModeloTmp.EscamaPacifico == false)
        //        {
        //            if (PermisoEscama.TipoPermiso != null)
        //                ListaPermisosOrdenada.Add(PermisoEscama);
        //        }
        //        //PermisoTunidos
        //        if (ModeloTmp.TunidosPacifico == false)
        //        {
        //            if (PermisoTunidos.TipoPermiso != null)
        //                ListaPermisosOrdenada.Add(PermisoTunidos);
        //        }
        //        //PermisoCalamar
        //        if (ModeloTmp.CalamarPacifico == false)
        //        {
        //            if (PermisoCalamar.TipoPermiso != null)
        //                ListaPermisosOrdenada.Add(PermisoCalamar);
        //        }
        //        //PermisoLangosta
        //        if (ModeloTmp.LangostaPacifico == false)
        //        {
        //            if (PermisoLangosta.TipoPermiso != null)
        //                ListaPermisosOrdenada.Add(PermisoLangosta);
        //        }
        //        //PermisoPulpo
        //        if (ModeloTmp.PulpoPacifico == false)
        //        {
        //            if (PermisoPulpo.TipoPermiso != null)
        //                ListaPermisosOrdenada.Add(PermisoPulpo);
        //        }


        //        if (PermisoPezEspada.TipoPermiso != null)
        //            ListaPermisosOrdenada.Add(PermisoPezEspada);
        //        if (PermisoPelagicosMenores.TipoPermiso != null)
        //            ListaPermisosOrdenada.Add(PermisoPelagicosMenores);
        //        if (PermisoAnchoveta.TipoPermiso != null)
        //            ListaPermisosOrdenada.Add(PermisoAnchoveta);
        //        if (PermisoCangrejo.TipoPermiso != null)
        //            ListaPermisosOrdenada.Add(PermisoCangrejo);
        //        if (PermisoJaiba.TipoPermiso != null)
        //            ListaPermisosOrdenada.Add(PermisoJaiba);
        //        if (PermisoSardina.TipoPermiso != null)
        //            ListaPermisosOrdenada.Add(PermisoSardina);
        //        if (PermisoCarnadaViva.TipoPermiso != null)
        //            ListaPermisosOrdenada.Add(PermisoCarnadaViva);

        //        if (PermisoPescaDidactica.TipoPermiso != null)
        //            ListaPermisosOrdenada.Add(PermisoPescaDidactica);
        //        //PermisosPangas
        //        if (PermisoTiburonMenores.TipoPermiso != null)
        //            ListaPermisosOrdenada.Add(PermisoTiburonMenores);
        //        if (PermisoJaibaMenores.TipoPermiso != null)
        //            ListaPermisosOrdenada.Add(PermisoJaibaMenores);
        //        if (PermisoEscamaMMenores.TipoPermiso != null)
        //            ListaPermisosOrdenada.Add(PermisoEscamaMMenores);
        //        if (PermisoCamaronAMMenores.TipoPermiso != null)
        //            ListaPermisosOrdenada.Add(PermisoCamaronAMMenores);

        //        if (ModeloTmp.FomentoPacifico == false)
        //        {
        //            if (PermisoFomento.TipoPermiso != null)
        //                ListaPermisosOrdenada.Add(PermisoFomento);
        //        }
        //        //Si no se agrego ningun permiso por que se seleccionaron las vedas, entonces se agrega
        //        //if (ListaPermisosOrdenada.Count == 0)
        //        //{
        //        if (ModeloTmp.CamaronPacifico == true && PermisoCamaron.TipoPermiso != null)
        //        {
        //            ListaPermisosOrdenada.Add(PermisoCamaron);
        //        }
        //        if (ModeloTmp.TiburonPacifico == true && PermisoTiburon.TipoPermiso != null)
        //        {
        //            ListaPermisosOrdenada.Add(PermisoTiburon);
        //        }
        //        if (ModeloTmp.EscamaPacifico == true && PermisoEscama.TipoPermiso != null)
        //        {
        //            ListaPermisosOrdenada.Add(PermisoEscama);
        //        }
        //        //PermisoTunidos
        //        if (ModeloTmp.TunidosPacifico == true && PermisoTunidos.TipoPermiso != null)
        //        {
        //            ListaPermisosOrdenada.Add(PermisoTunidos);
        //        }
        //        //PermisoCalamar
        //        if (ModeloTmp.CalamarPacifico == true && PermisoCalamar.TipoPermiso != null)
        //        {
        //            ListaPermisosOrdenada.Add(PermisoCalamar);
        //        }
        //        //PermisoLangosta
        //        if (ModeloTmp.LangostaPacifico == true && PermisoLangosta.TipoPermiso != null)
        //        {
        //            ListaPermisosOrdenada.Add(PermisoLangosta);
        //        }
        //        //PermisoPulpo
        //        if (ModeloTmp.PulpoPacifico == true && PermisoPulpo.TipoPermiso != null)
        //        {
        //            ListaPermisosOrdenada.Add(PermisoPulpo);
        //        }
        //        //}
        //        #endregion
        //    }

        //    return ListaPermisosOrdenada;
        //}

        ////-----------------------------Por Barco--------------------------------
        //public void PreparaInfo(string UserName_, ReporteSituacionFlotaPesquera ModeloTmp_)
        //{
        //    //Borra la informacion de tmpRepUltimaTxSF
        //    //Ejecuta el procedimiento SPMasReportesAvanzadosPreparaInfo1 para traer la ultima informacion de las embarcaciones
        //    List<ReportesAvanzadosPreparaInfo1_Result> ListaMatriculasNoUsada = db.ReportesAvanzadosPreparaInfo1(UserName_).ToList();


        //    #region "OBTIENE LAS MATRICULAS QUE ESTAN EN DE PUERTO"
        //    List<ReportesAvanzadosObtieneInfo1_Result> ListaMatriculas = db.ReportesAvanzadosObtieneInfo1(1).ToList();
        //    //Recorre la lista de Matriculas resultantes de la ejecucion del procedimiento anterior
        //    foreach (ReportesAvanzadosObtieneInfo1_Result Matricula in ListaMatriculas)
        //    {
        //        Boolean MarcadoExclusion = false;

        //        List<ReportesAvanzadosPermisosMatricula_Result> ListaPermisosMatricula = db.ReportesAvanzadosPermisosMatricula(Matricula.matricula).ToList();
        //        List<ReportesAvanzadosPermisosMatricula_Result> ListaOrdenadaDePermisos = new List<ReportesAvanzadosPermisosMatricula_Result>();
        //        if (ListaPermisosMatricula.Count > 0)
        //        {
        //            ListaOrdenadaDePermisos = RegresaListaOrdenadaEnPuerto(ListaPermisosMatricula, ModeloTmp_);
        //        }
        //        Boolean enVedaEmb = false;
        //        foreach (ReportesAvanzadosPermisosMatricula_Result PermisoMatricula in ListaOrdenadaDePermisos) //ListaPermisosMatricula
        //        {
        //            if (PermisoMatricula.TipoPermiso == "FOMENTO")
        //            {
        //                string Stop = "Se va a agregar con permiso de camaron para el litoral " + PermisoMatricula.Litoral;
        //            }
        //            MarcadoExclusion = EstaMarcadoParaExclusion(PermisoMatricula.TipoPermiso, PermisoMatricula.Litoral, ModeloTmp_);
        //            if (MarcadoExclusion == false)
        //            {
        //                if (ModeloTmp_.CamaronGolfo) {
        //                    var escamarad = "";
        //                }
        //                //DENTRO DEL CICLO DE LOS PERMISOS LOS REVISA SI ESTAN EN VEDA O NO E INSERTA EN LA TABLA TMasRepUltimaTxSF
        //                enVedaEmb = enVeda(PermisoMatricula.TipoPermiso, PermisoMatricula.Litoral, ModeloTmp_);
        //                //SI ES UNA VEDA PERO ESTA DENTRO DE PUERTO, ENTONCES NO SE TOMA EN CUENTA LA VEDA Y SE AGREGA EL PERMISO

        //                if (enVedaEmb == true && PermisoMatricula.EnPuerto == "1")
        //                {
        //                    //se le pone la veda 0 para que los tome en el reporte
        //                    db.ReportesAvanzadosPreparaInfo2(PermisoMatricula.Matricula, PermisoMatricula.Embarcacion, PermisoMatricula.RNP, PermisoMatricula.RazonSocial, PermisoMatricula.TipoPermiso, PermisoMatricula.PuertoBase, PermisoMatricula.Litoral, PermisoMatricula.Estado, PermisoMatricula.EnPuerto, PermisoMatricula.Tx, "0", PermisoMatricula.Latitud, PermisoMatricula.Longitud);
        //                    enVedaEmb = false;
        //                    break;
        //                }
        //                if (enVedaEmb == false)
        //                {
        //                    //inserta en tmpRepUltimaTxSF   enveda = 0
        //                    db.ReportesAvanzadosPreparaInfo2(PermisoMatricula.Matricula, PermisoMatricula.Embarcacion, PermisoMatricula.RNP, PermisoMatricula.RazonSocial, PermisoMatricula.TipoPermiso, PermisoMatricula.PuertoBase, PermisoMatricula.Litoral, PermisoMatricula.Estado, PermisoMatricula.EnPuerto, PermisoMatricula.Tx, "0", PermisoMatricula.Latitud, PermisoMatricula.Longitud);
        //                    break;
        //                }
        //            }

        //        }
        //        if (enVedaEmb == true)
        //        {
        //            //inserta en tmpRepUltimaTxSF     enveda = 1
        //            db.ReportesAvanzadosPreparaInfo2(ListaOrdenadaDePermisos[0].Matricula, ListaOrdenadaDePermisos[0].Embarcacion, ListaOrdenadaDePermisos[0].RNP, ListaOrdenadaDePermisos[0].RazonSocial, ListaOrdenadaDePermisos[0].TipoPermiso, ListaOrdenadaDePermisos[0].PuertoBase, ListaOrdenadaDePermisos[0].Litoral, ListaOrdenadaDePermisos[0].Estado, ListaOrdenadaDePermisos[0].EnPuerto, ListaOrdenadaDePermisos[0].Tx, "1", ListaOrdenadaDePermisos[0].Latitud, ListaOrdenadaDePermisos[0].Longitud);
        //        }

        //    }
        //    #endregion

        //    #region "OBTIENE LAS MATRICULAS QUE ESTAN FUERA DE PUERTO"
        //    ListaMatriculas = db.ReportesAvanzadosObtieneInfo1(0).ToList();
        //    foreach (ReportesAvanzadosObtieneInfo1_Result Matricula in ListaMatriculas)
        //    {
        //        Boolean MarcadoExclusion = false;
        //        List<ReportesAvanzadosPermisosMatricula_Result> ListaPermisosMatricula = db.ReportesAvanzadosPermisosMatricula(Matricula.matricula).ToList();
        //        List<ReportesAvanzadosPermisosMatricula_Result> ListaOrdenadaDePermisos = new List<ReportesAvanzadosPermisosMatricula_Result>();
        //        if (ListaPermisosMatricula.Count > 0)
        //        {
        //            ListaOrdenadaDePermisos = RegresaListaOrdenadaFueraPuerto(ListaPermisosMatricula, ModeloTmp_);
        //        }
        //        Boolean enVedaEmb = false;
        //        foreach (ReportesAvanzadosPermisosMatricula_Result PermisoMatricula in ListaOrdenadaDePermisos)
        //        {
        //            MarcadoExclusion = EstaMarcadoParaExclusion(PermisoMatricula.TipoPermiso, PermisoMatricula.Litoral, ModeloTmp_);
        //            if (MarcadoExclusion == false)
        //            {
        //                if (PermisoMatricula.TipoPermiso == "FOMENTO")
        //                {
        //                    string Stop = "Se va a agregar con permiso de camaron para el litoral " + PermisoMatricula.Litoral;
        //                }
        //                //DENTRO DEL CICLO DE LOS PERMISOS LOS REVISA SI ESTAN EN VEDA O NO E INSERTA EN LA TABLA TMasRepUltimaTxSF
        //                enVedaEmb = enVeda(PermisoMatricula.TipoPermiso, PermisoMatricula.Litoral, ModeloTmp_);
        //                if (enVedaEmb == false)
        //                {
        //                    //inserta en tmpRepUltimaTxSF   enveda = 0
        //                    db.ReportesAvanzadosPreparaInfo2(PermisoMatricula.Matricula, PermisoMatricula.Embarcacion, PermisoMatricula.RNP, PermisoMatricula.RazonSocial, PermisoMatricula.TipoPermiso, PermisoMatricula.PuertoBase, PermisoMatricula.Litoral, PermisoMatricula.Estado, PermisoMatricula.EnPuerto, PermisoMatricula.Tx, "0", PermisoMatricula.Latitud, PermisoMatricula.Longitud);
        //                    break;
        //                }
        //            }

        //        }
        //        if (enVedaEmb == true)
        //        {
        //            //inserta en tmpRepUltimaTxSF     enveda = 1,
        //            db.ReportesAvanzadosPreparaInfo2(ListaOrdenadaDePermisos[0].Matricula, ListaOrdenadaDePermisos[0].Embarcacion, ListaOrdenadaDePermisos[0].RNP, ListaOrdenadaDePermisos[0].RazonSocial, ListaOrdenadaDePermisos[0].TipoPermiso, ListaOrdenadaDePermisos[0].PuertoBase, ListaOrdenadaDePermisos[0].Litoral, ListaOrdenadaDePermisos[0].Estado, ListaOrdenadaDePermisos[0].EnPuerto, ListaOrdenadaDePermisos[0].Tx, "1", ListaOrdenadaDePermisos[0].Latitud, ListaOrdenadaDePermisos[0].Longitud);
        //        }

        //    }
        //    #endregion

        //}

        ////Agregado por SZ el 10/10/2017
        ////-------------------------------Por Panga---------------------------------------------
        //public void PreparaInfoPanga(string UserName_, ReporteSituacionFlotaPesquera ModeloTmp_)
        //{
        //    //Borra la informacion de tmpRepUltimaTxSF
        //    //Ejecuta el procedimiento SPMasReportesAvanzadosPreparaInfoPangas para traer la ultima informacion de las embarcaciones
        //    List<ReportesAvanzadosPreparaInfoPangas_Result> ListaMatriculasNoUsada = db.ReportesAvanzadosPreparaInfoPangas(UserName_).ToList();


        //    #region "OBTIENE LAS MATRICULAS QUE ESTAN EN DE PUERTO"
        //    //List<ReportesAvanzadosPreparaInfoPangas_Result> ListaMatriculas = db.ReportesAvanzadosObtieneInfoPangas("1").ToList();
        //    List<ReportesAvanzadosObtieneInfo1_Result> ListaMatriculas = db.ReportesAvanzadosObtieneInfo1(1).ToList();

        //    //Recorre la lista de Matriculas resultantes de la ejecucion del procedimiento anterior
        //    foreach (ReportesAvanzadosObtieneInfo1_Result Matricula in ListaMatriculas)
        //    {
        //        Boolean MarcadoExclusion = false;

        //        List<ReportesAvanzadosPermisosMatricula_Result> ListaPermisosMatricula = db.ReportesAvanzadosPermisosMatricula(Matricula.matricula).ToList();
        //        List<ReportesAvanzadosPermisosMatricula_Result> ListaOrdenadaDePermisos = new List<ReportesAvanzadosPermisosMatricula_Result>();
        //        if (ListaPermisosMatricula.Count > 0)
        //        {
        //            ListaOrdenadaDePermisos = RegresaListaOrdenadaEnPuerto(ListaPermisosMatricula, ModeloTmp_);
        //        }
        //        Boolean enVedaEmb = false;
        //        foreach (ReportesAvanzadosPermisosMatricula_Result PermisoMatricula in ListaOrdenadaDePermisos) //ListaPermisosMatricula
        //        {
        //            if (PermisoMatricula.TipoPermiso == "FOMENTO")
        //            {
        //                string Stop = "Se va a agregar con permiso de camaron para el litoral " + PermisoMatricula.Litoral;
        //            }
        //            MarcadoExclusion = EstaMarcadoParaExclusion(PermisoMatricula.TipoPermiso, PermisoMatricula.Litoral, ModeloTmp_);
        //            if (MarcadoExclusion == false)
        //            {
        //                if (ModeloTmp_.CamaronGolfo)
        //                {
        //                    var escamarad = "";
        //                }
        //                //DENTRO DEL CICLO DE LOS PERMISOS LOS REVISA SI ESTAN EN VEDA O NO E INSERTA EN LA TABLA TMasRepUltimaTxSF
        //                enVedaEmb = enVeda(PermisoMatricula.TipoPermiso, PermisoMatricula.Litoral, ModeloTmp_);
        //                //SI ES UNA VEDA PERO ESTA DENTRO DE PUERTO, ENTONCES NO SE TOMA EN CUENTA LA VEDA Y SE AGREGA EL PERMISO

        //                if (enVedaEmb == true && PermisoMatricula.EnPuerto == "1")
        //                {
        //                    //se le pone la veda 0 para que los tome en el reporte
        //                    db.ReportesAvanzadosPreparaInfo2(PermisoMatricula.Matricula, PermisoMatricula.Embarcacion, PermisoMatricula.RNP, PermisoMatricula.RazonSocial, PermisoMatricula.TipoPermiso, PermisoMatricula.PuertoBase, PermisoMatricula.Litoral, PermisoMatricula.Estado, PermisoMatricula.EnPuerto, PermisoMatricula.Tx, "0", PermisoMatricula.Latitud, PermisoMatricula.Longitud);
        //                    enVedaEmb = false;
        //                    break;
        //                }
        //                if (enVedaEmb == false)
        //                {
        //                    //inserta en tmpRepUltimaTxSF   enveda = 0
        //                    db.ReportesAvanzadosPreparaInfo2(PermisoMatricula.Matricula, PermisoMatricula.Embarcacion, PermisoMatricula.RNP, PermisoMatricula.RazonSocial, PermisoMatricula.TipoPermiso, PermisoMatricula.PuertoBase, PermisoMatricula.Litoral, PermisoMatricula.Estado, PermisoMatricula.EnPuerto, PermisoMatricula.Tx, "0", PermisoMatricula.Latitud, PermisoMatricula.Longitud);
        //                    break;
        //                }
        //            }

        //        }
        //        if (enVedaEmb == true)
        //        {
        //            //inserta en tmpRepUltimaTxSF     enveda = 1
        //            db.ReportesAvanzadosPreparaInfo2(ListaOrdenadaDePermisos[0].Matricula, ListaOrdenadaDePermisos[0].Embarcacion, ListaOrdenadaDePermisos[0].RNP, ListaOrdenadaDePermisos[0].RazonSocial, ListaOrdenadaDePermisos[0].TipoPermiso, ListaOrdenadaDePermisos[0].PuertoBase, ListaOrdenadaDePermisos[0].Litoral, ListaOrdenadaDePermisos[0].Estado, ListaOrdenadaDePermisos[0].EnPuerto, ListaOrdenadaDePermisos[0].Tx, "1", ListaOrdenadaDePermisos[0].Latitud, ListaOrdenadaDePermisos[0].Longitud);
        //        }

        //    }
        //    #endregion

        //    #region "OBTIENE LAS MATRICULAS QUE ESTAN FUERA DE PUERTO"
        //    ListaMatriculas = db.ReportesAvanzadosObtieneInfo1(0).ToList();
        //    foreach (ReportesAvanzadosObtieneInfo1_Result Matricula in ListaMatriculas)
        //    {
        //        Boolean MarcadoExclusion = false;
        //        List<ReportesAvanzadosPermisosMatricula_Result> ListaPermisosMatricula = db.ReportesAvanzadosPermisosMatricula(Matricula.matricula).ToList();
        //        List<ReportesAvanzadosPermisosMatricula_Result> ListaOrdenadaDePermisos = new List<ReportesAvanzadosPermisosMatricula_Result>();
        //        if (ListaPermisosMatricula.Count > 0)
        //        {
        //            ListaOrdenadaDePermisos = RegresaListaOrdenadaFueraPuerto(ListaPermisosMatricula, ModeloTmp_);
        //        }
        //        Boolean enVedaEmb = false;
        //        foreach (ReportesAvanzadosPermisosMatricula_Result PermisoMatricula in ListaOrdenadaDePermisos)
        //        {
        //            MarcadoExclusion = EstaMarcadoParaExclusion(PermisoMatricula.TipoPermiso, PermisoMatricula.Litoral, ModeloTmp_);
        //            if (MarcadoExclusion == false)
        //            {
        //                if (PermisoMatricula.TipoPermiso == "FOMENTO")
        //                {
        //                    string Stop = "Se va a agregar con permiso de camaron para el litoral " + PermisoMatricula.Litoral;
        //                }
        //                //DENTRO DEL CICLO DE LOS PERMISOS LOS REVISA SI ESTAN EN VEDA O NO E INSERTA EN LA TABLA TMasRepUltimaTxSF
        //                enVedaEmb = enVeda(PermisoMatricula.TipoPermiso, PermisoMatricula.Litoral, ModeloTmp_);
        //                if (enVedaEmb == false)
        //                {
        //                    //inserta en tmpRepUltimaTxSF   enveda = 0
        //                    db.ReportesAvanzadosPreparaInfo2(PermisoMatricula.Matricula, PermisoMatricula.Embarcacion, PermisoMatricula.RNP, PermisoMatricula.RazonSocial, PermisoMatricula.TipoPermiso, PermisoMatricula.PuertoBase, PermisoMatricula.Litoral, PermisoMatricula.Estado, PermisoMatricula.EnPuerto, PermisoMatricula.Tx, "0", PermisoMatricula.Latitud, PermisoMatricula.Longitud);
        //                    break;
        //                }
        //            }

        //        }
        //        if (enVedaEmb == true)
        //        {
        //            //inserta en tmpRepUltimaTxSF     enveda = 1,
        //            db.ReportesAvanzadosPreparaInfo2(ListaOrdenadaDePermisos[0].Matricula, ListaOrdenadaDePermisos[0].Embarcacion, ListaOrdenadaDePermisos[0].RNP, ListaOrdenadaDePermisos[0].RazonSocial, ListaOrdenadaDePermisos[0].TipoPermiso, ListaOrdenadaDePermisos[0].PuertoBase, ListaOrdenadaDePermisos[0].Litoral, ListaOrdenadaDePermisos[0].Estado, ListaOrdenadaDePermisos[0].EnPuerto, ListaOrdenadaDePermisos[0].Tx, "1", ListaOrdenadaDePermisos[0].Latitud, ListaOrdenadaDePermisos[0].Longitud);
        //        }

        //    }
        //    #endregion

        //}

        //---------------------------------Por Permiso-------------------------------------------------
        //public void PreparaInfoTodosLosPermisos(string UserName_, ReporteSituacionFlotaPesquera ModeloTmp_)
        //{
        //    //TODOS LOS PERMISOS
        //    //Ejecuta el procedimiento SPMasReportesAvanzadosPreparaInfo1 para traer la ultima informacion de las embarcaciones
        //    List<SPMasReportesAvanzadosPreparaInfoTodosLosPermisos_Result> ListaMatriculasNoUsada = db.SPMasReportesAvanzadosPreparaInfoTodosLosPermisos(UserName_).ToList();

        //    #region "OBTIENE LAS MATRICULAS QUE ESTAN EN DE PUERTO"            
        //    List<ReportesAvanzadosObtieneInfo1_Result> ListaMatriculas = db.ReportesAvanzadosObtieneInfo1(1).ToList();
        //    //Recorre la lista de Matriculas resultantes de la ejecucion del procedimiento anterior
        //    foreach (ReportesAvanzadosObtieneInfo1_Result Matricula in ListaMatriculas)
        //    {

        //        Boolean MarcadoExclusion = false;
        //        //  DENTRO DEL CICLO EJECUTA LA SELECCION DE PERMISOS POR MATRICULA
        //        List<ReportesAvanzadosPermisosMatricula_Result> ListaPermisosMatricula = db.ReportesAvanzadosPermisosMatricula(Matricula.matricula).ToList();
        //        foreach (ReportesAvanzadosPermisosMatricula_Result PermisoMatricula in ListaPermisosMatricula) //RECORRE LA LISTA DE LOS PERMISOS DE LA MATRICULA 
        //        {
        //            if (PermisoMatricula.Embarcacion.Contains("DON FRANCISCO"))
        //            {
        //                string asas = "";
        //            }
        //            //VALIDA SI EL PERMISO ESTA MARCADO PARA EXCLUIR
        //            MarcadoExclusion =  EstaMarcadoParaExclusion(PermisoMatricula.TipoPermiso, PermisoMatricula.Litoral, ModeloTmp_);
        //            if (MarcadoExclusion == true)
        //            {
        //                //Si MarcadoExclusion es igual a TRUE , entonces se inserta y marca enveda = 1 
        //                //db.ReportesAvanzadosPreparaInfo2(PermisoMatricula.Matricula, PermisoMatricula.Embarcacion, PermisoMatricula.RNP, PermisoMatricula.RazonSocial, PermisoMatricula.TipoPermiso, PermisoMatricula.PuertoBase, PermisoMatricula.Litoral, PermisoMatricula.Estado, PermisoMatricula.EnPuerto, PermisoMatricula.Tx, "1", PermisoMatricula.Latitud, PermisoMatricula.Longitud);
        //                //Si estan en veda o marcados como excluidos entonces no se inserta
        //            }
        //            else
        //            {
        //                //Si MarcadoExclusion es igual a FALSE , entonces se inserta y marca enveda = 0
        //                db.ReportesAvanzadosPreparaInfo2(PermisoMatricula.Matricula, PermisoMatricula.Embarcacion, PermisoMatricula.RNP, PermisoMatricula.RazonSocial, PermisoMatricula.TipoPermiso, PermisoMatricula.PuertoBase, PermisoMatricula.Litoral, PermisoMatricula.Estado, PermisoMatricula.EnPuerto, PermisoMatricula.Tx, "0", PermisoMatricula.Latitud, PermisoMatricula.Longitud);
        //            }                                        
        //        }                               
        //    }//FINALIZA EL CICLO DE LA LISTA DE MATRICULAS 
        //    #endregion

        //    #region "OBTIENE LAS MATRICULAS QUE ESTAN FUERA DE PUERTO"
        //    ListaMatriculas = db.ReportesAvanzadosObtieneInfo1(0).ToList();
        //    //Recorre la lista de Matriculas resultantes de la ejecucion del procedimiento anterior
        //    foreach (ReportesAvanzadosObtieneInfo1_Result Matricula in ListaMatriculas)
        //    {
        //        Boolean MarcadoExclusion = false;
        //        //  DENTRO DEL CICLO EJECUTA LA SELECCION DE PERMISOS POR MATRICULA
        //        List<ReportesAvanzadosPermisosMatricula_Result> ListaPermisosMatricula = db.ReportesAvanzadosPermisosMatricula(Matricula.matricula).ToList();
        //        foreach (ReportesAvanzadosPermisosMatricula_Result PermisoMatricula in ListaPermisosMatricula) //RECORRE LA LISTA DE LOS PERMISOS DE LA MATRICULA 
        //        {
        //            //VALIDA SI EL PERMISO ESTA MARCADO PARA EXCLUIR
        //            MarcadoExclusion = EstaMarcadoParaExclusion(PermisoMatricula.TipoPermiso, PermisoMatricula.Litoral, ModeloTmp_);
        //            if (MarcadoExclusion == true)
        //            {
        //                //Si MarcadoExclusion es igual a TRUE , entonces se inserta y marca enveda = 1 
        //                //db.ReportesAvanzadosPreparaInfo2(PermisoMatricula.Matricula, PermisoMatricula.Embarcacion, PermisoMatricula.RNP, PermisoMatricula.RazonSocial, PermisoMatricula.TipoPermiso, PermisoMatricula.PuertoBase, PermisoMatricula.Litoral, PermisoMatricula.Estado, PermisoMatricula.EnPuerto, PermisoMatricula.Tx, "1", PermisoMatricula.Latitud, PermisoMatricula.Longitud);
        //                //Si estan en veda o marcados como excluidos entonces no se inserta
        //            }
        //            else
        //            {
        //                //Si MarcadoExclusion es igual a FALSE , entonces se inserta y marca enveda = 0
        //                db.ReportesAvanzadosPreparaInfo2(PermisoMatricula.Matricula, PermisoMatricula.Embarcacion, PermisoMatricula.RNP, PermisoMatricula.RazonSocial, PermisoMatricula.TipoPermiso, PermisoMatricula.PuertoBase, PermisoMatricula.Litoral, PermisoMatricula.Estado, PermisoMatricula.EnPuerto, PermisoMatricula.Tx, "0", PermisoMatricula.Latitud, PermisoMatricula.Longitud);
        //            }
        //        }
        //    }//FINALIZA EL CICLO DE LA LISTA DE MATRICULAS 
        //    #endregion

        //}

        //public Boolean EstaMarcadoParaExclusion(string permiso, string litoral, ReporteSituacionFlotaPesquera ModeloTmp)
        //{
        //    Boolean MarcadoParaExclusion = false;
        //    if (litoral.ToUpper() == "GOLFO")
        //    {
        //        switch (permiso.ToUpper())
        //        {
        //            case "TUNIDOS":
        //                if (ModeloTmp.TunidosGolfo == true) MarcadoParaExclusion = true;    break;
        //            case "CAMARON DE ALTAMAR":
        //                if (ModeloTmp.CamaronGolfo == true) MarcadoParaExclusion = true;    break;
        //            case "CALAMAR":
        //                if (ModeloTmp.CalamarGolfo == true) MarcadoParaExclusion = true;    break;
        //            case "LANGOSTA":
        //                if (ModeloTmp.LangostaGolfo == true) MarcadoParaExclusion = true;   break;
        //            case "PULPO":
        //                if (ModeloTmp.PulpoGolfo == true) MarcadoParaExclusion = true;      break;
        //            case "TIBURON"  :
        //                if (ModeloTmp.TiburonGolfo == true) MarcadoParaExclusion = true;    break;
        //            case "PEZ ESPADA":
        //                if (ModeloTmp.PezEspadaGolfo == true) MarcadoParaExclusion = true;    break;
        //            case "ESCAMA MARINA":
        //                if (ModeloTmp.EscamaGolfo == true) MarcadoParaExclusion = true;     break;
        //            case "PELAGICOS MENORES":
        //                if (ModeloTmp.PelagicosGolfo == true) MarcadoParaExclusion = true;    break;
        //            case "ANCHOVETA":
        //                if (ModeloTmp.AnchovetaGolfo == true) MarcadoParaExclusion = true;    break;
        //            case "CANGREJO":
        //                if (ModeloTmp.CangrejoGolfo == true) MarcadoParaExclusion = true;    break;
        //            case "JAIBA":
        //                if (ModeloTmp.JaibaGolfo == true) MarcadoParaExclusion = true;    break;
        //            case "SARDINA":
        //                if (ModeloTmp.SardinaGolfo == true) MarcadoParaExclusion = true;    break;
        //            case "CARNADA VIVA":
        //                if (ModeloTmp.TunidosGolfo == true) MarcadoParaExclusion = true;    break;
        //            case "FOMENTO":
        //                if (ModeloTmp.FomentoGolfo == true) MarcadoParaExclusion = true; break;
        //            //PermisosPangas
        //            case "TIBURON MENORES":
        //                if (ModeloTmp.TiburonMGolfo == true) MarcadoParaExclusion = true; break;
        //            case "JAIBA MENORES":
        //                if (ModeloTmp.JaibaMGolfo == true) MarcadoParaExclusion = true; break;
        //            case "ESCAMA MARINA MENORES":
        //                if (ModeloTmp.EscamaMMGolfo == true) MarcadoParaExclusion = true; break;
        //            case "CAMARON DE ALTAMAR MENORES":
        //                if (ModeloTmp.CamaronAMMGolfo == true) MarcadoParaExclusion = true; break;
        //            default:
        //                // You can use the default case.
        //                break;
        //        }
        //    }
        //    if (litoral.ToUpper() == "PACIFICO")
        //    {
        //        switch (permiso.ToUpper())
        //        {
        //            case "TUNIDOS":
        //                if (ModeloTmp.TunidosPacifico == true) MarcadoParaExclusion = true;     break;
        //            case "CAMARON DE ALTAMAR":
        //                if (ModeloTmp.CamaronPacifico == true) MarcadoParaExclusion = true;     break;
        //            case "CALAMAR":
        //                if (ModeloTmp.CalamarPacifico == true) MarcadoParaExclusion = true;     break;
        //            case "LANGOSTA":
        //                if (ModeloTmp.LangostaPacifico == true) MarcadoParaExclusion = true;    break;
        //            case "PULPO":
        //                if (ModeloTmp.PulpoPacifico == true) MarcadoParaExclusion = true;       break;
        //            case "TIBURON":
        //                if (ModeloTmp.TiburonPacifico == true) MarcadoParaExclusion = true;     break;
        //            case "PEZ ESPADA":
        //                if (ModeloTmp.PezEspadaPacifico == true) MarcadoParaExclusion = true;     break;
        //            case "ESCAMA MARINA":
        //                if (ModeloTmp.EscamaPacifico == true) MarcadoParaExclusion = true;      break;
        //            case "PELAGICOS MENORES":
        //                if (ModeloTmp.PelagicosPacifico == true) MarcadoParaExclusion = true;     break;
        //            case "ANCHOVETA":
        //                if (ModeloTmp.AnchovetaPacifico == true) MarcadoParaExclusion = true;     break;
        //            case "CANGREJO":
        //                if (ModeloTmp.CangrejoPacifico == true) MarcadoParaExclusion = true;     break;
        //            case "JAIBA":
        //                if (ModeloTmp.JaibaPacifico == true) MarcadoParaExclusion = true;     break;
        //            case "SARDINA":
        //                if (ModeloTmp.SardinaPacifico == true) MarcadoParaExclusion = true;     break;
        //            case "CARNADA VIVA":
        //                if (ModeloTmp.TunidosPacifico == true) MarcadoParaExclusion = true;     break;
        //            case "FOMENTO":
        //                if (ModeloTmp.FomentoPacifico == true) MarcadoParaExclusion = true;     break;
        //            //PermisosPangas
        //            case "TIBURON MENORES":
        //                if (ModeloTmp.TiburonMPacifico == true) MarcadoParaExclusion = true; break;
        //            case "JAIBA MENORES":
        //                if (ModeloTmp.JaibaMPacifico == true) MarcadoParaExclusion = true; break;
        //            case "ESCAMA MARINA MENORES":
        //                if (ModeloTmp.EscamaMMPacifico == true) MarcadoParaExclusion = true; break;
        //            case "CAMARON DE ALTAMAR MENORES":
        //                if (ModeloTmp.CamaronAMMPacifico == true) MarcadoParaExclusion = true; break;
        //            default:
        //                // You can use the default case.
        //                break;
        //        }
        //    }

        //    return MarcadoParaExclusion;

        //}

        //public Boolean enVeda(string permiso, string litoral, ReporteSituacionFlotaPesquera ModeloTmp)
        //{
        //    Boolean ENVEDA = false;
        //    if (litoral.ToUpper() == "GOLFO")
        //    {
        //        switch (permiso.ToUpper())
        //        {
        //            case "TUNIDOS":
        //                if (ModeloTmp.TunidosGolfo == true) ENVEDA = true;
        //                break;
        //            case "CAMARON DE ALTAMAR":
        //                if (ModeloTmp.CamaronGolfo == true) ENVEDA = true;
        //                break;
        //            case "CALAMAR":
        //                if (ModeloTmp.CalamarGolfo == true) ENVEDA = true;
        //                break;
        //            case "LANGOSTA":
        //                if (ModeloTmp.LangostaGolfo == true) ENVEDA = true;
        //                break;
        //            case "PULPO":
        //                if (ModeloTmp.PulpoGolfo == true) ENVEDA = true;
        //                break;
        //            case "TIBURON"  :
        //                if (ModeloTmp.TiburonGolfo == true) ENVEDA = true;
        //                break;
        //            case "PEZ ESPADA":
        //                if (ModeloTmp.TiburonGolfo == true) ENVEDA = true;
        //                break;
        //            case "ESCAMA MARINA":
        //                if (ModeloTmp.EscamaGolfo == true) ENVEDA = true;
        //                break;
        //            case "PELAGICOS MENORES":
        //                if (ModeloTmp.TunidosGolfo == true) ENVEDA = true;
        //                break;
        //            case "ANCHOVETA":
        //                if (ModeloTmp.TunidosGolfo == true) ENVEDA = true;
        //                break;
        //            case "CANGREJO":
        //                if (ModeloTmp.TunidosGolfo == true) ENVEDA = true;
        //                break;
        //            case "JAIBA":
        //                if (ModeloTmp.TunidosGolfo == true) ENVEDA = true;
        //                break;
        //            case "SARDINA":
        //                if (ModeloTmp.TunidosGolfo == true) ENVEDA = true;
        //                break;
        //            case "CARNADA VIVA":
        //                if (ModeloTmp.TunidosGolfo == true) ENVEDA = true;
        //                break;                    
        //            default:
        //                // You can use the default case.
        //                break;
        //        }
        //    }
        //    if (litoral.ToUpper() == "PACIFICO")
        //    {
        //        switch (permiso.ToUpper())
        //        {
        //            case "TUNIDOS":
        //                if (ModeloTmp.TunidosPacifico == true) ENVEDA = true;
        //                break;
        //            case "CAMARON DE ALTAMAR":
        //                if (ModeloTmp.CamaronPacifico == true) ENVEDA = true;
        //                break;
        //            case "CALAMAR":
        //                if (ModeloTmp.CalamarPacifico == true) ENVEDA = true;
        //                break;
        //            case "LANGOSTA":
        //                if (ModeloTmp.LangostaPacifico == true) ENVEDA = true;
        //                break;
        //            case "PULPO":
        //                if (ModeloTmp.PulpoPacifico == true) ENVEDA = true;
        //                break;
        //            case "TIBURON":
        //                if (ModeloTmp.TiburonPacifico == true) ENVEDA = true;
        //                break;
        //            case "PEZ ESPADA":
        //                if (ModeloTmp.TiburonPacifico == true) ENVEDA = true;
        //                break;
        //            case "ESCAMA MARINA":
        //                if (ModeloTmp.EscamaPacifico == true) ENVEDA = true;
        //                break;
        //            case "PELAGICOS MENORES":
        //                if (ModeloTmp.TunidosPacifico == true) ENVEDA = true;
        //                break;
        //            case "ANCHOVETA":
        //                if (ModeloTmp.TunidosPacifico == true) ENVEDA = true;
        //                break;
        //            case "CANGREJO":
        //                if (ModeloTmp.TunidosPacifico == true) ENVEDA = true;
        //                break;
        //            case "JAIBA":
        //                if (ModeloTmp.TunidosPacifico == true) ENVEDA = true;
        //                break;
        //            case "SARDINA":
        //                if (ModeloTmp.TunidosPacifico == true) ENVEDA = true;
        //                break;
        //            case "CARNADA VIVA":
        //                if (ModeloTmp.TunidosPacifico == true) ENVEDA = true;
        //                break;
        //            default:
        //                // You can use the default case.
        //                break;
        //        }
        //    }

        //    return ENVEDA;

        //}



        ////VZ1611_RZPP agregar ini 
        ////ReportePescaZonaProhibida

        //public List<SPMasReportePescaZonaProhibida_Result> EncuentraRepAlertaPescaZonaProhibida(RepAlertaPescaZonaProhibidaModel ModeloTemporal)
        ////public List<ReportePescaZonaProhibidaList> EncuentraRepAlertaPescaZonaProhibida(RepAlertaPescaZonaProhibidaModel ModeloTemporal)
        //{
        //    //string FechaFormateadaIni = ModeloTemporal.fechaIni.ToString("d", CultureInfo.CreateSpecificCulture("en-US"));
        //    //string FechaFormateadaFin = ModeloTemporal.fechaFin.ToString("d", CultureInfo.CreateSpecificCulture("en-US"));

        //    string FechaFormateadaIni = ModeloTemporal.fechaIni.Year.ToString() + '-' + ModeloTemporal.fechaIni.Month.ToString().PadLeft(2, '0') + '-' + ModeloTemporal.fechaIni.Day.ToString().PadLeft(2, '0') + " 00:00:00";
        //    string FechaFormateadaFin = ModeloTemporal.fechaFin.Year.ToString() + '-' + ModeloTemporal.fechaFin.Month.ToString().PadLeft(2, '0') + '-' + ModeloTemporal.fechaFin.Day.ToString().PadLeft(2, '0') + " 23:59:59";

        //    if (ModeloTemporal.tipoAlerta != null)
        //    {
        //        ModeloTemporal.tipoAlerta = ModeloTemporal.tipoAlerta.Replace("'", "");
        //    }
        //    else
        //    {
        //        ModeloTemporal.tipoAlerta = "";
        //    }

        //    if (ModeloTemporal.usuario != null)
        //    {
        //        ModeloTemporal.usuario = ModeloTemporal.usuario.Replace("'", "");
        //    }
        //    else
        //    {
        //        ModeloTemporal.usuario = "";
        //    }
        //    // Se cambió el 19/10/2017 actualización de entity framework 5
        //    //db.CommandTimeout = 2000;
        //    ((IObjectContextAdapter)this.db).ObjectContext.CommandTimeout = 2000;
        //    return db.SPMasReportePescaZonaProhibida(FechaFormateadaIni, FechaFormateadaFin, ModeloTemporal.tipoAlerta, ModeloTemporal.usuario).ToList();

        //}
        ////VZ1611_RZPP agregar fin
        #endregion

        //public List<DistanciasList> ObtieneListaDistancias(ReporteDistanciaRecorridaM ModeloTmp, string UserName_)
        //{
        //    String fechaInicial = ModeloTmp.FechaInicio.ToString("yyyy-MM-dd HH:mm:ss");
        //    String fechaFinal = ModeloTmp.FechaFin.ToString("yyyy-MM-dd HH:mm:ss");
        //    List<DistanciasList> ResultadoListaDistancias = new List<DistanciasList>();
        //    ObtieneUserId_Result Usuario = db.ObtieneUserId(UserName_).FirstOrDefault();
        //    ObtieneVehiculoId_Result VehiculoRes = db.ObtieneVehiculoId(ModeloTmp.Vehiculo).FirstOrDefault();
        //    if (VehiculoRes != null)
        //    {


        //    string IdVehiculo = VehiculoRes.IdVehiculo.ToString();
        //    // Se cambió el 19/10/2017 actualización de entity framework 5
        //    //db.CommandTimeout = 60;
        //    ((IObjectContextAdapter)this.db).ObjectContext.CommandTimeout = 60;
        //    List<ReporteDistanciaRecorrida_Result> Resultado = db.ReporteDistanciaRecorrida(IdVehiculo, fechaInicial, fechaFinal, (Guid)Usuario.UserId).ToList();
        //    var ListaReporte = (from a in Resultado
        //                        select new
        //                        {
        //                            a.Embarcacion,
        //                            a.RowId,
        //                            a.LatitudFormato,
        //                            a.LongitudFormato,
        //                            a.FechaRecepcionUnitracFormato,
        //                            a.FechaRecepcionUnitrac,
        //                            a.VelocidadFormato,
        //                            Rumbo = a.Rumbo.ToString() + (string.IsNullOrEmpty(a.Rumbo.ToString()) ? String.Empty : "°"),
        //                            a.ZonasDescripcion,
        //                            Distancia = (a.Distancia > 0) ? Math.Round((Double)a.Distancia, 3).ToString() + " " + "Millas Nauticas" : string.Empty,
        //                            EstatusEnZona = (a.EstatusEnZona == true ? "Si" : "No"),
        //                            a.IdVehiculo,
        //                            //a.NombreIcono
        //                            a.RNP,
        //                            a.Matricula,
        //                            a.IdMensajeM,
        //                            a.IdMensajeUnitrac
        //                        }).OrderBy(c => c.FechaRecepcionUnitrac); 
        //    Double DistAcumulada = 0.0;
        //    Double DistanciaTotal = 0.0;
        //    foreach (var Res in ListaReporte)
        //    {
        //        DistanciasList Reg = new DistanciasList();
        //        Reg.RowId = Res.RowId;
        //        Reg.IdVehiculo = Guid.Parse(Res.IdVehiculo.ToString());
        //        Reg.RNP = Res.RNP;
        //        Reg.Embarcacion = Res.Embarcacion;
        //        Reg.Matricula = Res.Matricula;
        //        Reg.IdMensajeM = Guid.Parse(Res.IdMensajeM.ToString());
        //        Reg.IdMensajeUnitrac = Res.IdMensajeUnitrac;
        //        //Latitud
        //        //Longitud 
        //        Reg.LatitudFormato = Res.LatitudFormato;
        //        Reg.LongitudFormato = Res.LongitudFormato;
        //        //FechaRecepcionUnitrac 
        //        Reg.FechaRecepcionUnitracFormato = Res.FechaRecepcionUnitracFormato;
        //        //Velocidad 
        //        Reg.VelocidadFormato = Res.VelocidadFormato;
        //        Reg.Rumbo = Res.Rumbo.ToString();
        //        //IdZona 
        //        Reg.ZonasDescripcion = Res.ZonasDescripcion;
        //        Reg.Distancia = Res.Distancia;

        //        Double Dist = 0.0;
        //        if (Reg.Distancia != "")
        //        {
        //            Double.TryParse(Reg.Distancia.Replace("Millas Nauticas", ""), out Dist);
        //        }
        //        DistAcumulada += Dist;
        //        Reg.DistanciaCalculada = DistAcumulada.ToString() + " Millas Nauticas";

        //        Reg.EstatusEnZona = Res.EstatusEnZona;
        //        //EnZona 


        //        ResultadoListaDistancias.Add(Reg);
        //    }
        //    DistanciaTotal = DistAcumulada;
        //    }
        //    return ResultadoListaDistancias;
        //}

        //public List<RumbosList> ObtieneListaDeRumbos(ReporteRumbosM ModeloTmp, string UserName_)
        //{
        //    String fechaInicial = ModeloTmp.FechaInicio.ToString("yyyy-MM-dd HH:mm:ss");					
        //    String fechaFinal = ModeloTmp.FechaFin.ToString("yyyy-MM-dd HH:mm:ss");
        //    List<RumbosList> ResultadoListaRumbos = new List<RumbosList>();
        //    ObtieneUserId_Result Usuario = db.ObtieneUserId(UserName_).FirstOrDefault();
        //    string RNP = "";

        //    string[] Spl = ModeloTmp.Vehiculo.Split('/');              
        //    if (Spl.Length == 3)
        //    {
        //        RNP = Spl[2].Trim();
        //    }
        //    ObtieneVehiculoId_Result VehiculoRes = db.ObtieneVehiculoId(RNP).FirstOrDefault();
        //    //ObtieneVehiculoId_Result VehiculoRes = db.ObtieneVehiculoId(ModeloTmp.Vehiculo).FirstOrDefault();
        //    if (VehiculoRes != null)
        //    {
        //        string IdVehiculo = VehiculoRes.IdVehiculo.ToString();
        //        //Guid Usuarioooo =  Usuario.UserId.ToString()) ;
        //        //Guid.TryParse(Usuario.ToString(), out Usuarioooo);
        //        //List<ReporteEmbarcacionRuta_Result> Resultado = db.ReporteEmbarcacionRuta(ModeloTmp.Vehiculo, fechaInicial, fechaFinal, ModeloTmp.Distancia, Usuarioooo).ToList();
        //        // Se cambió el 19/10/2017 actualización de entity framework 5
        //        //db.CommandTimeout = TiempoCommandTimeout;
        //        ((IObjectContextAdapter)this.db).ObjectContext.CommandTimeout = 300;
        //        List<ReporteEmbarcacionRuta_Result> Resultado = db.ReporteEmbarcacionRuta(IdVehiculo, fechaInicial, fechaFinal, ModeloTmp.Distancia, (Guid)Usuario.UserId).ToList();
        //        var ListaReporte = (from a in Resultado
        //                            select new
        //                            {
        //                                a.Embarcacion,
        //                                a.RowId,
        //                                a.LatitudFormato,
        //                                a.LongitudFormato,
        //                                a.FechaRecepcionUnitracFormato,
        //                                a.FechaRecepcionUnitrac,
        //                                a.VelocidadFormato,
        //                                Rumbo = a.Rumbo.ToString() + (string.IsNullOrEmpty(a.Rumbo.ToString()) ? String.Empty : "°"),
        //                                a.ZonasDescripcion,
        //                                Distancia = (a.Distancia > 0) ? Math.Round((Double)a.Distancia, 3).ToString() + " " + "Millas Nauticas" : string.Empty,
        //                                EstatusEnZona = (a.EstatusEnZona == true ? "Si" : "No"),
        //                                a.IdVehiculo,
        //                                //a.NombreIcono
        //                                a.RNP,
        //                                a.Matricula,
        //                                a.IdMensajeM,
        //                                a.IdMensajeUnitrac
        //                            }).OrderBy(c => c.FechaRecepcionUnitrac);

        //        //MODIFICACION , EN LUGAR DE INSERTAR REGISTRO VACIO POR DIA DE NO TRANSMISION AHORA SERA POR HORA O DEPENDIENDO DEL PARAMETRO ESPECIFICADO   ParametroMinutos
        //        //FuncionesProcedimientos FunProc = new FuncionesProcedimientos();
        //        //ViewBag.opcionesbusqueda = new SelectList(LlenarResultadoGenerico("opcionesbusqueda"), "Resultado", "Resultado");

        //        int ParametroMinutos = 70;
        //        BuscaResultadoGenerico_Result BuscaParametroMinutos = db.BuscaResultadoGenericoSP("parametrominutosreporterumbos").FirstOrDefault();
        //        if (BuscaParametroMinutos != null)
        //        {
        //            int.TryParse(BuscaParametroMinutos.Resultado, out ParametroMinutos);                                          
        //        }


        //        Double DistAcumulada = 0.0;
        //        Double DistanciaTotal = 0.0;

        //        string UltimaFecha = "";
        //        string NuevaFecha = "";

        //        int Orden = 0;
        //        foreach (var Res in ListaReporte)
        //        {
        //        regresa:
        //            Orden += 1;
        //            RumbosList Reg = new RumbosList();
        //            NuevaFecha = Res.FechaRecepcionUnitrac.ToString();  //NuevaFecha = Res.FechaRecepcionUnitrac.Value.ToShortDateString();  
        //            if (UltimaFecha != "")
        //            {
        //                if (UltimaFecha != NuevaFecha)
        //                {
        //                    string STOP = "para el debug";
        //                }
        //                TimeSpan ts = Convert.ToDateTime(NuevaFecha) - Convert.ToDateTime(UltimaFecha);

        //                // Difference in days.
        //                //int differenceInDays = ts.Days;
        //                //if (differenceInDays > 1)
        //                int MinutosDiferencia = Convert.ToInt32( ts.TotalMinutes);
        //                if (MinutosDiferencia > ParametroMinutos)
        //                {
        //                    DateTime FechaVacia = Convert.ToDateTime(UltimaFecha).AddMinutes(ParametroMinutos);
        //                    RumbosList RegVacio = new RumbosList();
        //                    #region "LLENA REGISTRO VACIO"
        //                    RegVacio.RowId = Orden;
        //                    //RegVacio.IdVehiculo = Guid.Parse(Res.IdVehiculo.ToString());
        //                    RegVacio.RNP = "";
        //                    RegVacio.Embarcacion = Res.Embarcacion;//"NULO";
        //                    RegVacio.Matricula = "";
        //                    //RegVacio.IdMensajeM = Guid.Parse(Res.IdMensajeM.ToString());
        //                    RegVacio.IdMensajeUnitrac = "";
        //                    RegVacio.LatitudFormato = "";
        //                    RegVacio.LongitudFormato = "";
        //                    RegVacio.FechaRecepcionUnitracFormato = FechaVacia.ToString("dd MMM yyyy HH:mm:ss", CultureInfo.CreateSpecificCulture("en-US")); //es-MX  // FechaVacia.ToString("dd MMM yyyy", CultureInfo.CreateSpecificCulture("en-US"));
        //                    RegVacio.VelocidadFormato = "SIN REPORTE";
        //                    RegVacio.Rumbo = "";
        //                    RegVacio.ZonasDescripcion = "";
        //                    #region "DISTANCIA"
        //                    RegVacio.Distancia = "";
        //                    //Double Dista = 0.0;
        //                    //if (RegVacio.Distancia != "")
        //                    //{
        //                    //    Double.TryParse(RegVacio.Distancia.Replace("Millas Nauticas", ""), out Dista);
        //                    //}
        //                    //DistAcumulada += Dista;
        //                    RegVacio.DistanciaCalculada = DistAcumulada.ToString() + " Millas Nauticas";
        //                    #endregion

        //                    RegVacio.EstatusEnZona = "NULO";
        //                    #endregion
        //                    ResultadoListaRumbos.Add(RegVacio);
        //                    //UltimaFecha = FechaVacia.ToShortDateString();
        //                    UltimaFecha = FechaVacia.ToString();
        //                    goto regresa;
        //                }
        //            }

        //            //838062
        //            UltimaFecha = NuevaFecha;



        //            Reg.RowId = Orden; //Res.RowId;
        //            Reg.IdVehiculo = Guid.Parse(Res.IdVehiculo.ToString());
        //            Reg.RNP = Res.RNP;
        //            Reg.Embarcacion = Res.Embarcacion;
        //            Reg.Matricula = Res.Matricula;
        //            Reg.IdMensajeM = Guid.Parse(Res.IdMensajeM.ToString());
        //            Reg.IdMensajeUnitrac = Res.IdMensajeUnitrac;
        //            //Latitud
        //            //Longitud 
        //            Reg.LatitudFormato = Res.LatitudFormato;
        //            Reg.LongitudFormato = Res.LongitudFormato;
        //            //FechaRecepcionUnitrac 
        //            //Reg.FechaRecepcionUnitracFormato = Res.FechaRecepcionUnitracFormato;
        //            DateTime FechaTmp = Convert.ToDateTime(Res.FechaRecepcionUnitrac);
        //            Reg.FechaRecepcionUnitracFormato = FechaTmp.ToString("dd MMM yyyy HH:mm:ss", CultureInfo.CreateSpecificCulture("en-US"));

        //            //Velocidad 
        //            Reg.VelocidadFormato = Res.VelocidadFormato;
        //            Reg.Rumbo = Res.Rumbo.ToString();
        //            //IdZona 
        //            Reg.ZonasDescripcion = Res.ZonasDescripcion;

        //            #region "DISTANCIA"
        //            Reg.Distancia = Res.Distancia;
        //            Double Dist = 0.0;
        //            if (Reg.Distancia != "")
        //            {
        //                Double.TryParse(Reg.Distancia.Replace("Millas Nauticas", ""), out Dist);
        //            }
        //            DistAcumulada += Dist;
        //            Reg.DistanciaCalculada = DistAcumulada.ToString() + " Millas Nauticas";
        //            #endregion

        //            Reg.EstatusEnZona = Res.EstatusEnZona;
        //            //EnZona 


        //            ResultadoListaRumbos.Add(Reg);
        //        }
        //        DistanciaTotal = DistAcumulada;

        //    }

        //    return ResultadoListaRumbos;
        //}


        //#region "ACTIVIDADES DE OPERADORES CAPTURA MANUAL"

        //public CapturaActividadesM ObtieneDetalleActividad(int id)
        //{
        //    CapturaActividadesM ModeloTmp = new CapturaActividadesM();
        //    ActividadesCapturaManualSeleccionar_Result Resultado = db.ActividadesCapturaManualSeleccionar(id).FirstOrDefault();
        //    if (Resultado != null)
        //    {
        //        ModeloTmp.Usuario = Resultado.Ofp;
        //        ModeloTmp.Turno = Resultado.Turno;
        //        ModeloTmp.Fecha = Resultado.Fecha.Value.ToShortDateString();
        //        ModeloTmp.usuarioastrum = Resultado.UsuarioAstrum;
        //        ModeloTmp.Hora = Resultado.Fecha.Value.Hour.ToString();
        //        ModeloTmp.Minutos = Resultado.Fecha.Value.Minute.ToString();
        //        Agregado por SZ el 06 / 03 / 2017
        //        if (Resultado.Fecha.Value.Hour < 10)
        //        {
        //            ModeloTmp.Hora = "0" + Resultado.Fecha.Value.Hour.ToString();
        //        }
        //        Agregado por SZ el 06 / 03 / 2017
        //        if (Resultado.Fecha.Value.Minute < 10)
        //        {
        //            ModeloTmp.Minutos = "0" + Resultado.Fecha.Value.Minute.ToString();
        //        }
        //        ModeloTmp.Comentarios = Resultado.Descripcion;
        //    }
        //    return ModeloTmp;
        //}

        //public string InsertaActividadesCapturaManual(CapturaActividadesM ModeloTmp)
        //{
        //    DateTime Fecha;
        //    DateTime.TryParse(ModeloTmp.Fecha + ' ' + ModeloTmp.Hora + ':' + ModeloTmp.Minutos, out Fecha);

        //    Fecha = ModeloTmp.Fecha + ' ' + ModeloTmp.Hora + ':' + ModeloTmp.Minutos;

        //    string ResultadoTemporal = "";
        //    ActividadesCapturaManualInsertar_Result Result = db.ActividadesCapturaManualInsertar(ModeloTmp.Usuario, Fecha, "CAPTURA MANUAL", "CAPTURA MANUAL", ModeloTmp.Comentarios, ModeloTmp.Turno, ModeloTmp.usuarioastrum).FirstOrDefault();
        //    if (Result != null)
        //    {
        //        ResultadoTemporal = Result.Id.ToString();
        //    }
        //    return ResultadoTemporal;

        //}

        //public string ActualizaActividadesCapturaManual(CapturaActividadesM ModeloTmp, string Identificador)
        //{
        //    DateTime Fecha;
        //    DateTime.TryParse(ModeloTmp.Fecha + ' ' + ModeloTmp.Hora + ':' + ModeloTmp.Minutos, out Fecha);

        //    Fecha = ModeloTmp.Fecha + ' ' + ModeloTmp.Hora + ':' + ModeloTmp.Minutos;

        //    string ResultadoTemporal = "";
        //    ActividadesCapturaManualActualizar_Result Result = db.ActividadesCapturaManualActualizar(ModeloTmp.Usuario, Fecha, "CAPTURA MANUAL", "CAPTURA MANUAL", ModeloTmp.Comentarios, ModeloTmp.Turno, Identificador, ModeloTmp.usuarioastrum).FirstOrDefault();
        //    if (Result != null)
        //    {
        //        ResultadoTemporal = Result.Id.ToString();
        //    }
        //    return ResultadoTemporal;

        //}
        //#endregion

        #region "CARGA IMAGENES"

        //public List<CargaImagenesObtieneTipos_Result> LlenarTipoSubtipoCargaImagenes(string Tipo, string Subtipo, string Busqueda)
        //{
        //    //db = new BDMasContextEnt();  
        //    // Usando SP    SPTMasLitoralesSeleccionar
        //    List<CargaImagenesObtieneTipos_Result> listatmp = db.CargaImagenesObtieneTipos(Tipo,Subtipo,Busqueda).ToList();
        //    return listatmp;
        //}

        //public int VerificaSiExisteRNP(string Identificador)
        //{
        //    int Existe = 0;
        //    CargaImagenesExisteRNP_Result Resultado = db.CargaImagenesExisteRNP(Identificador).FirstOrDefault();
        //    if (Resultado != null)
        //        Existe = Convert.ToInt16(Resultado.Existe);

        //    return Existe;
        //}

        //public int VerificaSiExisteIdenfificador(string TipoDoc, string Identificador)
        //{
        //    int Existe = 0;
        //    CargaImagenesExiste_Result Resultado = db.CargaImagenesExiste(TipoDoc, Identificador).FirstOrDefault();
        //    if (Resultado != null)
        //        Existe = Convert.ToInt16(Resultado.Existe);

        //    return Existe;
        //}

        //public List<BuscaCargaImagenes_Result> ObtieneListaCargaImagenes(Boolean UsaFechas, DateTime FechaInicial, DateTime FechaFinal, string RNP, string tipo,string subtipo1,string subtipo2)
        //{
        //    if (RNP != null) RNP = RNP.Replace("'", "");
        //    return db.BuscaCargaImagenes(UsaFechas, FechaInicial, FechaFinal, RNP,tipo,subtipo1,subtipo2).ToList();
        //}

        //public List<BuscaCargaImagenesTipo_Result> ObtieneListaCargaImagenesTipo(string Identificador, string RNP, string TipoDoc)
        //{
        //    return db.BuscaCargaImagenesTipo(Identificador, RNP, TipoDoc).ToList();
        //}

        //public int ActualizaConsecutivo(string Tipo,string Subtipo1,string Subtipo2, string Identificador, string Consecutivo, string RNP)
        //{
        //    int CantActualizada = 0;
        //    CargaImagenesActualizar_Result Result = db.CargaImagenesActualizar(Tipo,Subtipo1,Subtipo2, Identificador, Consecutivo, RNP).FirstOrDefault();
        //    if (Result != null)
        //        CantActualizada = Convert.ToInt32(Result.Actualizados);
        //    return CantActualizada;
        //}

        //public List<string> InsertaCargaImagenesRegresaRuta(string Tipo,string Subtipo1, string Subtipo2, string Identificador,DateTime Fecha)
        //{
        //    List<string> ListaRutas = new List<string>();

        //    List<CargaImagenesInsertar_Result> Resultado = db.CargaImagenesInsertar(Tipo,Subtipo1,Subtipo2, Identificador,Fecha).ToList();
        //    if (Resultado.Count > 0)
        //    {
        //        foreach (CargaImagenesInsertar_Result res in Resultado)
        //        {
        //            string Ruta = "";
        //            Ruta = res.RUTABASE.Trim();
        //            Ruta += '\\' + res.RNP.Trim();
        //            Ruta += '\\' + res.TIPO.Trim();
        //            Ruta += '\\' + res.SUBTIPO1.Trim();
        //            Ruta += '\\' + res.SUBTIPO2.Trim();
        //            //Ruta += '\\' + res.IDENTIFICADOR.Trim();
        //            if (System.IO.Directory.Exists(Ruta) == false) System.IO.Directory.CreateDirectory(Ruta);
        //            string Acronimo = "";
        //           // DateTime Fecha = DateTime.Now.Date;
        //            if (res.IDENTIFICADOR != null)
        //                Acronimo = res.IDENTIFICADOR.Trim();                    
        //            ListaRutas.Add(Ruta + '|' + Acronimo + '|' + Fecha.ToShortDateString());
        //        }
        //    }
        //    return ListaRutas;
        //    //if (Resultado != null)
        //    //{
        //    //    Ruta = Resultado.RUTABASE.Trim() ;
        //    //    //if (System.IO.Directory.Exists(Ruta) == false) System.IO.Directory.CreateDirectory(Ruta);

        //    //    Ruta +=  '\\' + Resultado.RNP.Trim();
        //    //    //if (System.IO.Directory.Exists(Ruta) == false) System.IO.Directory.CreateDirectory(Ruta);

        //    //    Ruta += '\\' + Resultado.IDENTIFICADOR.Trim();
        //    //    if (System.IO.Directory.Exists(Ruta) == false) System.IO.Directory.CreateDirectory(Ruta);

        //    //}
        //    //return Ruta;
        //}

        #endregion

        //#region "NOTICIAS MONITOREO"

        //public int InsertaNoticia(NoticiasMonitoreo ModeloTmp)
        //{
        //    int IdRes = 0;
        //    List< NoticiasMonitoreoInsertar_Result> Result = db.NoticiasMonitoreoInsertar(ModeloTmp.titulo, ModeloTmp.ruta,ModeloTmp.vineta1,ModeloTmp.vineta2,ModeloTmp.fechadelanoticia.ToString(),ModeloTmp.fecha,ModeloTmp.cuerpo).ToList();
        //    if (Result.Count > 0)
        //    {
        //        IdRes = Result[0].IdNoticia;
        //    }
        //    return IdRes;
        //}

        //#endregion

        //#region "CEDULAS"

        //public ReporteCedulaUnica_Result EncuentraReporteCedulaUnica(int id)
        //{
        //    var Resultado = db.ReporteCedulaUnica(id).FirstOrDefault();
        //    return Resultado;
        //}

        //public List<ReporteCedulasExportarBusqueda_Result> EncuentraReporteCedulasExportarBusqueda(CedulaBusquedaModel CedBusModel)
        //{
        //    //string FechaFormateadaIni = ConvierteFechaInicialFinal(CedBusModel.FechaInicial, false);
        //    //string FechaFormateadaFin = ConvierteFechaInicialFinal(CedBusModel.FechaFinal, true);


        //    string FechaFormateadaIni = CedBusModel.FechaInicial.ToString("d", CultureInfo.CreateSpecificCulture("en-US"));
        //    string FechaFormateadaFin = CedBusModel.FechaFinal.ToString("d", CultureInfo.CreateSpecificCulture("en-US"));            
        //    var Resultado = db.ReporteCedulasExportarBusqueda(FechaFormateadaIni,FechaFormateadaFin,CedBusModel.alarmas,CedBusModel.numerodecedula,CedBusModel.responsable, CedBusModel.nombredelbarco,CedBusModel.rnpdelbarco,CedBusModel.razonsocial,CedBusModel.litorales,CedBusModel.puertobase,CedBusModel.tipopermiso,CedBusModel.causas).ToList();

        //    return Resultado;
        //}

        //#endregion

        //#region "MANTENIMIENTOS"

        //public List<BuscaMantenimientosResumenNegacion_Result> EncuentraMantenimientosResumenNegacion(MantenimientoBusquedaModel ModeloTemporal)
        //{
        //    var Resultado = db.BuscaMantenimientosResumenNegacion(ModeloTemporal.PuertoBase, ModeloTemporal.RazonSocial, ModeloTemporal.Permiso).ToList();
        //    return Resultado;
        //}

        //public List<BuscaMantenimientos_Result> EncuentraMantenimientos(MantenimientoBusquedaModel ModeloTemporal)
        //{
        //    string FechaFormateadaIni = ModeloTemporal.FechaInicio.Year.ToString() + '-' + ModeloTemporal.FechaInicio.Month.ToString().PadLeft(2, '0') + '-' + ModeloTemporal.FechaInicio.Day.ToString().PadLeft(2, '0') + " 00:00:00";
        //    string FechaFormateadaFin = ModeloTemporal.FechaFin.Year.ToString() + '-' + ModeloTemporal.FechaFin.Month.ToString().PadLeft(2, '0') + '-' + ModeloTemporal.FechaFin.Day.ToString().PadLeft(2, '0') + " 23:59:59";

        //    // comentado 17 Enero 2015 MOT if(ModeloTemporal.Servicios != null) ModeloTemporal.Servicios = ModeloTemporal.Servicios.Replace("'","");
        //    if(ModeloTemporal.Embarcacion != null) ModeloTemporal.Embarcacion = ModeloTemporal.Embarcacion.Replace("'","");
        //    if(ModeloTemporal.Antena != null) ModeloTemporal.Antena = ModeloTemporal.Antena.Replace("'","");
        //    if(ModeloTemporal.PuertoBase != null) ModeloTemporal.PuertoBase = ModeloTemporal.PuertoBase.Replace("'","");
        //    if(ModeloTemporal.RazonSocial != null) ModeloTemporal.RazonSocial = ModeloTemporal.RazonSocial.Replace("'","");
        //    if(ModeloTemporal.Permiso != null) ModeloTemporal.Permiso = ModeloTemporal.Permiso.Replace("'","");
        //    if(ModeloTemporal.Zona != null) ModeloTemporal.Zona = ModeloTemporal.Zona.Replace("'","");
        //    if(ModeloTemporal.Muelle != null) ModeloTemporal.Muelle = ModeloTemporal.Muelle.Replace("'","");
        //    if(ModeloTemporal.Localidad != null) ModeloTemporal.Localidad = ModeloTemporal.Localidad.Replace("'","");
        //    if(ModeloTemporal.Supervisor != null) ModeloTemporal.Supervisor = ModeloTemporal.Supervisor.Replace("'","");
        //    if(ModeloTemporal.Folio != null) ModeloTemporal.Folio = ModeloTemporal.Folio.Replace("'","");                

        //    var Resultado = db.BuscaMantenimientos(ModeloTemporal.UsaFechas, ModeloTemporal.FechaInicio, ModeloTemporal.FechaFin, ModeloTemporal.Servicios, ModeloTemporal.Embarcacion, ModeloTemporal.Antena, ModeloTemporal.PuertoBase, ModeloTemporal.RazonSocial, ModeloTemporal.Permiso, ModeloTemporal.Zona, ModeloTemporal.Muelle, ModeloTemporal.Localidad, ModeloTemporal.Supervisor, ModeloTemporal.Folio).ToList();
        //    return Resultado;
        //}

        //public List<MantenimientoResumen> EncuentraMantenimientosResumen(MantenimientoBusquedaModel ModeloTemporal)
        //{
        //    //string FechaFormateadaIni = ModeloTemporal.FechaInicio.Year.ToString() + '-' + ModeloTemporal.FechaInicio.Month.ToString().PadLeft(2, '0') + '-' + ModeloTemporal.FechaInicio.Day.ToString().PadLeft(2, '0') + " 00:00:00";
        //    //string FechaFormateadaFin = ModeloTemporal.FechaFin.Year.ToString() + '-' + ModeloTemporal.FechaFin.Month.ToString().PadLeft(2, '0') + '-' + ModeloTemporal.FechaFin.Day.ToString().PadLeft(2, '0') + " 23:59:59";

        //    //var Resultado = db.BuscaMantenimientosResumen(ModeloTemporal.ServicioSel,ModeloTemporal.PuertoBaseSel,ModeloTemporal.RazonSocialSel,ModeloTemporal.SupervisorSel,ModeloTemporal.ZonaSel,ModeloTemporal.BarcoSel,ModeloTemporal.NoBarcos,ModeloTemporal.NoServicios,ModeloTemporal.NoAmbos).ToList();
        //    List<MantenimientoResumen> ResultadoFinalLista = new List<MantenimientoResumen>();

        //    System.Data.SqlClient.SqlConnection conn = new System.Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings["conn"].ConnectionString); //conn
        //    conn.Open();

        //    System.Data.SqlClient.SqlCommand cmd = new System.Data.SqlClient.SqlCommand("SPMasBuscaMantenimientosResumen " +
        //    "'" + ModeloTemporal.ServicioSel + "', '" + ModeloTemporal.PuertoBaseSel + "', '" + ModeloTemporal.RazonSocialSel + "', '" + ModeloTemporal.SupervisorSel + "', '" + ModeloTemporal.ZonaSel +
        //    "', '" + ModeloTemporal.BarcoSel + "', '" + ModeloTemporal.NoBarcos + "', '" + ModeloTemporal.NoServicios + "', '" + ModeloTemporal.NoAmbos + "'" , conn);
        //    System.Data.SqlClient.SqlDataReader dr;

        //    dr = cmd.ExecuteReader();
        //    while (dr.Read())
        //    {
        //        MantenimientoResumen ResultadoFinalRenglon = new MantenimientoResumen();

        //        //ResultadoFinalRenglon.TipoSalida = "";
        //        //ResultadoFinalRenglon.Minutario = "";
        //        //ResultadoFinalRenglon.Barco = "";
        //        //ResultadoFinalRenglon.RazonSocial = "";
        //        //ResultadoFinalRenglon.Estatus = "";
        //        //ResultadoFinalRenglon.Mes = "";

        //        //ResultadoFinalRenglon.Total = Convert.ToInt32(dr["Total"].ToString());
        //        if (ValidarColumna(dr, "Servicio") == true)
        //            ResultadoFinalRenglon.Servicio = dr["Servicio"].ToString();
        //        if (ValidarColumna(dr, "Localidad") == true)
        //            ResultadoFinalRenglon.Localidad = dr["Localidad"].ToString();
        //        if (ValidarColumna(dr, "RazonSocial") == true)
        //            ResultadoFinalRenglon.RazonSocial = dr["RazonSocial"].ToString();
        //        if (ValidarColumna(dr, "Supervisor") == true)
        //            ResultadoFinalRenglon.Supervisor = dr["Supervisor"].ToString();
        //        if (ValidarColumna(dr, "Zona") == true)
        //            ResultadoFinalRenglon.Zona = dr["Zona"].ToString();
        //        if (ValidarColumna(dr, "NombreBarco") == true)
        //            ResultadoFinalRenglon.NombreBarco = dr["NombreBarco"].ToString();
        //        if (ValidarColumna(dr, "NoBarcos") == true)
        //            ResultadoFinalRenglon.NoBarcos = Convert.ToInt32( dr["NoBarcos"].ToString());
        //        if (ValidarColumna(dr, "NoServicios") == true)
        //            ResultadoFinalRenglon.NoServicios = Convert.ToInt32(dr["NoServicios"].ToString());

        //        ResultadoFinalLista.Add(ResultadoFinalRenglon);
        //    }
        //    dr.Close();
        //    if (conn.State == System.Data.ConnectionState.Open)
        //        conn.Close();



        //    //}
        //    //catch (Exception ex)
        //    //{              
        //    //    throw ex;
        //    //}






        //    return ResultadoFinalLista;


        //    //return Resultado;
        //}

        //public MantenimientoBusquedaModel EncuentraMantenimientosTotalBarcosFolio(MantenimientoBusquedaModel ModeloTemporal)
        //{
        //    ModeloTemporal.TotalBarcos = 0;
        //    ModeloTemporal.TotalFolios = 0;

        //    System.Data.SqlClient.SqlConnection conn = new System.Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings["conn"].ConnectionString);
        //    conn.Open();

        //    System.Data.SqlClient.SqlCommand cmd = new System.Data.SqlClient.SqlCommand("SPMasBuscaMantenimientosTotalBarcosFolios ", conn);
        //    System.Data.SqlClient.SqlDataReader dr;

        //    dr = cmd.ExecuteReader();
        //    while (dr.Read())
        //    {
        //        if(dr["DESCRIPCION"].ToString() == "TOTALBARCOS")
        //            ModeloTemporal.TotalBarcos =Convert.ToInt32( dr["CANTIDAD"].ToString());
        //        if (dr["DESCRIPCION"].ToString() == "TOTALFOLIOS")
        //            ModeloTemporal.TotalFolios = Convert.ToInt32(dr["CANTIDAD"].ToString());

        //    }
        //    dr.Close();
        //    if (conn.State == System.Data.ConnectionState.Open)
        //        conn.Close();


        //    //List<BuscaMantenimientosTotalBarcosFolios_Result> ResultadoTotal = db.BuscaMantenimientosTotalBarcosFolios().ToList();
        //    //if (ResultadoTotal != null)
        //    //{
        //    //    foreach (BuscaMantenimientosTotalBarcosFolios_Result Res in ResultadoTotal)
        //    //    {
        //    //        if (Res.DESCRIPCION == "TOTALBARCOS")
        //    //            ModeloTemporal.TotalBarcos = Convert.ToInt32(Res.CANTIDAD);
        //    //        if (Res.DESCRIPCION == "TOTALFOLIOS")
        //    //            ModeloTemporal.TotalFolios = Convert.ToInt32(Res.CANTIDAD);
        //    //    }
        //    //}
        //    return ModeloTemporal;
        //}
        //#endregion

        //#region "OFICIOS DE SALIDA"


        //#region "MODULO BUSQUEDA"
        //public List<BuscaOficioSalida_Result> EncuentraOficioSalidaSP(OficioSalidaModelBusqueda ModeloTemporal)
        //    {                
        //        //string FechaFormateadaIni = FechaInicial.ToString("d", CultureInfo.CreateSpecificCulture("en-US"));

        //        //string FechaFormateadaIniDGIV = ModeloTemporal.FechaInicioDGIV.Year.ToString() + '-' + ModeloTemporal.FechaInicioDGIV.Month.ToString().PadLeft(2, '0') + '-' + ModeloTemporal.FechaInicioDGIV.Day.ToString().PadLeft(2, '0') + " 00:00:00";
        //        //string FechaFormateadaFinDGIV = ModeloTemporal.FechaFinDGIV.Year.ToString() + '-' + ModeloTemporal.FechaFinDGIV.Month.ToString().PadLeft(2, '0') + '-' + ModeloTemporal.FechaFinDGIV.Day.ToString().PadLeft(2, '0') + " 23:59:59";
        //        string FechaFormateadaIni = ModeloTemporal.FechaInicio.Year.ToString() + '-' + ModeloTemporal.FechaInicio.Month.ToString().PadLeft(2, '0') + '-' + ModeloTemporal.FechaInicio.Day.ToString().PadLeft(2, '0') + " 00:00:00";
        //        string FechaFormateadaFin = ModeloTemporal.FechaFin.Year.ToString() + '-' + ModeloTemporal.FechaFin.Month.ToString().PadLeft(2, '0') + '-' + ModeloTemporal.FechaFin.Day.ToString().PadLeft(2, '0') + " 23:59:59";

        //    if(ModeloTemporal.NoOficio != null) ModeloTemporal.NoOficio = ModeloTemporal.NoOficio.Replace("'","");
        //    if(ModeloTemporal.Tipo != null) ModeloTemporal.Tipo = ModeloTemporal.Tipo.Replace("'","");
        //    if(ModeloTemporal.DirigidoA != null) ModeloTemporal.DirigidoA = ModeloTemporal.DirigidoA.Replace("'","");
        //    if(ModeloTemporal.Contestacion != null) ModeloTemporal.Contestacion = ModeloTemporal.Contestacion.Replace("'","");
        //    if(ModeloTemporal.Minutario != null) ModeloTemporal.Minutario = ModeloTemporal.Minutario.Replace("'","");
        //    if(ModeloTemporal.estatussalida != null) ModeloTemporal.estatussalida = ModeloTemporal.estatussalida.Replace("'","");
        //    if(ModeloTemporal.EnviadoPor != null) ModeloTemporal.EnviadoPor = ModeloTemporal.EnviadoPor.Replace("'","");
        //    if(ModeloTemporal.Barco != null) ModeloTemporal.Barco = ModeloTemporal.Barco.Replace("'","");            

        //        var Resultado =  db.BuscaOficioSalida(FechaFormateadaIni, FechaFormateadaFin, ModeloTemporal.NoMostrarBarcos, ModeloTemporal.NoOficio, ModeloTemporal.Tipo, ModeloTemporal.DirigidoA, ModeloTemporal.Contestacion, ModeloTemporal.Minutario, ModeloTemporal.Estatus, ModeloTemporal.EnviadoPor, ModeloTemporal.Barco).ToList();
        //        return Resultado;
        //    //return db.BuscaOficioSalida(FechaFormateadaIni, FechaFormateadaFin, ModeloTemporal.NoMostrarBarcos, ModeloTemporal.NoOficio, ModeloTemporal.Tipo, ModeloTemporal.DirigidoA, ModeloTemporal.Contestacion, ModeloTemporal.Minutario, ModeloTemporal.Estatus, ModeloTemporal.EnviadoPor, ModeloTemporal.Barco).ToList();

        //    }
        //public List<OficioSalidaResumen> EncuentraOficioSalidaResumen(OficioSalidaModelBusqueda ModeloTemporal)
        //{
        //    string FechaFormateadaIni = ModeloTemporal.FechaInicio.Year.ToString() + '-' + ModeloTemporal.FechaInicio.Month.ToString().PadLeft(2, '0') + '-' + ModeloTemporal.FechaInicio.Day.ToString().PadLeft(2, '0') + " 00:00:00";
        //    string FechaFormateadaFin = ModeloTemporal.FechaFin.Year.ToString() + '-' + ModeloTemporal.FechaFin.Month.ToString().PadLeft(2, '0') + '-' + ModeloTemporal.FechaFin.Day.ToString().PadLeft(2, '0') + " 23:59:59";

        //    //if (ModeloTemporal.MinutarioSel == false && ModeloTemporal.BarcoSel == false && ModeloTemporal.RazonSocialSel == false && ModeloTemporal.EstatusSel == false &&  ModeloTemporal.MesSel == false)
        //    //{
        //    //    //Muestra Solo Tipo y Total

        //    //}
        //    //if (ModeloTemporal.MinutarioSel == false && ModeloTemporal.BarcoSel == false && ModeloTemporal.RazonSocialSel == false && ModeloTemporal.EstatusSel == false && ModeloTemporal.MesSel == false &&  ModeloTemporal.TipoSalidaSel == true)
        //    //{
        //    //    //Muestra Solo Tipo y Total

        //    //}
        //    //if (ModeloTemporal.MinutarioSel == false && ModeloTemporal.BarcoSel == false && ModeloTemporal.RazonSocialSel == false && ModeloTemporal.EstatusSel == false && ModeloTemporal.MesSel == false &&  ModeloTemporal.TipoSalidaSel == false)
        //    //{
        //    //    //Muestra Solo Tipo y Total

        //    //}
        //    List<OficioSalidaResumen> ResultadoFinalLista = new List<OficioSalidaResumen>();

        //    //var resultado = db.BuscaOficioSalidaResumenTest(FechaFormateadaIni, FechaFormateadaFin, ModeloTemporal.NoMostrarBarcos, ModeloTemporal.NoOficio, ModeloTemporal.Tipo, ModeloTemporal.DirigidoA, ModeloTemporal.Contestacion, ModeloTemporal.Minutario, ModeloTemporal.Estatus, ModeloTemporal.EnviadoPor, ModeloTemporal.Barco,ModeloTemporal.MinutarioSel,ModeloTemporal.BarcoSel,ModeloTemporal.RazonSocialSel,ModeloTemporal.TipoSalidaSel,ModeloTemporal.EstatusSel,ModeloTemporal.MesSel).ToList();
        //    //try
        //    //{

        //        System.Data.SqlClient.SqlConnection conn = new System.Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings["conn"].ConnectionString);
        //        conn.Open();
        //        System.Data.SqlClient.SqlCommand cmd = new System.Data.SqlClient.SqlCommand("SPMasBuscaOficioSalidaResumen " +
        //        "'" + FechaFormateadaIni + "', '" + FechaFormateadaFin + "', '" + ModeloTemporal.NoMostrarBarcos + "', '" + ModeloTemporal.NoOficio + "', '" + ModeloTemporal.Tipo + 
        //        "', '" + ModeloTemporal.DirigidoA + "', '" + ModeloTemporal.Contestacion + "', '" + ModeloTemporal.Minutario + "', '" + ModeloTemporal.Estatus + "', '" + ModeloTemporal.EnviadoPor + 
        //        "', '" + ModeloTemporal.Barco + "'," + ModeloTemporal.MinutarioSel + "," + ModeloTemporal.BarcoSel + "," + ModeloTemporal.RazonSocialSel + "," + ModeloTemporal.TipoSalidaSel + "," + 
        //        ModeloTemporal.EstatusSel + "," + ModeloTemporal.MesSel, conn);
        //        System.Data.SqlClient.SqlDataReader dr;

        //        dr = cmd.ExecuteReader();
        //        while (dr.Read())
        //        {
        //            OficioSalidaResumen ResultadoFinalRenglon = new OficioSalidaResumen();

        //            //ResultadoFinalRenglon.TipoSalida = "";
        //            //ResultadoFinalRenglon.Minutario = "";
        //            //ResultadoFinalRenglon.Barco = "";
        //            //ResultadoFinalRenglon.RazonSocial = "";
        //            //ResultadoFinalRenglon.Estatus = "";
        //            //ResultadoFinalRenglon.Mes = "";

        //            ResultadoFinalRenglon.Total = Convert.ToInt32(dr["Total"].ToString());
        //            if (ValidarColumna(dr, "TipoSalida") == true)
        //                ResultadoFinalRenglon.TipoSalida = dr["TipoSalida"].ToString();
        //            if (ValidarColumna(dr, "Minutario") == true)
        //                ResultadoFinalRenglon.Minutario = dr["Minutario"].ToString();
        //            if (ValidarColumna(dr, "Barco") == true)
        //                ResultadoFinalRenglon.Barco = dr["Barco"].ToString();
        //            if (ValidarColumna(dr, "RazonSocial") == true)
        //                ResultadoFinalRenglon.RazonSocial = dr["RazonSocial"].ToString();
        //            if (ValidarColumna(dr, "Estatus") == true)
        //                ResultadoFinalRenglon.Estatus = dr["Estatus"].ToString();
        //            if (ValidarColumna(dr, "Mes") == true)
        //                ResultadoFinalRenglon.Mes = dr["Mes"].ToString();


        //            ResultadoFinalLista.Add(ResultadoFinalRenglon);
        //        }
        //        dr.Close();
        //    if( conn.State == System.Data.ConnectionState.Open)
        //        conn.Close();

        //    //}
        //    //catch (Exception ex)
        //    //{              
        //    //    throw ex;
        //    //}






        //    return ResultadoFinalLista;
        //}
        ////public List<BuscaOficioSalidaResumenTest_Result> EncuentraOficioSalidaResumen(OficioSalidaModelBusqueda ModeloTemporal)
        ////{
        ////    string FechaFormateadaIni = ModeloTemporal.FechaInicio.Year.ToString() + '-' + ModeloTemporal.FechaInicio.Month.ToString().PadLeft(2, '0') + '-' + ModeloTemporal.FechaInicio.Day.ToString().PadLeft(2, '0') + " 00:00:00";
        ////    string FechaFormateadaFin = ModeloTemporal.FechaFin.Year.ToString() + '-' + ModeloTemporal.FechaFin.Month.ToString().PadLeft(2, '0') + '-' + ModeloTemporal.FechaFin.Day.ToString().PadLeft(2, '0') + " 23:59:59";

        ////    var resultado = db.BuscaOficioSalidaResumenTest(FechaFormateadaIni, FechaFormateadaFin, ModeloTemporal.NoMostrarBarcos, ModeloTemporal.NoOficio, ModeloTemporal.Tipo, ModeloTemporal.DirigidoA, ModeloTemporal.Contestacion, ModeloTemporal.Minutario, ModeloTemporal.Estatus, ModeloTemporal.EnviadoPor, ModeloTemporal.Barco, ModeloTemporal.MinutarioSel, ModeloTemporal.BarcoSel, ModeloTemporal.RazonSocialSel, ModeloTemporal.TipoSalidaSel, ModeloTemporal.EstatusSel, ModeloTemporal.MesSel).ToList();
        ////    return resultado;
        ////}
        //#endregion

        //#region "MODULO DETALLE"
        //public OficioSalidaModel EncuentraOficioSalidaResultadoSP(int Id)
        //{
        //    OficioSalidaModel ModeloTemporal = new OficioSalidaModel();
        //    OficioSalidaSeleccionar_Result Resultado = db.OficioSalidaSeleccionar(Id).FirstOrDefault();

        //    if (Resultado != null)
        //    {
        //        ModeloTemporal.IdSalida = Resultado.IdSalida;
        //        ModeloTemporal.NoOficio = Resultado.NoOficio;
        //        ModeloTemporal.Fecha = Convert.ToDateTime(Resultado.Fecha);
        //        ModeloTemporal.Asunto = Resultado.Asunto;
        //        ModeloTemporal.Dirigido = Resultado.Dirigido;
        //        ModeloTemporal.dCargo = Resultado.dCargo;
        //        ModeloTemporal.dEmpresa = Resultado.dEmpresa;
        //        ModeloTemporal.Envia = Resultado.Envia;
        //        ModeloTemporal.eCargo = Resultado.eCargo;
        //        ModeloTemporal.eEmpresa = Resultado.eEmpresa;
        //        ModeloTemporal.Copias = Resultado.Copias;
        //        ModeloTemporal.Comentarios = Resultado.Comentarios;
        //        ModeloTemporal.TipoSalida = Resultado.TipoSalida;
        //        ModeloTemporal.Contesta = Resultado.Contesta;
        //        ModeloTemporal.Minutario = Resultado.Minutario;
        //        ModeloTemporal.Estatus = Resultado.Estatus;
        //        ModeloTemporal.TipoRel = Convert.ToInt32( Resultado.TipoRel);
        //        ModeloTemporal.ofp2 = Resultado.OFP;
        //    }

        //    return ModeloTemporal;
        //}

        //public List<RelacionSalidaSeleccionar_Result> EncuentraRelacionSalidaSP(int Id)
        //{
        //    return db.RelacionSalidaSeleccionar(Id).ToList();
        //}

        //public List<RelEntSalSeleccionar_Result> RegresaArregloStrings(int Id, int tipo)
        //{
        //    List<RelEntSalSeleccionar_Result> Resultado = db.RelEntSalSeleccionar(Id,tipo).ToList();
        //    return Resultado;
        //}

        //public List<string> RegresaArregloStringsOficiosLlamadasTmp(int Id, int tipo)
        //{
        //    List<string> ArregloTmp = new List<string>();
        //    List<RelEntSalSeleccionarTmp_Result> Resultado = db.RelEntSalSeleccionarTmp(Id, tipo).ToList();
        //    if (Resultado != null)
        //    {
        //        foreach (RelEntSalSeleccionarTmp_Result Res in Resultado)
        //        {
        //            ArregloTmp.Add(Res.NoOficio);
        //        }
        //    }
        //    return ArregloTmp;
        //}

        //#endregion

        //#region "MODULO EDITAR Y CREAR / NUEVO"

        ///////// CREAR

        //public List<RelacionSalidaSeleccionarTmp_Result> EncuentraRelacionSalidaSPTMP(int Id)
        //{
        //    return db.RelacionSalidaSeleccionarTmp(Id).ToList();
        //}

        //public void Borra_RelacionSalidaBorrarTMP(int Id_)
        //{
        //    int totalresultado = 0;
        //    List<RelacionSalidaBorrarTmp_Result> Resultado = db.RelacionSalidaBorrarTmp(Id_).ToList();
        //    if (Resultado != null)
        //    {
        //        totalresultado = Resultado.Count();   // No hago nada con el numero, solo lo dejo por si se necesita despues o bien la lista de embarcaciones
        //    }
        //}

        //public int Inserta_RelacionSalidaInsertarTMP(string UserName_, int Id_, string RNP_, string Matricula_, string Barco_, string RazonSocial_, string RepLegal_)
        //{
        //    int IdTemporal = 0;
        //    List<RelacionSalidaInsertarTMP_Result> Resultado = db.RelacionSalidaInsertarTMP(UserName_, Id_, RNP_, Matricula_, Barco_, RazonSocial_, RepLegal_).ToList();
        //    if (Resultado != null)
        //    {
        //        if (Resultado.Count > 0)
        //        {
        //            foreach (RelacionSalidaInsertarTMP_Result Res in Resultado)
        //            {
        //                IdTemporal = Res.IdSalida;
        //                break;
        //            }
        //        }
        //    }
        //    return IdTemporal;
        //}

        //public int Inserta_RelEntSalInsertarTMP(int idsalida_, int idrelacion_, string nooficio_, int tipo_)
        //{
        //    int IdTemporal = 0;
        //    List<RelEntSalInsertarTmp_Result> Resultado = db.RelEntSalInsertarTmp(idsalida_, idrelacion_, nooficio_, tipo_).ToList();
        //    if (Resultado != null)
        //    {
        //        if (Resultado.Count > 0)
        //        {
        //            foreach (RelEntSalInsertarTmp_Result Res in Resultado)
        //            {
        //                IdTemporal = Convert.ToInt32( Res.IdSalida);
        //                break;
        //            }
        //        }                
        //    }
        //    return IdTemporal;
        //}

        //public void Borra_RelEntSalBorrarTMP(int idsalida_, int idrelacion_, string nooficio_, int tipo_)
        //{
        //    int totalresultado = 0;
        //    List<RelEntSalBorrarTmp_Result> Resultado = db.RelEntSalBorrarTmp(idsalida_, idrelacion_, nooficio_, tipo_).ToList();

        //    if (Resultado != null)
        //    {
        //        totalresultado = Resultado.Count();
        //    }
        //}

        //public int Inserta_OficioSalidaInsertar(OficioSalidaModel ModeloTmp)
        //{
        //    int IdTemporal = 0;
        //    List<OficioSalidaInsertar_Result> Resultado = db.OficioSalidaInsertar(ModeloTmp.IdSalidaEmbarcacionTmp,ModeloTmp.IdSalidaOficiosLlamadasTmp, ModeloTmp.NoOficio, 
        //        ModeloTmp.Fecha, ModeloTmp.Asunto, ModeloTmp.Dirigido,ModeloTmp.dCargo, ModeloTmp.dEmpresa, ModeloTmp.Envia, ModeloTmp.eCargo, ModeloTmp.eEmpresa, 
        //        ModeloTmp.Copias, ModeloTmp.Comentarios, ModeloTmp.TipoSalida,ModeloTmp.Contesta, ModeloTmp.Minutario, ModeloTmp.Estatus, ModeloTmp.TipoRel,ModeloTmp.ofp2).ToList();
        //    if (Resultado != null)
        //    {
        //        if (Resultado.Count > 0)
        //        {
        //            foreach (OficioSalidaInsertar_Result Res in Resultado)
        //            {
        //                IdTemporal = Convert.ToInt32(Res.IdSalida);
        //                break;
        //            }
        //        } 
        //    }
        //    return IdTemporal;
        //}

        //////// AMBOS


        //public void Actualiza_OficioSalidaActualizar(OficioSalidaModel ModeloTmp)
        //{

        //    int totalresultado = 0;
        //    List<OficioSalidaActualizar_Result> Resultado = db.OficioSalidaActualizar(ModeloTmp.IdSalida, ModeloTmp.NoOficio, ModeloTmp.Fecha, ModeloTmp.Asunto, ModeloTmp.Dirigido,
        //        ModeloTmp.dCargo, ModeloTmp.dEmpresa, ModeloTmp.Envia, ModeloTmp.eCargo, ModeloTmp.eEmpresa, ModeloTmp.Copias, ModeloTmp.Comentarios, ModeloTmp.TipoSalida,
        //        ModeloTmp.Contesta, ModeloTmp.Minutario, ModeloTmp.Estatus, ModeloTmp.TipoRel,ModeloTmp.ofp2).ToList();
        //    if (Resultado != null)
        //    {
        //        totalresultado = Resultado.Count();
        //    }
        //}

        //public void Borra_RelEntSalBorrar(int idsalida_, int idrelacion_, string nooficio_, int tipo_)
        //{
        //    int totalresultado = 0;
        //    List<RelEntSalBorrar_Result> Resultado = db.RelEntSalBorrar(idsalida_, idrelacion_, nooficio_, tipo_).ToList();

        //    if (Resultado != null)
        //    {
        //        totalresultado = Resultado.Count();
        //    }
        //}

        //public void Borra_RelacionSalidaBorrar(int Id_)
        //{
        //    //int rows = db.RelacionSalidaBorrar(IdEntrada_);
        //    int totalresultado = 0;
        //    List<RelacionSalidaBorrar_Result> Resultado = db.RelacionSalidaBorrar(Id_).ToList();
        //    if (Resultado != null)
        //    {
        //        totalresultado = Resultado.Count();   // No hago nada con el numero, solo lo dejo por si se necesita despues o bien la lista de embarcaciones
        //    }
        //    string test = "";
        //}

        //public void Inserta_RelEntSalInsertar(int idsalida_, int idrelacion_,string nooficio_, int tipo_)
        //{
        //    int totalresultado = 0;
        //    List<RelEntSalInsertar_Result> Resultado = db.RelEntSalInsertar(idsalida_, idrelacion_, nooficio_, tipo_).ToList();
        //    if (Resultado != null)
        //    {
        //        totalresultado = Resultado.Count();
        //    }
        //}

        //public void Inserta_RelacionSalidaInsertar(string UserName_, int Id_, string RNP_, string Matricula_, string Barco_, string RazonSocial_, string RepLegal_)
        //{
        //    int totalresultado = 0;
        //    List<RelacionSalidaInsertar_Result> Resultado = db.RelacionSalidaInsertar(UserName_, Id_, RNP_, Matricula_, Barco_, RazonSocial_,RepLegal_).ToList();
        //    if (Resultado != null)
        //    {
        //        totalresultado = Resultado.Count();   // No hago nada con el numero, solo lo dejo por si se necesita despues o bien la lista de embarcaciones
        //    }
        //}



        //#endregion
        //#endregion

        //#region "OFICIOS DE ENTRADA"
        //#region "MODULO BUSQUEDA"
        //public List<BuscaOficioEntrada_Result> EncuentraOficioEntradaSP(OficioEntradaModelBusqueda ModeloTemporal)
        //    {
        //        //string FechaFormateadaIni = FechaInicial.ToString("d", CultureInfo.CreateSpecificCulture("en-US"));

        //        string FechaFormateadaIniDGIV = ModeloTemporal.FechaInicioDGIV.Year.ToString() + '-' + ModeloTemporal.FechaInicioDGIV.Month.ToString().PadLeft(2, '0') + '-' + ModeloTemporal.FechaInicioDGIV.Day.ToString().PadLeft(2, '0') + " 00:00:00";
        //        string FechaFormateadaFinDGIV = ModeloTemporal.FechaFinDGIV.Year.ToString() + '-' + ModeloTemporal.FechaFinDGIV.Month.ToString().PadLeft(2, '0') + '-' + ModeloTemporal.FechaFinDGIV.Day.ToString().PadLeft(2, '0') + " 23:59:59";
        //        string FechaFormateadaIniMon = ModeloTemporal.FechaInicioMon.Year.ToString() + '-' + ModeloTemporal.FechaInicioMon.Month.ToString().PadLeft(2, '0') + '-' + ModeloTemporal.FechaInicioMon.Day.ToString().PadLeft(2, '0') + " 00:00:00";
        //        string FechaFormateadaFinMon = ModeloTemporal.FechaFinMon.Year.ToString() + '-' + ModeloTemporal.FechaFinMon.Month.ToString().PadLeft(2, '0') + '-' + ModeloTemporal.FechaFinMon.Day.ToString().PadLeft(2, '0') + " 23:59:59";

        //    if(ModeloTemporal.NoOficio != null) ModeloTemporal.NoOficio = ModeloTemporal.NoOficio.Replace("'","");
        //    if(ModeloTemporal.Turno != null) ModeloTemporal.Turno = ModeloTemporal.Turno.Replace("'","");
        //    if(ModeloTemporal.Volante != null) ModeloTemporal.Volante = ModeloTemporal.Volante.Replace("'","");
        //    if(ModeloTemporal.Asunto != null) ModeloTemporal.Asunto = ModeloTemporal.Asunto.Replace("'","");
        //    if(ModeloTemporal.Minutario != null) ModeloTemporal.Minutario = ModeloTemporal.Minutario.Replace("'","");
        //    if(ModeloTemporal.Origen != null) ModeloTemporal.Origen = ModeloTemporal.Origen.Replace("'","");
        //    if (ModeloTemporal.Barco != null) ModeloTemporal.Barco = ModeloTemporal.Barco.Replace("'", "");
        //    var result = db.BuscaOficioEntrada(ModeloTemporal.NoOficio, ModeloTemporal.Estatus, ModeloTemporal.Turno, ModeloTemporal.TipoOficio, ModeloTemporal.Volante, ModeloTemporal.Asunto, ModeloTemporal.Minutario, ModeloTemporal.Origen, ModeloTemporal.FechaDGIV, FechaFormateadaIniDGIV, FechaFormateadaFinDGIV, ModeloTemporal.FechaMon, FechaFormateadaIniMon, FechaFormateadaFinMon, ModeloTemporal.Barco).ToList();
        //    return result;

        //    }

        //public void EncuentraTotalConcluidosPendientes(OficioEntradaModelBusqueda ModeloTemporal,out int total, out int concluidos, out int pendientes)
        //{
        //    total = 0;
        //    concluidos = 0;
        //    pendientes = 0;

        //    string FechaFormateadaIniDGIV = ModeloTemporal.FechaInicioDGIV.Year.ToString() + '-' + ModeloTemporal.FechaInicioDGIV.Month.ToString().PadLeft(2, '0') + '-' + ModeloTemporal.FechaInicioDGIV.Day.ToString().PadLeft(2, '0') + " 00:00:00";
        //    string FechaFormateadaFinDGIV = ModeloTemporal.FechaFinDGIV.Year.ToString() + '-' + ModeloTemporal.FechaFinDGIV.Month.ToString().PadLeft(2, '0') + '-' + ModeloTemporal.FechaFinDGIV.Day.ToString().PadLeft(2, '0') + " 23:59:59";
        //    string FechaFormateadaIniMon = ModeloTemporal.FechaInicioMon.Year.ToString() + '-' + ModeloTemporal.FechaInicioMon.Month.ToString().PadLeft(2, '0') + '-' + ModeloTemporal.FechaInicioMon.Day.ToString().PadLeft(2, '0') + " 00:00:00";
        //    string FechaFormateadaFinMon = ModeloTemporal.FechaFinMon.Year.ToString() + '-' + ModeloTemporal.FechaFinMon.Month.ToString().PadLeft(2, '0') + '-' + ModeloTemporal.FechaFinMon.Day.ToString().PadLeft(2, '0') + " 23:59:59";

        //    List< BuscaOficioEntradaTCP_Result> Resultado = db.BuscaOficioEntradaTCP(ModeloTemporal.NoOficio, ModeloTemporal.Estatus, ModeloTemporal.Turno, ModeloTemporal.TipoOficio, ModeloTemporal.Volante, ModeloTemporal.Asunto, ModeloTemporal.Minutario, ModeloTemporal.Origen, ModeloTemporal.FechaDGIV, FechaFormateadaIniDGIV, FechaFormateadaFinDGIV, ModeloTemporal.FechaMon, FechaFormateadaIniMon, FechaFormateadaFinMon, ModeloTemporal.Barco).ToList();
        //    if (Resultado != null)
        //    {
        //        foreach (BuscaOficioEntradaTCP_Result Row in Resultado)
        //        {
        //            if (Row.Tipo == "TODOS")
        //            {                        
        //                total = Convert.ToInt32( Row.Cantidad);
        //            }
        //            if (Row.Tipo == "CONCLUIDOS")
        //            {
        //                concluidos = Convert.ToInt32(Row.Cantidad);
        //            }
        //            if (Row.Tipo == "PENDIENTES")
        //            {
        //                pendientes = Convert.ToInt32(Row.Cantidad);
        //            }
        //        }
        //    }


        //}
        //#endregion
        //#region "MODULO DETALLE"
        //public List<RelacionEntradaSeleccionar_Result> EncuentraRelacionEntradaSP(int IdEntrada)
        //{
        //    return db.RelacionEntradaSeleccionar(IdEntrada).ToList();
        //}

        //#endregion

        //#region "MODULO CREAR / NUEVO"

        //public int Inserta_OficioEntradaInsertar(OficioEntradaModel Modelo)  //          int IdEntrada_, string Volante_, string NoOficio_, string TipoOficio_, DateTime FechaDGIV_, string HoraDGIV_, DateTime FechaMon_,string HoraMon_, string Asunto_, string Turno_, string Origen_, string Minutario_, string Estatus_, string Comentarios_, string Relacion_
        //{
        //    int IdActualizado = 0;
        //    OficioEntradaInsertar_Result Resultado = db.OficioEntradaInsertar(Modelo.IdEntrada, Modelo.Volante, Modelo.NoOficio, Modelo.TipoOficio, Modelo.FechaDGIV, Modelo.HoraDGIV, Modelo.FechaMon, Modelo.HoraMon, Modelo.Asunto,
        //        Modelo.Turno, Modelo.Origen, Modelo.Minutario, Modelo.Estatus, Modelo.Comentarios, "PENDIENTE",Modelo.OFP).FirstOrDefault();
        //    if (Resultado != null)
        //    {
        //        IdActualizado = Resultado.IdEntrada;
        //    }
        //    return IdActualizado;
        //}


        //public int Inserta_RelacionEntradaInsertarTMP(int IdEntrada_, string RNP_, string Matricula_, string Barco_, string RazonSocial_)
        //{
        //    int IdTemporal = 0;
        //    RelacionEntradaInsertarTMP_Result Resultado = db.RelacionEntradaInsertarTMP(IdEntrada_, RNP_, Matricula_, Barco_, RazonSocial_).FirstOrDefault();
        //   // //List<RelacionEntradaInsertar_Result> Resultado = db.RelacionEntradaInsertar(IdEntrada_, RNP_, Matricula_, Barco_, RazonSocial_).ToList();
        //    if (Resultado != null)
        //    {
        //        IdTemporal = Convert.ToInt32( Resultado.IdEntrada);
        //    }
        //    return IdTemporal;
        //}

        //public List<RelacionEntradaSeleccionarTMP_Result> EncuentraRelacionEntradaTMPSP(int IdEntrada)
        //{
        //    return db.RelacionEntradaSeleccionarTMP(IdEntrada).ToList();
        //}

        //public void Borra_RelacionEntradaBorrarTMP(int IdEntrada_)
        //{
        //    int rows = db.RelacionEntradaBorrarTMP(IdEntrada_);
        //}

        //#endregion
        //#region "MODULO EDITAR"

        //public Boolean Actualiza_OficioEntradaActualizar( OficioEntradaModel Modelo)  //          int IdEntrada_, string Volante_, string NoOficio_, string TipoOficio_, DateTime FechaDGIV_, string HoraDGIV_, DateTime FechaMon_,string HoraMon_, string Asunto_, string Turno_, string Origen_, string Minutario_, string Estatus_, string Comentarios_, string Relacion_
        //{
        //    Boolean Actualizado = false;
        //    OficioEntradaActualizar_Result Resultado = db.OficioEntradaActualizar(Modelo.IdEntrada, Modelo.Volante, Modelo.NoOficio, Modelo.TipoOficio, Modelo.FechaDGIV, Modelo.HoraDGIV, Modelo.FechaMon, Modelo.HoraMon, Modelo.Asunto,
        //        Modelo.Turno, Modelo.Origen, Modelo.Minutario, Modelo.Estatus, Modelo.Comentarios, "PENDIENTE",Modelo.OFP).FirstOrDefault();
        //    if (Resultado != null)
        //    {
        //        Actualizado = true;
        //    }
        //    return Actualizado;
        //}

        //public void Inserta_RelacionEntradaInsertar(int IdEntrada_, string RNP_,string Matricula_,string Barco_, string RazonSocial_)
        //{
        //    int totalresultado = 0;
        //    List< RelacionEntradaInsertar_Result> Resultado = db.RelacionEntradaInsertar(IdEntrada_, RNP_, Matricula_, Barco_, RazonSocial_).ToList();
        //    if (Resultado != null)
        //    {
        //       totalresultado = Resultado.Count();   // No hago nada con el numero, solo lo dejo por si se necesita despues o bien la lista de embarcaciones
        //    }
        //}
        //public void Borra_RelacionEntradaBorrar(int IdEntrada_)
        //{
        //   int rows =  db.RelacionEntradaBorrar(IdEntrada_);

        //    //int totalresultado = 0;
        //    //List<RelacionEntradaInsertar_Result> Resultado = db.RelacionEntradaInsertar(IdEntrada_, RNP_, Matricula_, Barco_, RazonSocial_).ToList();
        //    //if (Resultado != null)
        //    //{
        //    //    totalresultado = Resultado.Count();   // No hago nada con el numero, solo lo dejo por si se necesita despues o bien la lista de embarcaciones
        //    //}
        //   string test = "";
        //}


        //public OficioEntradaModel EncuentraOficioEntradaSP(int IdEntrada)
        //{
        //    OficioEntradaModel ModeloTemporal = new OficioEntradaModel();
        //    OficioEntradaSeleccionar_Result Resultado = db.OficioEntradaSeleccionar(IdEntrada).FirstOrDefault();

        //    if (Resultado != null)
        //    {
        //        ModeloTemporal.IdEntrada    =   Resultado.IdEntrada;   
        //        ModeloTemporal.Volante      =   Resultado.Volante;
        //        ModeloTemporal.NoOficio     =   Resultado.NoOficio;
        //        ModeloTemporal.TipoOficio   =   Resultado.TipoOficio; 
        //        ModeloTemporal.FechaDGIV    =   Convert.ToDateTime( Resultado.FechaDGIV);
        //        ModeloTemporal.HoraDGIV     =   Resultado.HoraDGIV;
        //        ModeloTemporal.FechaMon     =   Convert.ToDateTime( Resultado.FechaMon);
        //        ModeloTemporal.HoraMon      =   Resultado.HoraMon;
        //        ModeloTemporal.Asunto       =   Resultado.Asunto;
        //        ModeloTemporal.Turno        =   Resultado.Turno;
        //        ModeloTemporal.Origen       =   Resultado.Origen;
        //        ModeloTemporal.Minutario    =   Resultado.Minutario;
        //        ModeloTemporal.Estatus      =   Resultado.Estatus;
        //        ModeloTemporal.Comentarios  =   Resultado.Comentarios;
        //        ModeloTemporal.Relacion     =   Resultado.Relacion;
        //        ModeloTemporal.OFP          =   Resultado.OFP;

        //    }
        //    //string FechaFormateadaIni = FechaInicial.ToString("d", CultureInfo.CreateSpecificCulture("en-US"));
        //    //string FechaFormateadaIni = ModeloTemporal.fechainicial.Year.ToString() + '-' + ModeloTemporal.fechainicial.Month.ToString().PadLeft(2, '0') + '-' + ModeloTemporal.fechainicial.Day.ToString().PadLeft(2, '0') + " 00:00:00";
        //    ////string FechaFormateadaFin = FechaFinal.ToString("d", CultureInfo.CreateSpecificCulture("en-US"));
        //    //string FechaFormateadaFin = ModeloTemporal.fechafinal.Year.ToString() + '-' + ModeloTemporal.fechafinal.Month.ToString().PadLeft(2, '0') + '-' + ModeloTemporal.fechafinal.Day.ToString().PadLeft(2, '0') + " 23:59:59";
        //    //return db.BuscaKardex(FechaFormateadaIni, FechaFormateadaFin, ModeloTemporal.tramite, ModeloTemporal.barco, ModeloTemporal.rnp, ModeloTemporal.razon, ModeloTemporal.puerto).ToList();
        //    return ModeloTemporal;
        //}
        //#endregion
        //#endregion

        //#region "BUSQUEDA DE INFORMACION DE EMBARCACION"
        //public List<BuscaInformacionDeEmbarcacion_Result> EncuentraInformacionDeEmbarcacionSP(string Opcion, string Valor)
        //{
        //    if (Valor != null) Valor = Valor.Replace("'", "");
        //    return db.BuscaInformacionDeEmbarcacion(Opcion, Valor).ToList();
        //}
        //#endregion

        //#region "ANTERIORES NO SE USAN"

        //#region "COORDENADAS"
        //public string ConvertirCoordenada(string Cadena)
        //{
        //    string Resultado = Cadena;
        //    string Grados = Cadena;
        //    string Minutos = "00";
        //    string Segundos = "0.0000";


        //    string[] splitGrados = Resultado.Split('.');
        //    if (splitGrados.Count() > 0)
        //    {
        //        Grados = splitGrados[0];
        //        double Min = Convert.ToDouble("." + splitGrados[1]) *60;
        //        Minutos = Min.ToString();
        //        string[] splitMinutos = Minutos.Split('.');
        //        if (splitMinutos.Count() == 0)
        //        {
        //            Resultado = Grados + "°" + Minutos + "'" + Segundos + "''";
        //        }
        //        else
        //        {
        //            Minutos = splitMinutos[0];
        //            double Seg = Convert.ToDouble("." + splitMinutos[1]) * 60;
        //            Segundos = Seg.ToString();
        //            if (Seg < 10)

        //            {
        //                Segundos = "0" + Segundos;











        //            }
        //            Resultado = Grados + "°" + Minutos + "'" + Segundos + "''";
        //        }
        //    }
        //    else
        //    {
        //        Resultado = Grados + "°" + Minutos + "'" + Segundos + "''";
        //    }

        //    return Resultado;
        //}
        //#endregion


        //#region "LITORALES"
        //public List<LlenarLitorales_Result> LlenarLitoralesTodos()
        //{
        //    //db = new BDMasContextEnt();  
        //    // Usando SP    SPTMasLitoralesSeleccionar
        //    List<LlenarLitorales_Result> ListadeLitorales = db.LlenarLitorales().ToList();
        //    return ListadeLitorales;
        //}

        //#endregion

        //#region "ALARMAS TIPO"

        ////public List<LlenarAlarmasTipo_Result> LlenarAlarmasTipoTodos()
        ////{
        ////    List<LlenarAlarmasTipo_Result> lista =  db.LlenarAlarmasTipo().ToList();
        ////    return lista;
        ////}

        //#region "AUTOCOMPLETE"
        //public IEnumerable<string> FindRsultadoGenerico(string criteria, int limit, string busca)
        //{
        //    List<BuscaResultadoGenerico_Result> listatmp = db.BuscaResultadoGenericoSP(busca).ToList();

        //    return listatmp.
        //      Where(c => c.Resultado.Trim().ToUpper().Contains(criteria.Trim().ToUpper())).
        //      Take(15).                //limit
        //      Select(c => c.Resultado);
        //}

        //public IEnumerable<string> FindRsultadoGenerico(string busca)
        //{
        //    List<BuscaResultadoGenerico_Result> listatmp = db.BuscaResultadoGenericoSP(busca).ToList();
        //    return listatmp.Select(c => c.Resultado);
        //}

        ////public IEnumerable<string> FindAlarmas(string criteria, int limit)
        ////{
        ////    List<LlenarAlarmasTipo_Result> listatmp = db.LlenarAlarmasTipo().ToList();
        ////    return listatmp.
        ////       Where(c => c.NombreAlarma.ToLower().StartsWith(criteria.ToLower())).
        ////       Take(limit).
        ////       Select(c => c.NombreAlarma);
        ////}

        ////public IEnumerable<string> FindAlarmasLike(string criteria, int limit)
        ////{
        ////    List<LlenarAlarmasTipo_Result> listatmp = LlenarAlarmasTipoTodos();
        ////    return listatmp.
        ////       Where(c => c.NombreAlarma.ToLower().Contains(criteria.ToLower())).
        ////       Take(limit).
        ////       Select(c => c.NombreAlarma);
        ////}
        //#endregion

        //#region "Codigo comentado usado anteriormente"
        ////public List<AlarmasEntity> todos2()
        ////{
        ////    List<AlarmasEntity> lista = (from b in db.TMasAlarmasTipoes
        ////                                 select new AlarmasEntity { nombrealarma = b.NombreAlarma, idtipoalarma = b.IdTipoAlarma }).Distinct().ToList();
        ////    return lista;
        ////}

        ////public ICollection<AlarmasEntity> todos3()
        ////{
        ////    ICollection<AlarmasEntity> lista = (from b in db.TMasAlarmasTipoes
        ////                                        select new AlarmasEntity { nombrealarma = b.NombreAlarma, idtipoalarma = b.IdTipoAlarma }).Distinct().ToList();

        ////    return lista;
        ////}
        //#endregion
        //#endregion

        //#region "CAUSAS ALARMAS"
        //public List<LlenarCausasAlarmas_Result> LlenarCausasAlarmasTodos()
        //{
        //    List<LlenarCausasAlarmas_Result> lista = db.LlenarCausasAlarmas().ToList();
        //    return lista;
        //}
        //#endregion


        //#region "RESPONSABLE OFP"
        //public List<LlenarResponsableOFP_Result> LlenarResponsableOFPTodos()
        //{
        //    List<LlenarResponsableOFP_Result> lista = db.LlenarResponsableOFP().ToList();
        //    return lista;
        //}
        //#endregion

        //#region "TIPO PERMISO"
        //public List<LlenarTipoPermiso_Result> LlenarTipoPermisoTodos()
        //{
        //    List<LlenarTipoPermiso_Result> lista = db.LlenarTipoPermiso().ToList();
        //    return lista;
        //}

        //#endregion

        //#region "Nombre Barco"
        //public List<LlenarNombreBarco_Result> LlenarNombreBarcoTodos()
        //{
        //    List<LlenarNombreBarco_Result> lista = db.LlenarNombreBarco().ToList();
        //    return lista;
        //}
        //#endregion

        //#region "TURNOS"
        //public List<LlenarTurno_Result> LlenarTurnosTodos()
        //{
        //    List<LlenarTurno_Result> lista = db.LlenarTurno().ToList();
        //    return lista;
        //}
        //#endregion



        //#region "_"
        ////public List<AlarmasEntity> todos()
        ////{
        ////    List<AlarmasEntity> lista = (from b in db.TMasAlarmasTipoes
        ////                                 select new AlarmasEntity { nombrealarma = b.NombreAlarma, idtipoalarma = b.IdTipoAlarma }).Distinct().ToList();

        ////    return lista;
        ////}






        ////for (int i = 0; i < dr.FieldCount; i++)
        ////{
        ////    if(ValidarColumna(dr,"TipoSalida") == true)
        ////        ResultadoFinalRenglon.TipoSalida = dr["TipoSalida"].ToString();
        ////    if(ValidarColumna(dr,"Minutario") == true)
        ////        ResultadoFinalRenglon.Minutario = dr["Minutario"].ToString();
        ////    //dr.GetSchemaTable().Columns.Contains(field)
        ////    if(dr.GetSchemaTable().Columns.Contains("Minutario") == true)
        ////        ResultadoFinalRenglon.Minutario = dr["Minutario"].ToString();
        ////    //if (dr.GetName(i).ToString().ToUpper() == "TipoSalida")                       
        ////    //    ResultadoFinalRenglon.TipoSalida = dr["TipoSalida"].ToString();
        ////    //if (dr.GetName(i).ToString().ToUpper() == "Minutario")
        ////    //    ResultadoFinalRenglon.TipoSalida = dr["Minutario"].ToString();

        ////}
        ////if (dr.GetSchemaTable().Columns.ToString().ToUpper().Contains("TIPOSALIDA") == true)
        ////    ResultadoFinalRenglon.Minutario = dr["TipoSalida"].ToString();
        ////if (dr.GetSchemaTable().Columns.Contains("Minutario") == true)
        ////    ResultadoFinalRenglon.Minutario = dr["Minutario"].ToString();
        ////if (dr.GetSchemaTable().Columns.Contains("Barco") == true)
        ////    ResultadoFinalRenglon.Minutario = dr["Barco"].ToString();
        ////if (dr["TipoSalida"] != null)
        ////{
        ////    ResultadoFinalRenglon.TipoSalida = dr["TipoSalida"].ToString();
        ////}
        ////if (dr["Minutario"] != null)
        ////{
        ////    ResultadoFinalRenglon.Minutario = dr["Minutario"].ToString();
        ////}





        ////System.Configuration.Configuration rootWebConfig =
        ////System.Web.Configuration.WebConfigurationManager.OpenWebConfiguration("/MyWebSiteRoot");
        ////System.Configuration.ConnectionStringSettings connString;
        ////if (rootWebConfig.ConnectionStrings.ConnectionStrings.Count > 0)
        ////{
        ////    connString =
        ////        rootWebConfig.ConnectionStrings.ConnectionStrings["NorthwindConnectionString"];
        ////    if (connString != null)
        ////        Console.WriteLine("Northwind connection string = \"{0}\"",
        ////            connString.ConnectionString);
        ////    else
        ////        Console.WriteLine("No Northwind connection string");
        ////}

        ////con.ConnectionString = ConfigurationManager.ConnectionStrings["conn"].ConnectionString;





        //#endregion

        //#endregion

        //#region "CARGA IMAGENES"
        //public List<BuscaResultadoGenerico_Result> llenarTipoFormato(string busca)
        //{
        //    List<BuscaResultadoGenerico_Result> listatmp = db.BuscaResultadoGenericoSP(busca).ToList();
        //    return listatmp;
        //}

        ///// Obtiene el max id de la tabla TMasArchivosMonitoreoEmbarcPesqueras
        ///// para guardar un archivo pdf
        ///// </summary>
        ///// <returns></returns>
        //public string MaxArchivoMonitoreoEmbarcPesqueras(MonitoreoEmbarcPesqModel ModeloTmp)
        //{
        //    string nConsecutivo = "";
        //    if (ModeloTmp.Tipo == "Formatos") { nConsecutivo = "tFormatos"; }
        //    if (ModeloTmp.Tipo == "Planes y Manuales") { nConsecutivo = "tPlanes"; }
        //    if (ModeloTmp.Tipo == "Avisos y Circulares") { nConsecutivo = "tAvisos"; }

        //    var res = (from x in db.TMasArchivosMonitoreoEmbarcPesqueras where x.nArchivo.Contains(nConsecutivo) select x.nArchivo).Max();
        //    return res;
        //}

        //public int InsertMonitoreoEmbarcPesqueras(string descripcion, string nConsecutivo, string fenvio, string userid, string tipo)
        //{
        //    int ret = 0;
        //    try
        //    {
        //        List<ArchivosMonitoreoEmbarcPesqueras_Result> _lArchivos = new List<ArchivosMonitoreoEmbarcPesqueras_Result>();
        //        _lArchivos = db.ArchivosMonitoreoEmbarcPesqueras(descripcion, nConsecutivo, fenvio, userid, tipo).ToList();
        //        if (_lArchivos.ToString().Length > 0)
        //        {
        //            ret = 1;
        //        }
        //        else
        //        {
        //            ret = 0;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        ret = 0;
        //    }

        //    return ret;
        //}

        //#endregion
        //#region "CREACION DE LOG"


        //#endregion
        //#region Carga Imagenes Varias
        //////SPMasInsertarCargaImagenesSolWeb
        //public CargaImagenesVariasSolWeb InsertarCargaImagenesSolWeb(string Identificador, string Consecutivo)
        //{
        //    CargaImagenesVariasSolWeb Nuevo = new CargaImagenesVariasSolWeb();
        //    using (System.Data.SqlClient.SqlConnection connection = new System.Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings["conn"].ConnectionString))
        //    {
        //        connection.Open();
        //        using (System.Data.SqlClient.SqlCommand cmd = new System.Data.SqlClient.SqlCommand("SPMasInsertarCargaImagenesSolWeb", connection))
        //        {

        //            cmd.CommandType = System.Data.CommandType.StoredProcedure;
        //           // cmd.Parameters.AddWithValue("@Id_Reporte", ModeloTmp.Id_Reporte);
        //            cmd.Parameters.AddWithValue("@Id_Reporte", Identificador);
        //            cmd.Parameters.AddWithValue("@Consecutivo", Consecutivo);

        //            using (System.Data.SqlClient.SqlDataReader dr = cmd.ExecuteReader())
        //            {
        //                while (dr.Read())
        //                {
        //                    Nuevo.Id = Convert.ToInt32(dr["Id"].ToString());
        //                    Nuevo.Identificador = dr["Identificador"].ToString();
        //                    Nuevo.RutaBase = dr["RutaBase"].ToString();
        //                    Nuevo.Carpeta = dr["Carpeta"].ToString();
        //                    Nuevo.Fecha = Convert.ToDateTime(dr["Fecha"].ToString());
        //                    Nuevo.Consecutivo = dr["Consecutivo"].ToString();
        //                    Nuevo.Modulo = dr["Modulo"].ToString();                            
        //                }
        //            }                
        //        }
        //    }
        //    return Nuevo;
        //}

        //public Boolean EliminaCargaImagenesSolWeb(string IdArchivo, string IdModel)
        //{
        //    bool status = true;
        //    CargaImagenesVariasSolWeb imgSolWeb;
        //    #region Se elimina archivo
        //    imgSolWeb = SeleccionarCargaImagenesSolWebTodos(IdModel, "0").Where(n => n.Id == Int32.Parse(IdArchivo)).Select(n => new CargaImagenesVariasSolWeb { Id = n.Id, Carpeta = n.Carpeta, Consecutivo = n.Consecutivo, Fecha = n.Fecha, Identificador = n.Identificador, Modulo = n.Modulo, RutaBase = n.RutaBase }).ToList()[0];
        //    string rutaArchivo = imgSolWeb.RutaBase + "\\" + imgSolWeb.Identificador + "\\" + imgSolWeb.Consecutivo;
        //    if (System.IO.File.Exists(rutaArchivo))
        //    {
        //        try
        //        {
        //            System.IO.File.Delete(rutaArchivo);
        //        }
        //        catch (System.IO.IOException e)
        //        {
        //            status = false;
        //            return status;
        //        }
        //    }
        //    #endregion
        //    #region Se elimina registro desde Base de datos
        //    using (System.Data.SqlClient.SqlConnection connection = new System.Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings["conn"].ConnectionString))
        //    {
        //        connection.Open();
        //        using (System.Data.SqlClient.SqlCommand cmd = new System.Data.SqlClient.SqlCommand("SPMasEliminarCargaImagenesSolWeb", connection))
        //        {
        //            try
        //            {
        //                cmd.CommandType = System.Data.CommandType.StoredProcedure;
        //                cmd.Parameters.AddWithValue("@Id", IdArchivo);
        //                cmd.ExecuteNonQuery();
        //            }
        //            catch (SqlException sqlex)
        //            {
        //                status = false;
        //            }

        //        }
        //    }
        //    #endregion


        //    return status;
        //}

        //public List<CargaImagenesVariasSolWeb> SeleccionarCargaImagenesSolWebTodos(string Identificador, string Consecutivo)
        //{
        //    Consecutivo = ""; // Obtiene todos los registros de ese identificador
        //    List<CargaImagenesVariasSolWeb> Lista = new List<CargaImagenesVariasSolWeb>();
        //    using (System.Data.SqlClient.SqlConnection connection = new System.Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings["conn"].ConnectionString))
        //    {
        //        connection.Open();
        //        using (System.Data.SqlClient.SqlCommand cmd = new System.Data.SqlClient.SqlCommand("SPMasSeleccionarCargaImagenesSolWeb", connection))
        //        {

        //            cmd.CommandType = System.Data.CommandType.StoredProcedure;
        //            // cmd.Parameters.AddWithValue("@Id_Reporte", ModeloTmp.Id_Reporte);
        //            cmd.Parameters.AddWithValue("@Id_Reporte", Identificador);
        //            cmd.Parameters.AddWithValue("@Consecutivo", Consecutivo);

        //            using (System.Data.SqlClient.SqlDataReader dr = cmd.ExecuteReader())
        //            {
        //                while (dr.Read())
        //                {
        //                    CargaImagenesVariasSolWeb Nuevo = new CargaImagenesVariasSolWeb();                            
        //                    Nuevo.Id = Convert.ToInt32(dr["Id"].ToString());
        //                    Nuevo.Identificador = dr["Identificador"].ToString();
        //                    Nuevo.RutaBase = dr["RutaBase"].ToString();
        //                    Nuevo.Carpeta = dr["Carpeta"].ToString();
        //                    Nuevo.Fecha = Convert.ToDateTime(dr["Fecha"].ToString());
        //                    Nuevo.Consecutivo = dr["Consecutivo"].ToString();
        //                    Nuevo.Modulo = dr["Modulo"].ToString();
        //                    Lista.Add(Nuevo);
        //                }
        //            }
        //        }
        //    }
        //    return Lista;
        //}

        ////Agregado por SZ el 06/12/2016
        ////Permite Eliminar una embarcación agregada a una solicitud de Información.
        //public Boolean EliminaSolInfoRel(string IdEntrada, string RNP)
        //{
        //    bool status = true;

        //    #region Se elimina registro desde Base de datos
        //    using (System.Data.SqlClient.SqlConnection connection = new System.Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings["conn"].ConnectionString))
        //    {
        //        connection.Open();
        //        using (System.Data.SqlClient.SqlCommand cmd = new System.Data.SqlClient.SqlCommand("SPMasEliminarSolInfoRel", connection))
        //        {
        //            try
        //            {
        //                cmd.CommandType = System.Data.CommandType.StoredProcedure;
        //                cmd.Parameters.AddWithValue("@IdEntrada", IdEntrada);
        //                cmd.Parameters.AddWithValue("@RNP", RNP );
        //                cmd.ExecuteNonQuery();
        //            }
        //            catch (SqlException sqlex)
        //            {
        //                status = false;
        //            }

        //        }
        //    }
        //    #endregion
        //    return status;
        //}
        //#endregion

        //#region SOLICITUDES WEB

        //#region EJEMPLO
        //public List<String> ObtieneListaCorreosPorSolicitud(SolicitudesWeb_Entidad ModeloTmp)
        //{
        //    List<String> Lista = new List<String>();
        //    using (System.Data.SqlClient.SqlConnection connection = new System.Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings["conn"].ConnectionString))
        //    {
        //        connection.Open();
        //        using (System.Data.SqlClient.SqlCommand cmd = new System.Data.SqlClient.SqlCommand("SPMasObtieneListaCorreosPorSolicitud", connection))
        //        {
        //            cmd.CommandType = System.Data.CommandType.StoredProcedure;                    
        //            cmd.Parameters.AddWithValue("@Tipo", ModeloTmp.Tipo);

        //            using (System.Data.SqlClient.SqlDataReader dr = cmd.ExecuteReader())
        //            {
        //                while (dr.Read())
        //                {
        //                    Lista.Add(dr["cuentadecorreo"].ToString());                          
        //                }
        //            }
        //        }
        //    }
        //    return Lista;
        //}


        //public int CrearSolicitudWeb(SolicitudesWeb_Entidad ModeloTmp)
        //{
        //    int SolicitudCreada = 0;
        //    try
        //    {

        //        using (System.Data.SqlClient.SqlConnection connection = new System.Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings["connBitacora"].ConnectionString))
        //        {
        //            connection.Open();
        //            using (System.Data.SqlClient.SqlCommand cmd = new System.Data.SqlClient.SqlCommand("SPMasCrearSolicitudesWeb", connection))
        //            {

        //                cmd.CommandType = System.Data.CommandType.StoredProcedure;
        //                // cmd.Parameters.AddWithValue("@Id_Reporte", ModeloTmp.Id_Reporte);
        //                cmd.Parameters.AddWithValue("@Fecha", ModeloTmp.Fecha);
        //                cmd.Parameters.AddWithValue("@Hora", ModeloTmp.Hora);
        //                cmd.Parameters.AddWithValue("@Tipo", ModeloTmp.Tipo);
        //                cmd.Parameters.AddWithValue("@OficioCNP", ModeloTmp.OficioCNP);
        //                cmd.Parameters.AddWithValue("@Telefono", ModeloTmp.Telefono);
        //                cmd.Parameters.AddWithValue("@Contacto", ModeloTmp.Contacto);
        //                cmd.Parameters.AddWithValue("@Solicitante", ModeloTmp.Solicitante);
        //                cmd.Parameters.AddWithValue("@Recibio", ModeloTmp.Recibio);
        //                cmd.Parameters.AddWithValue("@Nombredelaembarcación", ModeloTmp.Nombredelaembarcacion);
        //                cmd.Parameters.AddWithValue("@RNP", ModeloTmp.RNP);
        //                cmd.Parameters.AddWithValue("@RazonSocial", ModeloTmp.RazonSocial);
        //                cmd.Parameters.AddWithValue("@Localidad", ModeloTmp.Localidad);
        //                cmd.Parameters.AddWithValue("@Supervisor", ModeloTmp.Supervisor);
        //                cmd.Parameters.AddWithValue("@Comentarios", ModeloTmp.Comentarios);
        //                cmd.Parameters.AddWithValue("@datoJustificacion", ModeloTmp.datoJustificacion); //VZ16

        //                using (System.Data.SqlClient.SqlDataReader dr = cmd.ExecuteReader())
        //                {
        //                    while (dr.Read())
        //                    {
        //                        SolicitudCreada = Convert.ToInt32(dr["Id_Reporte"].ToString());
        //                    }
        //                }
        //            }
        //        }
        //    }
        //    catch (Exception ex) { throw ex; }

        //    return SolicitudCreada;
        //}

        //public int EditarSolicitudWeb(SolicitudesWeb_Entidad ModeloTmp)
        //{

        //    int SolicitudEditada = 0;
        //    using (System.Data.SqlClient.SqlConnection connection = new System.Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings["connBitacora"].ConnectionString))
        //    {
        //        connection.Open();
        //        using (System.Data.SqlClient.SqlCommand cmd = new System.Data.SqlClient.SqlCommand("SPMasEditarSolicitudesWeb", connection))
        //        {

        //            cmd.CommandType = System.Data.CommandType.StoredProcedure;
        //            cmd.Parameters.AddWithValue("@Id_Reporte", ModeloTmp.Id_Reporte);
        //            cmd.Parameters.AddWithValue("@Fecha", ModeloTmp.Fecha);
        //            cmd.Parameters.AddWithValue("@Hora", ModeloTmp.Hora);
        //            cmd.Parameters.AddWithValue("@Tipo", ModeloTmp.Tipo);
        //            cmd.Parameters.AddWithValue("@OficioCNP", ModeloTmp.OficioCNP);
        //            cmd.Parameters.AddWithValue("@Telefono", ModeloTmp.Telefono);
        //            cmd.Parameters.AddWithValue("@Contacto", ModeloTmp.Contacto);
        //            cmd.Parameters.AddWithValue("@Solicitante", ModeloTmp.Solicitante);
        //            cmd.Parameters.AddWithValue("@Recibio", ModeloTmp.Recibio);
        //            cmd.Parameters.AddWithValue("@Nombredelaembarcación", ModeloTmp.Nombredelaembarcacion);
        //            cmd.Parameters.AddWithValue("@RNP", ModeloTmp.RNP);
        //            cmd.Parameters.AddWithValue("@RazonSocial", ModeloTmp.RazonSocial);
        //            cmd.Parameters.AddWithValue("@Localidad", ModeloTmp.Localidad);
        //            cmd.Parameters.AddWithValue("@Supervisor", ModeloTmp.Supervisor);
        //            cmd.Parameters.AddWithValue("@Comentarios", ModeloTmp.Comentarios);
        //            cmd.Parameters.AddWithValue("@datoJustificacion", ModeloTmp.datoJustificacion); //VZ16

        //            using (System.Data.SqlClient.SqlDataReader dr = cmd.ExecuteReader())
        //            {
        //                while (dr.Read())
        //                {
        //                    SolicitudEditada = Convert.ToInt32(dr["Id_Reporte"].ToString());
        //                }
        //            }
        //        }
        //    }
        //    return SolicitudEditada;
        //}


        //public SolicitudesWeb_Entidad LlenarResultadoDetalleSolicitudesWeb(int Id_Reporte)
        //{
        //    SolicitudesWeb_Entidad Resultado = new SolicitudesWeb_Entidad ();
        //    using (System.Data.SqlClient.SqlConnection connection = new System.Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings["connBitacora"].ConnectionString))
        //    {
        //        connection.Open();
        //        using (System.Data.SqlClient.SqlCommand cmd = new System.Data.SqlClient.SqlCommand("SPMasDetalleSolicitudesWeb", connection))
        //        {
        //            cmd.CommandType = System.Data.CommandType.StoredProcedure;
        //            cmd.Parameters.AddWithValue("@Id_Reporte", Id_Reporte);

        //            using (System.Data.SqlClient.SqlDataReader dr = cmd.ExecuteReader())
        //            {
        //                while (dr.Read())
        //                {                            				
        //                    Resultado.Id_Reporte = Convert.ToInt32  (dr["NumerodeReporte"].ToString());
        //                    Resultado.Fecha =Convert.ToDateTime( dr["Fecha"].ToString());
        //                    Resultado.Hora = dr["Hora"].ToString();
        //                    Resultado.Tipo = dr["Tipo"].ToString();
        //                    Resultado.datoJustificacion = dr["datoJustificacion"].ToString();
        //                    Resultado.OficioCNP = dr["OficioCNP"].ToString();
        //                    Resultado.Telefono = dr["Telefono"].ToString();
        //                    Resultado.Contacto = dr["Contacto"].ToString();
        //                    Resultado.Solicitante = dr["Solicitante"].ToString();
        //                    Resultado.Recibio = dr["Recibe"].ToString();
        //                    Resultado.Nombredelaembarcacion = dr["Barco"].ToString();
        //                    Resultado.RNP = dr["RNP"].ToString();
        //                    Resultado.RazonSocial = dr["RazonSocial"].ToString();
        //                    Resultado.Localidad = dr["Localidad"].ToString();
        //                    Resultado.Supervisor = dr["Supervisor"].ToString();
        //                    Resultado.Comentarios = dr["Comentarios"].ToString();
        //                }
        //            }
        //        }
        //    }
        //    return Resultado;
        //}

        //public List<SolicitudesWebEntidadResultadoBusqueda> LlenarResultadoBusquedaSolicitudWeb(SolicitudesWebEntidadBusqueda Modelo)
        //{
        //    string FechaFormateadaIni = Modelo.fechainicial.Year.ToString() + '-' + Modelo.fechainicial.Month.ToString().PadLeft(2, '0') + '-' + Modelo.fechainicial.Day.ToString().PadLeft(2, '0') + " 00:00:00";
        //    string FechaFormateadaFin = Modelo.fechafinal.Year.ToString() + '-' + Modelo.fechafinal.Month.ToString().PadLeft(2, '0') + '-' + Modelo.fechafinal.Day.ToString().PadLeft(2, '0') + " 00:00:00";

        //    List<SolicitudesWebEntidadResultadoBusqueda> Lista = new List<SolicitudesWebEntidadResultadoBusqueda>();

        //    using (System.Data.SqlClient.SqlConnection connection = new System.Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings["connBitacora"].ConnectionString))
        //    {
        //        connection.Open();
        //        using (System.Data.SqlClient.SqlCommand cmd = new System.Data.SqlClient.SqlCommand("SPMasBuscaSolicitudesWeb", connection))
        //        {
        //            cmd.CommandType = System.Data.CommandType.StoredProcedure;
        //            cmd.Parameters.AddWithValue("@fechainicial", FechaFormateadaIni);
        //            cmd.Parameters.AddWithValue("@fechafinal", FechaFormateadaFin);
        //            cmd.Parameters.AddWithValue("@NumerodeReporte", Modelo.NumerodeReporte);
        //            cmd.Parameters.AddWithValue("@OficioCNP", Modelo.OficioCNP);
        //            cmd.Parameters.AddWithValue("@FoliodeMantenimiento", Modelo.FoliodeMantenimiento);
        //            cmd.Parameters.AddWithValue("@Estado", Modelo.Estado);
        //            cmd.Parameters.AddWithValue("@Barco", Modelo.Barco);
        //            cmd.Parameters.AddWithValue("@Localidad", Modelo.Localidad);
        //            cmd.Parameters.AddWithValue("@Tipo", Modelo.Tipo);
        //            cmd.Parameters.AddWithValue("@RazonSocial", Modelo.RazonSocial);
        //            cmd.Parameters.AddWithValue("@Supervisor", Modelo.Supervisor);                         

        //            using (System.Data.SqlClient.SqlDataReader dr = cmd.ExecuteReader())
        //            {
        //                while (dr.Read())
        //                {
        //                    SolicitudesWebEntidadResultadoBusqueda Nuevo = new SolicitudesWebEntidadResultadoBusqueda();

        //                    Nuevo.NumerodeReporte = dr["NumerodeReporte"].ToString();
        //                    Nuevo.Fecha  = dr["Fecha"].ToString();
        //                    Nuevo.Hora  = dr["Hora"].ToString();
        //                    Nuevo.OficioCNP  = dr["OficioCNP"].ToString();
        //                    Nuevo.FoliodeMantenimiento  = dr["FoliodeMantenimiento"].ToString();
        //                    Nuevo.Estado  = dr["Estado"].ToString();
        //                    Nuevo.Barco  = dr["Barco"].ToString();
        //                    Nuevo.Localidad  = dr["Localidad"].ToString();
        //                    Nuevo.Tipo  = dr["Tipo"].ToString();
        //                    Nuevo.RazonSocial  = dr["RazonSocial"].ToString();
        //                    Nuevo.Supervisor  = dr["Supervisor"].ToString();

        //                    Lista.Add(Nuevo);
        //                }
        //            }
        //        }
        //    }
        //    return Lista;
        //}

        //public List<ResultadoDropDown> LlenarResultadoBitacoraDDL(string Busca)
        //{
        //    List<ResultadoDropDown> Lista = new List<ResultadoDropDown>();
        //    //System.Data.SqlClient.SqlConnection conn = new System.Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings["connBitacora"].ConnectionString); //conn
        //    //conn.Open();
        //    //System.Data.SqlClient.SqlCommand cmd = new System.Data.SqlClient.SqlCommand("SPMasBuscaResultadoGenericoSeleccionar", conn);
        //    //cmd.CommandType = System.Data.CommandType.StoredProcedure;
        //    //cmd.Parameters.AddWithValue("@Busca", "Busca");
        //    using (System.Data.SqlClient.SqlConnection connection = new System.Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings["connBitacora"].ConnectionString))
        //    {
        //        connection.Open();
        //        using (System.Data.SqlClient.SqlCommand cmd = new System.Data.SqlClient.SqlCommand("SPMasBuscaResultadoGenericoSeleccionar", connection))
        //        {
        //            cmd.CommandType = System.Data.CommandType.StoredProcedure;
        //            cmd.Parameters.AddWithValue("@Busca", Busca);
        //            using (System.Data.SqlClient.SqlDataReader dr = cmd.ExecuteReader())
        //            {
        //                while (dr.Read())
        //                {
        //                    ResultadoDropDown Nuevo = new ResultadoDropDown();
        //                    string[] ResultadoSPL = dr["Resultado"].ToString().Split('|');
        //                    if (ResultadoSPL.Length > 1)
        //                    {
        //                        Nuevo.ResultadoId = ResultadoSPL[0];
        //                        Nuevo.Resultado = ResultadoSPL[1];                                
        //                    }else
        //                    {
        //                        Nuevo.ResultadoId = dr["Resultado"].ToString();
        //                        Nuevo.Resultado = dr["Resultado"].ToString();
        //                    }

        //                    Lista.Add(Nuevo);
        //                }
        //            }
        //        }
        //    }                        
        //    return Lista;
        //}
        //public List<string> LlenarResultadoGenericoBitacoraListString(string Busca, string Valor)
        //{
        //    List<string> Lista = new List<string>();            
        //    using (System.Data.SqlClient.SqlConnection connection = new System.Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings["connBitacora"].ConnectionString))
        //    {
        //        connection.Open();
        //        using (System.Data.SqlClient.SqlCommand cmd = new System.Data.SqlClient.SqlCommand("SPMasBuscaResultadoGenericoSeleccionar", connection))
        //        {
        //            cmd.CommandType = System.Data.CommandType.StoredProcedure;
        //            cmd.Parameters.AddWithValue("@Busca", Busca);
        //            cmd.Parameters.AddWithValue("@Valor", Valor);
        //            using (System.Data.SqlClient.SqlDataReader dr = cmd.ExecuteReader())
        //            {
        //                while (dr.Read())
        //                {                            
        //                    string[] ResultadoSPL = dr["Resultado"].ToString().Split(';');
        //                    for (int i = 0; i < ResultadoSPL.Length; i++)
        //                    {
        //                       if(ResultadoSPL[i] != "") Lista.Add(ResultadoSPL[i]); 
        //                    }                                
        //                }
        //            }
        //        }
        //    }
        //    return Lista;
        //}
        //public string LlenarResultadoGenericoBitacoraString(string Busca, string Valor)
        //{
        //    string Resultado = "";
        //    using (System.Data.SqlClient.SqlConnection connection = new System.Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings["connBitacora"].ConnectionString))
        //    {
        //        connection.Open();
        //        using (System.Data.SqlClient.SqlCommand cmd = new System.Data.SqlClient.SqlCommand("SPMasBuscaResultadoGenericoSeleccionar", connection))
        //        {
        //            cmd.CommandType = System.Data.CommandType.StoredProcedure;
        //            cmd.Parameters.AddWithValue("@Busca", Busca);
        //            cmd.Parameters.AddWithValue("@Valor", Valor);
        //            using (System.Data.SqlClient.SqlDataReader dr = cmd.ExecuteReader())
        //            {
        //                while (dr.Read())
        //                {
        //                    Resultado = dr["Resultado"].ToString();                          
        //                }
        //            }
        //        }
        //    }
        //    return Resultado;
        //}

        ////public List<MantenimientoResumen> EncuentraMantenimientosResumenX_EJEMPLO(MantenimientoBusquedaModel ModeloTemporal)
        ////{
        ////    //string FechaFormateadaIni = ModeloTemporal.FechaInicio.Year.ToString() + '-' + ModeloTemporal.FechaInicio.Month.ToString().PadLeft(2, '0') + '-' + ModeloTemporal.FechaInicio.Day.ToString().PadLeft(2, '0') + " 00:00:00";
        ////    //string FechaFormateadaFin = ModeloTemporal.FechaFin.Year.ToString() + '-' + ModeloTemporal.FechaFin.Month.ToString().PadLeft(2, '0') + '-' + ModeloTemporal.FechaFin.Day.ToString().PadLeft(2, '0') + " 23:59:59";

        ////    //var Resultado = db.BuscaMantenimientosResumen(ModeloTemporal.ServicioSel,ModeloTemporal.PuertoBaseSel,ModeloTemporal.RazonSocialSel,ModeloTemporal.SupervisorSel,ModeloTemporal.ZonaSel,ModeloTemporal.BarcoSel,ModeloTemporal.NoBarcos,ModeloTemporal.NoServicios,ModeloTemporal.NoAmbos).ToList();
        ////    List<MantenimientoResumen> ResultadoFinalLista = new List<MantenimientoResumen>();

        ////    System.Data.SqlClient.SqlConnection conn = new System.Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings["conn"].ConnectionString); //conn
        ////    conn.Open();

        ////    System.Data.SqlClient.SqlCommand cmd = new System.Data.SqlClient.SqlCommand("SPMasBuscaMantenimientosResumen " +
        ////    "'" + ModeloTemporal.ServicioSel + "', '" + ModeloTemporal.PuertoBaseSel + "', '" + ModeloTemporal.RazonSocialSel + "', '" + ModeloTemporal.SupervisorSel + "', '" + ModeloTemporal.ZonaSel +
        ////    "', '" + ModeloTemporal.BarcoSel + "', '" + ModeloTemporal.NoBarcos + "', '" + ModeloTemporal.NoServicios + "', '" + ModeloTemporal.NoAmbos + "'", conn);
        ////    System.Data.SqlClient.SqlDataReader dr;

        ////    dr = cmd.ExecuteReader();
        ////    while (dr.Read())
        ////    {
        ////        MantenimientoResumen ResultadoFinalRenglon = new MantenimientoResumen();
        ////        if (ValidarColumna(dr, "Servicio") == true)
        ////            ResultadoFinalRenglon.Servicio = dr["Servicio"].ToString();
        ////        if (ValidarColumna(dr, "Localidad") == true)
        ////            ResultadoFinalRenglon.Localidad = dr["Localidad"].ToString();
        ////        if (ValidarColumna(dr, "RazonSocial") == true)
        ////            ResultadoFinalRenglon.RazonSocial = dr["RazonSocial"].ToString();
        ////        if (ValidarColumna(dr, "Supervisor") == true)
        ////            ResultadoFinalRenglon.Supervisor = dr["Supervisor"].ToString();
        ////        if (ValidarColumna(dr, "Zona") == true)
        ////            ResultadoFinalRenglon.Zona = dr["Zona"].ToString();
        ////        if (ValidarColumna(dr, "NombreBarco") == true)
        ////            ResultadoFinalRenglon.NombreBarco = dr["NombreBarco"].ToString();
        ////        if (ValidarColumna(dr, "NoBarcos") == true)
        ////            ResultadoFinalRenglon.NoBarcos = Convert.ToInt32(dr["NoBarcos"].ToString());
        ////        if (ValidarColumna(dr, "NoServicios") == true)
        ////            ResultadoFinalRenglon.NoServicios = Convert.ToInt32(dr["NoServicios"].ToString());

        ////        ResultadoFinalLista.Add(ResultadoFinalRenglon);
        ////    }
        ////    dr.Close();
        ////    if (conn.State == System.Data.ConnectionState.Open)
        ////        conn.Close();

        ////    return ResultadoFinalLista;

        ////}    
        //#endregion

        //#endregion

        //#region ENVIO DE CORREOS
        //public bool EstructurayEnviaCorreoCNP()
        //{
        //    bool SeEnvio = false;
        //    MailMessage message = new MailMessage();
        //    SmtpClient smtpClient = new SmtpClient();
        //    smtpClient.Host = "192.168.1.14";
        //    smtpClient.Port = 25;

        //    MailAddress  fromAddress = new MailAddress("sistema@conapesca.com", "Sistema Web Conapesca");

        //    message.From = fromAddress;
        //    message.To.Add("motmen@hotmail.com");
        //    //message.To.Add("pedro.castaneda@astrum.com.mx");
        //    smtpClient.Credentials = new NetworkCredential("conapesca_web", "conapesca2008");

        //    smtpClient.Send(message);

        //    return SeEnvio;
        //}
        //public bool EstructurayEnviaCorreoDLL(List<string> listPara, string asunto, string cuerpo)
        //{
        //    bool SeEnvio = false;
        //    Astrum.SendEmail.EMail Correo = new EMail("mail.astrum.com.mx", "infraestructura", "uny0009ikaxplir", 6000);
        //    Correo.Send("infraestructura@astrum.com.mx", "motmen@hotmail.com", "Prueba de correo", "test");

        //    return SeEnvio;           
        //}

        //public bool EstructurayEnviaCorreo(List<string> listPara, string asunto, string cuerpo)
        //{
        //    EntidadCorreo entidadCorreo = new EntidadCorreo();
        //    EntidadSMTP entidadSMTP = new EntidadSMTP();

        //    entidadCorreo.Para = listPara;
        //    entidadCorreo.De = LlenarResultadoGenericoBitacoraString("Correo_De", ""); //"infraestructura@astrum.com.mx"      
        //    entidadCorreo.NombreAMostrar = LlenarResultadoGenericoBitacoraString("Correo_NombreAMostrar", ""); ;//"Sistema Web Conapesca"
        //    entidadCorreo.Asunto = asunto;
        //    entidadCorreo.Cuerpo = cuerpo;

        //    entidadSMTP.Host = LlenarResultadoGenericoBitacoraString("Correo_Host", "");// "correo.astrum.com.mx";
        //    entidadSMTP.Port = Convert.ToInt32(LlenarResultadoGenericoBitacoraString("Correo_Puerto", "")); //25;
        //    entidadSMTP.Credentials_Email = LlenarResultadoGenericoBitacoraString("Correo_Credentials_Email", ""); //"conapesca_web";
        //    entidadSMTP.Credentials_Contrasena = LlenarResultadoGenericoBitacoraString("Correo_Credentials_Contrasena", ""); //"conapesca2008";            
        //    return EnviaCorreo(entidadCorreo, entidadSMTP);
        //}

        //public bool EnviaCorreo(EntidadCorreo Message, EntidadSMTP SMTP)
        //{
        //    MailMessage email = new MailMessage();

        //    foreach (string ElementoTo in Message.Para)
        //    {
        //        email.To.Add(new MailAddress(ElementoTo));
        //    }

        //    email.From = new MailAddress(Message.De,Message.NombreAMostrar);
        //    email.Subject = Message.Asunto;
        //    email.Body = Message.Cuerpo;
        //    email.IsBodyHtml = true;
        //    email.Priority = MailPriority.High;

        //    SmtpClient smtp = new SmtpClient();
        //    smtp.Host = SMTP.Host;
        //    smtp.Port = SMTP.Port;
        //    //smtp.EnableSsl = false;
        //    //smtp.UseDefaultCredentials = false;

        //    smtp.Credentials = new NetworkCredential(SMTP.Credentials_Email, SMTP.Credentials_Contrasena);

        //    try
        //    {
        //        smtp.Send(email);
        //        email.Dispose();
        //        return true;
        //    }
        //    catch (Exception ex)
        //    {
        //        return false;
        //    }
        //}

        //#endregion


        ////V16 comprime la carpata en un zip / descarga del archivo .zip
        //#region DESCARGATODOSOFICIOS 

        //public string AddFileToZip(string zipFilename, string fileToAdd, CompressionOption compression = CompressionOption.Normal)
        //{
        //    string destFilename = ".\\" + Path.GetFileName(fileToAdd);
        //    //verifica que exista el archivo el la ruta indicada
        //    if (System.IO.File.Exists(fileToAdd))
        //    {                
        //            using (Package zip = System.IO.Packaging.Package.Open(zipFilename, FileMode.OpenOrCreate))
        //            {

        //                Uri uri = PackUriHelper.CreatePartUri(new Uri(destFilename, UriKind.Relative));
        //                if (zip.PartExists(uri))
        //                {
        //                    zip.DeletePart(uri);
        //                }
        //                PackagePart part = zip.CreatePart(uri, "", compression);
        //                using (FileStream fileStream = new FileStream(fileToAdd, FileMode.Open, FileAccess.Read))
        //                {
        //                    using (Stream dest = part.GetStream())
        //                    {
        //                        fileStream.CopyTo(dest);
        //                    }
        //                }
        //                //return destFilename;                        
        //            }//using packages
        //    } //if
        //    return destFilename;
        //} //public

        //public string DescargaTodoOficiosZip(int idSol)
        //{
        //    string rutabase;
        //    string consecutivo;

        //    rutabase = "";
        //    consecutivo  = idSol.ToString()+ ".zip";

        //    //busca la ruta del directorio de los oficios
        //    using (System.Data.SqlClient.SqlConnection connection = new System.Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings["conn"].ConnectionString))
        //    {
        //        connection.Open();
        //        using (System.Data.SqlClient.SqlCommand cmd = new System.Data.SqlClient.SqlCommand("Sismep.SPTSismepConfiguracionSeleccionar", connection))
        //        {
        //            cmd.CommandType = System.Data.CommandType.StoredProcedure;
        //            cmd.Parameters.AddWithValue("@IdConfiguracion", DBNull.Value);
        //            using (System.Data.SqlClient.SqlDataReader dr = cmd.ExecuteReader())
        //            {
        //                while (dr.Read())
        //                {
        //                    if (dr["Configuracion"].ToString() == "RutaCargaImagenesMAS")
        //                    {
        //                        rutabase = dr["ValorString"].ToString();
        //                    }

        //                }
        //            }
        //        }
        //    }

        //    //rutabase = "C:\\CargaImagenesMAS";

        //    string RutaFormada = rutabase.Trim();
        //    RutaFormada += '\\' + consecutivo.Trim();
        //    RutaFormada = QuitaAcentos(RutaFormada);

        //    //si ya exisre el archivo zip se borra
        //    if (System.IO.File.Exists(RutaFormada))
        //    {
        //        System.IO.File.Delete(RutaFormada);
        //    }

        //    List<CargaImagenesVariasSolWeb> imgSolWeb = new List<CargaImagenesVariasSolWeb>();

        //    imgSolWeb = SeleccionarCargaImagenesSolWebTodos(idSol.ToString(), "0").ToList();

        //    foreach (CargaImagenesVariasSolWeb imgSol in imgSolWeb)
        //    {
        //        string rutaArchivo = imgSol.RutaBase + "\\" + imgSol.Identificador + "\\" + imgSol.Consecutivo;
        //        AddFileToZip(@RutaFormada, @rutaArchivo);
        //    }

        //    if (System.IO.File.Exists(RutaFormada) == false)
        //    {
        //        RutaFormada = "C:\\CARGA IMAGENES\\NoDisponible.jpg";
        //    }

        //    var file = string.Format("{0}.zip", consecutivo.Replace(".zip", "").Trim());

        //    var fullPath = RutaFormada;// Path.Combine(path, file);

        //    return fullPath;

        //}


        //#endregion

        //#region REPORTE ALERTAS ZONA/PESCA PROHIBIDA
        ////VZ1611_RZPP agregar toda la region

        ////MODULO INSERTAR
        //public int CrearTurnoOFP(TurnosOFPModel_Entidad ModeloTmp)
        //{

        //    int turnoCreado = 0;
        //    //using (System.Data.SqlClient.SqlConnection connection = new System.Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings["connBitacora"].ConnectionString))
        //    using (System.Data.SqlClient.SqlConnection connection = new System.Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings["conn"].ConnectionString))
        //    {
        //        connection.Open();
        //        using (System.Data.SqlClient.SqlCommand cmd = new System.Data.SqlClient.SqlCommand("SPMasCrearTurnoOFP", connection))
        //        {

        //            cmd.CommandType = System.Data.CommandType.StoredProcedure;
        //            cmd.Parameters.AddWithValue("@ofpNombre", ModeloTmp.ofpNombre);
        //            cmd.Parameters.AddWithValue("@turnoFechaIni", ModeloTmp.turnoFechaIni);
        //            cmd.Parameters.AddWithValue("@turnoHoraIni", ModeloTmp.turnoHoraIni);
        //            cmd.Parameters.AddWithValue("@turnoFechaFin", ModeloTmp.turnoFechaFin);
        //            cmd.Parameters.AddWithValue("@turnoHoraFin", ModeloTmp.turnoHoraFin);
        //            using (System.Data.SqlClient.SqlDataReader dr = cmd.ExecuteReader())
        //            {
        //                while (dr.Read())
        //                {
        //                    turnoCreado = Convert.ToInt32(dr["IdReg"].ToString());
        //                }
        //            }
        //        }
        //    }
        //    return turnoCreado;
        //}

        ////MODULO DETALLE

        ////public List<SPMasRelacionTurnoOFPSeleccionar_Result> EncuentraRelacionTurnoOFPSP(int IdReg)
        ////{
        ////    return db.SPMasRelacionTurnoOFPSeleccionar(IdReg).ToList();
        ////}

        ////MODULO EDIT
        //public TurnosOFPModel_ID EncuentraTurnoOFPSP(int IdReg)
        //{
        //    //TurnosOFPModel ModeloTemporal = new TurnosOFPModel();
        //    TurnosOFPModel_ID ModeloTemporal = new TurnosOFPModel_ID();

        //    SPMasRelacionTurnoOFPSeleccionar_Result Resultado = db.SPMasRelacionTurnoOFPSeleccionar(IdReg).FirstOrDefault();

        //    if (Resultado != null)
        //    {
        //        ModeloTemporal.IdReg = Resultado.IdReg;
        //        ModeloTemporal.ofpNombre = Resultado.ofpNombre;
        //        ModeloTemporal.turnoFechaIni = Convert.ToDateTime(Resultado.turnoFechaIni);
        //        ModeloTemporal.turnoHoraIni = Resultado.turnoHoraIni;
        //        ModeloTemporal.turnoFechaFin = Convert.ToDateTime(Resultado.turnoFechaFin);
        //        ModeloTemporal.turnoHoraFin = Resultado.turnoHoraFin;
        //    }
        //    return ModeloTemporal;
        //}

        //public Boolean ActualizaTrunoOFP(TurnosOFPModel_ID Modelo)
        //{
        //    Boolean Actualizado = false;
        //    SPMasActualizarTurnoOFP_Result Resultado = db.SPMasActualizarTurnoOFP(Modelo.IdReg, Modelo.ofpNombre, Modelo.turnoFechaIni, Modelo.turnoHoraIni, Modelo.turnoFechaFin, Modelo.turnoHoraFin).FirstOrDefault();
        //    if (Resultado != null)
        //    {
        //        Actualizado = true;
        //    }
        //    return Actualizado;
        //}


        //public List<SPMasBuscaTurnosOFP_Result> EncuentraBusTurnosOFPSP(TurnosOFPBusquedaModel ModeloTemporal)
        //{


        //    string FechaFormateadaIni = ModeloTemporal.turnoFechaIni.ToString("d", CultureInfo.CreateSpecificCulture("en-US"));
        //    string FechaFormateadaFin = ModeloTemporal.turnoFechaFin.ToString("d", CultureInfo.CreateSpecificCulture("en-US"));


        //    if (ModeloTemporal.ofpNombre != null)
        //    {
        //        ModeloTemporal.ofpNombre = ModeloTemporal.ofpNombre.Replace("'", "");
        //    }
        //    else
        //    {
        //        ModeloTemporal.ofpNombre = "";
        //    }

        //    //return db.SPMasBuscaTurnosOFP("20161118", "20161118", ModeloTemporal.ofpNombre).ToList();
        //    return db.SPMasBuscaTurnosOFP(FechaFormateadaIni, FechaFormateadaFin, ModeloTemporal.ofpNombre).ToList();
        //}


        //#endregion

    } 
}