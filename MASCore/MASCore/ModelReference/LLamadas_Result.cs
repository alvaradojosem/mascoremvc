﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MASCore.ModelReference
{
    public class LLamadas_Result
    {}

    public partial class LlamadasInsertarTMP_Result
    {
        public int IdBitaCel { get; set; }
        public int IdControl { get; set; }
        public Nullable<System.DateTime> Fecha { get; set; }
        public string Hora { get; set; }
        public string Tipo { get; set; }
        public string Numero { get; set; }
    }
    public partial class LlamadasInsertarEnEditar_Result
    {
        public int IdBitaCel { get; set; }
        public int IdControl { get; set; }
        public Nullable<System.DateTime> Fecha { get; set; }
        public string Hora { get; set; }
        public string Tipo { get; set; }
        public string Numero { get; set; }
    }

    public partial class LlamadasBorrarEnEditar_Result
    {
        public int IdBitaCel { get; set; }
        public int IdControl { get; set; }
        public Nullable<System.DateTime> Fecha { get; set; }
        public string Hora { get; set; }
        public string Tipo { get; set; }
        public string Numero { get; set; }
    }
    public partial class BuscaLlamadasTMP_Result
    {
        public int IdBitaCel { get; set; }
        public int IdControl { get; set; }
        public Nullable<System.DateTime> Fecha { get; set; }
        public string Hora { get; set; }
        public string Tipo { get; set; }
        public string Numero { get; set; }
        public Nullable<bool> Respuesta { get; set; }
    }

    public partial class SPMasBuscaLlamadasDesdeSismep_Result
    {
        public string RealizoLLamada { get; set; }
        public string AtendioLLamada { get; set; }
        public string Lugar { get; set; }
        public string OFP { get; set; }
        public string Descripcion { get; set; }
        public string Tipo { get; set; }
        public string Numero { get; set; }
        public Nullable<System.DateTime> Fecha { get; set; }
        public string Hora { get; set; }
        public string Respuesta { get; set; }
    }
    public partial class BuscaLlamadasEnDB_Result
    {
        public int IdBitaCel { get; set; }
        public int IdControl { get; set; }
        public Nullable<System.DateTime> Fecha { get; set; }
        public string Hora { get; set; }
        public string Tipo { get; set; }
        public string Numero { get; set; }
        public Nullable<bool> Respuesta { get; set; }
    }

    public partial class LlamadasInsertar_Result
    {
        public Nullable<int> Cantidad { get; set; }
    }


    public partial class BuscaLlamadas_Result
    {
        public Nullable<System.DateTime> Fecha { get; set; }
        public string Hora { get; set; }
        public string Respuesta { get; set; }
        public string Barco { get; set; }
        public string RNP { get; set; }
        public string Matricula { get; set; }
        public string RazonSocial { get; set; }
        public string OFP { get; set; }
        public int IdBitaCel { get; set; }
    }




}
