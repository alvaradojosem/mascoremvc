function init_menu() {
    $(".nav.side-menu.nomargenpadding > li").click(function () {
        if ($(this).hasClass("active")) {
            $(this).find(".nav.child_menu").removeClass("activo");
            $(this).removeClass("active");
        } else {
            $(".nav.child_menu").removeClass("activo");
            $(".nav.side-menu.nomargenpadding").find("li.active").removeClass("active");
            $(this).find(".nav.child_menu").addClass("activo");
            $(this).addClass("active");
        }
    });

    $("#menu_toggle").click(function () {
        if ($(".encabezado").hasClass("col-1")) {
            $(".esconder").show();
            $(".encabezado").removeClass("col-1").addClass("col-3").removeClass("flexible");
            $(".agrandar").addClass("col-9").removeClass("col-11");
            $(".foot").removeClass("pie");
            $(".agrandarEnChico").removeClass("agrandarEnCol1 row col-12");
            $(".abajoEngrande").removeClass("abajoEnCol12 row col-12");
            $(".aunladosubmenubase").removeClass("aunladosubmenu");
        } else {
            $(".esconder").hide();
            $(".encabezado").removeClass("col-3").addClass("col-1").addClass("flexible");
            $(".agrandar").removeClass("col-9").addClass("col-11");
            $(".foot").addClass("pie");
            $(".agrandarEnChico").addClass("agrandarEnCol1 row col-12");
            $(".abajoEngrande").addClass("abajoEnCol12 row col-12");
            $(".aunladosubmenubase").addClass("aunladosubmenu");
        }
    });
}

function init_tabs() {
    $(".tabul > ul > li").click(function () {
        $("li.activeted").removeClass("activeted");
        $(".tabul").parent().find("div.activo").removeClass("activo");
        $(this).addClass("activeted");
        $(this).parent().parent().parent().find("div." + $(this).attr("id")).addClass("activo");
    });
}

function print(url,params) {
    window.open(url + "/" + params, '_blank');
}

function edit(url, params) {
    window.location.href = url + params;
}

function comback(url) {
    window.location.href = url;
}

function ExportarPDF(url) {
    window.open(url, '_blank');
}

function tabs_itemClick(e) {
    $("div.activo").removeClass("activo");
    $("div." + e.itemData.id).addClass("activo");
}

function tabs_carrusel_itemClick(e) {
    $("div.tabin.activo").removeClass("activo");
    $("div.tabin." + e.itemData.id).addClass("activo");
    actualCarrusel = e.itemData.carrusel;
    carruselPagina = paginaCarruselPorArea[e.itemData.carrusel];
}

var paginaCarruselPorArea = [];
var carruselPagina = 1;
var actualCarrusel = "";

function init_carrusel(array) {
    paginaCarruselPorArea = array;

    $(".botonderechocarrusel").click(function () {
        carruselPagina = carruselPagina + 1;
        $("." + actualCarrusel + ".activo").removeClass("activo");
        if (carruselPagina > $("." + actualCarrusel).length) {
            carruselPagina = 1;
        }
        $("." + actualCarrusel + "." + carruselPagina).addClass("activo");
        paginaCarruselPorArea[actualCarrusel] = carruselPagina;
    });

    $(".botonizquierdocarrusel").click(function () {
        carruselPagina = carruselPagina - 1;
        $("." + actualCarrusel + ".activo").removeClass("activo");
        if (carruselPagina < 1) {
            carruselPagina = $("." + actualCarrusel).length;
        }
        $("." + actualCarrusel + "." + carruselPagina).addClass("activo");
        paginaCarruselPorArea[actualCarrusel] = carruselPagina;
    });
}

$(document).ready(() => {
    init_menu();
});