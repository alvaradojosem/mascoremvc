﻿var now = new Date();
var URLBase = 'https://10.215.71.239:44350/'; // Net Core

$(document).ready(function () {
    $("#generales").show();
    $("#alertas").hide();

    Controles.Calendarios.obj("#Fecha");
    Controles.CajaHora.obj("#myDatepicker3");

    //CtrlToolTip,CtrlDiv
    Controles.tooltips.obj("#Tooltip1", "#buscardatossolicitante");
    Controles.tooltips.obj("#ToolTip2", "#editarDetalle");
    Controles.tooltips.obj("#ToolTip3", "#agregar");
    Controles.tooltips.obj("#Tooltip1", "#buscardatossolicitante");

    Controles.Boton.obj("#buscardatossolicitante", "fas fa-search", "default");
    Controles.Boton.obj("#agregar", "fas fa-plus-circle", "default");

    //AutoCompletes
    Controles.CajaDeTexto.obj("#solicitante", "Solicitante", "");
    Controles.CajaDeTexto.obj("#Cargo", "Cargo", "");
    Controles.CajaDeTexto.obj("#folio", "Número de folio", "");


    //Texto con Mask
    Controles.TextBox.obj("#telefono", "", "", true, "(00) 0000-0000", "");
    Controles.CajaEmail.obj("#Email", "Email", "");
    Controles.TextArea.obj("#descripcion", "", 120);

    //url, CtrlDiv, PlaceHolder
    Controles.SelectBox.obj("api/LoadControls/GetResponsablesOFP", "#ofp", "-- Seleccione el OFP --");
    Controles.SelectBox.obj("api/LoadControls/GetResponsablesOFP", "#medio", "-- Seleccione el Medio --");    
    Controles.SelectBox.obj("api/LoadControls/GetResponsablesOFP", "#dependencia", "-- Seleccione la Dependencia --");
    Controles.SelectBox.obj("api/LoadControls/GetResponsablesOFP", "#estado", "-- Seleccione el Estado --");
    Controles.SelectBox.obj("api/LoadControls/GetResponsablesOFP", "#ciudad", "-- Seleccione la Ciudad --");
    Controles.SelectBox.obj("api/LoadControls/GetResponsablesOFP", "#Asunto", "-- Seleccione el Asunto --");

                            /////Seccion Alertas////
    Controles.LoadIndicator.obj("#button-indicator");

    Controles.CajaDeTexto.obj("#Barco", "Barco", "");

                                       


    Tabs();
});

function Tabs() {  
    var tabs = [
        { id: "generales", text: "Datos Generales", icon:"fab fa-wpforms", content: "generales" },
        { id: "alertas", text: "Datos Embarcaciones", icon : "fas fa-ship", content: "resumen" }
    ];

    var tabsInstance = $("#tabs > .tabs-container").dxTabs({
        dataSource: tabs,
        selectedIndex: 0,
        onItemClick: function (e) {
            if (e.itemData.id === "generales") {
                $("#generales").show();
                $("#alertas").hide();
                
            } else if (e.itemData.id === "alertas") {
                $("#generales").hide();
                $("#alertas").show();
            }
        }

    }).dxTabs("instance");

}