﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace MASCore.Models
{
    
    public class NoticiasMonitoreo
    {
        private BDMasContextEnt db = new BDMasContextEnt();

        [Required]
        [DisplayName("*Titulo de la noticia:")]
        public string titulo { get; set; }
        [DisplayName("Viñeta 1:")]
        public string vineta1 { get; set; }
        [DisplayName("Viñeta 2:")]
        public string vineta2 { get; set; }


        [Required]
        [DisplayName("*Fecha de la noticia:")]
        public DateTime fechadelanoticia { get; set; }

        [DisplayName("Fecha en BD:")]
        public DateTime fecha { get; set; }
        
        [Required]        
        [DisplayName("*Cuerpo de la noticia:")]
        public string cuerpo { get; set; }
        public Boolean guardado { get; set; }

        public string ruta { get; set; }

    }


    //VZ160920+ toda la region
    #region ValidacionHora    
    public static class FechaValidacion
    {
        public static ValidationResult validaFecha(string fechadelanoticia)
        {
            if (IsDate(fechadelanoticia))
            {      
                DateTime dt = Convert.ToDateTime(fechadelanoticia);
                if (dt <= DateTime.Now)
                {
                    return ValidationResult.Success;
                }
                return new ValidationResult("La fecha no debe ser mayor a la actual");
            }
            else
            {
                return new ValidationResult("El formato de la fecha es dd/mm/aaaa");
            }
        }
        //verifica si el formato es valido
        public static bool IsDate(string inputDate)
        {
            bool isDate = true;
            try
            {
                DateTime dateValue;
                dateValue = DateTime.ParseExact(inputDate, "dd/MM/yyyy", null);
            }
            catch
            {
                isDate = false;
            }
            return isDate;
        }

    }
    #endregion
}