﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MASCore.DataModel
{
    public class TMASSalida
    {
    }
    public partial class TMasOfSalida
    {
        public int IdSalida { get; set; }
        public string NoOficio { get; set; }
        public Nullable<System.DateTime> Fecha { get; set; }
        public string Asunto { get; set; }
        public string Dirigido { get; set; }
        public string dCargo { get; set; }
        public string dEmpresa { get; set; }
        public string Envia { get; set; }
        public string eCargo { get; set; }
        public string eEmpresa { get; set; }
        public string Copias { get; set; }
        public string Comentarios { get; set; }
        public string TipoSalida { get; set; }
        public string Contesta { get; set; }
        public string Minutario { get; set; }
        public string Estatus { get; set; }
        public Nullable<int> TipoRel { get; set; }
        public string OFP { get; set; }
    }
}
