﻿using System;


namespace MASCore.DataModel
{


    public partial class BuscaCargaImagenesTipo_Result
    {
        public int Id { get; set; }
        public string Identificador { get; set; }
        public string Tipo { get; set; }
        public string Rnp { get; set; }
        public string RutaBase { get; set; }
        public Nullable<System.DateTime> Fecha { get; set; }
        public string Consecutivo { get; set; }
    }
}
