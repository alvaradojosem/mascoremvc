﻿using ClosedXML.Excel;
using DevExtreme.AspNet.Data;
using MASCore.Components;
using MASCore.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using Microsoft.AspNetCore.Mvc;
using Xceed.Words.NET;
using MASCore.DataModel;
using DataSourceLoadOptions = DevExtreme.AspNet.Mvc.DataSourceLoadOptions;
using MASCore.ModelReference;
using BuscaLlamadas_Result = MASCore.DataModel.BuscaLlamadas_Result;
using Microsoft.AspNetCore.Http;
using System.IO;
using MASCore.Exportar;

namespace MASCore.Controllers
{
    public class LlamadasController : Controller
    {
        //private BDMasContextEnt db = new BDMasContextEnt();
        FuncionesProcedimientos FunProc = new FuncionesProcedimientos();

        #region Cargar Combos
        [HttpGet]
        public ActionResult GetActividades(DataSourceLoadOptions loadOptions)
        {
            return null;// Content(JsonConvert.SerializeObject(DataSourceLoader.Load(FunProc.LlenarResultadoGenerico("btactividades"), loadOptions)), "application/json");
        }

        [HttpGet]
        public ActionResult GetResponsablesOFP(DataSourceLoadOptions loadOptions)
        {
            return null;// Content(JsonConvert.SerializeObject(DataSourceLoader.Load(FunProc.LlenarResultadoGenerico("responsableofp"), loadOptions)), "application/json");
        }

        [HttpGet]
        public ActionResult GetFiltros(DataSourceLoadOptions loadOptions)
        {
            return null;//Content(JsonConvert.SerializeObject(DataSourceLoader.Load(FunProc.LlenarResultadoGenerico("filtro"), loadOptions)), "application/json");
        }

        [HttpGet]
        public ActionResult GetOpcionesBusqueda(DataSourceLoadOptions loadOptions)
        {
            return null;//Content(JsonConvert.SerializeObject(DataSourceLoader.Load(FunProc.LlenarResultadoGenerico("opcionesbusqueda"), loadOptions)), "application/json");
        }

        [HttpGet]
        public ActionResult GetBTTipos(DataSourceLoadOptions loadOptions)
        {
            return null;// Content(JsonConvert.SerializeObject(DataSourceLoader.Load(FunProc.LlenarResultadoGenerico("bttipos"), loadOptions)), "application/json");
        }

        [HttpGet]
        public ActionResult GetRespuestas(DataSourceLoadOptions loadOptions)
        {
            IList<BuscaResultadoGenerico_Result> res = new List<BuscaResultadoGenerico_Result>();
            res.Add(new BuscaResultadoGenerico_Result() { Resultado = Resources.Etiquetas.SI });
            res.Add(new BuscaResultadoGenerico_Result() { Resultado = Resources.Etiquetas.NO });
            return Content(JsonConvert.SerializeObject(DataSourceLoader.Load(res, loadOptions)), "application/json");
        }
        #endregion

        // GET: /Llamadas/
        public ActionResult Index()
        {
            //return View(db.TMasBTBitaCels.ToList());
            return View();
        }
        //GET
        public ActionResult BuscaLlamada()
        {
            //string currentLang = Thread.CurrentThread.CurrentCulture.TwoLetterISOLanguageName;//Thread.CurrentThread.CurrentCulture = new CultureInfo("");
            
            LlamadaBusquedaModel ModeloTmp = new LlamadaBusquedaModel();
            ModeloTmp.FechaInicial = DateTime.Now; //Convert.ToDateTime("01/01/2009");               
            ModeloTmp.FechaFinal = DateTime.Now;
            List<BuscaLlamadas_Result> Modelotemporal = new List<BuscaLlamadas_Result>();

            //ModeloTmp.ResultadoListaLlamadasSP = Modelotemporal;// ModeloTmp.EncuentraLasLlamadasSP("",ModeloTmp.FechaInicial, ModeloTmp.FechaFinal,"","","");
            //Session.Remove("SessionResultadoListaLlamadasSP");

            //ViewBag.actividad = FunProc.LlenarResultadoGenerico("btactividades");
            //ViewBag.ofp = FunProc.LlenarResultadoGenerico("responsableofp");
            //ViewBag.filtro = FunProc.LlenarResultadoGenerico("filtro");
            //Session["SessionResultadoListaLlamadasSP"] = ModeloTmp.ResultadoListaLlamadasSP;
            return View(ModeloTmp);
        }

        [HttpPost]
        public ActionResult BuscaLlamada(LlamadaBusquedaModel ModeloTmp)
        {
            List<BuscaLlamadas_Result> Modelotemporal = new List<BuscaLlamadas_Result>();
            //ModeloTmp.ResultadoListaLlamadasSP = Modelotemporal;

            #region "VALIDACIONES"
            Boolean Valido = true;
            if (ModeloTmp.FechaInicial > ModeloTmp.FechaFinal)
            {
                ViewBag.NoValidoPor = "No es posible realizar la búsqueda, la fecha inicial no puede ser mayor a la fecha final. Verificar.";
                Valido = false;
            }
            if (ModeloTmp.FechaInicial.Year < 1753 || ModeloTmp.FechaFinal.Year < 1753)
            {
                ViewBag.NoValidoPor = "No es posible realizar la búsqueda. Verificar Fechas.";
                Valido = false;
            }
            //if (ModeloTmp.Filtro == null)
            //{
            //    ViewBag.NoValidoPor = "No es posible realizar la busqueda. Debe Seleccionar un Filtro.";
            //    Valido = false;
            //}
            #endregion

            if (Valido == true)
            {            
                ModeloTmp.ResultadoListaLlamadasSP = ModeloTmp.EncuentraLasLlamadasSP(ModeloTmp.Filtro, ModeloTmp.FechaInicial, ModeloTmp.FechaFinal, ModeloTmp.Actividad, ModeloTmp.Embarcacion, ModeloTmp.Ofp);
            }

            //Session["SessionResultadoListaLlamadasSP"] = ModeloTmp.ResultadoListaLlamadasSP;

            //ViewBag.actividad = FunProc.LlenarResultadoGenerico("btactividades");
            //ViewBag.ofp = FunProc.LlenarResultadoGenerico("responsableofp");
            //ViewBag.filtro = FunProc.LlenarResultadoGenerico("filtro");

            string ParametrosDeBusqueda = "";            
            ParametrosDeBusqueda += "Desde " + ModeloTmp.FechaInicial.ToShortDateString() + " Hasta " + ModeloTmp.FechaFinal.ToShortDateString() + " |";
            if (ModeloTmp.Embarcacion != null && ModeloTmp.Embarcacion.Length > 0)
                ParametrosDeBusqueda += " Embarcacion: " + ModeloTmp.Embarcacion + " |";
            if (ModeloTmp.Actividad != null && ModeloTmp.Actividad.Length > 0)
                ParametrosDeBusqueda += " Actividad: " + ModeloTmp.Actividad + " |";
            if (ModeloTmp.Filtro != null && ModeloTmp.Filtro.Length > 0)
                ParametrosDeBusqueda += " Filtro: " + ModeloTmp.Filtro + " |";
            if (ModeloTmp.Ofp != null && ModeloTmp.Ofp.Length > 0)
                ParametrosDeBusqueda += " OFP: " + ModeloTmp.Ofp + " |";
           
            if (ParametrosDeBusqueda.Length > 0)
                ParametrosDeBusqueda = ParametrosDeBusqueda.Substring(0, ParametrosDeBusqueda.Length - 1);


            //Session["SessionParametrosDeBusqueda"] = ParametrosDeBusqueda;

            return View(ModeloTmp);
        }

        public static string EncodePassword(string originalPassword)
        {
            int nHash = originalPassword.GetHashCode();
            SHA1 sha1 = new SHA1CryptoServiceProvider();
            byte[] inputBytes = (new UnicodeEncoding()).GetBytes(originalPassword);
            byte[] hash = sha1.ComputeHash(inputBytes);

            return Convert.ToBase64String(hash);
        }   

        // GET: /Llamadas/Details/5
        public ActionResult Details(int id = 0)
        {
            if (id == 0)
            {
                return RedirectToAction("Index");
            }
            LlamadaModel ModeloTmp = new LlamadaModel();
            TMasBTBitaCel tmasbtbitacel = null;//db.TMasBTBitaCels.Single(t => t.IdBitaCel == id);
            ModeloTmp.TMASBTBITACEL = tmasbtbitacel;

            List<TMasBTControlTelefonos> ListadeTelefonos = new List<TMasBTControlTelefonos>();
            ListadeTelefonos = ModeloTmp.EncuentraLasLlamadasEnDBSP(id);
            ModeloTmp.TMASBTCONTROLTELEFONOS_LISTA = ListadeTelefonos;

            if (tmasbtbitacel == null)
            {
                return StatusCode(404);
            }

            List<BuscaCargaImagenesTipo_Result> ListaImagenesTmp = new List<BuscaCargaImagenesTipo_Result>();
            //ListaImagenesTmp = FunProc.ObtieneListaCargaImagenesTipo(tmasbtbitacel.IdBitaCel.ToString(), "", "LLAMADAS TELEFÓNICAS");
            //Session["SessionImagenesLlamadas"] = ListaImagenesTmp;

            return View(ModeloTmp);
        }
       
        // GET: /Llamadas/Create
        public ActionResult Create(LlamadaModel ModeloTmp2, string submitButton, string bttipos)
        {
            #region "Usados para la sección Llamadas"
            LlamadaModel ModeloTmp = new LlamadaModel();
            ModeloTmp.MostrandoBusquedaInformacionEmbarcacion = false;
            List<SPMASBuscaInformacionDeEmbarcacionyPangas_Result> ModeloInformacionDeEmbarcacion = new List<SPMASBuscaInformacionDeEmbarcacionyPangas_Result>();
            ModeloTmp.InformacionDeEmbarcacion = ModeloInformacionDeEmbarcacion;
            
            ModeloTmp.Identificador = "0";
            //ViewBag.opcionesbusqueda = new SelectList(FunProc.LlenarResultadoGenerico("opcionesbusqueda"), "Resultado", "Resultado");
            //ViewBag.actividad = new SelectList(FunProc.LlenarResultadoGenerico("btactividades"), "Resultado", "Resultado");
            //ViewBag.ofp = new SelectList(FunProc.LlenarResultadoGenerico("responsableofp"), "Resultado", "Resultado");
            //ViewBag.filtro = new SelectList(FunProc.LlenarResultadoGenerico("filtro"), "Resultado", "Resultado");
            //ViewBag.bttipos = new SelectList(FunProc.LlenarResultadoGenerico("bttipos"), "Resultado", "Resultado");
            //ViewBag.respuestallamada = new SelectList(FunProc.LlenarResultadoGenerico("respuestallamada"), "Resultado", "Resultado");
            //List<SelectListItem> respuestaList = new List<SelectListItem>();
            //respuestaList.Add(new SelectListItem() { Text = Resources.Etiquetas.SI });
            //respuestaList.Add(new SelectListItem() { Text = Resources.Etiquetas.NO });
            //ViewBag.respuestallamada = respuestaList;
            #endregion

            TMasBTControlTelefonos TelefonoInicial = new TMasBTControlTelefonos();
            TelefonoInicial.IdBitaCel = 0;
            TelefonoInicial.Tipo = "";
            TelefonoInicial.Numero = "";
            TelefonoInicial.Fecha = DateTime.Now;
            TelefonoInicial.Hora = DateTime.Now.TimeOfDay.ToString().Substring(0,8);
           
           
            ModeloTmp.TMASBTCONTROLTELEFONOS = TelefonoInicial;      
            List<TMasBTControlTelefonos> ListadeTelefonos = new List<TMasBTControlTelefonos>();
            ModeloTmp.TMASBTCONTROLTELEFONOS_LISTA = ListadeTelefonos;

            return View(ModeloTmp);
        }
        
        // POST: /Llamadas/Create
        [HttpPost]
        public ActionResult Create(LlamadaModel ModeloTmp, string submitButton, string IdentificadorFinal, string bttipos, string actividad, string ofp, string opcionesbusqueda, string txtvalor, string numero, string respuestallamada)
        {
            ModeloTmp.MostrandoBusquedaInformacionEmbarcacion = false;
            
            List<SPMASBuscaInformacionDeEmbarcacionyPangas_Result> ModeloInformacionDeEmbarcacion = new List<SPMASBuscaInformacionDeEmbarcacionyPangas_Result>();
            ModeloInformacionDeEmbarcacion = ModeloTmp.EncuentraInformacionDeEmbarcacionPangasSP(opcionesbusqueda, txtvalor);
            ModeloTmp.InformacionDeEmbarcacion = ModeloInformacionDeEmbarcacion;

            List<TMasBTControlTelefonos> ListadeTelefonos = new List<TMasBTControlTelefonos>();
           
            if (IdentificadorFinal != null && IdentificadorFinal.Length > 0 && IdentificadorFinal != "0")
            ModeloTmp.TMASBTCONTROLTELEFONOS.IdBitaCel = Convert.ToInt16(IdentificadorFinal);
            
            #region "Usados para la sección Llamadas"          
            //ViewBag.opcionesbusqueda = new SelectList(FunProc.LlenarResultadoGenerico("opcionesbusqueda"), "Resultado", "Resultado");
            //ViewBag.actividad = new SelectList(FunProc.LlenarResultadoGenerico("btactividades"), "Resultado", "Resultado");
            //ViewBag.ofp = new SelectList(FunProc.LlenarResultadoGenerico("responsableofp"), "Resultado", "Resultado");
            //ViewBag.filtro = new SelectList(FunProc.LlenarResultadoGenerico("filtro"), "Resultado", "Resultado");
            //ViewBag.bttipos = new SelectList(FunProc.LlenarResultadoGenerico("bttipos"), "Resultado", "Resultado");
            //ViewBag.respuestallamada = new SelectList(FunProc.LlenarResultadoGenerico("respuestallamada"), "Resultado", "Resultado");
            //List<SelectListItem> respuestaList = new List<SelectListItem>();
            //respuestaList.Add(new SelectListItem() { Text = Resources.Etiquetas.SI });
            //respuestaList.Add(new SelectListItem() { Text = Resources.Etiquetas.NO });
            //ViewBag.respuestallamada = respuestaList;
            #endregion

            switch (submitButton)
            {
                case "Guardar":                  
                    if (ModelState.IsValid)
                    {
                        #region "VALIDACIONES"
                        Boolean Valido = true;
                        if (ModeloTmp.TMASBTBITACEL.Fecha != null)
                        {
                            if (ModeloTmp.TMASBTBITACEL.Fecha > DateTime.Now.Date)
                            {
                                ViewBag.NoValidoPor = "La fecha no puede ser mayor a la fecha actual, Verificar Fechas.";
                                Valido = false;
                            }
                            if (ModeloTmp.TMASBTBITACEL.Fecha.Value.Year < 1753 || ModeloTmp.TMASBTBITACEL.Fecha.Value.Year > 2030)
                            {
                                ViewBag.NoValidoPor = "No es posible completar la acción, Verificar Fechas.";
                                Valido = false;
                            }
                           
                            //Con esto le damos el formato y la hora correcta para que se guarde.
                            if (Valido == true)
                            {
                                string soloFecha = ModeloTmp.TMASBTBITACEL.Fecha.ToString();
                                soloFecha = soloFecha.Substring(Convert.ToInt32(Resources.Etiquetas.Num1SoloFecha), Convert.ToInt32(Resources.Etiquetas.Num2SoloFecha));
                                string soloHoraHoy = DateTime.Now.ToString("HH:mm:ss");
                                //soloHoraHoy = soloHoraHoy.Substring(Convert.ToInt32(Resources.Etiquetas.Num1SoloHoraHoy), Convert.ToInt32(Resources.Etiquetas.Num2SoloHoraHoy));
                                ModeloTmp.TMASBTBITACEL.Fecha = Convert.ToDateTime(soloFecha + " " + soloHoraHoy);
                            }
                        }
                        else
                        {
                            ViewBag.NoValidoPor = "No es posible completar la acción, la fecha de la cedula es requerida.";
                            Valido = false;
                        }                                          
                        #endregion
                        if (Valido == true)
                        {                        
                            //Asigna los datos pendientes al modelo
                            //ModeloTmp.TMASBTBITACEL.Actividad = actividad;
                            //ModeloTmp.TMASBTBITACEL.OFP = ofp;
                            //Valida si no faltan campos por capturar
                            string Errores = "";
                            if (string.IsNullOrWhiteSpace(ModeloTmp.TMASBTBITACEL.Actividad) || string.IsNullOrWhiteSpace(ModeloTmp.TMASBTBITACEL.OFP))
                            {
                                Errores = "Verificar Datos :";
                                if(string.IsNullOrWhiteSpace(ModeloTmp.TMASBTBITACEL.Actividad))
                                    Errores = Errores + " Actividad,";
                                if (string.IsNullOrWhiteSpace(ModeloTmp.TMASBTBITACEL.OFP))
                                    Errores = Errores + " OFP,";

                            }
                            if( Errores.Length > 0)   
                                ModelState.AddModelError("", Errores);
                            if (ModelState.IsValid)
                            {
                                //Inserta en TMasBTBitaCel
                                ModeloTmp.Identificador = ModeloTmp.InsertaBTBitaCel(ModeloTmp.TMASBTBITACEL).ToString();

                                //Inserta en TMasBTControlTelefonos //if (ModeloTmp.TMASBTCONTROLTELEFONOS_LISTA.Count > 0)//{
                                int IdBTBitacelFinal =Convert.ToInt32(ModeloTmp.Identificador);
                                int IdTemporal = Convert.ToInt32( IdentificadorFinal);
                                int Cantidad = ModeloTmp.InsertaLlamada(IdBTBitacelFinal, IdTemporal);
                                //}//return RedirectToAction("Index");
                                return RedirectToAction("Details", "Llamadas", new { id= IdBTBitacelFinal });
                            }
                        }
                    }
                    break;
                //case "Agregar Telefono":
                //    //Se comento por que asi inserta con la hora del momento -->    ModeloTmp.TMASBTCONTROLTELEFONOS.Hora = DateTime.Now.TimeOfDay.ToString().Substring(0, 8);

                //   long  Numero = 0;
                //    long.TryParse(numero, out Numero);

                //    if (Numero > 0)
                //    {
                //        #region "VALIDACIONES"
                //        Boolean Valido = true;
                //        if (ModeloTmp.TMASBTCONTROLTELEFONOS.Fecha != null)
                //        {
                //            if (ModeloTmp.TMASBTCONTROLTELEFONOS.Fecha > DateTime.Now.Date)
                //            {
                //                ViewBag.NoValidoPor = "La fecha no puede ser mayor a la fecha actual, Verificar Fechas.";
                //                Valido = false;
                //            }
                //            if (ModeloTmp.TMASBTCONTROLTELEFONOS.Fecha.Value.Year < 1753 || ModeloTmp.TMASBTCONTROLTELEFONOS.Fecha.Value.Year > 2030)
                //            {
                //                ViewBag.NoValidoPor = "No es posible completar la acción, Verificar Fechas.";
                //                Valido = false;
                //            }

                //            Regex Val = new Regex(@"^[0-2][0-9]:[0-5][0-9]:[0-5][0-9]$");
                //            if (ModeloTmp.TMASBTCONTROLTELEFONOS.Hora != null)
                //            {
                //                if (Val.IsMatch(ModeloTmp.TMASBTCONTROLTELEFONOS.Hora))
                //                {
                //                    string x = ModeloTmp.TMASBTCONTROLTELEFONOS.Hora.Substring(0, 2);
                //                    if (Convert.ToInt16(x) > 23)
                //                    {
                //                        ViewBag.NoValidoPor = Resources.Etiquetas.HoraNoValida;
                //                        Valido = false;
                //                    }
                //                }
                //                else
                //                {
                //                    ViewBag.NoValidoPor = Resources.Etiquetas.HoraNoFormatoCorrecto;
                //                    Valido = false;
                //                }
                //            }
                //            else
                //            {
                //                ViewBag.NoValidoPor = Resources.Etiquetas.IngreseHora;
                //                Valido = false;
                //            }


                //            if (Valido == true)
                //            {
                //                string soloFechaTel = ModeloTmp.TMASBTCONTROLTELEFONOS.Fecha.ToString();
                //                soloFechaTel = soloFechaTel.Substring(Convert.ToInt32(Resources.Etiquetas.Num1SoloFechaTel), Convert.ToInt32(Resources.Etiquetas.Num2SoloFechaTel));
                //                string soloHoraHoyTel = ModeloTmp.TMASBTCONTROLTELEFONOS.Hora.ToString();
                //                soloHoraHoyTel = soloHoraHoyTel.Substring(Convert.ToInt32(Resources.Etiquetas.Num1SoloHoraHoyTel), Convert.ToInt32(Resources.Etiquetas.Num2SoloHoraHoyTel));
                //                ModeloTmp.TMASBTCONTROLTELEFONOS.Fecha = Convert.ToDateTime(soloFechaTel + " " + soloHoraHoyTel);
                //            }

                //        }
                //        else
                //        {
                //            ViewBag.NoValidoPor = "No es posible completar la acción, La fecha en datos de llamada es requerida.";
                //            Valido = false;
                //        }
                //        #endregion
                //        if (Valido == true)
                //        {                        
                //            ModeloTmp.TMASBTCONTROLTELEFONOS.Numero = numero;
                //            //ModeloTmp.TMASBTCONTROLTELEFONOS.Tipo = Numero;
                //            ModeloTmp.TMASBTCONTROLTELEFONOS.Tipo = bttipos;
                //            bool respuesta = false;
                //            if (respuestallamada == "SI") respuesta = true;
                //            if (respuestallamada == "NO") respuesta = false;
                //            ModeloTmp.TMASBTCONTROLTELEFONOS.Respuesta = respuesta;
                //            ModeloTmp.TMASBTCONTROLTELEFONOS.IdBitaCel = ModeloTmp.InsertaLlamadaTmp(ModeloTmp.TMASBTCONTROLTELEFONOS);
                //            ModeloTmp.Identificador = ModeloTmp.TMASBTCONTROLTELEFONOS.IdBitaCel.ToString();
                //            //ModeloTmp.TMASBTCONTROLTELEFONOS = TelefonoInicial;                    
                //            ListadeTelefonos = ModeloTmp.EncuentraLasLlamadasTMPSP(ModeloTmp.TMASBTCONTROLTELEFONOS);
                //        }
                //    }
                //    else
                //    {
                //        ModelState.AddModelError("", "Telefono no es un numero valido, Verificar.");
                //    }
                    
                   
                //    //ModeloTmp.TMASBTCONTROLTELEFONOS_LISTA = ModeloTmp.EncuentraLasLlamadasTMPSP(ModeloTmp.TMASBTCONTROLTELEFONOS);
                //    break;
                //case "Buscar Informacion":
                //    ModeloTmp.MostrandoBusquedaInformacionEmbarcacion = true;
                //    break;
                ////return View(Send(ModeloTmp));
                default:
                    break;
            }

            //ModeloTmp.TMASBTCONTROLTELEFONOS_LISTA = ListadeTelefonos;

            return View(ModeloTmp);
        }

        [HttpPost]
        public ActionResult AgregarLlamadaTmp(TMasBTControlTelefonos datos)
        {
            LlamadaModel ModeloTmp = new LlamadaModel();
            StringBuilder msgs = new StringBuilder();
            Boolean Valido = true;

            if (!string.IsNullOrWhiteSpace(datos.Numero))
            {
                #region "VALIDACIONES"
                if (datos.Fecha != null)
                {
                    if (datos.Fecha.Value.Date > DateTime.Now.Date)
                    {
                        msgs.AppendLine("La fecha no puede ser mayor a la fecha actual, Verificar Fechas.");
                        Valido = false;
                    }
                    if (datos.Fecha.Value.Year < 1753 || datos.Fecha.Value.Year > 2030)
                    {
                        msgs.AppendLine("No es posible completar la acción, Verificar Fechas.");
                        Valido = false;
                    }

                    if (datos.Tipo == null)
                    {
                        ViewBag.NoValidoPor = "Debe Seleccionar un Tipo";
                        Valido = false;
                    }
                    if (datos.Numero == "(__) ____-____")
                    {
                        ViewBag.NoValidoPor = "Debe Seleccionar un Número";
                        Valido = false;
                    }
                    if (datos.Respuesta == null )
                    {
                        ViewBag.NoValidoPor = "Debe Seleccionar una Respuesta";
                        Valido = false;
                    }

                    Regex Val = new Regex(@"^[0-2][0-9]:[0-5][0-9]:[0-5][0-9]$");
                    if (datos.Hora != null)
                    {
                        if (Val.IsMatch(datos.Hora))
                        {
                            string x = datos.Hora.Substring(0, 2);
                            if (Convert.ToInt16(x) > 23)
                            {
                                msgs.AppendLine(Resources.Etiquetas.HoraNoValida);
                                Valido = false;
                            }
                        }
                        else
                        {
                            msgs.AppendLine(Resources.Etiquetas.HoraNoFormatoCorrecto);
                            Valido = false;
                        }
                    }
                    else
                    {
                        msgs.AppendLine(Resources.Etiquetas.IngreseHora);
                        Valido = false;
                    }

                    if (Valido == true)
                    {
                        string soloFechaTel = datos.Fecha.ToString();
                        soloFechaTel = soloFechaTel.Substring(Convert.ToInt32(Resources.Etiquetas.Num1SoloFechaTel), Convert.ToInt32(Resources.Etiquetas.Num2SoloFechaTel));
                        string soloHoraHoyTel = datos.Hora.ToString();
                        //soloHoraHoyTel = soloHoraHoyTel.Substring(Convert.ToInt32(Resources.Etiquetas.Num1SoloHoraHoyTel), Convert.ToInt32(Resources.Etiquetas.Num2SoloHoraHoyTel));
                        datos.Fecha = Convert.ToDateTime(soloFechaTel + " " + soloHoraHoyTel);
                    }
                }
                else
                {
                    msgs.AppendLine("No es posible completar la acción, La fecha en datos de llamada es requerida.");
                    Valido = false;
                }
                #endregion
                if (Valido == true)
                {
                    //ModeloTmp.TMASBTCONTROLTELEFONOS.Numero = numero;
                    //ModeloTmp.TMASBTCONTROLTELEFONOS.Tipo = bttipos;
                    //bool respuesta = false;
                    //if (respuestallamada == "SI") respuesta = true;
                    //if (respuestallamada == "NO") respuesta = false;
                    //ModeloTmp.TMASBTCONTROLTELEFONOS.Respuesta = respuesta;
                    datos.Numero = datos.Numero.Replace("_", "").Replace("(", "").Replace(")", "");
                    datos.IdBitaCel = ModeloTmp.InsertaLlamadaTmp(datos);
                    ModeloTmp.Identificador = datos.IdBitaCel.ToString();
                    //ModeloTmp.TMASBTCONTROLTELEFONOS = TelefonoInicial;                    
                    ModeloTmp.TMASBTCONTROLTELEFONOS_LISTA = ModeloTmp.EncuentraLasLlamadasTMPSP(datos);
                }
            }
            else
            {
                msgs.AppendLine("Telefono no es un numero valido, Verificar.");
            }

            return Content(JsonConvert.SerializeObject(new { errores = msgs.ToString(), valido = Valido, identificador = ModeloTmp.Identificador, lista = ModeloTmp.TMASBTCONTROLTELEFONOS_LISTA }), "application/json");
        }

        // GET: /Llamadas/Edit/5
        public ActionResult Edit(int id = 0)
        {
            #region "Usados para llenar los ddl"
            //ViewBag.opcionesbusqueda = new SelectList(FunProc.LlenarResultadoGenerico("opcionesbusqueda"), "Resultado", "Resultado");
            //ViewBag.actividad = new SelectList(FunProc.LlenarResultadoGenerico("btactividades"), "Resultado", "Resultado");
            //ViewBag.ofp = new SelectList(FunProc.LlenarResultadoGenerico("responsableofp"), "Resultado", "Resultado");
            //ViewBag.filtro = new SelectList(FunProc.LlenarResultadoGenerico("filtro"), "Resultado", "Resultado");
            //ViewBag.bttipos = new SelectList(FunProc.LlenarResultadoGenerico("bttipos"), "Resultado", "Resultado");
            #endregion

            LlamadaModel ModeloTmp = new LlamadaModel();
            TMasBTBitaCel tmasbtbitacel = null;//db.TMasBTBitaCels.Single(t => t.IdBitaCel == id);
            ModeloTmp.TMASBTBITACEL = tmasbtbitacel;

            TMasBTControlTelefonos TelefonoInicial = new TMasBTControlTelefonos();
            TelefonoInicial.Fecha = DateTime.Now;
            TelefonoInicial.Hora = DateTime.Now.TimeOfDay.ToString().Substring(0, 8);
            ModeloTmp.TMASBTCONTROLTELEFONOS = TelefonoInicial;

            ModeloTmp.MostrandoBusquedaInformacionEmbarcacion = false;
            List<SPMASBuscaInformacionDeEmbarcacionyPangas_Result> ModeloInformacionDeEmbarcacion = new List<SPMASBuscaInformacionDeEmbarcacionyPangas_Result>();
            ModeloTmp.InformacionDeEmbarcacion = ModeloInformacionDeEmbarcacion;

            List<TMasBTControlTelefonos> ListadeTelefonos = new List<TMasBTControlTelefonos>();
            ListadeTelefonos = ModeloTmp.EncuentraLasLlamadasEnDBSP(id);
            ModeloTmp.TMASBTCONTROLTELEFONOS_LISTA = ListadeTelefonos;

            //List<SelectListItem> respuestaList = new List<SelectListItem>();
            //respuestaList.Add(new SelectListItem() { Text = Resources.Etiquetas.SI, Selected = true });
            //respuestaList.Add(new SelectListItem() { Text = Resources.Etiquetas.NO, Selected = false });
            //ViewBag.respuestallamada = respuestaList;

            if (tmasbtbitacel == null)
            {
                return StatusCode(404);
            }
            return View(ModeloTmp);
        }
        
        // POST: /Llamadas/Edit/5
       [HttpPost]
        public ActionResult Edit(LlamadaModel ModeloTmp, string submitButton, string bttipos, string actividad, string ofp, string opcionesbusqueda, string txtvalor, string txtidcontrol, string numero, string respuestallamada)
        {
            #region "Usados para llenar los ddl"
            //ViewBag.opcionesbusqueda = new SelectList(FunProc.LlenarResultadoGenerico("opcionesbusqueda"), "Resultado", "Resultado");
            //ViewBag.actividad = new SelectList(FunProc.LlenarResultadoGenerico("btactividades"), "Resultado", "Resultado");
            //ViewBag.ofp = new SelectList(FunProc.LlenarResultadoGenerico("responsableofp"), "Resultado", "Resultado");
            //ViewBag.filtro = new SelectList(FunProc.LlenarResultadoGenerico("filtro"), "Resultado", "Resultado");
            //ViewBag.bttipos = new SelectList(FunProc.LlenarResultadoGenerico("bttipos"), "Resultado", "Resultado");
            //ViewBag.respuestallamada = new SelectList(FunProc.LlenarResultadoGenerico("respuestallamada"), "Resultado", "Resultado");
            //List<SelectListItem> respuestaList = new List<SelectListItem>();
            //respuestaList.Add(new SelectListItem() { Text = Resources.Etiquetas.SI });
            //respuestaList.Add(new SelectListItem() { Text = Resources.Etiquetas.NO });
            //ViewBag.respuestallamada = respuestaList;
            #endregion
            List<SPMASBuscaInformacionDeEmbarcacionyPangas_Result> ModeloInformacionDeEmbarcacion = new List<SPMASBuscaInformacionDeEmbarcacionyPangas_Result>();
            ModeloInformacionDeEmbarcacion = ModeloTmp.EncuentraInformacionDeEmbarcacionPangasSP(opcionesbusqueda, txtvalor);
            ModeloTmp.InformacionDeEmbarcacion = ModeloInformacionDeEmbarcacion;
            switch (submitButton)
            {
                case "Guardar":                    
                    if (ModelState.IsValid)
                    {
                        #region "VALIDACIONES"
                        Boolean Valido = true;
                        if (ModeloTmp.TMASBTBITACEL.Fecha > DateTime.Now.Date )
                        {
                            ViewBag.NoValidoPor = "La fecha no puede ser mayor a la fecha actual, Verificar Fechas.";
                            Valido = false;
                        }
                        if (ModeloTmp.TMASBTBITACEL.Fecha.Value.Year < 1753 || ModeloTmp.TMASBTBITACEL.Fecha.Value.Year > 2030)
                        {
                            ViewBag.NoValidoPor = "No es posible completar la acción, Verificar Fechas.";
                            Valido = false;
                        }
                        if (respuestallamada == "")
                        {
                            ViewBag.NoValidoPor = "La respuesta no puede estar vacía, Verificar.";
                            Valido = false;
                        }

                        if (Valido == true)
                        {
                            string soloFecha = ModeloTmp.TMASBTBITACEL.Fecha.ToString();
                            soloFecha = soloFecha.Substring(Convert.ToInt32(Resources.Etiquetas.Num1SoloFecha), Convert.ToInt32(Resources.Etiquetas.Num2SoloFecha));
                            string soloHoraHoy = DateTime.Now.ToString("HH:mm:ss");
                            //soloHoraHoy = soloHoraHoy.Substring(Convert.ToInt32(Resources.Etiquetas.Num1SoloHoraHoy), Convert.ToInt32(Resources.Etiquetas.Num2SoloHoraHoy));
                            ModeloTmp.TMASBTBITACEL.Fecha = Convert.ToDateTime(soloFecha + " " + soloHoraHoy);
                        }
                        
                        #endregion
                        if (Valido == true)
                        {                        
                            //Asigna los datos pendientes al modelo
                            ModeloTmp.TMASBTCONTROLTELEFONOS.IdBitaCel = ModeloTmp.TMASBTBITACEL.IdBitaCel;
                            //ModeloTmp.TMASBTBITACEL.Actividad = actividad;
                            //ModeloTmp.TMASBTBITACEL.OFP = ofp;
                            
                           
                            //Valida si no faltan campos por capturar
                            string Errores = "";
                            if (string.IsNullOrWhiteSpace(ModeloTmp.TMASBTBITACEL.Actividad) || string.IsNullOrWhiteSpace(ModeloTmp.TMASBTBITACEL.OFP))
                            {
                                Errores = "Verificar Datos :";
                                if(string.IsNullOrWhiteSpace(ModeloTmp.TMASBTBITACEL.Actividad))
                                    Errores = Errores + " Actividad,";
                                if (string.IsNullOrWhiteSpace(ModeloTmp.TMASBTBITACEL.OFP))
                                    Errores = Errores + " OFP,";
                            }
                            if( Errores.Length > 0)   
                                ModelState.AddModelError("", Errores);
                            if (ModelState.IsValid)
                            {
                                //Actualiza el registro en TMasBTBitaCel
                                ModeloTmp.Identificador = ModeloTmp.ActualizaBTBitaCel(ModeloTmp.TMASBTBITACEL).ToString();
                                return RedirectToAction("Details", "Llamadas", new { id = ModeloTmp.Identificador });
                            }
                        }
                    }
                    break;
                //case "Agregar Telefono":
                //    //Hace el insert y no se trae los datos, para obtenerlos usa EncuentraLasLlamadasEnDBSP 
                //    long  Numero = 0;
                //    long.TryParse(numero, out Numero);

                //    if (Numero > 0)
                //    {
                //        #region "VALIDACIONES"
                //        Boolean Valido = true;
                //        if (ModeloTmp.TMASBTCONTROLTELEFONOS.Fecha > DateTime.Now.Date)
                //        {
                //            ViewBag.NoValidoPor = "La fecha no puede ser mayor a la fecha actual, Verificar Fechas.";
                //            Valido = false;
                //        }                       
                //        if (ModeloTmp.TMASBTCONTROLTELEFONOS.Fecha.Value.Year < 1753 || ModeloTmp.TMASBTCONTROLTELEFONOS.Fecha.Value.Year > 2030)
                //        {
                //            ViewBag.NoValidoPor = "No es posible completar la acción, Verificar Fechas.";
                //            Valido = false;
                //        }

                //        //AGREGADO POR SZ EL 09/02/2018
                        

                //        Regex Val = new Regex(@"^[0-2][0-9]:[0-5][0-9]:[0-5][0-9]$");
                //        if (ModeloTmp.TMASBTCONTROLTELEFONOS.Hora != null)
                //        {
                //            if (Val.IsMatch(ModeloTmp.TMASBTCONTROLTELEFONOS.Hora))
                //            {
                //                string x = ModeloTmp.TMASBTCONTROLTELEFONOS.Hora.Substring(0, 2);
                //                if (Convert.ToInt16(x) > 23)
                //                {
                //                    ViewBag.NoValidoPor = Resources.Etiquetas.HoraNoValida;
                //                    Valido = false;
                //                }
                //            }
                //            else
                //            {
                //                ViewBag.NoValidoPor = Resources.Etiquetas.HoraNoFormatoCorrecto;
                //                Valido = false;
                //            }
                //        }
                //        else
                //        {
                //            ViewBag.NoValidoPor = Resources.Etiquetas.IngreseHora;
                //            Valido = false;
                //        }

                //        if (Valido == true)
                //        {
                //            string soloFechaTel = ModeloTmp.TMASBTCONTROLTELEFONOS.Fecha.ToString();
                //            soloFechaTel = soloFechaTel.Substring(Convert.ToInt32(Resources.Etiquetas.Num1SoloFechaTel), Convert.ToInt32(Resources.Etiquetas.Num2SoloFechaTel));
                //            string soloHoraHoyTel = ModeloTmp.TMASBTCONTROLTELEFONOS.Hora.ToString();
                //            soloHoraHoyTel = soloHoraHoyTel.Substring(Convert.ToInt32(Resources.Etiquetas.Num1SoloHoraHoyTel), Convert.ToInt32(Resources.Etiquetas.Num2SoloHoraHoyTel));
                //            ModeloTmp.TMASBTCONTROLTELEFONOS.Fecha = Convert.ToDateTime(soloFechaTel + " " + soloHoraHoyTel);
                //        }
                //        //FIN-AGREGADO POR SZ EL 09/02/2018

                //        #endregion
                //        if (Valido == true)
                //        {                        
                //            ModeloTmp.TMASBTCONTROLTELEFONOS.Numero = numero;
                //            ModeloTmp.TMASBTCONTROLTELEFONOS.Tipo = bttipos;
                //            int SeInserto = 0;
                //            ModeloTmp.TMASBTCONTROLTELEFONOS.IdBitaCel = ModeloTmp.TMASBTBITACEL.IdBitaCel;
                //            bool respuesta = false;
                //            if (respuestallamada == Resources.Etiquetas.SI) respuesta = true;
                //            if (respuestallamada == Resources.Etiquetas.NO) respuesta = false;
                //            ModeloTmp.TMASBTCONTROLTELEFONOS.Respuesta = respuesta;
                //            SeInserto = ModeloTmp.InsertaLlamadaEnEditar(ModeloTmp.TMASBTCONTROLTELEFONOS);
                //        }
                //    }
                //    else
                //    {
                //        ModelState.AddModelError("", "Telefono no es un numero valido, Verificar.");
                //    }
                                        
                //    break;
                //case "Buscar Informacion":
                //    ModeloTmp.MostrandoBusquedaInformacionEmbarcacion = true;
                //    break;
                //case "Borrar Llamada":
                //    if (txtidcontrol != "")
                //    {
                //        int SeBorro = 0;
                //        ModeloTmp.TMASBTCONTROLTELEFONOS.IdBitaCel = ModeloTmp.TMASBTBITACEL.IdBitaCel;
                //        ModeloTmp.TMASBTCONTROLTELEFONOS.IdControl = Convert.ToInt32( txtidcontrol);
                //        SeBorro = ModeloTmp.BorraLlamadaEnEditar(ModeloTmp.TMASBTCONTROLTELEFONOS);
                //    }
                //    break;
                default:
                    break;
            }

            List<TMasBTControlTelefonos> ListadeTelefonos = new List<TMasBTControlTelefonos>();
            ListadeTelefonos = ModeloTmp.EncuentraLasLlamadasEnDBSP(ModeloTmp.TMASBTBITACEL.IdBitaCel);
            ModeloTmp.TMASBTCONTROLTELEFONOS_LISTA = ListadeTelefonos;
            return View(ModeloTmp);
        }

        [HttpPost]
        public ActionResult AgregarLlamada(TMasBTControlTelefonos datos)
        {
            LlamadaModel ModeloTmp = new LlamadaModel();
            StringBuilder msgs = new StringBuilder();
            Boolean Valido = true;

            if (!string.IsNullOrWhiteSpace(datos.Numero))
            {
                #region "VALIDACIONES"
                if (datos.Fecha.Value.Date > DateTime.Now.Date)
                {
                    msgs.AppendLine("La fecha no puede ser mayor a la fecha actual, Verificar Fechas.");
                    Valido = false;
                }
                if (datos.Fecha.Value.Year < 1753 || datos.Fecha.Value.Year > 2030)
                {
                    msgs.AppendLine("No es posible completar la acción, Verificar Fechas.");
                    Valido = false;
                }

                Regex Val = new Regex(@"^[0-2][0-9]:[0-5][0-9]:[0-5][0-9]$");
                if (datos.Hora != null)
                {
                    if (Val.IsMatch(datos.Hora))
                    {
                        string x = datos.Hora.Substring(0, 2);
                        if (Convert.ToInt16(x) > 23)
                        {
                            msgs.AppendLine(Resources.Etiquetas.HoraNoValida);
                            Valido = false;
                        }
                    }
                    else
                    {
                        msgs.AppendLine(Resources.Etiquetas.HoraNoFormatoCorrecto);
                        Valido = false;
                    }
                } else {
                    msgs.AppendLine(Resources.Etiquetas.IngreseHora);
                    Valido = false;
                }

                if (Valido == true)
                {
                    string soloFechaTel = datos.Fecha.ToString();
                    soloFechaTel = soloFechaTel.Substring(Convert.ToInt32(Resources.Etiquetas.Num1SoloFechaTel), Convert.ToInt32(Resources.Etiquetas.Num2SoloFechaTel));

                    string soloHoraHoyTel = datos.Hora.ToString();
                    soloHoraHoyTel = soloHoraHoyTel.Substring(Convert.ToInt32(Resources.Etiquetas.Num1SoloHoraHoyTel), Convert.ToInt32(Resources.Etiquetas.Num2SoloHoraHoyTel));

                    datos.Fecha = Convert.ToDateTime(soloFechaTel + " " + soloHoraHoyTel);
                }
                //FIN-AGREGADO POR SZ EL 09/02/2018
                #endregion

                if (Valido == true)
                {
                    datos.Numero = datos.Numero.Replace("_", "").Replace("(", "").Replace(")", "");
                    ModeloTmp.InsertaLlamadaEnEditar(datos);
                    ModeloTmp.TMASBTCONTROLTELEFONOS_LISTA = ModeloTmp.EncuentraLasLlamadasEnDBSP(datos.IdBitaCel);
                }
            } else {
                msgs.AppendLine("Telefono no es un numero valido, Verificar.");
            }

            return Content(JsonConvert.SerializeObject(new { errores = msgs.ToString(), valido = Valido, lista = ModeloTmp.TMASBTCONTROLTELEFONOS_LISTA }), "application/json");
        }

        [HttpPost]
        public ActionResult BorrarLlamada(TMasBTControlTelefonos datos)
        {
            LlamadaModel ModeloTmp = new LlamadaModel();

            if (datos.IdControl > 0 && datos.IdBitaCel > 0)
            {
                ModeloTmp.BorraLlamadaEnEditar(datos);
            }

            ModeloTmp.TMASBTCONTROLTELEFONOS_LISTA = ModeloTmp.EncuentraLasLlamadasEnDBSP(datos.IdBitaCel);

            return Content(JsonConvert.SerializeObject(ModeloTmp.TMASBTCONTROLTELEFONOS_LISTA), "application/json");
        }

        [HttpPost]
        public ActionResult BuscarEmbarcaciones(string opcionesbusqueda, string valor)
        {
            LlamadaModel ModeloTmp = new LlamadaModel();

            ModeloTmp.InformacionDeEmbarcacion = ModeloTmp.EncuentraInformacionDeEmbarcacionPangasSP(opcionesbusqueda, valor);

            return Content(JsonConvert.SerializeObject(ModeloTmp.InformacionDeEmbarcacion), "application/json");
        }

        // GET: /Llamadas/Delete/5
        public ActionResult Delete(int id = 0)
        {
            TMasBTBitaCel tmasbtbitacel = null;//db.TMasBTBitaCels.Single(t => t.IdBitaCel == id);
            if (tmasbtbitacel == null)
            {
                return StatusCode(404);
            }
            return View(tmasbtbitacel);
        }
      
        // POST: /Llamadas/Delete/5
        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            TMasBTBitaCel tmasbtbitacel = null;//db.TMasBTBitaCels.Single(t => t.IdBitaCel == id);
            //db.TMasBTBitaCels.DeleteObject(tmasbtbitacel);
            //db.TMasBTBitaCels.Remove(tmasbtbitacel);
            //db.SaveChanges();
            return RedirectToAction("Index");
        }

        #region "EXPORTAR A WORD"
        public ActionResult GeneraReporteLlamadaWord(int id) 
        {
            LlamadaModel ModeloTmp = new LlamadaModel();
            //      OBTIENE LOS DATOS DE LA LLAMADA
            TMasBTBitaCel Datos = null;//db.TMasBTBitaCels.Single(t => t.IdBitaCel == id);
            //ModeloTmp.TMASBTBITACEL = Datos;

            List<TMasBTControlTelefonos> ListadeTelefonos = new List<TMasBTControlTelefonos>();
            ListadeTelefonos = ModeloTmp.EncuentraLasLlamadasEnDBSP(id);
            //ModeloTmp.TMASBTCONTROLTELEFONOS_LISTA = ListadeTelefonos;


            DocX doc;
            string Plantilla = "Llamada";
            string ruta = Path.GetFileName("~/Templates/Llamadas/" + Plantilla + ".docx");
            string rutadestino = Path.GetFileName("~/Templates/GENERADOS/" + Plantilla + " " + FunProc.ObtieneIdentificadorFechaHora() + ".docx");
            if (System.IO.File.Exists(ruta) == true)
            {
                doc = DocX.Load(ruta);
                if (Datos != null)
                {
                    #region "ASIGNA VALOR A LOS CAMPOS CUANDO ES NULO"
                    if (Datos.Actividad == null) Datos.Actividad = "";
                    if (Datos.Barco == null) Datos.Barco = "";
                    if (Datos.RNP == null) Datos.RNP = "";
                    if (Datos.Matricula == null) Datos.Matricula = "";
                    if (Datos.RazonSocial == null) Datos.RazonSocial = "";
                    if (Datos.PuertoBase == null) Datos.PuertoBase = "";
                    //if (Datos.Fecha == null) Datos.Fecha = "";
                    if (Datos.Empresa == null) Datos.Empresa = "";
                    if (Datos.Atendio == null) Datos.Atendio = "";
                    if (Datos.Lugar == null) Datos.Lugar = "";
                    if (Datos.Descripcion == null) Datos.Descripcion = "";
                    if (Datos.OFP == null) Datos.OFP = "";

                    string respuestaLlamada = "";
                    switch (Datos.Respuesta)
                    {
                        case true:
                            respuestaLlamada = Resources.Etiquetas.SI;
                            break;
                        case false:
                            respuestaLlamada = Resources.Etiquetas.NO;
                            break;
                        default:
                            respuestaLlamada = Resources.Etiquetas.NA;
                            break;
                    }
                    #endregion

                    #region "LLENA LOS CAMPOS EN EL DOCUMENTO WORD"
                    //      LLENA LA INFORMACION DE LA LLAMADA
                    doc.ReplaceText("«ACTIVIDAD»", Datos.Actividad);
                    doc.ReplaceText("«BARCO»", Datos.Barco);
                    doc.ReplaceText("«RNP»", Datos.RNP);
                    doc.ReplaceText("«MATRICULA»", Datos.Matricula);
                    doc.ReplaceText("«RAZONSOCIAL»", Datos.RazonSocial);                    
                    doc.ReplaceText("«EMPRESA»", Datos.Empresa);
                    doc.ReplaceText("«ATENDIO»", Datos.Atendio);
                    doc.ReplaceText("«LUGAR»", Datos.Lugar);
                    doc.ReplaceText("«DESCRIPCION»", Datos.Descripcion);
                    doc.ReplaceText("«OFP»", Datos.OFP);
                    //doc.ReplaceText("«RESPUESTA»", respuestaLlamada);
                    #endregion

                    #region "CREA E INSERTA LA TABLA EN EL DOCUMENTO WORD"
                    //      LLENA LA RELACION DE LLAMADAS
                    if (ListadeTelefonos.Count > 0)
                    {
                        int totaltablas = doc.Tables.Count;
                        // Empty table
                        Table TablaVacia = doc.Tables[2];
                        // New table
                        Table TablaNueva = doc.AddTable(ListadeTelefonos.Count + 1, 5);
                        TablaNueva.Alignment = Alignment.center;
                        TablaNueva.Design = TableDesign.TableGrid;

                        //CREA LA CABECERA
                        TablaNueva.Rows[0].Cells[0].Paragraphs.First().Append("TIPO").Font(("Arial")).FontSize(10).Bold();
                        TablaNueva.Rows[0].Cells[1].Paragraphs.First().Append("FECHA").Font(("Arial")).FontSize(10).Bold();
                        TablaNueva.Rows[0].Cells[2].Paragraphs.First().Append("HORA").Font(("Arial")).FontSize(10).Bold();
                        TablaNueva.Rows[0].Cells[3].Paragraphs.First().Append("NUMERO").Font(("Arial")).FontSize(10).Bold();
                        TablaNueva.Rows[0].Cells[4].Paragraphs.First().Append("RESPUESTA").Font(("Arial")).FontSize(10).Bold();
                        //CREA EL CUERPO DE LA TABLA
                        int cont = 1;
                        foreach (TMasBTControlTelefonos Rel in ListadeTelefonos)
                        {

                            #region "ASIGNA VALOR A LOS CAMPOS CUANDO ES NULO"
                            if (Rel.Tipo == null) Rel.Tipo = "";
                            if (Rel.Hora == null) Rel.Hora = "";
                            if (Rel.Numero == null) Rel.Numero = "";
                            #endregion

                            TablaNueva.Rows[cont].Cells[0].Paragraphs.First().Append(Rel.Tipo).Font(("Arial")).FontSize(10);
                            TablaNueva.Rows[cont].Cells[1].Paragraphs.First().Append(Rel.Fecha.Value.ToShortDateString());   //DateTime.Now.ToString("dd-MM-yyyy") 
                            TablaNueva.Rows[cont].Cells[2].Paragraphs.First().Append(Rel.Hora);
                            TablaNueva.Rows[cont].Cells[3].Paragraphs.First().Append(Rel.Numero);
                            string Respuesta = "";
                            if (Rel.Respuesta == true) { Respuesta = Resources.Etiquetas.SI; }if (Rel.Respuesta == false) { Respuesta = Resources.Etiquetas.NO; } if(string.IsNullOrEmpty(Rel.Respuesta.ToString())){ Respuesta = Resources.Etiquetas.NA; }
                            TablaNueva.Rows[cont].Cells[4].Paragraphs.First().Append(Respuesta);
                            cont += 1;
                            //RelacionLlamadas += "TIPO: " + Rel.Tipo.PadRight(18, ' ') + "|FECHA: " + Rel.Fecha.Value.Date.ToShortDateString() + "|HORA: " + Rel.Hora.PadRight(10, ' ') + "|NUMERO: " + Rel.Numero + Environment.NewLine;
                        }

                        Table newTable = TablaVacia.InsertTableAfterSelf(TablaNueva);
                        TablaVacia.Remove();
                        
                    }
                    else
                    {
                        Table TablaVacia = doc.Tables[2];
                        TablaVacia.Remove();
                    }

                    #endregion
                }
                
                doc.SaveAs(rutadestino);
            }
            else
            {
                doc = DocX.Create(ruta);
                string NoExisteLaPlantilla = "";
                //CREA UN DOCUMENTO NUEVO
                doc.Save();
            }


            return new WordResult(doc, "Reporte " + Plantilla + " " + FunProc.ObtieneIdentificadorFechaHora());

        }
        #endregion

        #region "EXPORTAR A PDF"
        public ActionResult GeneratePdfLlamadas()
        {
            List<BuscaLlamadas_Result> ListaTmp = new List<BuscaLlamadas_Result>();
            //ListaTmp = Session["SessionResultadoListaLlamadasSP"] as List<BuscaLlamadas_Result>;


            //var pdf = new PdfResult(ListaTmp, "GeneratePdfLlamadas");

            return null; //pdf;
        }
        #endregion

        #region "EXPORTAR A EXCEL"

        public ExcelResult GeneraReporteLlamadasExportarBusqueda()
        {
            string UserName = "";
            if (HttpContext.Request.Cookies["userName"] != null)
                UserName = HttpContext.Request.Cookies["userName"].ToString();
            //      Recupera los datos que se insertaran en el archivo excel
            List<BuscaLlamadas_Result> ListaTmp = new List<BuscaLlamadas_Result>();
            ListaTmp = null;// HttpContext.Session.SetString("SessionResultadoListaLlamadasSP",List<BuscaLlamadas_Result>);

            //      Obtiene la ruta de la plantilla
            string ruta = Path.GetFileName("~/Templates/Llamadas/ExportarTodo.xlsx");
            //      Abre el libro de excel..
            var workbook = new XLWorkbook(ruta);
            //      Selecciona la hoja 1 del libro
            var worksheet = workbook.Worksheet(1);
            worksheet.Cell("A8").Value = "Impreso por: " + UserName + " | Fecha: " + DateTime.Now.ToShortDateString();
            string ParametrosDeBusqueda = "";
            ParametrosDeBusqueda = HttpContext.Session.GetString("SessionParametrosDeBusqueda");
            worksheet.Cell("A9").Value = ParametrosDeBusqueda;


            worksheet.Cell("A12").Value = ListaTmp;

            return new ExcelResult(workbook, "Exportar Búsqueda Llamadas");
        }

        #endregion

        #region "AUTOCOMPLETE"
        [HttpGet]
        public ActionResult SearchResultadoGenerico_nombrebarco(DataSourceLoadOptions loadOptions)
        {
            var criterio = (from opciones in ((JArray)loadOptions.Filter[0]).ToList<dynamic>()
                            where opciones.Value != "this" && opciones.Value != "contains"
                            select opciones.Value).FirstOrDefault();

            return null;// Content(JsonConvert.SerializeObject(DataSourceLoader.Load(FunProc.FindRsultadoGenerico(criterio, 0, "embarcacion2"), loadOptions)), "application/json");
        }
        #endregion

        protected override void Dispose(bool disposing)
        {
            //db.Dispose();
            base.Dispose(disposing);
        }

        [RequiredAuthentication(Check = false)]
        public ActionResult DetailsBySismepWeb(string rnp = "")
        {
            if (rnp == "")
            {
                return RedirectToAction("Index");
            }

            List<MASCore.ModelReference.SPMasBuscaLlamadasDesdeSismep_Result> Modelotemporal = new List<MASCore.ModelReference.SPMasBuscaLlamadasDesdeSismep_Result>();
            //Modelotemporal = db.SPMasBuscaLlamadasDesdeSismep(rnp).ToList();
            //Session["ModeloTMPBuscaLLamadasSismep"] = Modelotemporal;
            return View(Modelotemporal);
        }
    }
}
