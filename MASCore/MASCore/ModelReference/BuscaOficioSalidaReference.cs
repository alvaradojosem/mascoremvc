﻿using MASCore.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MASCore.ModelReference
{
    public class BuscaOficioSalidaReference
    {
    }
    public class OficioSalidaModelBusqueda
    {
        private BDMasContextEnt db = new BDMasContextEnt();

        #region "PARAMETROS PARA LA BUSQUEDA"

        [DisplayName("Fecha Inicial:")]
        public DateTime FechaInicio { get; set; }
        [DisplayName("Fecha Final:")]
        public DateTime FechaFin { get; set; }
        [DisplayName("No mostrar barcos")]
        public Boolean NoMostrarBarcos { get; set; }
        [DisplayName("No. Oficio:")]
        public string NoOficio { get; set; }
        [DisplayName("Tipo:")]
        public string Tipo { get; set; }
        [DisplayName("Dirigido a:")]
        public string DirigidoA { get; set; }
        [DisplayName("Contestación:")]
        public string Contestacion { get; set; }
        [DisplayName("Minutario:")]
        public string Minutario { get; set; }
        [DisplayName("Estatus:")]
        public string Estatus { get; set; }
        [DisplayName("Estatus:")]
        public string estatussalida { get; set; }
        [DisplayName("Enviado por:")]
        public string EnviadoPor { get; set; }
        [DisplayName("Barco:")]
        public string Barco { get; set; }
        #endregion

        #region "PARAMETROS PARA LA BUSQUEDA DE RESUMEN"
        [DisplayName("Minutario")]
        public Boolean MinutarioSel { get; set; }
        [DisplayName("Barco")]
        public Boolean BarcoSel { get; set; }
        [DisplayName("Razón Social")]
        public Boolean RazonSocialSel { get; set; }
        [DisplayName("Tipo")]
        public Boolean TipoSalidaSel { get; set; }
        [DisplayName("Estatus")]
        public Boolean EstatusSel { get; set; }
        [DisplayName("Mes")]
        public Boolean MesSel { get; set; }
        #endregion

        #region "USADOS PARA MOSTRAR DIVS"
        public Boolean MostrandoResumenOficioSalida { get; set; }
        #endregion

        public List<BuscaOficioSalida_Result> ResultadoListaOficioSalidaSP { get; set; }
        public List<OficioSalidaResumen> ResultadoListaOficioSalidaResumenSP { get; set; }
        public int NumeroDePersonas { get; set; }
        public int PersonasPorPagina { get; set; }
    }
    public partial class BuscaOficioSalida_Result
    {
        public int IdSalida { get; set; }
        public string NoOficio { get; set; }
        public Nullable<System.DateTime> Fecha { get; set; }
        public string TipoSalida { get; set; }
        public string Asunto { get; set; }
        public string Barco { get; set; }
        public string RazonSocial { get; set; }
        public string Dirigido { get; set; }
        public string Envia { get; set; }
        public string Minutario { get; set; }
        public string Estatus { get; set; }
        public string Contesta { get; set; }
    }
    public class OficioSalidaResumen
    {
        public string Minutario { get; set; }
        public string Barco { get; set; }
        public string RazonSocial { get; set; }
        public string TipoSalida { get; set; }
        public string Estatus { get; set; }
        public string Mes { get; set; }
        public int Total { get; set; }
    }

    public class OficioSalidaModel
    {
        #region "USADOS PARA AGREGAR EMBARCACION"
        [DisplayName("Barco:")]
        public string embarcacion_barco { get; set; }
        [DisplayName("RNP:")]
        public string embarcacion_rnp { get; set; }
        [DisplayName("Razón Social:")]
        public string embarcacion_razonsocial { get; set; }
        [DisplayName("Matrícula:")]
        public string embarcacion_matricula { get; set; }
        [DisplayName("Rep. Legal:")]
        public string embarcacion_replegal { get; set; }

        public string ErroresAgregarEmbarcacion { get; set; }

        public List<RelacionSalidaSeleccionar_Result> ResultadoListaRelacionSalidaSP { get; set; }  // USADO PARA DATALLE, EDITAR
        public List<RelacionSalidaSeleccionarTmp_Result> ResultadoListaRelacionSalidaSPTMP { get; set; }  // USADO PARA CREAR

        #endregion
       
        #region "USADOS PARA LA SECCION DE ENLAZAR OFICIOS Y LLAMADAS"

        public partial class RelacionSalidaSeleccionarTmp_Result
        {
            public int IdSalida { get; set; }
            public string RNP { get; set; }
            public string Barco { get; set; }
            public string Matricula { get; set; }
            public string RazonSocial { get; set; }
            public string UltimaTx { get; set; }
            public string RepLegal { get; set; }
        }

        public partial class RelacionSalidaSeleccionar_Result
        {
            public int IdSalida { get; set; }
            public string RNP { get; set; }
            public string Barco { get; set; }
            public string Matricula { get; set; }
            public string RazonSocial { get; set; }
            public string UltimaTx { get; set; }
            public string RepLegal { get; set; }
        }
        public int IdSalidaEmbarcacionTmp { get; set; }
        public int IdSalidaOficiosLlamadasTmp { get; set; }

        #region "USADOS PARA MOSTRAR DIVS"
        public Boolean MostrandoBusquedaOficios { get; set; }
        public Boolean MostrandoBusquedaLlamadas { get; set; }
        #endregion
        #endregion

        #region "USADOS EN MODULO DETALLE - SECCION EMBARCACIONES RELACIONADAS"
        #region "USADOS PARA MOSTRAR DIVS"
        public Boolean MostrandoBusquedaInformacionEmbarcacion { get; set; }
        #endregion
        public List<BuscaInformacionDeEmbarcacion_Result> InformacionDeEmbarcacion { get; set; }
        #endregion

        #region "INFORMACION GENERAL"
        public int IdSalida { get; set; }
        [Required]
        [DisplayName("No. Oficio:")]
        public string NoOficio { get; set; }
        [DisplayName("Fecha:")]
        public DateTime Fecha { get; set; }

        [Required]
        [DisplayName("Asunto:")]
        public string Asunto { get; set; }
        // A quien va dirigido el Oficio
        [Required]
        [DisplayName("Nombre:")]
        public string Dirigido { get; set; }
        [Required]
        [DisplayName("Puesto:")]
        public string dCargo { get; set; }
        [Required]
        [DisplayName("Empresa:")]
        public string dEmpresa { get; set; }
        //Quien envia el Oficio
        [Required]
        [DisplayName("Nombre:")]
        public string Envia { get; set; }
        [Required]
        [DisplayName("Puesto:")]
        public string eCargo { get; set; }
        [Required]
        [DisplayName("Empresa:")]
        public string eEmpresa { get; set; }
        [DisplayName("Copias:")]
        public string Copias { get; set; }
        [DisplayName("Comentarios:")]
        public string Comentarios { get; set; }
        [DisplayName("Tipo:")]
        public string TipoSalida { get; set; }
        public string Contesta { get; set; } //   <---------------   AL PARECER NO SE COLOCA 
        [Required]
        [DisplayName("Minutario:")]
        public string Minutario { get; set; }
        [Required]
        [DisplayName("Estatus:")]
        public string Estatus { get; set; }
        public int TipoRel { get; set; } //   <---------------   AL PARECER NO SE COLOCA
        [DisplayName("OFP:")]
        public string ofp2 { get; set; }
        #endregion

        public OficioEntradaModelBusqueda ModeloOficiosDeEntrada { get; set; }
        public LlamadaBusquedaModel ModeloDeLLamada { get; set; }
    }
}
