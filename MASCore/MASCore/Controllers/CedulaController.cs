﻿//using ClosedXML.Excel;
using MASCore.Models;
using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using MASCore.Components;
using MASCore;
using Microsoft.Ajax.Utilities;
using System.Net.Http;
using System.Threading.Tasks;
using DevExtreme.AspNet.Mvc;

namespace MASCore.Controllers
{
    //[Authorize] // prueba para quitar  [RequiredAuthentication(Check = false)] 
    public class CedulaController : Controller
    {

        public ActionResult Overview()
        {
            return View();
        }


        public class MultipleMoldelTmasCedula
        {
            public IEnumerable<TMasCedula> TMasCedula { get; set; }

            public AlarmasTipoModel AlarmasTipoModel { get; set; }

        }

        //InfoMensajesEmbarcacionesModel ModeloTmpX = new InfoMensajesEmbarcacionesModel();
        FuncionesProcedimientos FunProc = new FuncionesProcedimientos();
        //private BDMasContextEnt db = new BDMasContextEnt();

        ////private const decimal Tax = 10m;

        public ActionResult Opciones()
        {
            return View();
        }

        //// GET: /Cedula/
        public ActionResult Index()
        {

            return RedirectToAction("Opciones");
            //return View(db.TMasCedulas.ToList());
        }
        //// GET: /Cedula/
        public ActionResult BuscaCedula()
        {

            return View();
        }

        //    public async Task<ActionResult> BuscaCedula() //RG.Link de la página principal
        //    {
        //        ViewBag.NoValidoPor = "";
        //        //HttpContext.Session.Remove("SessionCedBusModel");
        //        //System.Threading.Thread.CurrentThread.CurrentCulture = "es-Mx";
        //        //string HostName = System.Net.Dns.GetHostEntry(Request.UserHostAddress).HostName;
        //        using (var httpClient = new HttpClient())
        //        {
        //            using (var response = await httpClient.GetAsync("https://10.215.71.239:44350/api/Cedula/BuscaCedulas"))
        //            {
        //                CedulaBusquedaModel cbm = new CedulaBusquedaModel();
        //                try
        //                {
        //                    cbm.FechaInicial = DateTime.Now;  // Convert.ToDateTime("01/01/2009") ;               
        //                    cbm.FechaFinal = DateTime.Now;
        //                    //List<CedulasSeleccionar_Result> ResultadoListaCedulasTmp = new List<CedulasSeleccionar_Result>();
        //                    //cbm.ResultadoListaCedulasSP = ResultadoListaCedulasTmp;//cbm.TodasLasCedulasSP(cbm.FechaInicial,cbm.FechaFinal);
        //                    ////#region "Llena los Dropdownlist"

        //                    //ViewBag.Alarmas = FunProc.LlenarResultadoGenerico("alarmastipo");
        //                    //ViewBag.Causas = FunProc.LlenarResultadoGenerico("causasalarmas");
        //                    //ViewBag.Litorales = FunProc.LlenarResultadoGenerico("litorales");

        //                    ////#endregion

        //                    //Context.Session["SessionCedBusModel"] = cbm;
        //                    //Context.Session.Timeout = 120;
        //                }
        //                catch (Exception e)
        //                {
        //                    string erroMsg = e.StackTrace + e.Message + e.InnerException;
        //                }
        //                return View(cbm);
        //            }

        //            //#region Cargar Combos
        //            //[HttpGet]
        //            //public ActionResult GetAlarmas(DataSourceLoadOptions loadOptions)
        //            //{
        //            //    return Content(JsonConvert.SerializeObject(DataSourceLoader.Load(FunProc.LlenarResultadoGenerico("alarmastipo"), loadOptions)), "application/json");
        //            //}

        //            //[HttpGet]
        //            //public ActionResult GetCausasAlarmas(DataSourceLoadOptions loadOptions)
        //            //{
        //            //    return Content(JsonConvert.SerializeObject(DataSourceLoader.Load(FunProc.LlenarResultadoGenerico("causasalarmas"), loadOptions)), "application/json");
        //            //}

        //            //[HttpGet]
        //            //public ActionResult GetLitorales(DataSourceLoadOptions loadOptions)
        //            //{
        //            //    return Content(JsonConvert.SerializeObject(DataSourceLoader.Load(FunProc.LlenarResultadoGenerico("litorales"), loadOptions)), "application/json");
        //            //}

        //            //[HttpGet]
        //            //public ActionResult GetActividades(DataSourceLoadOptions loadOptions)
        //            //{
        //            //    return Content(JsonConvert.SerializeObject(DataSourceLoader.Load(FunProc.LlenarResultadoGenerico("btactividades"), loadOptions)), "application/json");
        //            //}

        //            //[HttpGet]
        //            //public ActionResult GetResponsablesOFP(DataSourceLoadOptions loadOptions)
        //            //{
        //            //    return Content(JsonConvert.SerializeObject(DataSourceLoader.Load(FunProc.LlenarResultadoGenerico("responsableofp"), loadOptions)), "application/json");
        //            //}

        //            //[HttpGet]
        //            //public ActionResult GetFiltros(DataSourceLoadOptions loadOptions)
        //            //{
        //            //    return Content(JsonConvert.SerializeObject(DataSourceLoader.Load(FunProc.LlenarResultadoGenerico("filtro"), loadOptions)), "application/json");
        //            //}

        //            //[HttpGet]
        //            //public ActionResult GetCriteriosDesconexion(DataSourceLoadOptions loadOptions)
        //            //{
        //            //    return Content(JsonConvert.SerializeObject(DataSourceLoader.Load(FunProc.LlenarResultadoGenerico("criteriodedesconexion"), loadOptions)), "application/json");
        //            //}

        //            //[HttpGet]
        //            //public ActionResult GeModoBusqueda(DataSourceLoadOptions loadOptions)
        //            //{
        //            //    return Content(JsonConvert.SerializeObject(DataSourceLoader.Load(FunProc.LlenarResultadoGenerico("modobusqueda"), loadOptions)), "application/json");
        //            //}

        //            //[HttpGet]
        //            //public ActionResult GetTurnos(DataSourceLoadOptions loadOptions)
        //            //{
        //            //    return Content(JsonConvert.SerializeObject(DataSourceLoader.Load(FunProc.LlenarResultadoGenerico("turno"), loadOptions)), "application/json");
        //            //}

        //            //[HttpGet]
        //            //public ActionResult GetUsuariosAstrum(DataSourceLoadOptions loadOptions)
        //            //{
        //            //    return Content(JsonConvert.SerializeObject(DataSourceLoader.Load(FunProc.LlenarResultadoGenerico("usuarioastrum"), loadOptions)), "application/json");
        //            //}
        //            //#endregion

        //            //[HttpPost]
        //            //public ActionResult BuscaCedula(CedulaBusquedaModel CedBusModel) //RG.Boton en Forma de Busqueda de cedula 
        //            //{
        //            //    try
        //            //    {
        //            //        #region " Llena nuevamente los Dropdownlist"
        //            //        ViewBag.NoValidoPor = "";
        //            //        ViewBag.Alarmas = FunProc.LlenarResultadoGenerico("alarmastipo");
        //            //        ViewBag.Causas = FunProc.LlenarResultadoGenerico("causasalarmas");
        //            //        ViewBag.Litorales = FunProc.LlenarResultadoGenerico("litorales");
        //            //        #endregion
        //            //        //if (ModelState.IsValid)
        //            //        //{
        //            //        string causaseleccionada = CedBusModel.causas;
        //            //        string alarmaseleccionada = CedBusModel.alarmas;
        //            //        string litoralesseleccionados = CedBusModel.litorales;

        //            //        DateTime FechaInicial = CedBusModel.FechaInicial;
        //            //        DateTime FechaFinal = CedBusModel.FechaFinal;                                                          //Formateando para obtener solo la fecha   var dbDate = CedBusModel.FechaHoraFinal.ToString("dd/MM/yyyy");
        //            //        List<CedulasSeleccionar_Result> ResultadoListaCedulasTmp = new List<CedulasSeleccionar_Result>();
        //            //        CedBusModel.ResultadoListaCedulasSP = ResultadoListaCedulasTmp;

        //            //        #region "VALIDACIONES"
        //            //        Boolean Valido = true;
        //            //        if (CedBusModel.FechaInicial > CedBusModel.FechaFinal)
        //            //        {
        //            //            ViewBag.NoValidoPor = "No es posible realizar la búsqueda, la fecha inicial no puede ser mayor a la fecha final. Verificar.";
        //            //            Valido = false;
        //            //        }
        //            //        if (CedBusModel.FechaInicial.Year < 1753 || CedBusModel.FechaFinal.Year < 1753)
        //            //        {
        //            //            ViewBag.NoValidoPor = "No es posible realizar la búsqueda. Verificar Fechas.";
        //            //            Valido = false;
        //            //        }
        //            //        #endregion
        //            //        if (Valido == true)
        //            //        {
        //            //            CedBusModel.ResultadoListaCedulasSP = CedBusModel.EncuentraLasCedulasSP(FechaInicial, FechaFinal, alarmaseleccionada, causaseleccionada, CedBusModel.numerodecedula, CedBusModel.responsable, CedBusModel.tipopermiso, CedBusModel.nombredelbarco, CedBusModel.rnpdelbarco, CedBusModel.razonsocial, CedBusModel.puertobase, litoralesseleccionados);
        //            //        }

        //            //        Session["SessionCedBusModel"] = CedBusModel;

        //            //        string ParametrosDeBusqueda = "";
        //            //        ParametrosDeBusqueda += "Desde " + CedBusModel.FechaInicial.ToShortDateString() + " Hasta " + CedBusModel.FechaFinal.ToShortDateString();
        //            //        Session["SessionParametrosDeBusqueda"] = ParametrosDeBusqueda;
        //            //    }
        //            //    catch (Exception e)
        //            //    {
        //            //        string erroMsg = e.StackTrace + e.Message + e.InnerException;
        //            //    }
        //            //    return View(CedBusModel);
        //            //}

        //        }

        //    }




        //    public ActionResult ResultadoDeConsulta()
        //    {
        //        //return View(db.TMasCedulas.ToList());
        //        return View();
        //    }

        //    //public ActionResult MensajesEmbarcaciones(InfoMensajesEmbarcacionesModel Modelo, string alarmas)
        //    //{
        //    //    string UserName = "";
        //    //    if (HttpContext.Request.Cookies["userName"] != null)
        //    //        UserName = HttpContext.Request.Cookies["userName"].Value;
        //    //    InfoMensajesEmbarcacionesModel ModeloTmp = new InfoMensajesEmbarcacionesModel();

        //    //    DateTime FechaInicial = ModeloTmp.FechaInicial;
        //    //    DateTime FechaFinal = ModeloTmp.FechaFinal;
        //    //    ModeloTmp.ResultadoMensajesEmbarcacionesSeleccionar = ModeloTmp.EncuentraMensajesEmbarcacionesSeleccionarVacio("", "", "", "", "", "", "", "", UserName, "", "");

        //    //    return View(ModeloTmp);
        //    //}

        //    //public string CalcEdades(string fechaNacimiento)
        //    //{
        //    //    // Inicializo la variable edad
        //    //    int edad = 0;
        //    //    try
        //    //    {
        //    //        // Inicializo las variables
        //    //        DateTime actual = DateTime.Now;
        //    //        DateTime fecha = Convert.ToDateTime(fechaNacimiento);

        //    //        // Consulto si el parámetro es mayor y es correcto
        //    //        if (fecha <= actual && fecha != null)
        //    //        {
        //    //            edad = actual.Year - fecha.Year;
        //    //        }
        //    //        else
        //    //        {
        //    //            return "La fecha seleccionada es superior a la fecha actual";
        //    //        }
        //    //        // Consulto si ya cumplió años
        //    //        if (new DateTime(actual.Year, fecha.Month, fecha.Day) < actual) edad--;
        //    //    }
        //    //    catch (Exception e)
        //    //    {
        //    //        string erroMsg = e.StackTrace + e.Message + e.InnerException;
        //    //    }
        //    //    return "Tu tienes " + edad + " años";
        //    //}

        //    //[HttpPost]
        //    //public ActionResult PruebaBusqueda(string selectedCity, string idcausax, string Litoral, string idcausa, string Causa, string cbocausas)
        //    //{
        //    //    try
        //    //    {
        //    //        var id = Int32.Parse(selectedCity);
        //    //        // Recuperamos la ciudad ==> Consulta a BBDD
        //    //        var city = db.TMasCausasAlarmas.FirstOrDefault(c => c.IdCausa == id);
        //    //        var Usua = Request.Form["idtipoalarma"];
        //    //        var LITORALLLLL = Litoral;
        //    //    }
        //    //    catch (Exception e)
        //    //    {
        //    //        string erroMsg = e.StackTrace + e.Message + e.InnerException;
        //    //    }
        //    //    return View();
        //    //}

        //    ////TMasCedula
        //    //public ActionResult ImprimirCedula(TMasCedula ModeloCedula)
        //    //{
        //    //    string TMP;
        //    //    TMP = "Aqui se realiza la accion de imprimir y manda nuevamente al modulo detalles";
        //    //    return Content(string.Format("{0}, your ID is {1}", TMP, TMP));
        //    //}

        //    //[RequiredAuthentication(Check = false)]
        //    //public ActionResult CedulaGuardada(string id = "0")
        //    //{
        //    //    try
        //    //    {
        //    //        int Identificador = 0;
        //    //        int.TryParse(id, out Identificador);
        //    //        TMasCedula tmascedula = db.TMasCedulas.Single(t => t.IdCedula == Identificador);
        //    //        ViewBag.mensaje = "Proceso realizado con exito. ";
        //    //        if (tmascedula != null)
        //    //        {
        //    //            ViewBag.mensaje = "Proceso realizado con exito, cedula creada : " + tmascedula.NoCedula;
        //    //        }
        //    //        else
        //    //        {
        //    //            ViewBag.mensaje = "Proceso realizado con exito, Id cedula creada : " + id;
        //    //        }
        //    //    }
        //    //    catch (Exception e)
        //    //    {
        //    //        string erroMsg = e.StackTrace + e.Message + e.InnerException;
        //    //    }
        //    //    return View();
        //    //}

        //    //// GET: /Cedula/Details/5
        //    //public ActionResult Details(int id = 0)
        //    //{
        //    //    Session.Remove("SessionIdCedula");
        //    //    Session["SessionIdCedula"] = id;

        //    //    TMasCedula tmascedula = db.TMasCedulas.Single(t => t.IdCedula == id);
        //    //    try
        //    //    {
        //    //        if (tmascedula == null)
        //    //        {
        //    //            return StatusCode(404);
        //    //        }
        //    //        List<BuscaCargaImagenesTipo_Result> ListaImagenesTmp = new List<BuscaCargaImagenesTipo_Result>();
        //    //        ListaImagenesTmp = FunProc.ObtieneListaCargaImagenesTipo(tmascedula.NoCedula, "", "CEDULAS");
        //    //        Session["SessionImagenesCedula"] = ListaImagenesTmp;
        //    //    }
        //    //    catch (Exception e)
        //    //    {
        //    //        string erroMsg = e.StackTrace + e.Message + e.InnerException;
        //    //    }
        //    //    return View(tmascedula);
        //    //}

        //    //[HttpGet]
        //    //public PartialViewResult SomeAction()
        //    //{
        //    //    return PartialView("_LoginPartial");
        //    //}

        //    //[AllowAnonymous]
        //    [RequiredAuthentication(Check = false)]
        //    public ActionResult CreateBySismepWeb(string id = "", string alarmas = "", string MostrarObtieneInformacion = "NO")
        //    {
        //        //string UserName = "";
        //        //if (HttpContext.Request.Cookies["userName"] != null)
        //        //    UserName = HttpContext.Request.Cookies["userName"].Value;
        //        //InfoMensajesEmbarcacionesModel ModeloTmp = new InfoMensajesEmbarcacionesModel();
        //        //ModeloTmp.FechaInicioRep = DateTime.Now;
        //        //ModeloTmp.FechaFinRep = DateTime.Now;
        //        //ModeloTmp.NoValidoPor_ = "";
        //        //ModeloTmp.MostrarObtieneInformacion = "SI";
        //        //if (MostrarObtieneInformacion == "NO")
        //        //    ModeloTmp.MostrarObtieneInformacion = "NO";

        //        //#region "Llenado de combos y autocomplete"

        //        //ViewBag.alarmasb = new SelectList(FunProc.LlenarResultadoGenerico("alarmastipo"), "Resultado", "Resultado");
        //        //ViewBag.alarmas = new SelectList(FunProc.LlenarResultadoGenerico("alarmastipo"), "Resultado", "Resultado");
        //        //ViewBag.causas = new SelectList(FunProc.LlenarResultadoGenerico("causasalarmas"), "Resultado", "Resultado");
        //        //ViewBag.turnos = new SelectList(FunProc.LlenarResultadoGenerico("turno"), "Resultado", "Resultado");
        //        //ViewBag.responsableofp = new SelectList(FunProc.LlenarResultadoGenerico("responsableofp"), "Resultado", "Resultado");
        //        //ViewBag.usuarioastrum = new SelectList(FunProc.LlenarResultadoGenerico("usuarioastrum"), "Resultado", "Resultado");
        //        //#region "Usados para la sección Obtener todos los datos posibles"
        //        //ModeloTmp.FechaInicial = DateTime.Now;
        //        //ModeloTmp.FechaFinal = DateTime.Now;
        //        //ViewBag.criteriodedesconexion = new SelectList(FunProc.LlenarResultadoGenerico("criteriodedesconexion"), "Resultado", "Resultado");
        //        //ViewBag.modobusqueda = new SelectList(FunProc.LlenarResultadoGenerico("modobusqueda"), "Resultado", "Resultado");
        //        //ModeloTmp.ResultadoMensajesEmbarcacionesSeleccionar = ModeloTmp.EncuentraMensajesEmbarcacionesSeleccionarVacio("", "", "", "", "", "", "", "", UserName, "", "");
        //        //#endregion

        //        //#region "Usados para la sección Llamadas"
        //        //LlamadaBusquedaModel ModeloTmpLlamadas = new LlamadaBusquedaModel();
        //        //ModeloTmpLlamadas.FechaInicial = DateTime.Now;// Convert.ToDateTime("01/01/2013");
        //        //ModeloTmpLlamadas.FechaFinal = DateTime.Now;
        //        //ModeloTmpLlamadas.ResultadoListaLlamadasSP = ModeloTmpLlamadas.EncuentraLasLlamadasSP("", ModeloTmpLlamadas.FechaInicial, ModeloTmpLlamadas.FechaFinal, "", "", "");  // Ejecuta sp y regresa las llamadas encontradas y regresa una lista  list<BuscaLlamadas_Result>
        //        //ModeloTmp.ModeloDeLLamada = ModeloTmpLlamadas;                  // Le asigna el modelo de llamadas a el modelo principal. CedulaBusquedaModel contiene una propiedad de LlamadaBusquedaModel
        //        //ViewBag.actividad = new SelectList(FunProc.LlenarResultadoGenerico("btactividades"), "Resultado", "Resultado");
        //        //ViewBag.ofp = new SelectList(FunProc.LlenarResultadoGenerico("responsableofp"), "Resultado", "Resultado");
        //        //ViewBag.filtro = new SelectList(FunProc.LlenarResultadoGenerico("filtro"), "Resultado", "Resultado");
        //        //#endregion

        //        //#endregion

        //        //if (id != "")  // Si tiene Id significa que escogio una alerta para obtener la informacion.  El id es el IdMensajeM
        //        //{
        //        //    ModeloTmp.TMASCEDULA = ModeloTmp.AsingaResultadoMensajeEmbarcacionSeleccionada(id, ModeloTmp.TMASCEDULA, "", "");
        //        //    //ModeloTmp.TMASCEDULA.Llamada = 0;                   //Inicializa la llamada vinculada por que se escogio un evento determinado
        //        //    alarmas = ModeloTmp.TMASCEDULA.TipoAlerta;
        //        //    ModeloTmp.alarmasb = alarmas;
        //        //}

        //        //return View(ModeloTmp);
        //        return View();
        //    }

        //    //[HttpPost]
        //    ////[AllowAnonymous]
        //    //[RequiredAuthentication(Check = false)]
        //    //public ActionResult CreateBySismepWeb(InfoMensajesEmbarcacionesModel ModeloTmp, LlamadaBusquedaModel ModeloDeLLamada, string submitButton, string alarmas, string TipoAlerta,
        //    //    string id_ = "", string criteriodedesconexion = "", string filtro = "", string actividad = "", string embarcacion = "", string ofp = "", string causas = "", string turnos = "", string responsableofp = "", string lblLlamada = "", string usuarioastrum = "")
        //    //{
        //    //    string UserName = "";
        //    //    if (HttpContext.Request.Cookies["userName"] != null)
        //    //        UserName = HttpContext.Request.Cookies["userName"].Value;
        //    //    //ModeloTmp.TMASCEDULA.TipoAlerta = alarmas;
        //    //    ModeloTmp.NoValidoPor_ = "";

        //    //    ModeloTmp.MostrarLlamadas = "NO";                                       // Inicializa la pantalla sin mostrar el div de la busqueda de llamadas, esto cambia si se busca una alarma de tipo SIN TRANSMISION

        //    //    switch (submitButton)
        //    //    {
        //    //        case "Guardar":
        //    //            if (usuarioastrum != "") ModeloTmp.TMASCEDULA.UsuarioAstrum = usuarioastrum;
        //    //            string NoValidoPor = "";
        //    //            if (ModelState.IsValid)
        //    //            {
        //    //                #region "VALIDACIONES"
        //    //                Boolean Valido = true;
        //    //                if (ModeloTmp.TMASCEDULA.FechaAlerta.Value > DateTime.Now.Date || ModeloTmp.TMASCEDULA.FechaCedula.Value > DateTime.Now.Date)
        //    //                {
        //    //                    ViewBag.NoValidoPor = "La fecha no puede ser mayor a la fecha actual, Verificar Fechas.";
        //    //                    Valido = false;
        //    //                }
        //    //                if (ModeloTmp.TMASCEDULA.FechaAlerta.Value.Year < 1753 || ModeloTmp.TMASCEDULA.FechaAlerta.Value.Year > 2030)
        //    //                {
        //    //                    ViewBag.NoValidoPor = "No es posible completar la acción, Verificar Fechas.";
        //    //                    Valido = false;
        //    //                }
        //    //                if (ModeloTmp.TMASCEDULA.FechaCedula.Value.Year < 1753 || ModeloTmp.TMASCEDULA.FechaCedula.Value.Year > 2030)
        //    //                {
        //    //                    ViewBag.NoValidoPor = "No es posible completar la acción, Verificar Fechas.";
        //    //                    Valido = false;
        //    //                }
        //    //                if (ModeloTmp.TMASCEDULA.TipoAlerta == "EMERGENCIA")
        //    //                {
        //    //                    if (causas == null || causas == "")
        //    //                    {
        //    //                        ViewBag.NoValidoPor = "No es posible completar la acción. Para la alerta EMERGENCIA es requerido el campo causa de la alarma.";
        //    //                        Valido = false;
        //    //                    }
        //    //                    else
        //    //                    {

        //    //                    }

        //    //                }
        //    //                #endregion

        //    //                if (Valido == true)
        //    //                {
        //    //                    if (lblLlamada.Length > 0)
        //    //                        ModeloTmp.TMASCEDULA.Llamada = Convert.ToInt32(lblLlamada);
        //    //                    //Obtiene el nombre del usuario 
        //    //                    //Comentado por SZ el 14/03/2017
        //    //                    //NoValidoPor = ModeloTmp.ValidaeInsertaCedula(ModeloTmp, alarmas, causas, turnos, responsableofp, UserName);
        //    //                    if (ModeloTmp.IdMensaje==null)
        //    //                    {
        //    //                        NoValidoPor = ModeloTmp.ValidaeInsertaCedula(ModeloTmp, alarmas, causas, turnos, responsableofp, UserName, Guid.Parse("00000000-0000-0000-0000-000000000000"));
        //    //                    }
        //    //                    else
        //    //                    {
        //    //                        NoValidoPor = ModeloTmp.ValidaeInsertaCedula(ModeloTmp, alarmas, causas, turnos, responsableofp, UserName, Guid.Parse(ModeloTmp.IdMensaje));
        //    //                    }
        //    //                    if (NoValidoPor.Length <= 6)
        //    //                    {
        //    //                        return RedirectToAction("CedulaGuardada", "Cedula", new { id = NoValidoPor });
        //    //                    }
        //    //                }
        //    //            }

        //    //            //No se pudo guardar el registro , hay que mostrar que falto 
        //    //            ModeloTmp.ResultadoMensajesEmbarcacionesSeleccionar = ModeloTmp.EncuentraMensajesEmbarcacionesSeleccionarVacio("", "", "", "", "", "", "", "",UserName,"","");   //Pasa vacio la busqueda de embarcaciones si es que no se pudo guardar el registro
        //    //            //Asigno los valores no validos a el modelo para mostrar el mensaje  NoValidoPor  
        //    //            ModeloTmp.NoValidoPor_ = NoValidoPor;
        //    //            ModeloTmp.MostrarObtieneInformacion = "NO";
        //    //            break;
        //    //        case "Buscar Informacion":
        //    //            //ModeloTmp.ResultadoMensajesEmbarcacionesSeleccionar = ModeloTmp.EncuentraMensajesEmbarcacionesSeleccionarVacio("", "", "", "", "", "", "", "");   //Pasa vacio la busqueda de embarcaciones si es que no se pudo guardar el registro
        //    //            ModeloTmp = BuscaMensajesEmbarcaciones(ModeloTmp, alarmas, criteriodedesconexion);
        //    //            break;
        //    //        case "Buscar Llamada":
        //    //            ModeloTmp.ResultadoMensajesEmbarcacionesSeleccionar = ModeloTmp.EncuentraMensajesEmbarcacionesSeleccionarVacio("", "", "", "", "", "", "", "",UserName,"","");   //Pasa vacio la busqueda de embarcaciones si es que no se pudo guardar el registro
        //    //            ModeloTmp.alarmasb = "SIN TRANSMISIÓN";                         // Esta parte de llamadas solo se usa para las alarmas de tipo SIN TRANSMISION
        //    //            ModeloTmp.MostrarLlamadas = "SI";                               //Al cargar la pantalla sigue mostrando el div usado buscar las llamadas vinculadas            
        //    //            break;
        //    //    }

        //    //    ModeloTmp.MostrarObtieneInformacion = "NO";                             //Al cargar la pantalla sigue mostrando el div usado para obtener toda la informacion posible            
        //    //    #region "Llenado de combos y autocomplete"
        //    //    ViewBag.alarmasb = new SelectList(FunProc.LlenarResultadoGenerico("alarmastipo"), "Resultado", "Resultado");
        //    //    ViewBag.alarmas = new SelectList(FunProc.LlenarResultadoGenerico("alarmastipo"), "Resultado", "Resultado");
        //    //    ViewBag.causas = new SelectList(FunProc.LlenarResultadoGenerico("causasalarmas"), "Resultado", "Resultado");
        //    //    ViewBag.turnos = new SelectList(FunProc.LlenarResultadoGenerico("turno"), "Resultado", "Resultado");
        //    //    ViewBag.responsableofp = new SelectList(FunProc.LlenarResultadoGenerico("responsableofp"), "Resultado", "Resultado");
        //    //    ViewBag.usuarioastrum = new SelectList(FunProc.LlenarResultadoGenerico("usuarioastrum"), "Resultado", "Resultado");
        //    //    #region "Usados para la sección Obtener todos los datos posibles(Mensajes de embarcacion)"
        //    //    ViewBag.criteriodedesconexion = new SelectList(FunProc.LlenarResultadoGenerico("criteriodedesconexion"), "Resultado", "Resultado");
        //    //    ViewBag.modobusqueda = new SelectList(FunProc.LlenarResultadoGenerico("modobusqueda"), "Resultado", "Resultado");
        //    //    #endregion
        //    //    #region "Usados para la sección Llamadas"
        //    //    ModeloDeLLamada.ResultadoListaLlamadasSP = ModeloDeLLamada.EncuentraLasLlamadasSP(filtro, ModeloDeLLamada.FechaInicial, ModeloDeLLamada.FechaFinal, actividad, embarcacion, ofp);
        //    //    ModeloTmp.ModeloDeLLamada = ModeloDeLLamada;
        //    //    ViewBag.actividad = new SelectList(FunProc.LlenarResultadoGenerico("btactividades"), "Resultado", "Resultado");
        //    //    ViewBag.ofp = new SelectList(FunProc.LlenarResultadoGenerico("responsableofp"), "Resultado", "Resultado");
        //    //    ViewBag.filtro = new SelectList(FunProc.LlenarResultadoGenerico("filtro"), "Resultado", "Resultado");
        //    //    #endregion
        //    //    #endregion

        //    //    return View(ModeloTmp);
        //    //}

        //    //// GET: /Cedula/Create
        //    //public ActionResult Create(string id = "", string alarmas = "", string MostrarObtieneInformacion = "")
        //    //{
        //    //    string UserName = "";
        //    //    if (HttpContext.Request.Cookies["userName"] != null)
        //    //        UserName = HttpContext.Request.Cookies["userName"].Value;

        //    //    InfoMensajesEmbarcacionesModel ModeloTmp = new InfoMensajesEmbarcacionesModel();
        //    //    ModeloTmp.FechaInicioRep = DateTime.Now;
        //    //    ModeloTmp.FechaFinRep = DateTime.Now;
        //    //    ModeloTmp.NoValidoPor_ = "";
        //    //    ModeloTmp.MostrarObtieneInformacion = "SI";
        //    //    if (MostrarObtieneInformacion == "NO" || MostrarObtieneInformacion == "") // se le agrego || MostrarObtieneInformacion == "" para que al iniciar no mostrara los resultados de informacion
        //    //        ModeloTmp.MostrarObtieneInformacion = "NO";

        //    //    #region "Llenado de combos y autocomplete"
        //    //    //ViewBag.alarmasb = new SelectList(FunProc.LlenarResultadoGenerico("alarmastipo"), "Resultado", "Resultado");
        //    //    //ViewBag.alarmas = new SelectList(FunProc.LlenarResultadoGenerico("alarmastipo"), "Resultado", "Resultado");
        //    //    //ViewBag.causas = new SelectList(FunProc.LlenarResultadoGenerico("causasalarmas"), "Resultado", "Resultado");
        //    //    //ViewBag.turnos = new SelectList(FunProc.LlenarResultadoGenerico("turno"), "Resultado", "Resultado");
        //    //    //ViewBag.responsableofp = new SelectList(FunProc.LlenarResultadoGenerico("responsableofp"), "Resultado", "Resultado");
        //    //    //ViewBag.usuarioastrum = new SelectList(FunProc.LlenarResultadoGenerico("usuarioastrum"), "Resultado", "Resultado");
        //    //    #region "Usados para la sección Obtener todos los datos posibles"
        //    //    ModeloTmp.FechaInicial = DateTime.Now; //Convert.ToDateTime("01/01/2013");
        //    //    ModeloTmp.FechaFinal = DateTime.Now;
        //    //    //ViewBag.criteriodedesconexion = new SelectList(FunProc.LlenarResultadoGenerico("criteriodedesconexion"), "Resultado", "Resultado");
        //    //    //ViewBag.modobusqueda = new SelectList(FunProc.LlenarResultadoGenerico("modobusqueda"), "Resultado", "Resultado");
        //    //    ModeloTmp.ResultadoMensajesEmbarcacionesSeleccionar = ModeloTmp.EncuentraMensajesEmbarcacionesSeleccionarVacio("", "", "", "", "", "", "", "", UserName, "", "");
        //    //    #endregion
        //    //    #region "Usados para la sección Llamadas"
        //    //    LlamadaBusquedaModel ModeloTmpLlamadas = new LlamadaBusquedaModel();
        //    //    ModeloTmpLlamadas.FechaInicial = DateTime.Now;
        //    //    ModeloTmpLlamadas.FechaFinal = DateTime.Now;
        //    //    ModeloTmpLlamadas.ResultadoListaLlamadasSP = ModeloTmpLlamadas.EncuentraLasLlamadasSP("", ModeloTmpLlamadas.FechaInicial, ModeloTmpLlamadas.FechaFinal, "", "", "");  // Ejecuta sp y regresa las llamadas encontradas y regresa una lista  list<BuscaLlamadas_Result>
        //    //    ModeloTmp.ModeloDeLLamada = ModeloTmpLlamadas;                  // Le asigna el modelo de llamadas a el modelo principal. CedulaBusquedaModel contiene una propiedad de LlamadaBusquedaModel
        //    //    //ViewBag.actividad = new SelectList(FunProc.LlenarResultadoGenerico("btactividades"), "Resultado", "Resultado");
        //    //    //ViewBag.ofp = new SelectList(FunProc.LlenarResultadoGenerico("responsableofp"), "Resultado", "Resultado");
        //    //    //ViewBag.filtro = new SelectList(FunProc.LlenarResultadoGenerico("filtro"), "Resultado", "Resultado");
        //    //    #endregion
        //    //    #endregion

        //    //    if (id != "")  // Si tiene Id significa que escogio una alerta para obtener la informacion.  El id es el IdMensajeM
        //    //    {
        //    //        ModeloTmp.TMASCEDULA = ModeloTmp.AsingaResultadoMensajeEmbarcacionSeleccionada(id, ModeloTmp.TMASCEDULA, UserName, alarmas);
        //    //        //ModeloTmp.TMASCEDULA.Llamada = 0;                   //Inicializa la llamada vinculada por que se escogio un evento determinado
        //    //        ModeloTmp.alarmasb = alarmas;
        //    //        ModeloTmp.IdMensaje = id;  //VZ1611_RPZPP 
        //    //    }
        //    //    else
        //    //    {
        //    //        TMasCedula TMASCEDULATMP = new TMasCedula();
        //    //        ModeloTmp.TMASCEDULA = TMASCEDULATMP;
        //    //        ModeloTmp.IdMensaje = id;  //VZ1611_RPZPP 
        //    //    }
        //    //    string NombreUsuario = "UserName";
        //    //    BuscaResultadoGenericoConParametros_Result ResultadoGenerico = db.BuscaResultadoGenericoConParametros("nombreusuario", "", UserName, 0, 0).FirstOrDefault();
        //    //    if (ResultadoGenerico != null) NombreUsuario = ResultadoGenerico.Resultado;
        //    //    ModeloTmp.TMASCEDULA.OFP = NombreUsuario.ToUpper();

        //    //    return View(ModeloTmp);
        //    //}

        //    //private ActionResult Cancel()
        //    //{
        //    //    // process the cancellation request here.
        //    //    return (View("Cancelled"));
        //    //}

        //    //private InfoMensajesEmbarcacionesModel BuscaMensajesEmbarcaciones(InfoMensajesEmbarcacionesModel ModeloTmp, string alarmas, string _criteriodedesconexion = "")
        //    //{
        //    //    string UserName = "";
        //    //    if (HttpContext.Request.Cookies["userName"] != null)
        //    //        UserName = HttpContext.Request.Cookies["userName"].Value;
        //    //    ModeloTmp.alarmasb = alarmas;
        //    //    ModeloTmp.ResultadoMensajesEmbarcacionesSeleccionar = ModeloTmp.EncuentraMensajesEmbarcacionesSeleccionar(UserName, ModeloTmp.FechaInicial, ModeloTmp.FechaFinal, ModeloTmp.alarmasb, ModeloTmp.MatriculaBarcob, ModeloTmp.RazonSocialb, ModeloTmp.PuertoBaseb, ModeloTmp.TipoPermisob, ModeloTmp.modobusqueda, ModeloTmp.Embarcacionb,ModeloTmp.Rnpb ,_criteriodedesconexion);

        //    //    return (ModeloTmp);
        //    //}

        //    //// POST: /Cedula/Create
        //    //[HttpPost]
        //    public ActionResult Create(InfoMensajesEmbarcacionesModel ModeloTmp, LlamadaBusquedaModel ModeloDeLLamada, string submitButton, string alarmas, string TipoAlerta,
        //        string id_ = "", string criteriodedesconexion = "", string filtro = "", string actividad = "", string embarcacion = "", string ofp = "", string causas = "", string turnos = "", string responsableofp = "", string lblLlamada = "", string usuarioastrum = "")
        //    {
        //        //    string UserName = "";
        //        //    if (HttpContext.Request.Cookies["userName"] != null)
        //        //        UserName = HttpContext.Request.Cookies["userName"].Value;
        //        //    ModeloTmp.NoValidoPor_ = "";
        //        //    ModeloTmp.MostrarLlamadas = "NO";// Inicializa la pantalla sin mostrar el div de la busqueda de llamadas, esto cambia si se busca una alarma de tipo SIN TRANSMISION

        //        //    switch (submitButton)
        //        //    {
        //        //        case "Guardar":

        //        //            if (usuarioastrum != "") ModeloTmp.TMASCEDULA.UsuarioAstrum = usuarioastrum;

        //        //            string NoValidoPor = "";
        //        //            if (ModelState.IsValid)
        //        //            {
        //        //                #region "VALIDACIONES"
        //        //                Boolean Valido = true;
        //        //                if (ModeloTmp.TMASCEDULA.FechaAlerta.Value > DateTime.Now.Date || ModeloTmp.TMASCEDULA.FechaCedula.Value > DateTime.Now.Date)
        //        //                {
        //        //                    ViewBag.NoValidoPor = "La fecha no puede ser mayor a la fecha actual. Verificar Fechas.";
        //        //                    Valido = false;
        //        //                }
        //        //                if (ModeloTmp.TMASCEDULA.FechaAlerta.Value.Year < 1753 || ModeloTmp.TMASCEDULA.FechaAlerta.Value.Year > 2030)
        //        //                {
        //        //                    ViewBag.NoValidoPor = "No es posible completar la acción. Verificar Fechas.";
        //        //                    Valido = false;
        //        //                }
        //        //                if (ModeloTmp.TMASCEDULA.FechaCedula.Value.Year < 1753 || ModeloTmp.TMASCEDULA.FechaCedula.Value.Year > 2030)
        //        //                {
        //        //                    ViewBag.NoValidoPor = "No es posible completar la acción. Verificar Fechas.";
        //        //                    Valido = false;
        //        //                }
        //        //                if (ModeloTmp.TMASCEDULA.TipoAlerta == "EMERGENCIA")
        //        //                {
        //        //                    //if (causas == null || causas == "")
        //        //                    if (string.IsNullOrWhiteSpace(ModeloTmp.TMASCEDULA.Causas))
        //        //                    {
        //        //                        ViewBag.NoValidoPor = "No es posible completar la acción. Para la alerta EMERGENCIA es requerido el campo causa de la alarma.";
        //        //                        Valido = false;
        //        //                    }
        //        //                    //else
        //        //                    //{
        //        //                    //}
        //        //                }

        //        //                if(ModeloTmp.TMASCEDULA.TipoAlerta == "ZONA DECRETO")
        //        //                {
        //        //                    ViewBag.Texto5 = new SelectList(FunProc.ObtieneTexto5(ModeloTmp.TMASCEDULA.RNP), "Resultado", "Resultado");
        //        //                    if (ViewBag.Texto5.Items[0].Resultado.ToString().ToUpper() == Resources.ReportesAvanzados.CedulaPangaLbl)
        //        //                    {
        //        //                        ViewBag.NoValidoPor = Resources.ReportesAvanzados.CedulaZonaDecretoMsj;
        //        //                        Valido = false;
        //        //                    }
        //        //                }
        //        //                #endregion

        //        //                if (Valido == true)
        //        //                {
        //        //                    ModeloTmp.TMASCEDULA.Latitud = (ModeloTmp.TMASCEDULA.Latitud ?? string.Empty).Replace("_", "");
        //        //                    ModeloTmp.TMASCEDULA.Longitud = (ModeloTmp.TMASCEDULA.Longitud ?? string.Empty).Replace("_", "");
        //        //                    //    if (lblLlamada.Length > 0)
        //        //                    //        ModeloTmp.TMASCEDULA.Llamada = Convert.ToInt32(lblLlamada);
        //        //                    //Comentado por SZ el 22/02/2017
        //        //                    //NoValidoPor = ModeloTmp.ValidaeInsertaCedula(ModeloTmp, alarmas, causas, turnos, responsableofp,UserName)
        //        //                    //Comentado por SZ el 14/03/2017
        //        //                    //NoValidoPor = ModeloTmp.ValidaeInsertaCedula(ModeloTmp, alarmas, causas, turnos, responsableofp,usuarioastrum);
        //        //                    //Agregado por SZ el 15/03/2017
        //        //                    if (ModeloTmp.IdMensaje == null)
        //        //                    {
        //        //                        //NoValidoPor = ModeloTmp.ValidaeInsertaCedula(ModeloTmp, alarmas, causas, turnos, responsableofp, usuarioastrum, new Guid());
        //        //                        NoValidoPor = ModeloTmp.ValidaeInsertaCedula(ModeloTmp, alarmas, causas, turnos, responsableofp, ModeloTmp.TMASCEDULA.UsuarioAstrum, new Guid());
        //        //                    }
        //        //                    else
        //        //                    {
        //        //                        //NoValidoPor = ModeloTmp.ValidaeInsertaCedula(ModeloTmp, alarmas, causas, turnos, responsableofp, usuarioastrum, Guid.Parse(ModeloTmp.IdMensaje));
        //        //                        NoValidoPor = ModeloTmp.ValidaeInsertaCedula(ModeloTmp, alarmas, causas, turnos, responsableofp, ModeloTmp.TMASCEDULA.UsuarioAstrum, Guid.Parse(ModeloTmp.IdMensaje));
        //        //                    }
        //        //                    if (NoValidoPor.Length <= 6)
        //        //                    {
        //        //                        return RedirectToAction("Details", "Cedula", new { id = NoValidoPor });                       
        //        //                    }
        //        //                }
        //        //            }

        //        //            //No se pudo guardar el registro , hay que mostrar que falto 
        //        //            ModeloTmp.ResultadoMensajesEmbarcacionesSeleccionar = ModeloTmp.EncuentraMensajesEmbarcacionesSeleccionarVacio("", "", "", "", "", "", "", "",UserName,"","");   //Pasa vacio la busqueda de embarcaciones si es que no se pudo guardar el registro
        //        //            //Asigno los valores no validos a el modelo para mostrar el mensaje  NoValidoPor  
        //        //            ModeloTmp.NoValidoPor_ = NoValidoPor;
        //        //            ModeloTmp.MostrarObtieneInformacion = "NO";
        //        //            break;
        //        //        //case "Buscar Informacion":
        //        //        //    #region "VALIDACIONES"
        //        //        //    Boolean ValidoBuscaInfo = true;
        //        //        //    if (ModeloTmp.FechaInicial > ModeloTmp.FechaFinal)
        //        //        //    {
        //        //        //        ViewBag.NoValidoPor = "No es posible realizar la busqueda, la fecha inicial no puede ser mayor a la fecha final. Verificar.";
        //        //        //        ValidoBuscaInfo = false;
        //        //        //    }
        //        //        //    if (ModeloTmp.FechaInicial.Year < 1753 || ModeloTmp.FechaFinal.Year < 1753)
        //        //        //    {
        //        //        //        ViewBag.NoValidoPor = "No es posible realizar la busqueda. Verificar Fechas.";
        //        //        //        ValidoBuscaInfo = false;
        //        //        //    }
        //        //        //    #endregion
        //        //        //    if (ValidoBuscaInfo == true)
        //        //        //    {
        //        //        //        ModeloTmp = BuscaMensajesEmbarcaciones(ModeloTmp, alarmas, criteriodedesconexion);
        //        //        //    }
        //        //        //    else
        //        //        //    {
        //        //        //        ModeloTmp.ResultadoMensajesEmbarcacionesSeleccionar = ModeloTmp.EncuentraMensajesEmbarcacionesSeleccionarVacio("", "", "", "", "", "", "", "", UserName, "", ""); 
        //        //        //    }
        //        //        //    ModeloTmp.MostrarObtieneInformacion = "SI";
        //        //        //    break;
        //        //        //case "Buscar Llamada":
        //        //        //    ModeloTmp.ResultadoMensajesEmbarcacionesSeleccionar = ModeloTmp.EncuentraMensajesEmbarcacionesSeleccionarVacio("", "", "", "", "", "", "", "",UserName,"","");   //Pasa vacio la busqueda de embarcaciones si es que no se pudo guardar el registro
        //        //        //    ModeloTmp.alarmasb = "SIN TRANSMISIÓN";                         // Esta parte de llamadas solo se usa para las alarmas de tipo SIN TRANSMISION
        //        //        //    ModeloTmp.MostrarLlamadas = "SI";                               //Al cargar la pantalla sigue mostrando el div usado buscar las llamadas vinculadas            
        //        //        //    break;
        //        //    }

        //        //    #region "Llenado de combos y autocomplete"
        //        //    //ViewBag.alarmasb = new SelectList(FunProc.LlenarResultadoGenerico("alarmastipo"), "Resultado", "Resultado");
        //        //    //ViewBag.alarmas = new SelectList(FunProc.LlenarResultadoGenerico("alarmastipo"), "Resultado", "Resultado");
        //        //    //ViewBag.causas = new SelectList(FunProc.LlenarResultadoGenerico("causasalarmas"), "Resultado", "Resultado");
        //        //    //ViewBag.turnos = new SelectList(FunProc.LlenarResultadoGenerico("turno"), "Resultado", "Resultado");
        //        //    //ViewBag.responsableofp = new SelectList(FunProc.LlenarResultadoGenerico("responsableofp"), "Resultado", "Resultado");
        //        //    //ViewBag.usuarioastrum = new SelectList(FunProc.LlenarResultadoGenerico("usuarioastrum"), "Resultado", "Resultado");
        //        //    //#region "Usados para la sección Obtener todos los datos posibles(Mensajes de embarcacion)"
        //        //    //ViewBag.criteriodedesconexion = new SelectList(FunProc.LlenarResultadoGenerico("criteriodedesconexion"), "Resultado", "Resultado");
        //        //    //ViewBag.modobusqueda = new SelectList(FunProc.LlenarResultadoGenerico("modobusqueda"), "Resultado", "Resultado");
        //        //    //#endregion
        //        //    #region "Usados para la sección Llamadas"
        //        //    //ModeloDeLLamada.ResultadoListaLlamadasSP = ModeloDeLLamada.EncuentraLasLlamadasSP(filtro, ModeloDeLLamada.FechaInicial, ModeloDeLLamada.FechaFinal, actividad, embarcacion, ofp);
        //        //    ModeloDeLLamada.ResultadoListaLlamadasSP = ModeloDeLLamada.EncuentraLasLlamadasSP("", ModeloDeLLamada.FechaInicial, ModeloDeLLamada.FechaFinal, "", "", "");  // Ejecuta sp y regresa las llamadas encontradas y regresa una lista  list<BuscaLlamadas_Result>
        //        //    ModeloTmp.ModeloDeLLamada = ModeloDeLLamada;
        //        //    //ViewBag.actividad = new SelectList(FunProc.LlenarResultadoGenerico("btactividades"), "Resultado", "Resultado");
        //        //    //ViewBag.ofp = new SelectList(FunProc.LlenarResultadoGenerico("responsableofp"), "Resultado", "Resultado");
        //        //    //ViewBag.filtro = new SelectList(FunProc.LlenarResultadoGenerico("filtro"), "Resultado", "Resultado");
        //        //    #endregion
        //        //    #endregion

        //        //    return View(ModeloTmp);
        //        return View();
        //        }

        //    //[HttpPost]
        //    //public ActionResult CreateBuscarLlamadas()
        //    //{
        //    //    LlamadaBusquedaModel busquedaBS = new LlamadaBusquedaModel();
        //    //    dynamic filtros = JObject.Parse(Request.Form[0]);
        //    //    DateTime inicio = (DateTime)filtros.FechaInicial;
        //    //    DateTime final = (DateTime)filtros.FechaFinal;
        //    //    var llamadas = busquedaBS.EncuentraLasLlamadasSP(filtros.filtro.ToString(), inicio, final, filtros.actividad.ToString(), filtros.embarcacion.ToString(), filtros.ofp.ToString());
        //    //    return Content(JsonConvert.SerializeObject(llamadas), "application/json");
        //    //}

        //    //[HttpPost]
        //    //public ActionResult CreateBuscarLlamadasEmbarcacionesParaClonar(InfoMensajesEmbarcacionesModel ModeloTmp)
        //    //{
        //    //    string UserName = "";
        //    //    if (HttpContext.Request.Cookies["userName"] != null)
        //    //        UserName = HttpContext.Request.Cookies["userName"].Value;
        //    //    ModeloTmp.ResultadoMensajesEmbarcacionesSeleccionar = ModeloTmp.EncuentraMensajesEmbarcacionesSeleccionar(UserName, ModeloTmp.FechaInicial, 
        //    //        ModeloTmp.FechaFinal, ModeloTmp.alarmasb, ModeloTmp.MatriculaBarcob, ModeloTmp.RazonSocialb, ModeloTmp.PuertoBaseb, ModeloTmp.TipoPermisob, 
        //    //        ModeloTmp.modobusqueda, ModeloTmp.Embarcacionb, ModeloTmp.Rnpb, _criteriodedesconexionS: ModeloTmp.MostrarObtieneInformacion);
        //    //    return Content(JsonConvert.SerializeObject(ModeloTmp), "application/json");
        //    //}

        //    //public ActionResult Editar(int idCedula = 0, string id = "", string alarmas = "", string MostrarObtieneInformacion = "", string NoCedula = "")
        //    //{
        //    //    string UserName = "";
        //    //    if (HttpContext.Request.Cookies["userName"] != null)
        //    //        UserName = HttpContext.Request.Cookies["userName"].Value;
        //    //    InfoMensajesEmbarcacionesModel ModeloTmp = new InfoMensajesEmbarcacionesModel();

        //    //    ModeloTmp.NoValidoPor_ = "";
        //    //    ModeloTmp.MostrarObtieneInformacion = "SI";
        //    //    if (MostrarObtieneInformacion == "NO" || MostrarObtieneInformacion == "")
        //    //        ModeloTmp.MostrarObtieneInformacion = "NO";

        //    //    if (idCedula > 0)
        //    //    {
        //    //        ModeloTmp.TMASCEDULA = ModeloTmp.ObtieneCedulaSeleccionadaSP(idCedula.ToString());
        //    //        ModeloTmp.alarmasb = ModeloTmp.TMASCEDULA.TipoAlerta;
        //    //        if (id != "")  // Si tiene Id significa que escogio una alerta para obtener la informacion.  El id es el IdMensajeM
        //    //        {
        //    //            //Comentado por SZ el 23/02/2017
        //    //            //ModeloTmp.TMASCEDULA = ModeloTmp.AsingaResultadoMensajeEmbarcacionSeleccionada(id, ModeloTmp.TMASCEDULA, UserName, alarmas);
        //    //            ModeloTmp.TMASCEDULA = ModeloTmp.AsingaResultadoMensajeEmbarcacionSeleccionada(id, ModeloTmp.TMASCEDULA, ModeloTmp.TMASCEDULA.UsuarioAstrum, alarmas);
        //    //            //ModeloTmp.TMASCEDULA.Llamada = 0;                   //Inicializa la llamada vinculada por que se escogio un evento determinado
        //    //            ModeloTmp.TMASCEDULA.IdCedula = idCedula;
        //    //            ModeloTmp.TMASCEDULA.NoCedula = NoCedula;

        //    //            //ModeloTmp.alarmasb = alarmas;
        //    //            //    ModeloTmp.IdMensaje = id;  //SZ170315
        //    //            //}
        //    //            //else
        //    //            //{
        //    //            //    ModeloTmp.IdMensaje = id;  //SZ170315
        //    //        }
        //    //        ModeloTmp.IdMensaje = id;  //SZ170315
        //    //    }

        //    //    #region "Llenado de combos y autocomplete"
        //    //    //ViewBag.alarmasb = new SelectList(FunProc.LlenarResultadoGenerico("alarmastipo"), "Resultado", "Resultado");
        //    //    //ViewBag.alarmas = new SelectList(FunProc.LlenarResultadoGenerico("alarmastipo"), "Resultado", "Resultado");
        //    //    //if (ModeloTmp.TMASCEDULA.TipoAlerta == "TRANSMISION IRREGULAR")
        //    //    //{
        //    //    //    List<BuscaResultadoGenerico_Result> AlarmasEnDB = FunProc.LlenarResultadoGenerico("alarmastipo").ToList();
        //    //    //    if (AlarmasEnDB != null)
        //    //    //    {
        //    //    //        List<BuscaResultadoGenerico_Result> AlarmasEnDBFinal = new List<BuscaResultadoGenerico_Result>();
        //    //    //        foreach (BuscaResultadoGenerico_Result ResultadoAlarmas in AlarmasEnDB)
        //    //    //        {
        //    //    //            AlarmasEnDBFinal.Add(ResultadoAlarmas);
        //    //    //        }
        //    //    //        BuscaResultadoGenerico_Result Result = new BuscaResultadoGenerico_Result();
        //    //    //        Result.Resultado = "TRANSMISION IRREGULAR";
        //    //    //        AlarmasEnDBFinal.Add(Result);
        //    //    //        ViewBag.alarmas2 = new SelectList(AlarmasEnDBFinal, "Resultado", "Resultado");
        //    //    //    }
        //    //    //    else
        //    //    //    {
        //    //    //        ViewBag.alarmas2 = new SelectList(FunProc.LlenarResultadoGenerico("alarmastipo"), "Resultado", "Resultado");
        //    //    //    }
        //    //    //}
        //    //    //else
        //    //    //{
        //    //    //    ViewBag.alarmas2 = new SelectList(FunProc.LlenarResultadoGenerico("alarmastipo"), "Resultado", "Resultado");
        //    //    //}

        //    //    //ViewBag.causas = new SelectList(FunProc.LlenarResultadoGenerico("causasalarmas"), "Resultado", "Resultado");
        //    //    //ViewBag.turnos = new SelectList(FunProc.LlenarResultadoGenerico("turno"), "Resultado", "Resultado");
        //    //    //ViewBag.responsableofp = new SelectList(FunProc.LlenarResultadoGenerico("responsableofp"), "Resultado", "Resultado");
        //    //    //ViewBag.usuarioastrum = new SelectList(FunProc.LlenarResultadoGenerico("usuarioastrum"), "Resultado", "Resultado");
        //    //    #region "Usados para la sección Obtener todos los datos posibles"
        //    //    ModeloTmp.FechaInicial = DateTime.Now;// Convert.ToDateTime("01/01/2013");
        //    //    ModeloTmp.FechaFinal = DateTime.Now;
        //    //    //ViewBag.criteriodedesconexion = new SelectList(FunProc.LlenarResultadoGenerico("criteriodedesconexion"), "Resultado", "Resultado");
        //    //    //ViewBag.modobusqueda = new SelectList(FunProc.LlenarResultadoGenerico("modobusqueda"), "Resultado", "Resultado");
        //    //    ModeloTmp.ResultadoMensajesEmbarcacionesSeleccionar = ModeloTmp.EncuentraMensajesEmbarcacionesSeleccionarVacio("", "", "", "", "", "", "", "", UserName, "", "");
        //    //    #endregion
        //    //    #region "Usados para la sección Llamadas"
        //    //    LlamadaBusquedaModel ModeloTmpLlamadas = new LlamadaBusquedaModel();
        //    //    ModeloTmpLlamadas.FechaInicial = DateTime.Now;
        //    //    ModeloTmpLlamadas.FechaFinal = DateTime.Now;
        //    //    ModeloTmpLlamadas.ResultadoListaLlamadasSP = ModeloTmpLlamadas.EncuentraLasLlamadasSP("", ModeloTmpLlamadas.FechaInicial, ModeloTmpLlamadas.FechaFinal, "", "", "");  // Ejecuta sp y regresa las llamadas encontradas y regresa una lista  list<BuscaLlamadas_Result>
        //    //    ModeloTmp.ModeloDeLLamada = ModeloTmpLlamadas;                  // Le asigna el modelo de llamadas a el modelo principal. CedulaBusquedaModel contiene una propiedad de LlamadaBusquedaModel
        //    //    //ViewBag.actividad = new SelectList(FunProc.LlenarResultadoGenerico("btactividades"), "Resultado", "Resultado");
        //    //    //ViewBag.ofp = new SelectList(FunProc.LlenarResultadoGenerico("responsableofp"), "Resultado", "Resultado");
        //    //    //ViewBag.filtro = new SelectList(FunProc.LlenarResultadoGenerico("filtro"), "Resultado", "Resultado");
        //    //    #endregion
        //    //    #endregion

        //    //    return View(ModeloTmp);
        //    //}

        //    //[HttpPost]
        //    //public ActionResult Editar(InfoMensajesEmbarcacionesModel ModeloTmp, LlamadaBusquedaModel ModeloDeLLamada, string submitButton, string alarmas, string TipoAlerta, string txttipoalerta, string alarmas2,
        //    //    string id_ = "", string criteriodedesconexion = "", string filtro = "", string actividad = "", string embarcacion = "", string ofp = "", string causas = "", string turnos = "", string responsableofp = "", string lblLlamada = "", string usuarioastrum = "")
        //    //{
        //    //    string UserName = "";
        //    //    if (HttpContext.Request.Cookies["userName"] != null)
        //    //        UserName = HttpContext.Request.Cookies["userName"].Value;
        //    //    ModeloTmp.NoValidoPor_ = "";
        //    //    ModeloTmp.MostrarLlamadas = "NO"; // Inicializa la pantalla sin mostrar el div de la busqueda de llamadas, esto cambia si se busca una alarma de tipo SIN TRANSMISION
        //    //    //if (responsableofp != "")
        //    //    //{
        //    //    //    ModeloTmp.TMASCEDULA.OFP = responsableofp;
        //    //    //}
        //    //    switch (submitButton)
        //    //    {
        //    //        case "Guardar":

        //    //            if(usuarioastrum != "")  ModeloTmp.TMASCEDULA.UsuarioAstrum = usuarioastrum ;

        //    //            string NoValidoPor = "";
        //    //            if (ModelState.IsValid)
        //    //            {
        //    //                #region "VALIDACIONES"            
        //    //                Boolean Valido = true;
        //    //                //if (responsableofp == null || responsableofp == "")
        //    //                //{
        //    //                //    ViewBag.NoValidoPor = "Responsable(OFP) es un campo requerido, Verificar.";
        //    //                //    Valido = false;
        //    //                //}
        //    //                //else
        //    //                //{
        //    //                //    ModeloTmp.TMASCEDULA.OFP = responsableofp;
        //    //                //}
        //    //                if (ModeloTmp.TMASCEDULA.FechaAlerta.Value > DateTime.Now.Date || ModeloTmp.TMASCEDULA.FechaCedula.Value > DateTime.Now.Date)
        //    //                {
        //    //                    ViewBag.NoValidoPor = "La fecha no puede ser mayor a la fecha actual, Verificar Fechas.";
        //    //                    Valido = false;
        //    //                }
        //    //                if (ModeloTmp.TMASCEDULA.FechaAlerta.Value.Year < 1753 || ModeloTmp.TMASCEDULA.FechaAlerta.Value.Year > 2030)
        //    //                {
        //    //                    ViewBag.NoValidoPor = "No es posible completar la acción, Verificar Fechas.";
        //    //                    Valido = false;
        //    //                }
        //    //                if (ModeloTmp.TMASCEDULA.FechaCedula.Value.Year < 1753 || ModeloTmp.TMASCEDULA.FechaCedula.Value.Year > 2030)
        //    //                {
        //    //                    ViewBag.NoValidoPor = "No es posible completar la acción, Verificar Fechas.";
        //    //                    Valido = false;
        //    //                }
        //    //                if (ModeloTmp.TMASCEDULA.TipoAlerta == "EMERGENCIA")
        //    //                {
        //    //                    if (string.IsNullOrWhiteSpace(ModeloTmp.TMASCEDULA.Causas))
        //    //                    //if (causas == null || causas == "")
        //    //                    {
        //    //                        ViewBag.NoValidoPor = "No es posible completar la acción. Para la alerta EMERGENCIA es requerido el campo causa de la alarma.";
        //    //                        Valido = false;
        //    //                    }
        //    //                    //else
        //    //                    //{
        //    //                    //}
        //    //                }
        //    //                if (ModeloTmp.TMASCEDULA.TipoAlerta == "ZONA DECRETO")
        //    //                {
        //    //                    ViewBag.Texto5 = new SelectList(FunProc.ObtieneTexto5(ModeloTmp.TMASCEDULA.RNP), "Resultado", "Resultado");
        //    //                    if (ViewBag.Texto5.Items[0].Resultado.ToString().ToUpper() == Resources.ReportesAvanzados.CedulaPangaLbl)
        //    //                    {
        //    //                        ViewBag.NoValidoPor = Resources.ReportesAvanzados.CedulaZonaDecretoEditarMsj;
        //    //                        Valido = false;
        //    //                    }
        //    //                }
        //    //                #endregion
        //    //                if (Valido == true)
        //    //                {                        
        //    //                   if (lblLlamada.Length > 0)
        //    //                        ModeloTmp.TMASCEDULA.Llamada = Convert.ToInt32(lblLlamada);
        //    //                   //Comentado por SZ el 15/03/2017
        //    //                   // NoValidoPor = ModeloTmp.ValidayActualizaCedula(ModeloTmp, alarmas2, causas, turnos, responsableofp,UserName);
        //    //                    if (ModeloTmp.IdMensaje == null)
        //    //                    {
        //    //                        NoValidoPor = ModeloTmp.ValidayActualizaCedula(ModeloTmp, alarmas2, causas, turnos, responsableofp, UserName, new Guid());
        //    //                    }
        //    //                    else
        //    //                    {
        //    //                        NoValidoPor = ModeloTmp.ValidayActualizaCedula(ModeloTmp, alarmas2, causas, turnos, responsableofp, UserName, Guid.Parse(ModeloTmp.IdMensaje));
        //    //                    }
        //    //                    if (NoValidoPor.Length <= 6)
        //    //                    {
        //    //                        return RedirectToAction("Details", "Cedula", new { id = ModeloTmp.TMASCEDULA.IdCedula });                      
        //    //                    }
        //    //                }
        //    //            }

        //    //            //No se pudo guardar el registro , hay que mostrar que falto 
        //    //            ModeloTmp.ResultadoMensajesEmbarcacionesSeleccionar = ModeloTmp.EncuentraMensajesEmbarcacionesSeleccionarVacio("", "", "", "", "", "", "", "",UserName,"","");   //Pasa vacio la busqueda de embarcaciones si es que no se pudo guardar el registro
        //    //            //Asigno los valores no validos a el modelo para mostrar el mensaje  NoValidoPor  
        //    //            ModeloTmp.NoValidoPor_ = NoValidoPor;
        //    //            ModeloTmp.MostrarObtieneInformacion = "NO";
        //    //            break;
        //    //        //case "Buscar Informacion":
        //    //        //    #region "VALIDACIONES"
        //    //        //    Boolean ValidoBuscaInfo = true;
        //    //        //    if (ModeloTmp.FechaInicial > ModeloTmp.FechaFinal)
        //    //        //    {
        //    //        //        ViewBag.NoValidoPor = "No es posible realizar la busqueda, la fecha inicial no puede ser mayor a la fecha final. Verificar.";
        //    //        //        ValidoBuscaInfo = false;
        //    //        //    }
        //    //        //    if (ModeloTmp.FechaInicial.Year < 1753 || ModeloTmp.FechaFinal.Year < 1753)
        //    //        //    {
        //    //        //        ViewBag.NoValidoPor = "No es posible realizar la busqueda. Verificar Fechas.";
        //    //        //        ValidoBuscaInfo = false;
        //    //        //    }
        //    //        //    #endregion
        //    //        //    if (ValidoBuscaInfo == true)
        //    //        //    {
        //    //        //        ModeloTmp = BuscaMensajesEmbarcaciones(ModeloTmp, alarmas, criteriodedesconexion);
        //    //        //    }
        //    //        //    else
        //    //        //    {
        //    //        //        ModeloTmp.ResultadoMensajesEmbarcacionesSeleccionar = ModeloTmp.EncuentraMensajesEmbarcacionesSeleccionarVacio("", "", "", "", "", "", "", "", UserName, "", ""); 
        //    //        //    }
        //    //        //    ModeloTmp.MostrarObtieneInformacion = "SI"; 
        //    //        //    break;
        //    //        //case "Buscar Llamada":
        //    //        //    ModeloTmp.ResultadoMensajesEmbarcacionesSeleccionar = ModeloTmp.EncuentraMensajesEmbarcacionesSeleccionarVacio("", "", "", "", "", "", "", "",UserName,"","");   //Pasa vacio la busqueda de embarcaciones si es que no se pudo guardar el registro
        //    //        //    ModeloTmp.alarmasb = "SIN TRANSMISIÓN";                         // Esta parte de llamadas solo se usa para las alarmas de tipo SIN TRANSMISION
        //    //        //    ModeloTmp.MostrarLlamadas = "SI";                               //Al cargar la pantalla sigue mostrando el div usado buscar las llamadas vinculadas            
        //    //        //    break;
        //    //    }


        //    //    #region "Llenado de combos y autocomplete"
        //    //    //ViewBag.alarmasb = new SelectList(FunProc.LlenarResultadoGenerico("alarmastipo"), "Resultado", "Resultado");
        //    //    //ViewBag.alarmas = new SelectList(FunProc.LlenarResultadoGenerico("alarmastipo"), "Resultado", "Resultado");
        //    //    //ViewBag.alarmas2 = new SelectList(FunProc.LlenarResultadoGenerico("alarmastipo"), "Resultado", "Resultado");
        //    //    //if (ModeloTmp.TMASCEDULA.TipoAlerta == "TRANSMISION IRREGULAR")
        //    //    //{
        //    //    //    List<BuscaResultadoGenerico_Result> AlarmasEnDB = FunProc.LlenarResultadoGenerico("alarmastipo").ToList();
        //    //    //    if (AlarmasEnDB != null)
        //    //    //    {
        //    //    //        List<BuscaResultadoGenerico_Result> AlarmasEnDBFinal = new List<BuscaResultadoGenerico_Result>();
        //    //    //        foreach (BuscaResultadoGenerico_Result ResultadoAlarmas in AlarmasEnDB)
        //    //    //        {
        //    //    //            AlarmasEnDBFinal.Add(ResultadoAlarmas);
        //    //    //        }
        //    //    //        BuscaResultadoGenerico_Result Result = new BuscaResultadoGenerico_Result();
        //    //    //        Result.Resultado = "TRANSMISION IRREGULAR";
        //    //    //        AlarmasEnDBFinal.Add(Result);
        //    //    //        ViewBag.alarmas2 = new SelectList(AlarmasEnDBFinal, "Resultado", "Resultado");
        //    //    //    }
        //    //    //    else
        //    //    //    {
        //    //    //        ViewBag.alarmas2 = new SelectList(FunProc.LlenarResultadoGenerico("alarmastipo"), "Resultado", "Resultado");
        //    //    //    }
        //    //    //}
        //    //    //else
        //    //    //{
        //    //    //    ViewBag.alarmas2 = new SelectList(FunProc.LlenarResultadoGenerico("alarmastipo"), "Resultado", "Resultado");
        //    //    //}

        //    //    //ViewBag.causas = new SelectList(FunProc.LlenarResultadoGenerico("causasalarmas"), "Resultado", "Resultado");
        //    //    //ViewBag.turnos = new SelectList(FunProc.LlenarResultadoGenerico("turno"), "Resultado", "Resultado");
        //    //    //ViewBag.responsableofp = new SelectList(FunProc.LlenarResultadoGenerico("responsableofp"), "Resultado", "Resultado");
        //    //    //ViewBag.usuarioastrum = new SelectList(FunProc.LlenarResultadoGenerico("usuarioastrum"), "Resultado", "Resultado");
        //    //    //#region "Usados para la sección Obtener todos los datos posibles(Mensajes de embarcacion)"
        //    //    //ViewBag.criteriodedesconexion = new SelectList(FunProc.LlenarResultadoGenerico("criteriodedesconexion"), "Resultado", "Resultado");
        //    //    //ViewBag.modobusqueda = new SelectList(FunProc.LlenarResultadoGenerico("modobusqueda"), "Resultado", "Resultado");
        //    //    //#endregion
        //    //    #region "Usados para la sección Llamadas"
        //    //    //ModeloDeLLamada.ResultadoListaLlamadasSP = ModeloDeLLamada.EncuentraLasLlamadasSP(filtro, ModeloDeLLamada.FechaInicial, ModeloDeLLamada.FechaFinal, actividad, embarcacion, ofp);
        //    //    ModeloDeLLamada.ResultadoListaLlamadasSP = ModeloDeLLamada.EncuentraLasLlamadasSP("", ModeloDeLLamada.FechaInicial, ModeloDeLLamada.FechaFinal, "", "", "");
        //    //    ModeloTmp.ModeloDeLLamada = ModeloDeLLamada;
        //    //    //ViewBag.actividad = new SelectList(FunProc.LlenarResultadoGenerico("btactividades"), "Resultado", "Resultado");
        //    //    //ViewBag.ofp = new SelectList(FunProc.LlenarResultadoGenerico("responsableofp"), "Resultado", "Resultado");
        //    //    //ViewBag.filtro = new SelectList(FunProc.LlenarResultadoGenerico("filtro"), "Resultado", "Resultado");
        //    //    #endregion
        //    //    #endregion

        //    //    return View(ModeloTmp);
        //    //}

        //    //// GET: /Cedula/Edit/5
        //    //public ActionResult Edit(int id = 0)
        //    //{
        //    //    ViewBag.alarmas = new SelectList(FunProc.LlenarResultadoGenerico("alarmastipo"), "Resultado", "Resultado");
        //    //    ViewBag.causas = new SelectList(FunProc.LlenarResultadoGenerico("causasalarmas"), "Resultado", "Resultado");
        //    //    ViewBag.turnos = new SelectList(FunProc.LlenarResultadoGenerico("turno"), "Resultado", "Resultado");
        //    //    ViewBag.responsableofp = new SelectList(FunProc.LlenarResultadoGenerico("responsableofp"), "Resultado", "Resultado");
        //    //    ViewBag.anexos = new SelectList(FunProc.LlenarResultadoGenerico("anexo"), "Resultado", "Resultado");

        //    //    TMasCedula tmascedula = db.TMasCedulas.Single(t => t.IdCedula == id);
        //    //    if (tmascedula == null)
        //    //    {
        //    //        return StatusCode(404);
        //    //    }
        //    //    return View(tmascedula);
        //    //}

        //    //// POST: /Cedula/Edit/5
        //    //[HttpPost]
        //    //public ActionResult Edit(TMasCedula tmascedula)
        //    //{
        //    //    if (ModelState.IsValid)
        //    //    {
        //    //        db.TMasCedulas.Attach(tmascedula);
        //    //        //db.ObjectStateManager.ChangeObjectState(tmascedula, EntityState.Modified);
        //    //        db.SaveChanges();
        //    //        return RedirectToAction("Index");
        //    //    }
        //    //    return View(tmascedula);
        //    //}

        //    //// GET: /Cedula/Delete/5
        //    //public ActionResult Delete(int id = 0)
        //    //{
        //    //    TMasCedula tmascedula = db.TMasCedulas.Single(t => t.IdCedula == id);
        //    //    if (tmascedula == null)
        //    //    {
        //    //        return StatusCode(404);
        //    //    }
        //    //    return View(tmascedula);
        //    //}

        //    //// POST: /Cedula/Delete/5
        //    //[HttpPost, ActionName("Delete")]
        //    //public ActionResult DeleteConfirmed(int id)
        //    //{
        //    //    TMasCedula tmascedula = db.TMasCedulas.Single(t => t.IdCedula == id);
        //    //    db.TMasCedulas.Remove(tmascedula);
        //    //    db.SaveChanges();
        //    //    return RedirectToAction("Index");
        //    //}

        //    //#region "EXPORTAR A HTML"

        //    //private Boolean RangoDeFechas(DateTime FechaEnUso, DateTime FechaFinal)
        //    //{
        //    //    Boolean EnRango = true;

        //    //    if (FechaEnUso > FechaFinal)
        //    //        EnRango = false;
        //    //    return EnRango;
        //    //}
        //    //private int ObtieneTipoFecha(DateTime FechaEnUso)
        //    //{
        //    //    int TipoFecha = 0;
        //    //    TipoFecha = EsDiaVerano(FechaEnUso);
        //    //    return TipoFecha;
        //    //}
        //    //private DateTime RegresaFin(DateTime FechaTmp, int TipoFechaTmp)
        //    //{
        //    //    FechaTmp = Convert.ToDateTime(FechaTmp.ToShortDateString() + " 23:59:59");
        //    //    DateTime FechaFinal = FechaTmp;
        //    //    if (TipoFechaTmp == 0)
        //    //        FechaFinal = FechaTmp.AddHours(6);
        //    //    if (TipoFechaTmp == 1)
        //    //        FechaFinal = FechaTmp.AddHours(6);
        //    //    if (TipoFechaTmp == 2)
        //    //        FechaFinal = FechaTmp.AddHours(6);
        //    //    if (TipoFechaTmp == 3)
        //    //        FechaFinal = FechaTmp.AddHours(7);

        //    //    return FechaFinal;
        //    //}

        //    //private DateTime RegresaInicio(DateTime FechaTmp, int TipoFechaTmp)
        //    //{
        //    //    FechaTmp = Convert.ToDateTime(FechaTmp.ToShortDateString() + " 00:00:00");
        //    //    DateTime FechaFinal = FechaTmp;
        //    //    if (TipoFechaTmp == 0)
        //    //        FechaFinal = FechaTmp.AddHours(6);
        //    //    if (TipoFechaTmp == 1)
        //    //        FechaFinal = FechaTmp.AddHours(7);
        //    //    if (TipoFechaTmp == 2)
        //    //        FechaFinal = FechaTmp.AddHours(6);
        //    //    if (TipoFechaTmp == 3)
        //    //        FechaFinal = FechaTmp.AddHours(7);

        //    //    return FechaFinal;
        //    //}
        //    //private int EsDiaVerano(DateTime FechaEnUsoTmp)
        //    //{
        //    //    int Resp = 0;
        //    //    DateTime Inicia = IniciaVeranoSinHora(FechaEnUsoTmp.Year);
        //    //    DateTime Termina = TerminaVeranoSinHora(FechaEnUsoTmp.Year);
        //    //    if (FechaEnUsoTmp > Inicia && FechaEnUsoTmp < Termina)
        //    //    {
        //    //        Resp = 0;
        //    //    }
        //    //    else if (FechaEnUsoTmp == Inicia)
        //    //    {
        //    //        Resp = 1;
        //    //    }
        //    //    else if (FechaEnUsoTmp == Termina)
        //    //    {
        //    //        Resp = 2;
        //    //    }
        //    //    else
        //    //    {
        //    //        Resp = 3;
        //    //    }

        //    //    return Resp;
        //    //}
        //    //private DateTime IniciaVeranoSinHora(int year)
        //    //{
        //    //    DateTime FechaInicia;
        //    //    Boolean PrimerDomingo = false;
        //    //    string fecha = "";
        //    //    fecha = year.ToString() + "-04-01";
        //    //    FechaInicia = Convert.ToDateTime(fecha);
        //    //    while (PrimerDomingo == false)
        //    //    {
        //    //        if (FechaInicia.DayOfWeek.ToString() == "")
        //    //        {
        //    //            PrimerDomingo = true;
        //    //            FechaInicia = Convert.ToDateTime(fecha);
        //    //        }
        //    //        FechaInicia = FechaInicia.AddDays(1);
        //    //    }
        //    //    return FechaInicia;

        //    //}
        //    //private DateTime TerminaVeranoSinHora(int year)
        //    //{
        //    //    DateTime Fecha;
        //    //    Boolean UltimoDomingo = false;
        //    //    string fecha = "";
        //    //    fecha = year.ToString() + "-10-31";
        //    //    Fecha = Convert.ToDateTime(fecha);
        //    //    while (UltimoDomingo == false)
        //    //    {
        //    //        if (Fecha.DayOfWeek.ToString() == "")
        //    //        {
        //    //            UltimoDomingo = true;
        //    //            Fecha = Convert.ToDateTime(fecha);
        //    //        }
        //    //        Fecha = Fecha.AddDays(-1);
        //    //    }
        //    //    return Fecha;

        //    //}

        //    //public ActionResult GeneraReporteCedulaHTML(string Embarcacion, string Matricula, DateTime FechaInicioRep, DateTime FechaFinRep)
        //    //{

        //    //    string example = "Tabular de Ruta";
        //    //    string ruta_encabezado = Server.MapPath("~/Templates/Cedulas/encabezado.png");
        //    //    //todo: add some data from your database into that string:
        //    //    var CuerpoHTML = example;
        //    //    CuerpoHTML = "<html><body>" + Environment.NewLine;
        //    //    CuerpoHTML += "<img src='" + ruta_encabezado + "'>" + Environment.NewLine;
        //    //    ReporteCedulasTabularRutaCabecera_Result Cabecera = db.ReporteCedulasTabularRutaCabecera(Matricula).FirstOrDefault();
        //    //    if (Cabecera != null)
        //    //    {
        //    //        CuerpoHTML += "<h4>Reporte de transmision de la embarcacion " + Cabecera.NOMBRE + "</h4>";
        //    //        CuerpoHTML += "<h4>Matricula: " + Cabecera.NOMBRE + " " + " RNP: " + Cabecera.RNP + "</h4>";
        //    //        //CuerpoHTML += "<h4>RNP: " + Cabecera.RNP + "</h4>" + Environment.NewLine;
        //    //        CuerpoHTML += "<h4>Razon Social: " + Cabecera.RAZONSOCIAL + "</h4>";
        //    //        CuerpoHTML += "<h4>Periodo comprendido del " + FechaInicioRep.ToShortDateString() + " al " + FechaFinRep.ToShortDateString() + "</h4>" + Environment.NewLine;
        //    //    }
        //    //    CuerpoHTML += "<table border=1 cellspacing=1 cellpadding=1>";
        //    //    CuerpoHTML += "<tr><td><b>No</b></td><td><b>Fecha y Hora</b></td><td><b>Latitud</b></td><td><b>Longitud</b></td><td><b>Rumbo</b></td><td><b>Velocidad</b></td><td><b>Referencia</b></td></tr>";

        //    //    int contador = 1;
        //    //    DateTime FechaEnUso = FechaInicioRep;
        //    //    while (RangoDeFechas(FechaEnUso, FechaFinRep))
        //    //    {
        //    //        int TipoFecha = 0;
        //    //        TipoFecha = ObtieneTipoFecha(FechaEnUso);
        //    //        DateTime ini = RegresaInicio(FechaEnUso, TipoFecha);
        //    //        DateTime fin = RegresaFin(FechaEnUso, TipoFecha);
        //    //        List<ReporteCedulasTabularRutaCuerpo_Result> Cuerpo = db.ReporteCedulasTabularRutaCuerpo(Cabecera.MATRICULA, ini, fin).ToList();
        //    //        if (Cuerpo.Count > 0)
        //    //        {
        //    //            foreach (ReporteCedulasTabularRutaCuerpo_Result Registro in Cuerpo)
        //    //            {
        //    //                CuerpoHTML += "<tr>";
        //    //                CuerpoHTML += "<td>" + contador.ToString() + "</td>";

        //    //                CuerpoHTML += "<td>" + Registro.FECHA + "</td>";
        //    //                CuerpoHTML += "<td>" + Registro.LATITUD + "</td>";
        //    //                CuerpoHTML += "<td>" + Registro.LATITUD + "</td>";
        //    //                CuerpoHTML += "<td>" + Registro.RUMBO + "</td>";
        //    //                CuerpoHTML += "<td>" + Registro.VELOCIDAD + "</td>";

        //    //                //      CONSULTA LA REFERENCIA
        //    //                //CuerpoHTML += "<td>" + Registro.VELOCIDAD + "</td>";
        //    //                CuerpoHTML += "</tr>";
        //    //                contador += 1;
        //    //            }
        //    //        }
        //    //        else
        //    //        {
        //    //            CuerpoHTML += "<tr>";
        //    //            CuerpoHTML += "<td>" + contador.ToString() + "</td>";
        //    //            CuerpoHTML += "<td>" + FechaEnUso.ToString() + "</td>";
        //    //            CuerpoHTML += "<td>" + "NULO" + "</td>";
        //    //            CuerpoHTML += "<td>" + "NULO" + "</td>";
        //    //            CuerpoHTML += "<td>" + "NULO" + "</td>";
        //    //            CuerpoHTML += "<td>" + "NULO" + "</td>";
        //    //            //      CONSULTA LA REFERENCIA
        //    //            CuerpoHTML += "<td>" + "NULO" + "</td>";
        //    //            CuerpoHTML += "</tr>";
        //    //            contador += 1;
        //    //        }

        //    //        FechaEnUso = FechaEnUso.AddDays(1);
        //    //    }

        //    //    CuerpoHTML += "</body></html>" + Environment.NewLine;

        //    //    //Build your stream
        //    //    var byteArray = Encoding.ASCII.GetBytes(CuerpoHTML);
        //    //    var stream = new MemoryStream(byteArray);

        //    //    //Returns a file that will match your value passed in (ie TestID2.txt)
        //    //    return File(stream, "text/html", String.Format("{0}.html", example));

        //    //    //Si funciona
        //    //    //string Plantilla = "FileName";
        //    //    //string doc = "Cuerpo del documento";
        //    //    //return new ArchivoResult(doc, "Reporte " + Plantilla + " " + FunProc.ObtieneIdentificadorFechaHora());

        //    //}

        //    //public class ArchivoResult : ActionResult
        //    //{
        //    //    private readonly string _archivo;
        //    //    private readonly string _fileName;

        //    //    public ArchivoResult(string archivo, string fileName)
        //    //    {
        //    //        _archivo = archivo;
        //    //        _fileName = fileName;
        //    //    }

        //    //    public override void ExecuteResult(ControllerContext context)
        //    //    {
        //    //        var response = context.HttpContext.Response;
        //    //        response.Clear();
        //    //        response.ContentType = "text/html";
        //    //        response.Output.WriteLine(_archivo);
        //    //        response.AppendHeader("content-disposition", "attachment; filename=" + _fileName);
        //    //        response.End();
        //    //    }
        //    //}

        //    //#endregion

        //    //#region "EXPORTAR A WORD"
        //    //public ActionResult GeneraReporteCedulaUnicaWord(int? id) //Documento documento
        //    //{
        //    //    int? IdCedula = new int();
        //    //    IdCedula = Session["SessionIdCedula"] as int?;
        //    //    ReporteCedulaUnica_Result Datos = FunProc.EncuentraReporteCedulaUnica(Convert.ToInt32(id));
        //    //    if (Datos.TipoAlerta == "PROXIMIDAD A EMBARCACIONES" || Datos.TipoAlerta == "MÚLTIPLES PERMISOS" || Datos.TipoAlerta == "PERMISO VENCIDO" || Datos.TipoAlerta == "TÉCNICAMENTE PESCANDO" || Datos.TipoAlerta == "DESCONECTADO")
        //    //    {
        //    //        Datos.TipoAlerta = "TRANSMISION IRREGULAR";
        //    //    }

        //    //    string Plantilla = "vacio";
        //    //    if (Datos != null)
        //    //    {
        //    //        Plantilla = FunProc.QuitaAcentos(Datos.TipoAlerta);

        //    //    }
        //    //    DocX doc;
        //    //    string ruta = Server.MapPath("~/Templates/Cedulas/" + Plantilla + ".docx");

        //    //    string rutadestino = Server.MapPath("~/Templates/GENERADOS/" + Plantilla + " " + FunProc.ObtieneIdentificadorFechaHora() + ".docx");
        //    //    if (System.IO.File.Exists(ruta) == true)
        //    //    {
        //    //        doc = DocX.Load(ruta);
        //    //        #region "ASIGNA VALOR A LOS CAMPOS CUANDO ES NULO"
        //    //        if (Datos.NoCedula == null) Datos.NoCedula = "NO APLICA";
        //    //        if (Datos.TipoAlerta == null) Datos.TipoAlerta = "NO APLICA";
        //    //        //if (Datos.FechaCedula == null) Datos.FechaCedula = DateTime.Now.Date;
        //    //        if (Datos.Latitud == null) Datos.Latitud = "NO APLICA";
        //    //        if (Datos.Longitud == null) Datos.Longitud = "NO APLICA";
        //    //        if (Datos.Velocidad == null) Datos.Velocidad = "NO APLICA";
        //    //        if (Datos.Profundidad == null) Datos.Profundidad = "NO APLICA";
        //    //        if (Datos.DistanciaCosta == null) Datos.DistanciaCosta = "NO APLICA";
        //    //        if (Datos.Zona == null) Datos.Zona = "NO APLICA";
        //    //        if (Datos.Referencia == null) Datos.Referencia = "NO APLICA";
        //    //        if (Datos.Causas == null) Datos.Causas = "NO APLICA";
        //    //        if (Datos.Estatus == null) Datos.Estatus = "NO APLICA";
        //    //        if (Datos.UltimaTx == null) Datos.UltimaTx = "NO APLICA";
        //    //        //if (Datos.FechaAlerta == null) Datos.FechaAlerta = DateTime.Now.Date;
        //    //        if (Datos.HoraAlerta == null) Datos.HoraAlerta = "NO APLICA";
        //    //        if (Datos.MatriculaBarco == null) Datos.MatriculaBarco = "NO APLICA";
        //    //        if (Datos.NombreBarco == null) Datos.NombreBarco = "NO APLICA";
        //    //        if (Datos.RNP == null) Datos.RNP = "NO APLICA";
        //    //        if (Datos.TipoPermiso == null) Datos.TipoPermiso = "NO APLICA";
        //    //        if (Datos.PuertoBase == null) Datos.PuertoBase = "NO APLICA";
        //    //        if (Datos.RazonSocial == null) Datos.RazonSocial = "NO APLICA";
        //    //        if (Datos.Litoral == null) Datos.Litoral = "NO APLICA";
        //    //        if (Datos.Domicilio == null) Datos.Domicilio = "NO APLICA";
        //    //        if (Datos.Contacto == null) Datos.Contacto = "NO APLICA";
        //    //        if (Datos.Puesto == null) Datos.Puesto = "NO APLICA";
        //    //        if (Datos.Telefono == null) Datos.Telefono = "NO APLICA";
        //    //        if (Datos.Comentarios == null) Datos.Comentarios = "NO APLICA";
        //    //        if (Datos.Anexos == null) Datos.Anexos = "NO APLICA";
        //    //        if (Datos.OFP == null) Datos.OFP = "NO APLICA";
        //    //        if (Datos.Turno == null) Datos.Turno = "NO APLICA";
        //    //        if (Datos.NombreContactada == null) Datos.NombreContactada = "NO APLICA";
        //    //        if (Datos.PuestoContactada == null) Datos.PuestoContactada = "NO APLICA";
        //    //        if (Datos.TelefonoContactada == null) Datos.TelefonoContactada = "NO APLICA";
        //    //        if (Datos.Llamada == null) Datos.Llamada = "";
        //    //        if (Datos.LlamadaFecha == null) Datos.LlamadaFecha = "";
        //    //        if (Datos.Estado == null) Datos.Estado = "NO APLICA";
        //    //        if (Datos.Vigencias == null) Datos.Vigencias = "NO APLICA";
        //    //        if (Datos.Periodo == null) Datos.Periodo = "NO APLICA";
        //    //        #endregion

        //    //        if (Datos.Estatus.Contains("EN PUERTO") == true)
        //    //        {
        //    //            doc.ReplaceText("«enpuerto»", "X");
        //    //            doc.ReplaceText("«fueradepuerto»", " ");
        //    //        }
        //    //        else
        //    //        {
        //    //            doc.ReplaceText("«fueradepuerto»", "X");
        //    //            doc.ReplaceText("«enpuerto»", " ");
        //    //        }

        //    //        //SIN TRANSMISION
        //    //        doc.ReplaceText("«NoCedula»", Datos.NoCedula);
        //    //        doc.ReplaceText("«FechaCed»", Datos.FechaCedula.Value.Date.ToShortDateString());
        //    //        doc.ReplaceText("«OFP»", Datos.OFP);
        //    //        doc.ReplaceText("«Turno»", Datos.Turno);
        //    //        doc.ReplaceText("«TipoPermiso»", Datos.TipoPermiso);
        //    //        doc.ReplaceText("«Vigencias»", Datos.Vigencias);
        //    //        doc.ReplaceText("«NombreBarco»", Datos.NombreBarco);
        //    //        doc.ReplaceText("«MatBarco»", Datos.MatriculaBarco);
        //    //        doc.ReplaceText("«RNP»", Datos.RNP);
        //    //        doc.ReplaceText("«PuertoBase»", Datos.PuertoBase);
        //    //        doc.ReplaceText("«RazonSocial»", Datos.RazonSocial);
        //    //        doc.ReplaceText("«Domicilio»", Datos.Domicilio);
        //    //        doc.ReplaceText("«PuertoBase»", Datos.PuertoBase);
        //    //        doc.ReplaceText("«Estado»", Datos.Estado);
        //    //        doc.ReplaceText("«Estatus»", Datos.Estatus);
        //    //        doc.ReplaceText("«UltimaTx»", Datos.UltimaTx);
        //    //        doc.ReplaceText("«Latitud»", Datos.Latitud);
        //    //        doc.ReplaceText("«Longitud»", Datos.Longitud);

        //    //        doc.ReplaceText("«LlamadaFecha»", Datos.LlamadaFecha);
        //    //        if (Datos.LlamadaFecha != null && Datos.LlamadaFecha != "")
        //    //        {
        //    //            doc.ReplaceText("«si»", "X");
        //    //            doc.ReplaceText("«no»", "");
        //    //        }
        //    //        else
        //    //        {
        //    //            doc.ReplaceText("«si»", "");
        //    //            doc.ReplaceText("«no»", "X");
        //    //        }


        //    //        doc.ReplaceText("«Comentarios»", Datos.Comentarios);
        //    //        doc.ReplaceText("«Anexos»", Datos.Anexos);

        //    //        //EMERGENCIA
        //    //        doc.ReplaceText("«TContactada»", Datos.TelefonoContactada);
        //    //        doc.ReplaceText("«NContactada»", Datos.NombreContactada);
        //    //        doc.ReplaceText("«PContactada»", Datos.PuestoContactada);

        //    //        doc.ReplaceText("«FechaAlerta»", Datos.FechaAlerta.Value.ToShortDateString());
        //    //        doc.ReplaceText("«HoraAlerta»", Datos.HoraAlerta);
        //    //        //doc.ReplaceText("«Latitud»", Datos.);
        //    //        //doc.ReplaceText("«Longitud»", Datos.);
        //    //        doc.ReplaceText("«TipoAlerta»", Datos.TipoAlerta);
        //    //        doc.ReplaceText("«Causas»", Datos.Causas);

        //    //        //PESCA PROHIBIDA
        //    //        doc.ReplaceText("«Domicilio»", Datos.Domicilio);
        //    //        doc.ReplaceText("«Telefono»", Datos.Telefono);
        //    //        doc.ReplaceText("«Contacto»", Datos.Contacto);
        //    //        doc.ReplaceText("«TipoPermiso»", Datos.TipoPermiso);
        //    //        doc.ReplaceText("«DistCosta»", Datos.DistanciaCosta);
        //    //        doc.ReplaceText("«Velocidad»", Datos.Velocidad);
        //    //        doc.ReplaceText("«Profundidad»", Datos.Profundidad);
        //    //        doc.ReplaceText("«Zona»", Datos.Zona);
        //    //        doc.ReplaceText("«Referencia»", Datos.Referencia);
        //    //        //doc.ReplaceText("", Datos.);

        //    //        doc.SaveAs(rutadestino);
        //    //    }
        //    //    else
        //    //    {
        //    //        doc = DocX.Load(ruta);
        //    //        //string NoExisteLaPlantilla = "";
        //    //    }
        //    //    return new WordResult(doc, "Reporte " + Plantilla + " " + FunProc.ObtieneIdentificadorFechaHora());
        //    //}
        //    //#endregion

        //    //#region "EXPORTAR A PDF"
        //    //public ActionResult GeneratePdfCedulas()
        //    //{
        //    //    CedulaBusquedaModel CedBusModel = new CedulaBusquedaModel();
        //    //    CedBusModel = Session["SessionCedBusModel"] as CedulaBusquedaModel;
        //    //    #region "VALIDACIONES"
        //    //    Boolean Valido = true;
        //    //    if (CedBusModel.FechaInicial > CedBusModel.FechaFinal)
        //    //    {
        //    //        ViewBag.NoValidoPor = "No es posible realizar la búsqueda, la fecha inicial no puede ser mayor a la fecha final. Verificar.";
        //    //        Valido = false;
        //    //    }
        //    //    if (CedBusModel.FechaInicial.Year < 1753 || CedBusModel.FechaFinal.Year < 1753)
        //    //    {
        //    //        ViewBag.NoValidoPor = "No es posible realizar la búsqueda. Verificar Fechas.";
        //    //        Valido = false;
        //    //    }
        //    //    #endregion
        //    //    List<ReporteCedulasExportarBusqueda_Result> ListaTmp = new List<ReporteCedulasExportarBusqueda_Result>();
        //    //    if (Valido == true)
        //    //    {
        //    //        if (CedBusModel != null)
        //    //            ListaTmp = FunProc.EncuentraReporteCedulasExportarBusqueda(CedBusModel);
        //    //    }
        //    //    var pdf = new PdfResult(ListaTmp, "GeneratePdfCedulas");

        //    //    return pdf;
        //    //    //return View(ListaTmp);
        //    //}
        //    //#endregion

        //    //#region "EXPORTAR A EXCEL"

        //    //public ActionResult GenerateExcel()
        //    //{
        //    //    string UserName = "";
        //    //    if (HttpContext.Request.Cookies["userName"] != null)
        //    //        UserName = HttpContext.Request.Cookies["userName"].Value;
        //    //    CedulaBusquedaModel CedBusModel = new CedulaBusquedaModel();
        //    //    CedBusModel = Session["SessionCedBusModel"] as CedulaBusquedaModel;
        //    //    #region "VALIDACIONES"
        //    //    Boolean Valido = true;
        //    //    if (CedBusModel.FechaInicial > CedBusModel.FechaFinal)
        //    //    {
        //    //        ViewBag.NoValidoPor = "No es posible realizar la búsqueda, la fecha inicial no puede ser mayor a la fecha final. Verificar.";
        //    //        Valido = false;
        //    //    }
        //    //    if (CedBusModel.FechaInicial.Year < 1753 || CedBusModel.FechaFinal.Year < 1753)
        //    //    {
        //    //        ViewBag.NoValidoPor = "No es posible realizar la búsqueda. Verificar Fechas.";
        //    //        Valido = false;
        //    //    }
        //    //    #endregion
        //    //    List<ReporteCedulasExportarBusqueda_Result> ListaTmp = new List<ReporteCedulasExportarBusqueda_Result>();
        //    //    if (Valido == true)
        //    //    {
        //    //        if (CedBusModel != null)
        //    //            ListaTmp = FunProc.EncuentraReporteCedulasExportarBusqueda(CedBusModel);
        //    //    }
        //    //    string ruta = Server.MapPath("~/Templates/Cedulas/ExportarTodo.xlsx");
        //    //    var workbook = new XLWorkbook(ruta);
        //    //    var worksheet = workbook.Worksheet(1);
        //    //    worksheet.Cell("A8").Value = "Impreso por: " + UserName + " | Fecha: " + DateTime.Now.ToShortDateString();
        //    //    string ParametrosDeBusqueda = "";
        //    //    ParametrosDeBusqueda = Session["SessionParametrosDeBusqueda"] as string;
        //    //    worksheet.Cell("A9").Value = ParametrosDeBusqueda;
        //    //    worksheet.Cell("A12").Value = ListaTmp;

        //    //    return new ExcelResult(workbook, "Exportar Búsqueda Cedulas");
        //    //}

        //    //#endregion

        //    //#region "AUTOCOMPLETE"
        //    //[HttpGet]
        //    //public ActionResult SearchResultadoGenerico_numerodecedula(DataSourceLoadOptions loadOptions)
        //    //{
        //    //    var criterio = (from opciones in ((JArray)loadOptions.Filter[0]).ToList<dynamic>()
        //    //             where opciones.Value != "this" && opciones.Value != "contains"
        //    //             select opciones.Value).FirstOrDefault();

        //    //    return Content(JsonConvert.SerializeObject(DataSourceLoader.Load(FunProc.FindRsultadoGenerico(criterio, 0, "nocedula"), loadOptions)), "application/json");
        //    //}

        //    //[HttpGet]
        //    //public ActionResult SearchResultadoGenerico_matriculabarco(DataSourceLoadOptions loadOptions)
        //    //{
        //    //    var criterio = (from opciones in ((JArray)loadOptions.Filter[0]).ToList<dynamic>()
        //    //                    where opciones.Value != "this" && opciones.Value != "contains"
        //    //                    select opciones.Value).FirstOrDefault();

        //    //    return Content(JsonConvert.SerializeObject(DataSourceLoader.Load(FunProc.FindRsultadoGenerico(criterio, 0, "matriculabarco"), loadOptions)), "application/json");
        //    //}

        //    //[HttpGet]
        //    //public ActionResult SearchResultadoGenerico_turno(DataSourceLoadOptions loadOptions)
        //    //{
        //    //    var criterio = (from opciones in ((JArray)loadOptions.Filter[0]).ToList<dynamic>()
        //    //                    where opciones.Value != "this" && opciones.Value != "contains"
        //    //                    select opciones.Value).FirstOrDefault();

        //    //    return Content(JsonConvert.SerializeObject(DataSourceLoader.Load(FunProc.FindRsultadoGenerico(criterio, 0, "turno"), loadOptions)), "application/json");
        //    //}

        //    //[HttpGet]
        //    //public ActionResult SearchResultadoGenerico_anexo(DataSourceLoadOptions loadOptions)
        //    //{
        //    //    var criterio = (from opciones in ((JArray)loadOptions.Filter[0]).ToList<dynamic>()
        //    //                    where opciones.Value != "this" && opciones.Value != "contains"
        //    //                    select opciones.Value).FirstOrDefault();

        //    //    return Content(JsonConvert.SerializeObject(DataSourceLoader.Load(FunProc.FindRsultadoGenerico(criterio, 0, "anexo"), loadOptions)), "application/json");
        //    //}

        //    //[HttpGet]
        //    //public ActionResult SearchResultadoGenerico_nombrebarco(DataSourceLoadOptions loadOptions)
        //    //{
        //    //    var criterio = (from opciones in ((JArray)loadOptions.Filter[0]).ToList<dynamic>()
        //    //                    where opciones.Value != "this" && opciones.Value != "contains"
        //    //                    select opciones.Value).FirstOrDefault();

        //    //    return Content(JsonConvert.SerializeObject(DataSourceLoader.Load(FunProc.FindRsultadoGenerico(criterio, 0, "nombrebarco2"), loadOptions)), "application/json");
        //    //}

        //    //[HttpGet]
        //    //public ActionResult SearchResultadoGenerico_embarcacion(DataSourceLoadOptions loadOptions)
        //    //{
        //    //    var criterio = (from opciones in ((JArray)loadOptions.Filter[0]).ToList<dynamic>()
        //    //                    where opciones.Value != "this" && opciones.Value != "contains"
        //    //                    select opciones.Value).FirstOrDefault();

        //    //    return Content(JsonConvert.SerializeObject(DataSourceLoader.Load(FunProc.FindRsultadoGenerico(criterio, 0, "embarcacion2"), loadOptions)), "application/json");
        //    //}

        //    //[HttpGet]
        //    //public ActionResult SearchResultadoGenerico_rnpdelbarco(DataSourceLoadOptions loadOptions)
        //    //{
        //    //    var criterio = (from opciones in ((JArray)loadOptions.Filter[0]).ToList<dynamic>()
        //    //                    where opciones.Value != "this" && opciones.Value != "contains"
        //    //                    select opciones.Value).FirstOrDefault();

        //    //    return Content(JsonConvert.SerializeObject(DataSourceLoader.Load(FunProc.FindRsultadoGenerico(criterio, 0, "rnpdelbarco"), loadOptions)), "application/json");
        //    //}

        //    //[HttpGet]
        //    //public ActionResult SearchResultadoGenerico_puertobase(DataSourceLoadOptions loadOptions)
        //    //{
        //    //    var criterio = (from opciones in ((JArray)loadOptions.Filter[0]).ToList<dynamic>()
        //    //                    where opciones.Value != "this" && opciones.Value != "contains"
        //    //                    select opciones.Value).FirstOrDefault();

        //    //    return Content(JsonConvert.SerializeObject(DataSourceLoader.Load(FunProc.FindRsultadoGenerico(criterio, 0, "puertobase"), loadOptions)), "application/json");
        //    //}

        //    //[HttpGet]
        //    //public ActionResult SearchResultadoGenerico_tipopermiso(DataSourceLoadOptions loadOptions)
        //    //{
        //    //    var criterio = (from opciones in ((JArray)loadOptions.Filter[0]).ToList<dynamic>()
        //    //                    where opciones.Value != "this" && opciones.Value != "contains"
        //    //                    select opciones.Value).FirstOrDefault();

        //    //    return Content(JsonConvert.SerializeObject(DataSourceLoader.Load(FunProc.FindRsultadoGenerico(criterio, 0, "tipopermiso"), loadOptions)), "application/json");
        //    //}

        //    //[HttpGet]
        //    //public ActionResult SearchResultadoGenerico_razonsocial(DataSourceLoadOptions loadOptions)
        //    //{
        //    //    var criterio = (from opciones in ((JArray)loadOptions.Filter[0]).ToList<dynamic>()
        //    //                    where opciones.Value != "this" && opciones.Value != "contains"
        //    //                    select opciones.Value).FirstOrDefault();

        //    //    return Content(JsonConvert.SerializeObject(DataSourceLoader.Load(FunProc.FindRsultadoGenerico(criterio, 0, "razonsocial"), loadOptions)), "application/json");
        //    //}

        //    //[HttpGet]
        //    //public ActionResult SearchResultadoGenerico_responsableofp(DataSourceLoadOptions loadOptions)
        //    //{
        //    //    var criterio = (from opciones in ((JArray)loadOptions.Filter[0]).ToList<dynamic>()
        //    //                    where opciones.Value != "this" && opciones.Value != "contains"
        //    //                    select opciones.Value).FirstOrDefault();

        //    //    return Content(JsonConvert.SerializeObject(DataSourceLoader.Load(FunProc.FindRsultadoGenerico(criterio, 0, "responsableofp"), loadOptions)), "application/json");
        //    //}
        //    //#endregion

        //    //protected override void Dispose(bool disposing)
        //    //{
        //    //    db.Dispose();
        //    //    base.Dispose(disposing);
        //    //}
        //}
    }
}


