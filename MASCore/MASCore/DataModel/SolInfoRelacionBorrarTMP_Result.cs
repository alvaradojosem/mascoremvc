﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MASCore.DataModel
{
    public partial class SolInfoRelacionBorrarTMP_Result
    {
        public Nullable<int> IdEntrada { get; set; }
        public string RNP { get; set; }
        public string Matricula { get; set; }
        public string Barco { get; set; }
        public string RazonSocial { get; set; }
        public string Transmitiendo { get; set; }
        public string UltimaTx { get; set; }
        public string EnPuerto { get; set; }
        public string NombrePuerto { get; set; }
        public string Permisos { get; set; }
    }
}
