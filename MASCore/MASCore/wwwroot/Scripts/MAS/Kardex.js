﻿var kardex = {
    popup: {},

    init: () => {
        kardex.popup = $("#popup").dxPopup("instance");
        kardex.popup.option("contentTemplate", $("#popup-template"));
        //$("#tramite").dxSelectBox("instance").option({ value: $("#valorx").value() });
    },

    showPopUp: () => {
    	kardex.popup.show();
    },

    ReloadGridEmbarcaciones: () => {
        $("#embarcaciones-indicator").dxLoadIndicator("option", "visible", true);
        $.ajax({
            url: buscarembarcaciones,
            type: "POST",
            dataType: "json",
            data: JSON.stringify({ opcionesbusqueda: $("#embarcaciones-tipo").dxSelectBox("instance").option("value"), valor: $("#embarcaciones-valor").dxTextBox("instance").option("value") }),
            contentType: "application/json",
            success: function (result) {
                $("#embarcaciones-indicator").dxLoadIndicator("option", "visible", false);
                console.log(result);
                $("#gridEmbarcaciones").dxDataGrid("instance").option({ dataSource: result });
            },
            error: function (err) {
                $("#embarcaciones-indicator").dxLoadIndicator("option", "visible", false);
                console.log(err);
            },
            timeout: 5000
        });
    },

    RegresaValoresEmbarcacion: (Nombre, RNP, PuertoBase, RazonSocial) => {
    	$("#barco").dxTextBox("instance").option({ value: Nombre });
    	$("#razon").dxAutocomplete("instance").option({ value: RazonSocial });
    	$("#rnp").dxTextBox("instance").option({ value: RNP });
    	$("#puerto").dxAutocomplete("instance").option({ value: PuertoBase });
    	$("#popup").dxPopup("instance").hide();
    }

};

(() => {
	setTimeout(() => { kardex.init(); }, 500);
})()
