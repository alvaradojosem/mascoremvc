﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace MASCore.Models
{
    public class EnlacesModel
    {
    #region Campos utilizados para Nueva Solicitud
        public DateTime Fecha { get; set; }
        public string Hora { get; set; }
        //public string TipoReporte { get; set; }
        public IEnumerable<string> TipoReporte { get; set; } //VZ6
    #endregion
    }
}