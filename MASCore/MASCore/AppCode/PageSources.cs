﻿using Microsoft.AspNetCore.Hosting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace MASCore.AppCode
{
    public class PageSources
    {
        public class Scripts
        {

            private readonly IHostingEnvironment _env;
          

            public Scripts( IHostingEnvironment env) : base()
            {
                _env = env;
            }

            // Style sheets .css
            public const string Bootstrap = " ../lib/bootstrap/dist/css/bootstrap.css";
            public const string custommin = " ../css/Content/themes/custom.min.css";
            public const string bootstraptheme = " ../lib/bootstrap/dist/css/bootstrap-theme.css";
            public const string Bootstraprebootmin = " ../css/Content/bootstrap-reboot.css";
            public const string Bootstrapgrid = " ../css/Content/bootstrap-grid.min.css";
            public const string Bootstrapmin = " ../lib/bootstrap/dist/css/bootstrap.min.css";
            //public const string fontawesome = " ../css/Content/font-awesome.css";
            public const string fontawesomeMin = "../lib/font-awesome/css/all.css";
            public const string nprogress = " ../css/Content/nprogress.css";
            public const string animate = " ../css/Content/themes/animate.css/animate.css";
            public const string animateMin = " ../css/Content/themes/animate.css/animate.min.css";
           

            //DevExtreme CSS

            public const string dxcarmine = " ../css/devextreme/dx.carmine.css";
            public const string dxcommon = " ../css/devextreme/dx.common.css";
            public const string dxcontrast = " ../css/devextreme/dx.contrast.css";
            public const string dxlight = " ../css/devextreme/dx.light.css";
            public const string dxdark = " ../css/devextreme/dx.dark.css";
            public const string dxdarkmoon = " ../css/devextreme/dx.darkmoon.css";
            public const string dxdarkviolet = " ../css/devextreme/dx.darkviolet.css";
            public const string dxgreenmist = " ../css/devextreme/dx.greenmist.css";
            public const string dxios7default = " ../css/devextreme/dx.ios7.default.css";
            public const string dxmaterialbluedark = " ../css/devextreme/dx.material.blue.dark.css";
            public const string dxmaterialbluelight = " ../css/devextreme/dx.material.blue.light.css";
            public const string dxmateriallimedark = " ../css/devextreme/dx.material.lime.dark.css";
            public const string dxmateriallimelight = " ../css/devextreme/dx.material.lime.light.css";
            public const string dxmaterialorangedark = " ../css/devextreme/dx.material.orange.dark.css";
            public const string dxmaterialpurpledark = " ../css/devextreme/dx.material.purple.dark.css";
            public const string dxmaterialpurplelight = " ../css/devextreme/dx.material.purple.light.css";
            public const string dxmaterialtealdark = " ../css/devextreme/dx.material.teal.dark.css";
            public const string dxmaterialteallight = " ../css/devextreme/dx.material.teal.light.css";
            public const string dxsoftblue = " ../css/devextreme/dx.softblue.css";
            public const string dxdiagram = " ../css/devextreme/dx-diagram.css";

            //jquery 
            public const string jquery = " ../lib/jquery/jquery.js";
            public const string bootstrapJS = " ../lib/bootstrap/dist/js/bootstrap.js";
            public const string autocomplete = " ../js/jquery.autocomplete.js";
            public const string unobtrusive = " ../js/jquery.unobtrusive-ajax.js";
            public const string validate = " ../js/jquery.validate.js";
            public const string popper = " ../Scripts/umd/popper.min.js";
            public const string customjs = " ../Scripts/custom.js";
            public const string nprogres = " ../Scripts/nprogress.js";

            //Globalizacion
            public const string globalize = " ..//Scripts/globalize.js";
            public const string datejs = " ../js/globalize/date.js";
            public const string message = " ../js/globalize/message.js";
            public const string number = " ../js/globalize/number.js";
            public const string currency = " ../js/globalize/currency.js";
            //Localizacion
            //public const string dxmessagescs = " ../js/localization/dx.messages.cs.js";
            //public const string dxmessagesde = " ../js/localization/dx.messages.de.js";
            //public const string dxmessagesen = " ../js/localization/date.js";
            //public const string dxmessageses = " ../js/localization/dx.messages.es.js";
            //public const string dxmessagesfi = " ../js/localization/dx.messages.fi.js";
            //public const string dxmessagesfr = " ../js/localization/dx.messages.fr.js";
            //public const string dxmessagesit = " ../js/localization/dx.messages.it.js";
            //public const string dxmessagesja = " ../js/localization/dx.messages.ja.js";
            //public const string dxmessagesnl = " ../js/localization/dx.messages.nl.js";
            //public const string dxmessagespt = " ../js/localization/dx.messages.pt.js";
            //public const string dxmessagesru = " ../js/localization/dx.messages.ru.js";
            //public const string dxmessagessv = " ../js/localization/dx.messages.sv.js";
            //public const string dxmessageszh = " ../js/localization/dx.messages.zh.js";

            //Bundle Script 
            // CLDR scripts
            public const string cldr = " ../js/cldr.js";
            public const string eventjs = " ../js/cldr/event.js";
            public const string supplemental = " ../js/cldr/supplemental.js";
            public const string xunresolved = " ../js/cldr/unresolved.js";

            public const string dxall = " ../js/dx.all.js";
            public const string dxaspnetdata = " ../js/aspnet/dx.aspnet.data.js";
            public const string dxaspnetmvc = " ../js/aspnet/dx.aspnet.mvc.js";

            // DevExtreme
            public const string es = " ../js/cldr-data/es.js";

            //Pages
            public const string Cedulas = " ../js/MAS/Cedulas.js";
            public const string Kardex = " ../js/MAS/Kardex.js";
            public const string LlamadasTelefonicas = " ../js/MAS/date.js";

        }

        public static string Cedulas()
        {
            return Includejs(Scripts.Cedulas);
         }

        public static string CssDevExtreme()
        {
            return IncludeCss(
                            Scripts.dxcarmine,
                            Scripts.dxcommon,
                            Scripts.dxcontrast,
                            Scripts.dxlight,
                            Scripts.dxdark,
                            Scripts.dxdarkmoon,
                            Scripts.dxdarkviolet,
                            Scripts.dxgreenmist,
                            Scripts.dxios7default,
                            Scripts.dxmaterialbluedark,
                            Scripts.dxmaterialbluelight,
                            Scripts.dxmateriallimedark,
                            Scripts.dxmateriallimelight,
                            Scripts.dxmaterialorangedark,
                            Scripts.dxmaterialpurpledark,
                            Scripts.dxmaterialpurplelight,
                            Scripts.dxmaterialtealdark,
                            Scripts.dxmaterialteallight,
                            Scripts.dxsoftblue,
                            Scripts.dxdiagram
                );
        }
        public static string LayOut()
        {
            return IncludeCss(Scripts.Bootstraprebootmin,
                            Scripts.Bootstrapgrid,
                            Scripts.Bootstrapmin,
                            Scripts.Bootstrap,
                            Scripts.fontawesomeMin,
                            Scripts.animateMin,
                            Scripts.custommin
                 );

        }

        public static string LayOutJS()
        {
            return Includejs(Scripts.jquery,
                            Scripts.bootstrapJS,
                            Scripts.popper,
                            Scripts.nprogres,
                            Scripts.customjs
                            
                 );

        }


        public static string Globalizacion()
        {
            return Includejs(Scripts.jquery,
                           //Scripts.message,
                           //Scripts.number,
                           Scripts.currency,
                           //Scripts.dxmessagescs,
                           //Scripts.dxmessagesde,
                           //Scripts.dxmessagesen,
                           //Scripts.dxmessageses,
                           //Scripts.dxmessagesfi,
                           //Scripts.dxmessagesfr,
                           //Scripts.dxmessagesit,
                           //Scripts.dxmessagesja,
                           //Scripts.dxmessagesnl,
                           //Scripts.dxmessagespt,
                           //Scripts.dxmessagesru,
                           //Scripts.dxmessagessv,
                           //Scripts.dxmessageszh,
                           //Scripts.cldr,
                           //Scripts.eventjs,
                           //Scripts.supplemental,
                           //Scripts.xunresolved,
                           //Scripts.dxall,
                           //Scripts.dxaspnetdata,
                           //Scripts.dxaspnetmvc,                           
                           Scripts.es
                           );
        }

        public static string JqueryJs()
        {
            return Includejs(Scripts.datejs,
                           Scripts.bootstrapJS,
                           Scripts.autocomplete,
                           Scripts.unobtrusive,
                           Scripts.validate,
                           Scripts.popper,
                           Scripts.customjs
                           );
        }

        public static string StyleSheetsCss()
        {
            return IncludeCss(Scripts.Bootstraprebootmin,
                                Scripts.custommin,
                                Scripts.Bootstrapgrid,
                                Scripts.bootstraptheme,
                                Scripts.Bootstrap,
                                //Scripts.fontawesome,
                                Scripts.nprogress,
                                Scripts.animate,
                                Scripts.animateMin,                                
                                Scripts.fontawesomeMin,
                                Scripts.dxcommon,
                                Scripts.dxlight

                           );
        }


        static string IncludeCss(params string[] l)
        {
            if (l == null || l.Length == 0) return string.Empty;

            var list = l.ToList();
            list.Insert(0, "");

            return list.Aggregate((i, j) => i + $"<link href=\"{j}\" type='text/css' rel='stylesheet' />\n");
        }

        static string Includejs(params string[] l)
        {
            if (l == null || l.Length == 0) return string.Empty;

            var list = l.ToList();
            list.Insert(0, "");

            return list.Aggregate((i, j) => i + $"<script src=\"{j}\"></script>\n");
        }
    }


}
