﻿using System;
using MASCore.Components;
using MASCore.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace MASCore.Controllers
{
    public class UserController 
    {
        #region "ATRIBUTOS"
        private Byte[] _salt;
        #endregion  
        //
        // GET: /User/
        //protected override void OnAuthorization(AuthorizationHandlerContext filterContext)
        //{
        //    var vr = filterContext.Controller.ValidateRequest;
        //    base.OnAuthorization(filterContext);
        //}

        public ActionResult Index()
        {
            return View();
        }

        private ActionResult View()
        {
            throw new NotImplementedException();
        }

        //        [HttpGet]
        //        [RequiredAuthentication(Check = false)]
        //        public ActionResult LogIn()
        //        {
        //            Response.Cookies["userName"].Value = "";
        //            Response.Cookies["userName"].Expires = DateTime.Now.AddSeconds(1);
        //            if (Request.IsAuthenticated)
        //            {
        //                //Si ve la pantalla de login y se encuentra autenticado, lo saco
        //                //FormsAuthentication.SignOut();
        //                //return RedirectToAction("Login", "User");
        //                //Si ve la pantalla de login y se encuentra autenticado, lo mando a la pantalla de bienvenida !!!
        //                return RedirectToAction("Index", "Home");
        //            }

        //            return View();
        //        }

        //        [HttpPost]
        //        [RequiredAuthentication(Check=false)]
        //        public ActionResult LogIn(Models.UserModel User)
        //        {
        //            if (ModelState.IsValid)
        //            {
        //                if (EsValido(User.NombreCorto, User.Contraseña))
        //                {
        //                    if (EsValidoMAS(User.NombreCorto))
        //                    {   
        ////                        TiempodeSesionHoras
        //                        string ConfigTiempoSessionHoras = ConfigurationManager.ConnectionStrings["TiempodeSesionHoras"].ConnectionString ;
        //                        int TiempoSessionHoras = 0; // Convert.ToInt32(ConfigTiempoSessionHoras);
        //                        int.TryParse(ConfigTiempoSessionHoras, out  TiempoSessionHoras);

        //                        Response.Cookies["userName"].Value = User.NombreCorto;
        //                        Response.Cookies["userName"].Expires = DateTime.Now.AddHours(TiempoSessionHoras);
        //                        return RedirectToAction("Index", "Home");
        //                    }
        //                    else
        //                    {
        //                        Response.Cookies["userName"].Value = "";
        //                        Response.Cookies["userName"].Expires = DateTime.Now.AddSeconds(1);
        //                        ModelState.AddModelError("", "El usuario no tiene permisos para acceder al sistema.");
        //                    }
        //                }
        //                else
        //                {
        //                    Response.Cookies["userName"].Value = "";
        //                    Response.Cookies["userName"].Expires = DateTime.Now.AddSeconds(1);
        //                    ModelState.AddModelError("", "Los datos son Incorrectos.");
        //                }
        //            }
        //            return View();
        //        }

        //        [HttpGet]
        //        public ActionResult Registration()
        //        {
        //            return View();
        //        }

        //        [HttpPost]
        //        public ActionResult Registration(Models.UserModel User)
        //        {
        //            if (ModelState.IsValid)
        //            {
        //                using (var db = new BDMasContextEnt())
        //                {
        //                    var crypto = new SimpleCrypto.PBKDF2();
        //                    var encripPass = crypto.Compute(User.Contraseña);
        //                    var sysUser = db.TMasUsuarios.Create();
        //                    int CantRegistros = db.TMasUsuarios.Count() + 1;

        //                    sysUser.NombreCorto = User.NombreCorto;
        //                    sysUser.Contraseña = encripPass;
        //                    sysUser.ContraseñaSalt = crypto.Salt;
        //                    sysUser.Nombre = User.Nombre;
        //                    sysUser.Nivel = 0;

        //                    //Guid guid = Guid.NewGuid();
        //                    //sysUser.IdUsuario = guid;
        //                    sysUser.IdUsuario = CantRegistros;
        //                    sysUser.Idtmp = Guid.NewGuid();

        //                    db.TMasUsuarios.Add(sysUser);
        //                    db.SaveChanges();

        //                    return RedirectToAction("Index","Home");
        //                }
        //            }
        //            else
        //            {
        //                ModelState.AddModelError("", "Registro Incorrecto");
        //            }
        //            return View();
        //        }

        //        public ActionResult Logout()
        //        {
        //            //FormsAuthentication.SignOut();
        //            Response.Cookies["userName"].Value = "";
        //            Response.Cookies["userName"].Expires = DateTime.Now.AddSeconds(1);
        //            return RedirectToAction("Login", "User");
        //        }

        //        private void GetPasswordWithFormat(String username, out String password, out Int32 passwordFormat, out String passwordSalt)
        //        {
        //            password = "";
        //            passwordFormat = 0;
        //            passwordSalt = "";
        //            try
        //            {
        //                FuncionesProcedimientos FunProc = new FuncionesProcedimientos();
        //                GetPasswordSISMEP_Result DatosDeUsuario = FunProc.BuscaDatosDeUsuario(username);
        //                if (DatosDeUsuario != null)
        //                {
        //                    password = DatosDeUsuario.Password;
        //                    passwordFormat = DatosDeUsuario.PasswordFormat;
        //                    passwordSalt = DatosDeUsuario.PasswordSalt;
        //                }

        //                //password = "u9XOqyaCyT9KneZnD3vgYsiUSmPKWG7CcQqZ80dCrwI=";
        //                //passwordFormat = 2;
        //                //passwordSalt = "Lrs056PZmiQuM+wc8PgMcg==";
        //            }
        //            catch (Exception ex)
        //            {
        //                Debug.Write("GetPasswordWithFormat:" + ex.Message);
        //                 throw ex;
        //            }
        //        }

        //        private bool EsValidoMAS(string username)
        //        {
        //            string respuesta = "false";
        //            FuncionesProcedimientos FunProc = new FuncionesProcedimientos();
        //            var db = new BDMasContextEnt();
        //            ValidaPermisoUsuarioMAS_Result Res = db.ValidaPermisoUsuarioMAS(username).FirstOrDefault();
        //            if (Res != null)
        //            {
        //                if (Res.Valido >= 1)
        //                    respuesta = "true";
        //            }                
        //            //respuesta = "true";  //06 de Marzo del 2014   se comento la validacion temporalmente
        //            return Convert.ToBoolean(respuesta);
        //        }

        //        private bool EsValido(String username,String password)
        //        {
        //            Boolean isPasswordCorrect;
        //            String passwdFromDB;
        //            //String password;
        //            Int32 passwordFormat = 0;
        //            String salt = "";
        //            GetPasswordWithFormat( username,out passwdFromDB, out passwordFormat, out salt);


        //            String encodedPasswd = EncodePassword(password, passwordFormat, salt);

        //            isPasswordCorrect = passwdFromDB.Equals(encodedPasswd);

        //            if (isPasswordCorrect)
        //            {
        //                return true;
        //            }
        //            else
        //            {
        //                return false;
        //            }
        //        }

        //        internal String EncodePassword(String pass, Int32 passwordFormat, String salt)
        //        {
        //            if (passwordFormat == 0) // MembershipPasswordFormat.Clear
        //                return pass;
        //            Byte[] bIn = Encoding.Unicode.GetBytes(pass);
        //            _salt = Convert.FromBase64String(salt);
        //            Byte[] bAll = new Byte[_salt.Length + bIn.Length];
        //            Byte[] bRet = null;

        //            Buffer.BlockCopy(_salt, 0, bAll, 0, _salt.Length);
        //            Buffer.BlockCopy(bIn, 0, bAll, _salt.Length, bIn.Length);
        //            if (passwordFormat == 1)
        //            { // MembershipPasswordFormat.Hashed
        //                HashAlgorithm s = HashAlgorithm.Create(Membership.HashAlgorithmType);
        //                bRet = s.ComputeHash(bAll);
        //            }
        //            else
        //                bRet = EncryptPassword(bAll);
        //            //bRet = EncryptPassword(Encoding.Unicode.GetBytes(pass));
        //            return Convert.ToBase64String(bRet);
        //        }

        //        private Byte[] EncryptPassword(Byte[] password)
        //        {
        //            TripleDESCryptoServiceProvider tdes = new TripleDESCryptoServiceProvider();
        //            tdes.Key = _salt;
        //            tdes.Mode = CipherMode.ECB;
        //            tdes.Padding = PaddingMode.PKCS7;

        //            ICryptoTransform cTransform = tdes.CreateEncryptor();
        //            //transform the specified region of bytes array to resultArray
        //            byte[] resultArray =
        //              cTransform.TransformFinalBlock(password, 0,
        //              password.Length);
        //            //Release resources held by TripleDes Encryptor
        //            tdes.Clear();
        //            return resultArray;
        //            //Return the encrypted data into unreadable string format
        //            //return Convert.ToBase64String(resultArray, 0, resultArray.Length);
        //        }

        //        private bool IsValid(string NombreCorto, string Contraseña)
        //        {
        //            var crypto = new SimpleCrypto.PBKDF2();
        //            bool isValid = false;

        //            using (var db = new BDMasContextEnt())
        //            {
        //                var user = db.TMasUsuarios.FirstOrDefault(u => u.NombreCorto == NombreCorto);
        //                if (user != null)
        //                {
        //                    if (user.Contraseña == Contraseña)
        //                    {
        //                        isValid = true;
        //                    }
        //                }
        //            }
        //            return isValid;
        //        }
    }
}
