﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MASCore.DataModel
{
    public partial class BuscaUltimaTx_Result
    {
        public Nullable<System.DateTime> UltimaTx { get; set; }
        public string Lugar { get; set; }
        public string NombrePuerto { get; set; }
    }
}
