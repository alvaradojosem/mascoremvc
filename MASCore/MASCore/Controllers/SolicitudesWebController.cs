﻿using ClosedXML.Excel;
using DevExtreme.AspNet.Data;
using DevExtreme.AspNet.Mvc;
using MASCore.Exportar;
using MASCore.Models;
using Newtonsoft.Json;
//
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using Microsoft.AspNetCore.Mvc;
using MASCore.ModelReference;
using Microsoft.AspNetCore.Http;

namespace MASCore.Controllers
{
    public class SolicitudesWebController : Controller
    {
        //private BDMasContextEnt db = new BDMasContextEnt();
        FuncionesProcedimientos FunProc = new FuncionesProcedimientos();

        // GET: /SolicitudesWeb/

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult BuscaSolicitudesWeb()
        {
            SolicitudesWebVMBusqueda ModeloTmp = new SolicitudesWebVMBusqueda();
            SolicitudesWebEntidadBusqueda Entidad = new SolicitudesWebEntidadBusqueda();
            Entidad.fechainicial = DateTime.Now;
            Entidad.fechafinal = DateTime.Now;
            ModeloTmp.EntidadBusqueda = Entidad;
            List<SolicitudesWebEntidadResultadoBusqueda> Lista = new List<SolicitudesWebEntidadResultadoBusqueda>();
            Lista = null;// FunProc.LlenarResultadoBusquedaSolicitudWeb(ModeloTmp.EntidadBusqueda);
            ModeloTmp.ListaSolicitudesWeb = Lista;

            //ViewBag.Tipo = FunProc.LlenarResultadoBitacoraDDL("TipoSol");
            //ViewBag.Estado = FunProc.LlenarResultadoBitacoraDDL("Estatus");
            //ViewBag.Localidad = FunProc.LlenarResultadoBitacoraDDL("Localidad");
            //ViewBag.RazonSocial = FunProc.LlenarResultadoGenerico("RazonSocial");
            #region Codigo agregado por SZ 14/07/2016
            //ViewBag.Barco = new SelectList(FunProc.LlenarResultadoGenerico("nombrebarco2"), "Resultado", "Resultado");
            #endregion

            #region Codigo agregado por SZ 19/07/2016
            List<SolicitudesWebEntidadResultadoBusqueda> ResultadoListaSolWebTmp = new List<SolicitudesWebEntidadResultadoBusqueda>();
            Lista = null;//FunProc.LlenarResultadoBusquedaSolicitudWeb(ModeloTmp.EntidadBusqueda);
            //Session.Remove("SessionResultadoListaSolWebTmp");
            //Session["SessionResultadoListaSolWebTmp"] = Lista;
            #endregion Fin Codigo por SZ 19/07/2016

            return View(ModeloTmp);  
            
        }
        [HttpPost]
        //Comentado el 26/07/2016 se agregó un parametro.
        public ActionResult BuscaSolicitudesWeb(SolicitudesWebVMBusqueda ModeloTmp, string submitButton)
        {
            // 29/Ene/2019 MAAM Northware Inicio - Se agregó esta línea debido a que en la vista se utiliza ahora un control Button tipo DevExpress
            submitButton = "Buscar";
            // 29/Ene/2019 MAAM Northware Fin

            List<SolicitudesWebEntidadResultadoBusqueda> Lista = new List<SolicitudesWebEntidadResultadoBusqueda>();
            Lista = ModeloTmp.ListaSolicitudesWeb;
            #region "VALIDACIONES"
            Boolean Valido = true;
            if (ModeloTmp.EntidadBusqueda.fechainicial > ModeloTmp.EntidadBusqueda.fechafinal)
            {
                ViewBag.NoValidoPor = "No es posible realizar la búsqueda, la fecha inicial no puede ser mayor a la fecha final. Verificar.";
                Valido = false;
            }
            if (ModeloTmp.EntidadBusqueda.NumerodeReporte != null )
            {
                if(ModeloTmp.EntidadBusqueda.NumerodeReporte != "0")
                {
                    int result;
                    int.TryParse(ModeloTmp.EntidadBusqueda.NumerodeReporte, out result);
                    if (result == 0)
                    {
                        ViewBag.NoValidoPor = "No es posible realizar la búsqueda, Numero de Reporte debe ser numerico. Verificar.";
                        Valido = false;
                    }
                }
            }
            #endregion
            if(Valido == true)
            {
                switch (submitButton)
                {
                    case "Buscar":
                        Lista = null;// FunProc.LlenarResultadoBusquedaSolicitudWeb(ModeloTmp.EntidadBusqueda);
                        
                        break;
                    default:
                        break;
                }
            }
           
            if(Lista == null)
            {
                List<SolicitudesWebEntidadResultadoBusqueda> ListaVacia = new List<SolicitudesWebEntidadResultadoBusqueda>();
                ModeloTmp.ListaSolicitudesWeb = ListaVacia;
            }
            else
            {
                ModeloTmp.ListaSolicitudesWeb = Lista;
            }

            //ViewBag.Tipo = FunProc.LlenarResultadoBitacoraDDL("TipoSol");
            //ViewBag.Estado = FunProc.LlenarResultadoBitacoraDDL("Estatus");
            //ViewBag.Localidad = FunProc.LlenarResultadoBitacoraDDL("Localidad");
            //ViewBag.RazonSocial = FunProc.LlenarResultadoGenerico("RazonSocial");
            #region Codigo agregado por SZ 14/07/2016
            //ViewBag.Barco = new SelectList(FunProc.LlenarResultadoGenerico("nombrebarco2"), "Resultado", "Resultado");
            #endregion

            #region Código Agregado por SZ 19/07/2016
            List<SolicitudesWebEntidadResultadoBusqueda> ResultadoListaSolWebTmp = new List<SolicitudesWebEntidadResultadoBusqueda>();
            Lista = null;//FunProc.LlenarResultadoBusquedaSolicitudWeb(ModeloTmp.EntidadBusqueda);
            //Session.Remove("SessionResultadoListaSolWebTmp");
            //Session["SessionResultadoListaSolWebTmp"] = Lista;
            #endregion Fin Código  SZ 19/07/2016

            return View(ModeloTmp);
        }

        // GET: /SolicitudesWeb/Details/5
        public ActionResult Details(int id)
        {
            SolicitudesWeb_Entidad ModeloTmp = new SolicitudesWeb_Entidad();
            //ViewBag.Tipo = FunProc.LlenarResultadoBitacoraDDL("TipoSol");
            //ModeloTmp = FunProc.LlenarResultadoDetalleSolicitudesWeb(id);
            return View(ModeloTmp);
        }

        //V16
        public ActionResult DetailsCreate(int id)
        {
            SolicitudesWeb_Entidad ModeloTmp = new SolicitudesWeb_Entidad();
            List<CargaImagenesVariasSolWeb> Lista = new List<CargaImagenesVariasSolWeb>();

            //ModeloTmp = FunProc.LlenarResultadoDetalleSolicitudesWeb(id);
            //ViewBag.Tipo = FunProc.LlenarResultadoBitacoraDDL("TipoSol");
            //Lista = FunProc.SeleccionarCargaImagenesSolWebTodos(id.ToString(), "0");
            ModeloTmp.ListaImagenesCarga = Lista;

            return View(ModeloTmp); 
        }

        [HttpPost]
        public ActionResult DetailsCreate(int id, SolicitudesWeb_Entidad ModeloTmp, string submitButton, HttpPostedFileBase file, int? chunk)
        {
            List<CargaImagenesVariasSolWeb> Lista = new List<CargaImagenesVariasSolWeb>();

            ModeloTmp.Id_Reporte = id;
            ViewBag.Tipo = null;// FunProc.LlenarResultadoBitacoraDDL("TipoSol");
            ViewBag.Localidad = null;//FunProc.LlenarResultadoBitacoraDDL("Localidad");

            #region CARGA ARCHIVO
            var fileUpload = "";// Request.Files[0];
            //if (fileUpload.ContentLength > 0)
            //{
            //    string filename = fileUpload.FileName.Replace("-", "");
            //    if (filename.Length > 40)
            //        filename = filename.Replace(".pdf", "").Substring(0, 40) + ".pdf";

            //    CargaImagenesVariasSolWeb Insertado = new CargaImagenesVariasSolWeb();
            //    Insertado = null;//FunProc.InsertarCargaImagenesSolWeb(id.ToString(), filename);
            //    if (Insertado.Id > 0)
            //    {
            //        string RutaSolicitudes = Insertado.RutaBase + "\\" + Insertado.Carpeta + "\\" + Insertado.Consecutivo;
            //        if (!Directory.Exists(Insertado.RutaBase)) Directory.CreateDirectory(Insertado.RutaBase);
            //        if (!Directory.Exists(Insertado.RutaBase + "\\" + Insertado.Carpeta)) Directory.CreateDirectory(Insertado.RutaBase + "\\" + Insertado.Carpeta);
            //        chunk = chunk ?? 0;
            //        using (var fs = new FileStream(RutaSolicitudes, chunk == 0 ? FileMode.Create : FileMode.Append))
            //        {
            //            var buffer = new byte[fileUpload.InputStream.Length];
            //            fileUpload.InputStream.Read(buffer, 0, buffer.Length);
            //            fs.Write(buffer, 0, buffer.Length);
            //            ViewBag.Message = "Carga Exitosa";
            //        }
            //    }
            //    else
            //    {
            //        // NO SE INSERTO O NO TRAJO NADA; REVISAR !!!!!!!!!!!!!!!!!!!!!!!!!!!!
            //    }
            //}
            //else
            //{
            //    ViewBag.Message = "No Selecciono archivo";
            //}
            #endregion
            
            Lista = null;//FunProc.SeleccionarCargaImagenesSolWebTodos(id.ToString(), "0");
            ModeloTmp.ListaImagenesCarga = Lista;

            return RedirectToAction("DetailsCreate", "SolicitudesWeb", new { id = id.ToString() });
        }

        [HttpPost]
        public ActionResult Upload(int id)
        {
            var myFile = "";// Request.Files["file"];
            var targetLocation = "";//  Server.MapPath("~/Content/Files/");

            try
            {
                string filename = null;// myFile.FileName.Replace("-", "");
                if (filename.Length > 40) filename = filename.Replace(".pdf", "").Substring(0, 40) + ".pdf";

                CargaImagenesVariasSolWeb Insertado = new CargaImagenesVariasSolWeb();
                Insertado = null;//FunProc.InsertarCargaImagenesSolWeb(id.ToString(), filename);
                if (Insertado.Id > 0)
                {
                    string RutaSolicitudes = Insertado.RutaBase + "\\" + Insertado.Carpeta + "\\" + Insertado.Consecutivo;
                    if (!Directory.Exists(Insertado.RutaBase)) Directory.CreateDirectory(Insertado.RutaBase);
                    if (!Directory.Exists(Insertado.RutaBase + "\\" + Insertado.Carpeta)) Directory.CreateDirectory(Insertado.RutaBase + "\\" + Insertado.Carpeta);

                    //try
                    //{
                    //    myFile.SaveAs(RutaSolicitudes);
                    //} catch
                    //{
                    //    //Response.StatusDescription = "No fue posible guardar el archivo " + RutaSolicitudes;
                    //    Response.StatusCode = 500;
                    //}
                }
                else {
                    //Response.StatusDescription = "No fue posible guardar en base de datos el archivo";
                    Response.StatusCode = 500;
                }
            }
            catch
            {
                Response.StatusCode = 400;
            }

            var l = new Dictionary<string, object>();
            //l.Add("lista", FunProc.SeleccionarCargaImagenesSolWebTodos(id.ToString(), "0"));
            return Content(JsonConvert.SerializeObject(l));
        }

        // GET: /SolicitudesWeb/Create
        public ActionResult Create()
        {
            SolicitudesWeb_Entidad ModeloTmp = new SolicitudesWeb_Entidad();
            ModeloTmp.Fecha = DateTime.Now;
            ViewBag.Tipo = null;// FunProc.LlenarResultadoBitacoraDDL("TipoSol");
            return View(ModeloTmp);
        }

        // POST: /SolicitudesWeb/Create
        [HttpPost]
        public ActionResult Create(SolicitudesWeb_Entidad ModeloTmp)
        {
            ViewBag.Tipo = null;//FunProc.LlenarResultadoBitacoraDDL("TipoSol");

            if (ModelState.IsValid) {
                try {
                    int? NuevaSolicitud = null;//FunProc.CrearSolicitudWeb(ModeloTmp);

                    List<string> listCorreos = new List<string>();
                    listCorreos = null;//FunProc.LlenarResultadoGenericoBitacoraListString("Correo_Para", ModeloTmp.Tipo);
                    if (listCorreos.Count > 0 )
                    {
                        string asunto = null;// FunProc.LlenarResultadoGenericoBitacoraString("Correo_Asunto", ModeloTmp.Tipo);
                        string Cuerpo = null;// FunProc.LlenarResultadoGenericoBitacoraString("Correo_Cuerpo", ModeloTmp.Tipo);
                        #region Crea cuerpo de correo
                        StringBuilder sb = new StringBuilder();
                        if (Cuerpo.Trim() != "") sb.Append("<div> " + Cuerpo + " </div>");
                        sb.Append("<div> Nueva <b> Solicitud Web </b> </div> <br/>");

                        sb.Append("<b>A continuación se muestra el detalle:</b>");
                        sb.Append("<br/><br/>");

                        sb.Append("<table cellspacing=0 border=1>");
                        sb.Append("<tr>");
                        sb.Append("<td style='text-align:center; background-color:#424242; color:#FFFFFF;'>Id_Reporte </td>");
                        sb.Append("<td style='text-align:center;'>" + NuevaSolicitud.ToString() + "</td>");
                        sb.Append("</tr><tr>");
                        sb.Append("<td style='text-align:center; background-color:#424242; color:#FFFFFF;'>Fecha </td>");
                        sb.Append("<td style='text-align:center;'>" + ModeloTmp.Fecha.ToShortDateString() + "</td>");
                        sb.Append("</tr><tr>");
                        sb.Append("<td style='text-align:center; background-color:#424242; color:#FFFFFF;'>Hora</td>");
                        sb.Append("<td style='text-align:center;'> " + ModeloTmp.Hora + " </td>");
                        sb.Append("</tr><tr>");
                        sb.Append("<td style='text-align:center; background-color:#424242; color:#FFFFFF;'>Tipo </td>");
                        sb.Append("<td style='text-align:center;'> " + ModeloTmp.Tipo + " </td>");
                        sb.Append("</tr><tr>");
                        sb.Append("<td style='text-align:center; background-color:#424242; color:#FFFFFF;'>Oficio CNP </td>");
                        sb.Append("<td style='text-align:center;'> " + ModeloTmp.OficioCNP + " </td>");
                        sb.Append("</tr><tr>");
                        sb.Append("<td style='text-align:center; background-color:#424242; color:#FFFFFF;'>Telefono </td>");
                        sb.Append("<td style='text-align:center;'> " + ModeloTmp.Telefono + " </td>");
                        sb.Append("</tr><tr>");
                        sb.Append("<td style='text-align:center; background-color:#424242; color:#FFFFFF;'>Contacto </td>");
                        sb.Append("<td style='text-align:center;'> " + ModeloTmp.Contacto + " </td>");
                        sb.Append("</tr><tr>");
                        sb.Append("<td style='text-align:center; background-color:#424242; color:#FFFFFF;'>Solicitante </td>");
                        sb.Append("<td style='text-align:center;'> " + ModeloTmp.Solicitante + " </td>");
                        sb.Append("</tr><tr>");
                        sb.Append("<td style='text-align:center; background-color:#424242; color:#FFFFFF;'>Recibio </td>");
                        sb.Append("<td style='text-align:center;'> " + ModeloTmp.Recibio + " </td>");
                        sb.Append("</tr><tr>");
                        sb.Append("<td style='text-align:center; background-color:#424242; color:#FFFFFF;'>Nombre de barco </td>");
                        sb.Append("<td style='text-align:center;'> " + ModeloTmp.Nombredelaembarcacion + " </td>");
                        sb.Append("</tr><tr>");
                        sb.Append("<td style='text-align:center; background-color:#424242; color:#FFFFFF;'>RNP </td>");
                        sb.Append("<td style='text-align:center;'> " + ModeloTmp.RNP + " </td>");
                        sb.Append("</tr><tr>");
                        sb.Append("<td style='text-align:center; background-color:#424242; color:#FFFFFF;'>Localidad  </td>");
                        sb.Append("<td style='text-align:center;'> " + ModeloTmp.Localidad + " </td>");
                        sb.Append("</tr><tr>");
                        sb.Append("<td style='text-align:center; background-color:#424242; color:#FFFFFF;'>Supervisor </td>");
                        sb.Append("<td style='text-align:center;'> " + ModeloTmp.Supervisor + " </td>");
                        sb.Append("</tr><tr>");
                        sb.Append("<td style='text-align:center; background-color:#424242; color:#FFFFFF;'>Comentarios  </td>");
                        sb.Append("<td style='text-align:center;'> " + ModeloTmp.Comentarios + " </td>");
                        sb.Append("</tr><tbody>");
                        #endregion

                        //FunProc.EstructurayEnviaCorreo(listCorreos, asunto, sb.ToString());
                    }
                    return RedirectToAction("DetailsCreate", "SolicitudesWeb", new { id = NuevaSolicitud });
                } catch { }
            }
            return View(ModeloTmp);
        }

        // GET: /SolicitudesWeb/Edit/5
        public ActionResult Edit(int id=0)
        {
            SolicitudesWeb_Entidad ModeloTmp = new SolicitudesWeb_Entidad();
            List<CargaImagenesVariasSolWeb> Lista = new List<CargaImagenesVariasSolWeb>();
            
            ModeloTmp.Fecha = DateTime.Now;
            //ViewBag.Tipo = FunProc.LlenarResultadoBitacoraDDL("TipoSol");
            //ModeloTmp = FunProc.LlenarResultadoDetalleSolicitudesWeb(id);
            //Lista = FunProc.SeleccionarCargaImagenesSolWebTodos(id.ToString(), "0");
            ModeloTmp.ListaImagenesCarga = Lista;
            return View(ModeloTmp);            
        }

        // POST: /SolicitudesWeb/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, SolicitudesWeb_Entidad ModeloTmp, string submitButton, HttpPostedFileBase file, int? chunk)
        {
            List<CargaImagenesVariasSolWeb> Lista = new List<CargaImagenesVariasSolWeb>();

            ModeloTmp.Id_Reporte = id;
            ViewBag.Tipo = null;//FunProc.LlenarResultadoBitacoraDDL("TipoSol");
            switch (submitButton)
            {
                case "Guardar Cambios":
                    if (ModelState.IsValid)
                    {
                        try
                        {
                            int? SolicitudEditada = null;// FunProc.EditarSolicitudWeb(ModeloTmp);
                            return RedirectToAction("Details", "SolicitudesWeb", new { id = SolicitudEditada });
                        }
                        catch
                        {
                            return View(ModeloTmp);
                        }
                    }
                    break;
                default:
                    break;
            }
            Lista = null;//FunProc.SeleccionarCargaImagenesSolWebTodos(id.ToString(), "0");
            ModeloTmp.ListaImagenesCarga = Lista;
            return View(ModeloTmp);
        }

        string QuitaAcentos(string Cadena)
        {
            Cadena = Cadena.ToUpper().Replace("Á", "A");
            Cadena = Cadena.ToUpper().Replace("É", "E");
            Cadena = Cadena.ToUpper().Replace("Í", "I");
            Cadena = Cadena.ToUpper().Replace("Ó", "O");
            Cadena = Cadena.ToUpper().Replace("Ú", "U");



            return Cadena;
        }

        public ActionResult VerImagenVarias(string rutabase, string carpeta, string consecutivo)
        {
            string RutaFormada = rutabase.Trim();
            RutaFormada += '\\' + carpeta.Trim();
            RutaFormada += '\\' + consecutivo.Trim();

            RutaFormada = QuitaAcentos(RutaFormada);
            if (System.IO.File.Exists(RutaFormada) == false)
            {
                RutaFormada = "C:\\CARGA IMAGENES\\NoDisponible.jpg";
            }
            ViewBag.ImageUploaded = RutaFormada; //"C:\\CARGA IMAGENES\\15003\\SOLICITUDES DE INFORMACION\\SIT-003-2012\\0001.jpg";

            return File(RutaFormada, "application/pdf");
        }
        public ActionResult DescargarImagenVarias(string rutabase, string carpeta, string consecutivo)
        {
            string RutaFormada = rutabase.Trim();
            RutaFormada += '\\' + carpeta.Trim();
            RutaFormada += '\\' + consecutivo.Trim();

            RutaFormada = QuitaAcentos(RutaFormada);
            if (System.IO.File.Exists(RutaFormada) == false)
            {
                RutaFormada = "C:\\CARGA IMAGENES\\NoDisponible.jpg";
            }

            var file = string.Format("{0}.pdf", consecutivo.Replace(".pdf", "").Trim());

            var fullPath = RutaFormada;// Path.Combine(path, file);
            return File(fullPath, "application/pdf", file);
        }

        public ActionResult EliminarImagenVarias(string IdArchivo, string idModel, int accion)
        {
            //FunProc.EliminaCargaImagenesSolWeb(IdArchivo, idModel);
            string action = string.Empty;
            switch (accion) {
                default: break;
                case 1: action = "DetailsCreate"; break;
                case 3: action = "Edit"; break;
                case 2: action = "ListaOficios"; break;
            }
            return RedirectToAction(action, new { id = idModel });
        }

        // GET: /SolicitudesWeb/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: /SolicitudesWeb/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        public JsonResult getinfoembarcacion(string NomBarco)
        {
            List<SPMasBuscaInformacionDeEmbarcacionExacta_Result> ListaInformacionDeSolcitante = new List<SPMasBuscaInformacionDeEmbarcacionExacta_Result>();
            ListaInformacionDeSolcitante = null;//db.SPMasBuscaInformacionDeEmbarcacionExacta("NOMBRE BARCO", NomBarco).ToList();
            return null;// Json(ListaInformacionDeSolcitante, JsonRequestBehavior.AllowGet);
        }

        //V16+
        public ActionResult ListaOficios(int id)
        {
            SolicitudesWeb_Entidad ModeloTmp = new SolicitudesWeb_Entidad();
            List<CargaImagenesVariasSolWeb> Lista = new List<CargaImagenesVariasSolWeb>();
            ModeloTmp = null;// FunProc.LlenarResultadoDetalleSolicitudesWeb(id);
            Lista = null;// FunProc.SeleccionarCargaImagenesSolWebTodos(id.ToString(), "0");
            ModeloTmp.ListaImagenesCarga = Lista;
            return View(ModeloTmp);
        }

        //V16 + para descargar todos los oficios
        public ActionResult DescargaTodoOficios(int idSol)
        {
            string rutaZip;
            rutaZip = null;// FunProc.DescargaTodoOficiosZip(idSol);
            return File(rutaZip, "application/zip", idSol.ToString() + ".zip");
        }

        //Código Agregado por SZ el 19/07/2016
        #region "EXPORTAR A EXCEL"
        public ActionResult GeneraReporteSolicitudWebExportarBusqueda()
        {
            string UserName = "";
            if (HttpContext.Request.Cookies["userName"] != null)
                UserName = HttpContext.Request.Cookies["userName"].ToString();
            //      Recupera los datos que se insertaran en el archivo excel

            List<SolicitudesWebEntidadResultadoBusqueda> ListaTmp = new List<SolicitudesWebEntidadResultadoBusqueda>();
            ListaTmp = null;//Session["SessionResultadoListaSolWebTmp"] as List<SolicitudesWebEntidadResultadoBusqueda>;



            //      Obtiene la ruta de la plantilla
            string ruta = null;// Server.MapPath("~/Templates/SolicitudWeb/ExportarTodo.xlsx");
            //      Abre el libro de excel..
            var workbook = new XLWorkbook(ruta);
            //      Selecciona la hoja 1 del libro
            var worksheet = workbook.Worksheet(1);
            worksheet.Cell("A8").Value = "Impreso por: " + UserName + " | Fecha: " + DateTime.Now.ToShortDateString();
            string ParametrosDeBusqueda = "";
            ParametrosDeBusqueda = null;// Session["SessionParametrosDeBusqueda"] as string;
            worksheet.Cell("A9").Value = ParametrosDeBusqueda;
            if (ListaTmp != null)
            {

                #region "CREA LA CABECERA"
                ////////////List<string> Cabecera = new List<string>();
                ////////////Cabecera.Add("Folio");
                ////////////Cabecera.Add("Fecha");
                ////////////Cabecera.Add("Solicitante");
                ////////////Cabecera.Add("Cargo");
                ////////////Cabecera.Add("No. Total de Barcos");
                ////////////Cabecera.Add("Telefono");

                //////////////Crea la cabecera
                ////////////int RenglonIniciaCabecera = 5;
                ////////////int ColumnaIniciaCabecera = 1;
                ////////////for (var i = 0 ; i <= Cabecera.Count -1; i++)
                ////////////{

                ////////////    worksheet.Cell(RenglonIniciaCabecera, ColumnaIniciaCabecera + i).Value = Cabecera[i].ToString();
                ////////////}

                ////////////var rngHeaders = worksheet.Range(RenglonIniciaCabecera, ColumnaIniciaCabecera, RenglonIniciaCabecera, Cabecera.Count);

                ////////////rngHeaders.Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
                ////////////rngHeaders.Style.Font.Bold = true;
                ////////////rngHeaders.Style.Font.FontColor = XLColor.DarkBlue;
                ////////////rngHeaders.Style.Fill.BackgroundColor = XLColor.Aqua;
                #endregion

                #region "AGREGA VALORES"
                worksheet.Cell("A12").Value = ListaTmp;
                ////////worksheet.Cell("A5").Value = ListaTmp;
                //worksheet.Cell(5, 1).Value = ListaTmp;
                //worksheet.Columns().AdjustToContents();
                #endregion
            }



            return new ExcelResult(workbook, "Exportar Busqueda Solicitudes Web");
        }
        #endregion
        //Código Fin por SZ el 19/07/2016

        #region "EXPORTAR A PDF"
        public ActionResult GeneratePdfSolicitudesWeb()
        {
            CedulaBusquedaModel CedBusModel = new CedulaBusquedaModel();
            CedBusModel = null;// Session["SessionCedBusModel"] as CedulaBusquedaModel;
            #region "VALIDACIONES"
            Boolean Valido = true;
            if (CedBusModel.FechaInicial > CedBusModel.FechaFinal)
            {
                ViewBag.NoValidoPor = "No es posible realizar la búsqueda, la fecha inicial no puede ser mayor a la fecha final. Verificar.";
                Valido = false;
            }
            if (CedBusModel.FechaInicial.Year < 1753 || CedBusModel.FechaFinal.Year < 1753)
            {
                ViewBag.NoValidoPor = "No es posible realizar la búsqueda. Verificar Fechas.";
                Valido = false;
            }
            #endregion
            List<ReporteCedulasExportarBusqueda_Result> ListaTmp = new List<ReporteCedulasExportarBusqueda_Result>();
            if (Valido == true)
            {
                if (CedBusModel != null)
                    ListaTmp = null;//FunProc.EncuentraReporteCedulasExportarBusqueda(CedBusModel);
            }
            var pdf = "";//new PdfResult(ListaTmp, "GeneratePdfCedulas");

            return null;//pdf;
        }
        #endregion

        [HttpGet]
        public ActionResult GetBarcos(DataSourceLoadOptions loadOptions)
        {
            return null;//Content(JsonConvert.SerializeObject(DataSourceLoader.Load(FunProc.LlenarResultadoGenerico("nombrebarco2"), loadOptions)), "application/json");
        }

        public ActionResult GetEstados(DataSourceLoadOptions loadOptions)
        {
            return null;//Content(JsonConvert.SerializeObject(DataSourceLoader.Load(FunProc.LlenarResultadoBitacoraDDL("Estatus"), loadOptions)), "application/json");
        }

        public ActionResult GetLocalidades(DataSourceLoadOptions loadOptions)
        {
            return null;//Content(JsonConvert.SerializeObject(DataSourceLoader.Load(FunProc.LlenarResultadoBitacoraDDL("Localidad"), loadOptions)), "application/json");
        }

        public ActionResult GetTipos(DataSourceLoadOptions loadOptions)
        {
            return null;// Content(JsonConvert.SerializeObject(DataSourceLoader.Load(FunProc.LlenarResultadoBitacoraDDL("TipoSol"), loadOptions)), "application/json");
        }

        public ActionResult GetRazonesSociales(DataSourceLoadOptions loadOptions)
        {
            return null;// Content(JsonConvert.SerializeObject(DataSourceLoader.Load(FunProc.LlenarResultadoGenerico("RazonSocial"), loadOptions)), "application/json");
        }
    }
}
