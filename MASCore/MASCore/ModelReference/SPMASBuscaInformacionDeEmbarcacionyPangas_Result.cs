﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MASCore.ModelReference
{
    public partial class SPMASBuscaInformacionDeEmbarcacionyPangas_Result
    {
        public string Matricula { get; set; }
        public string Nombre { get; set; }
        public string SerieAntena { get; set; }
        public string Localidad { get; set; }
        public string RNP { get; set; }
        public string Permiso { get; set; }
        public string TipoPermiso { get; set; }
        public string RazonSocial { get; set; }
        public string RepLegal { get; set; }
        public string RNPTitular { get; set; }
        public string PuertoBASe { get; set; }
        public string Zona { get; set; }
    }
}
