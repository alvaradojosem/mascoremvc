﻿using Microsoft.AspNetCore.Mvc;
using System.IO;

using Xceed.Words.NET;

namespace MASCore.Exportar
{
    public class WordResult : ActionResult
    {

        private readonly DocX _workbook;
        private readonly string _fileName;

        public WordResult(DocX workbook, string fileName)
        {
            _workbook = workbook;
            _fileName = fileName;
        }

        //public override void ExecuteResult(ControllerContext context)
        //{
        //    var response = context.HttpContext.Response;
        //    context.HttpContext.Request.Clear();
        //    response.ContentType = "application/vnd.openxmlformats-officedocument.wordprocessingml.document"; //application/msword
        //    response.AddHeader("content-disposition",
        //                       "attachment;filename=\"" + _fileName + ".docx\"");

        //    using (var memoryStream = new MemoryStream())
        //    {
        //        _workbook.SaveAs(memoryStream);
        //        memoryStream.WriteTo(response.OutputStream);
        //    }
        //    response.End();
        //}


    }
}