﻿using ClosedXML.Excel;
using DevExtreme.AspNet.Data;
using DevExtreme.AspNet.Mvc;
using MASCore.Components;
using MASCore.Exportar;
using MASCore.Models;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Configuration; 
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;





namespace MASCore.Controllers
{
    public class ReportesAvanzadosController : Controller
    {
        private BDMasContextEnt db = new BDMasContextEnt();
        FuncionesProcedimientos FunProc = new FuncionesProcedimientos();



        string Nota = "Nota importante: De conformidad con los artículos de la Ley Federal de la Trasparencia y Acceso a la Información Pública Gubernamental, y lineamientos Trigésimo Séptimo de los Lineamientos Generales ";
        string Nota2 = "para la clasificación y desclasificación de información de las Dependencias Públicas Gubernamentales de la Administración Pública Federal, la información relacionada con el Sistema de Localización ";
        string Nota3 = "y Monitoreo Satelital de Embarcaciones Pesqueras de la Dirección General de Inspección y Vigilancia de la CONAPESCA, contenida  en este mensaje electrónico o anexa al mismo, es considerada como ";
        string Nota4 = "reservada o confidencial, por lo que cualquier uso, reproducción, difusión o distribución ya sea total o parcial de este mensaje y sus anexos está estrictamente prohibida. Si usted ha recibido ";
        string Nota5 = "esta transmisión por error, por favor notifique al remitente inmediatamente a vuelta de correo electrónico y borrar este mensaje y sus anexos; en caso de incumplimiento, además de las responsabilidades ";
        string Nota6 = "administrativas que se generen, podrá ser sujeto de las acciones civiles y/o penales que procedan. Lo anterior conforme a los artículos 210,211, 211 Bis, 211 Bis 1, 211 Bis 2, 211 Bis 3, 211 Bis 4 y ";
        string Nota7 = "de más relativos al Código Penal Federal.";
        string NotaCompleta = "Nota importante: De conformidad con los artículos de la Ley Federal de la Trasparencia y Acceso a la Información Pública Gubernamental, y lineamientos Trigésimo Séptimo de los Lineamientos Generales para la clasificación y desclasificación de información de las Dependencias Públicas Gubernamentales de la Administración Pública Federal, la información relacionada con el Sistema de Localización y Monitoreo Satelital de Embarcaciones Pesqueras de la Dirección General de Inspección y Vigilancia de la CONAPESCA, contenida  en este mensaje electrónico o anexa al mismo, es considerada como reservada o confidencial, por lo que cualquier uso, reproducción, difusión o distribución ya sea total o parcial de este mensaje y sus anexos está estrictamente prohibida. Si usted ha recibido esta transmisión por error, por favor notifique al remitente inmediatamente a vuelta de correo electrónico y borrar este mensaje y sus anexos; en caso de incumplimiento, además de las responsabilidades administrativas que se generen, podrá ser sujeto de las acciones civiles y/o penales que procedan. Lo anterior conforme a los artículos 210,211, 211 Bis, 211 Bis 1, 211 Bis 2, 211 Bis 3, 211 Bis 4 y de más relativos al Código Penal Federal.";

        // GET: /ReportesAvanzados/
        public ActionResult Index()
        {

            string UserName = "";
            if (HttpContext.Request.Cookies["userName"] != null)
                UserName = HttpContext.Request.Cookies["userName"].ToString();
            ViewBag.Valida7X24 = FunProc.ValidaPermisoUsuario7X24(UserName); //User.Identity.Name
            return View();
        }


        #region "MODULO DE CAPTURA ACTIVIDADES DE OPERADORES"
        public class CapturaActividadesM
        {
            [DisplayName("Turno:")]
            public String Turno { get; set; }
            [DisplayName("Usuario:")]
            public String Usuario { get; set; }
            [DisplayName("Fecha:")]
            public String Fecha { get; set; }
            [Range(0, 23)]
            [DisplayName("Hora:")]
            public String Hora { get; set; }
            [Range(1, 59)]
            [DisplayName("Minutos:")]
            public String Minutos { get; set; }

            [DisplayName("Usuario Astrum:")]
            public String usuarioastrum { get; set; }

            [DisplayName("Comentarios:")]
            public String Comentarios { get; set; }
        }
        #endregion


    }
}    