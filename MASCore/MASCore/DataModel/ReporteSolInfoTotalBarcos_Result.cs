﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MASCore.DataModel
{
    public partial class ReporteSolInfoTotalBarcos_Result
    {
        public Nullable<int> TOTAL { get; set; }
        public Nullable<int> TX { get; set; }
        public Nullable<int> NOTX { get; set; }
    }
}
