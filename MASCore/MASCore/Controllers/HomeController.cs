﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using MASCore.Models;
using Microsoft.AspNetCore.Http;

namespace MASCore.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            CookieOptions Cookies = new CookieOptions();
            Cookies.Expires = DateTime.Now.AddHours(2);

            Response.Cookies.Append("Admin", "Admin"); 

            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
