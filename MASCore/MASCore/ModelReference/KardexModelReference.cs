﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MASCore.ModelReference
{
    public class KardexModelReference
    {
    }

    public partial class BuscaKardex_Result
    {
        public string NoId { get; set; }
        public string IdDoc { get; set; }
        public Nullable<System.DateTime> Fecha { get; set; }
        public string Embarcacion { get; set; }
        public string RazonSocial { get; set; }
        public string Tramite { get; set; }
        public string Descripcion { get; set; }
        public string OficioCNP { get; set; }
        public string OficioAstrum { get; set; }
        public string Folio { get; set; }
        public string PuertoBase { get; set; }
        public string RNP { get; set; }
        public Nullable<int> Tipo { get; set; }
        public Nullable<int> EnSISMEP { get; set; }
        public Nullable<int> Tx { get; set; }
        public string UltimaTx { get; set; }
        public string Matricula { get; set; }
    }
    public partial class KardexDetalleJustificacion_Result
    {
        public int IdJustificacion { get; set; }
        public Nullable<System.DateTime> FechaEscrito { get; set; }
        public Nullable<System.DateTime> FechaRegistro { get; set; }
        public string Embarcacion { get; set; }
        public string RazonSocial { get; set; }
        public string RNP { get; set; }
        public string Permiso { get; set; }
        public string PuertoBase { get; set; }
        public Nullable<System.DateTime> FechaDesconexion { get; set; }
        public string Motivo { get; set; }
        public Nullable<int> LlamadaTelefonica { get; set; }
        public string Matricula { get; set; }
        public string Estatus { get; set; }
    }


    public class KardexDetalleSolicitudes_Result
    {
        public int Id_Reporte { get; set; }
        public Nullable<System.DateTime> fecha { get; set; }
        public string barco { get; set; }
        public string razonsocial { get; set; }
        public string tiposol { get; set; }
        public string NoOficio { get; set; }
        public string OficioPTI { get; set; }
        public string Id_Mantto { get; set; }
        public string Localidad { get; set; }
        public string RNP { get; set; }
    }
}
