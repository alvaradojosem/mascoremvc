﻿var now = new Date();

var URLBase = 'https://10.215.71.239:44350/'; // Net Core

$(document).ready(function () {
    //Calendarios.obj(CtrlDiv)
    Controles.Calendarios.obj("#FechaInicial");
    Controles.Calendarios.obj("#FechaFinal");

     //CtrlToolTip,CtrlDiv
    Controles.tooltips.obj("#ToolTip1", "#FechaInicial");
    Controles.tooltips.obj("#ToolTip2", "#FechaFinal");
    Controles.tooltips.obj("#ToolTip3", "#btnBuscaLlamada");
    Controles.tooltips.obj("#ToolTip4", "#exportToExcel");
    Controles.tooltips.obj("#ToolTip5", "#exportToPDF");

    //url, CtrlDiv, PlaceHolder
    Controles.SelectBox.obj("api/LoadControls/GetActividades", "#actividades", "-- Seleccione un Tipo de Cédula --");
    Controles.SelectBox.obj("api/LoadControls/GetResponsablesOFP", "#responsableofp", "-- Seleccione un Responsable de OFP--");
    Controles.SelectBox.obj("api/LoadControls/GetFiltros", "#filtro", "-- Seleccione un Filtro --");

    //CtrlName, PlaceHolder, ArrayItems
    Controles.CajaDeTexto.obj("#embarcacion", "Nombre del Barco", "Si,No");

    $("#btnBuscaLlamada").dxButton({
        width: 50,
        type: "default",
        icon: "search"
        //onClick: function (e) {
        //    var result = e.validationGroup.validate();
        //    if (result.isValid) {
        //        ConsultaReporte();
        //    } else {
        //        DevExpress.ui.notify("Favor de ingresar los filtros del reporte.", "error");
        //    }
        //}
    });

    $("#exportToExcel").dxButton({
        width: 50,
        type: "default",
        icon: "exportxlsx"

    });

    $("#exportToPDF").dxButton({
        width: 50,
        type: "default",
        icon: "exportpdf"

    });
    GridLlamadas();
});

function GridLlamadas() {
    $("#gridContainer").dxDataGrid({
        dataSource: null,
        showBorders: true,
        ColumnHidingEnabled: true,
        ShowColumnLines: true,
        ShowRowLines: true,
        RowAlternationEnabled: true,
        ShowBorders: true,

        paging: {
            pageSize: 5,
            ShowColumHeader: true,
            WordWrapEnabled: true
        },
        pager: {
            showPageSizeSelector: true,
            allowedPageSizes: [5, 8, 15, 30],
            showInfo: true,
            ShowNavigationButtons: true,
            ShowPageSizeSelector: true,
            visible: true
        },
        columns: ["Fecha", "Hora", "Respuesta", "Barco", "RNP", "Matrícula", "Razón Social", "OFP", "Acción",
            "Acción"]

    });





}