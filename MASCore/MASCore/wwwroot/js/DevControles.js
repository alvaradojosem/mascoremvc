﻿var URLBase = 'https://10.215.71.239:44350/'; // Net Core

var Controles = {

    Calendarios: {
         obj:function (CtrlCalendar) {
            return calendarios(CtrlCalendar);
        }
    },
       SelectBox: {
           obj: function (url, CtrlDiv, PlaceHolder) {
               return ObjSelectBox(url, CtrlDiv, PlaceHolder);
        }
    }
    ,
    CajaDeTexto: {
        obj: function (url, CtrlName, PlaceHolder, Data) {
            return ConsultaAutocomplete(url, CtrlName, PlaceHolder, Data);
        }
    }
    ,
    tooltips: {
        obj: function (CtrlTooltip, CtrlDiv) {
            return tooltips(CtrlTooltip, CtrlDiv);
        }
    },
    Boton: {
        obj: function (CtrlDiv, Icon, Type) {
            return CrearBoton(CtrlDiv, Icon, Type);
        }
    },
    TextBox: {
        obj: function (CtrlDiv, Value, PlaceHolder, showClearButton, mask, maskRules) {
            return textBoxFor(CtrlDiv, Value, PlaceHolder, showClearButton, mask, maskRules);
        }
    }
    ,
    CajaTabs: {
        obj: function (NombreVirtual, CtrlDiv) {
            return CajaTabs(NombreVirtual, CtrlDiv);
        }
    },
    TextArea: {
        obj: function (CtrlDiv, LongText, Heigth) {
            return TextArea(CtrlDiv, LongText, Heigth);
        }
    }
    ,
    CajaHora: {
        obj: function (CtrlDiv) {
            return CajaHora(CtrlDiv);
        }
    },
    CajaEmail: {
        obj: function (CtrlDiv) {
            return CajaEmail(CtrlDiv);
        }
    },
    Ctrlswitch: {
        obj: function (CtrlDiv, offtext, ontext, Width,value) {
            return Ctrlswitch(CtrlDiv, offtext, ontext, Width,value);
        }
    },
    GetApiUrl: {
       
        obj: function () {
            var apiUrl = '';
                    $.getJSON("../js/appsettings.API.json", function (data) {
                        var items = [];
                        $.each(data, function (i, val) {
                            apiUrl = val.APIMASCoreString;
                        });
                });
            return apiUrl;
        }
    }
    ,
    LoadIndicator: {
        obj: function (CtrlDiv) {
            return LoadIndicator(CtrlDiv);
        }
    },
    GetAutoComplete: {
        obj: function (NameAutoComplete) {
            return ConsultaAutocomplete(NameAutoComplete);
        }
    },
    POSTGridCedulas: {
        obj: function (Nurl, ObjVariables) {
            return ConsultaGrid(Nurl, ObjVariables);
        }
    }

};


function ConsultaGrid(Nurl,ObjVariables) {
    ArrayItems = [];
    Url = URLBase + Nurl;
    $.ajax({
        type: "POST",
        url: Url,
        data: JSON.stringify(ObjVariables),
        contentType: 'application/json',
        success: function (data) { 

            var stop = '';

            $.each(data, function (i, item) {

                var Column = {
                    noCedula: data[i].noCedula,
                    Causa: data[i].causas,
                    tipoAlerta: data[i].tipoAlerta,
                    fechaCedula: data[i].fechaCedula,
                    fechaAlerta: data[i].fechaAlerta,
                    nombreBarco: data[i].nombreBarco,
                    rnp: data[i].rnp,
                    tipoPermiso: data[i].tipoPermiso,
                    razonSocial: data[i].razonSocial
                };
                ArrayItems.push(Column);
            });

            return ArrayItems;
        },
        error: function (response) {
            console.log(response);
        }

    });

}

function ConsultaAutocomplete(url, CtrlName, PlaceHolder, Data) {

    ArrayItems = [];

    $.ajax({
        type: "POST",
        url: 'https://10.215.71.239:44350/api/FunProc/LlenarResultadoGenerico?parametro='+Data,
        //data: 'parametro='+Data,
        success: function (data) {

            $.each(data, function (i, val) {
                var str = val;
                var arr = str.resultado;
                ArrayItems.push(arr);

            });
            return CrearCajaDeTexto(CtrlName, PlaceHolder, ArrayItems);
        },
        error: function (response) {
            console.log(response);
        }

    });



}

function Ctrlswitch(CtrlDiv, offtext, ontext,Width,value) {
    return $(CtrlDiv).dxSwitch({
        value: value,
        offText: offtext,
        onText: ontext,
        width: Width
    });
}

function CajaHora(CtrlDiv) {
    return $(CtrlDiv).dxDateBox({
        pickerType: "rollers",
        type: "time",
        showClearButton: true,
        displayFormat: "HH:mm:ss",
        value: now
    });
}

function CajaEmail(CtrlDiv) {
    return $(CtrlDiv).dxTextBox({})
        .dxValidator({
            validationRules: [{
                type: "required",
                message: "Email is required"
            }, {
                type: "email",
                message: "Email is invalid"
                }],
            showClearButton: true,
            placeholder:"Email"

        });
}

function LoadIndicator(CtrlDiv) {
    return $(CtrlDiv).dxLoadIndicator({
        height: 30,
        width: 30,
        visible: false
    });
}
function TextArea(CtrlDiv,LongText,Heigth) {

  return      $(CtrlDiv).dxTextArea({
        value: LongText,
        height: Heigth
    }).dxTextArea("instance");
}

function CajaTabs(NombreVirtual,CtrlDiv) {
  return  $(NombreVirtual).dxSelectBox({
        value: 0,
        dataSource: tabs,
        displayExpr: "text",
        valueExpr: "id",
        onValueChanged: function (e) {
            tabsInstance.option(CtrlDiv, e.value);
            $(".left-aligned").text(tabs[e.value].content);
        }
    }).dxSelectBox("instance");

}

function textBoxFor(CtrlDiv, Value, PlaceHolder, showClearButton, mask, maskRules) {
  return  $(CtrlDiv).dxTextBox({
        value: Value,
        placeholder: PlaceHolder,
        showClearButton: showClearButton,
        mask: mask,
        maskRules: maskRules
   
    });
}

function CrearBoton(CtrlDiv,Icon,Type) {
    return $(CtrlDiv).dxButton({
        width: 50,
        type: Type,
        icon: Icon

    });
}

function ObjSelectBox(url, CtrlDiv, PlaceHolder) {

    var ArrayItems = [];

    $.ajax({
        type: "GET",
        url: URLBase + url,
        success: function (data) {

            $.each(data, function (key, val) {

                var str = val;
                var arr = str.Resultado;
                ArrayItems.push(arr);

            });

          return  SelectBox(CtrlDiv, PlaceHolder, ArrayItems);
        },
        error: function (response) {
            console.log(response);
        }

    });



}



function CrearCajaDeTexto(CtrlName, PlaceHolder, ArrayItems) {
    $(CtrlName).dxAutocomplete({
        dataSource: ArrayItems,
        placeholder: PlaceHolder,
        onValueChanged: function (item) {
            $(CtrlName).val($(CtrlName + " .dx-texteditor-input").val());
        }
    });
}

function SelectBox(CtrlDiv, PlaceHolder, ArrayItems) {

    return $(CtrlDiv).dxSelectBox({
        items: ArrayItems,
        placeholder: PlaceHolder
    });

}

function tooltips(CtrlTooltip, CtrlDiv) {
    $(CtrlTooltip).dxTooltip({
        target: CtrlDiv,
        showEvent: "mouseenter",
        hideEvent: "mouseleave",
        closeOnOutsideClick: false
    });

}

function calendarios(CtrlCalendar) {
    return $(CtrlCalendar).dxDateBox({
        type: "date",
        value: now,
        displayFormat: "dd/MM/yyyy",
        width: "100%",
        onValueChanged: function (item) {             
           $(CtrlCalendar).val($(CtrlCalendar + " .dx-texteditor-input").val());
        }
        
    });
}
