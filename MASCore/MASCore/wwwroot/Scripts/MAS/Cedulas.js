﻿function RegresaValoresLlamadas(id, rnp) {
    $("#lblLlamada").dxTextBox({ value: id });

    var rnpcedula = "";

    try { rnpcedula = $("#RNP").dxAutocomplete("instance").option("value"); } catch (o0) { console.log(o0); }

    if (rnp === rnpcedula) {
        $('#llamadasRNP').hide();
    }
    else {
        $('#llamadasRNP').show();
    }

    $("#popup-cedula").dxPopup("instance").hide();
}

function ocultar_mostrar(div) {
    MiDiv = document.getElementById(div);
    MiDiv.style.display != 'none' ?
        MiDiv.style.display = 'none' : MiDiv.style.display = 'block';
}

function Muestra(div) {
    MiDiv = document.getElementById(div);
    MiDiv.style.display = 'block';
}

function Oculta(div) {
    MiDiv = document.getElementById(div);
    MiDiv.style.display = 'none';
}

function Inicio() {
    COD_ORIGEN = document.getElementById('txtofp').value;
    ORIGEN = document.getElementById('responsableofp');
    var alarma = document.getElementById('TipoAlerta').value;
    ElementoSeleccionado(alarma);
}

function InicioEditar() {
    var alarma = document.getElementById('TipoAlertaX').value;
    ElementoSeleccionado(alarma);
}

function IgualaAlarmasAlerta() {
    alert('Busca');
    var COD_ORIGEN = document.getElementById('TipoAlerta');
            var ORIGEN = document.getElementById('alarmas');
            var CENTRO_NEGOCIO = ORIGEN.options[ORIGEN.selectedIndex].value;
            COD_ORIGEN.value = CENTRO_NEGOCIO;
}

function ElementoSeleccionado(Valor) {
    $('#fechaalerta_').hide();
    $('#horaalerta_').hide();
    $('#velocidad_').hide();
    $('#profundidad_').hide();
    $('#distanciadelacosta_').hide();
    $('#zona_').hide();
    $('#ubicacionoreferencia_').hide();
    $('#causas__').hide();
    $('#datospersonacontactada_').hide();
    $('#llamadasRNP').hide();
    $('#estatus_').hide();
    $('#cuentaconllamadas_').hide();
    $('#llamadas_').hide();
    $('#lblLlamada').val('0');

    switch (Valor.value) {
        default: break;
        case 'SIN TRANSMISIÓN':
        case 'TRANSMISION IRREGULAR':
            $('#estatus_').show();
            $('#ultimatx_').show();
            $('#cuentaconllamadas_').show();
            $('#llamadas_').show();
            break;
        case 'EMERGENCIA':
        case 'FALSA ALARMA':
            $('#fechaalerta_').show();
            $('#horaalerta_').show();
            $('#ultimatx_').show();
            $('#causas__').show();
            $('#datospersonacontactada_').show();
            break;
        case 'PESCA PROHIBIDA':
        case 'AREA NATURAL PROTEGIDA':
        case 'ZONA PROHIBIDA':
        case 'SEGUNDA ZONA PROHIBIDA':
        case 'ZONA DECRETO':
            $('#fechaalerta_').show();
            $('#horaalerta_').show();
            $('#velocidad_').show();
            $('#ultimatx_').show();
            $('#profundidad_').show();
            $('#distanciadelacosta_').show();
            $('#zona_').show();
            $('#ubicacionoreferencia_').show();
            $('#datospersonacontactada_').show();
            break;
    }

    //var COD_ORIGEN = $('#alarmas');
    //var ORIGEN = $('#TipoAlerta');

    //var CENTRO_NEGOCIO = ORIGEN.options[ORIGEN.selectedIndex].value;
    //COD_ORIGEN.value = CENTRO_NEGOCIO;
}

function showPopUpLlamadas() {
    showPopUpBase($("#popup-template-buscarllamadas"));
}

function showPopUpClonar() {
    showPopUpBase($("#popup-template-clonar"));
}

function showPopUpBase(template) {
    var popup = $("#popup-cedula").dxPopup("instance");
    popup.option("contentTemplate", template);
    popup.show();
}

function ReloadGridLlamadas() {
    $("#button-indicator").dxLoadIndicator("option", "visible", true);
    var args = {};
    args.filtro = $("#PopupLlamadas-filtro").dxSelectBox("instance").option("value");
    args.FechaInicial = $("#PopupLlamadas-FechaInicial").dxDateBox("instance").option("value");
    args.FechaFinal = $("#PopupLlamadas-FechaFinal").dxDateBox("instance").option("value");
    args.actividad = $("#PopupLlamadas-actividad").dxSelectBox("instance").option("value");
    args.embarcacion = $("#PopupLlamadas-nombrebarco").dxAutocomplete("instance").option("value");
    args.ofp = $("#PopupLlamadas-ofp").dxSelectBox("instance").option("value");

    $.ajax({
        url: urlReloadGridLlamadas,
        type: "POST",
        dataType: "json",
        data: JSON.stringify(args),
        success: function (result) {
            console.log(result);
            $("#gridLlamadas").dxDataGrid("instance").option({ dataSource: result });
            $("#button-indicator").dxLoadIndicator("option", "visible", false);
        },
        error: function (err) {
            console.log(err);
            $("#button-indicator").dxLoadIndicator("option", "visible", false);
        },
        timeout: 5000
    });
}

function ReloadGridEmbarcaciones() {
    $("#PopupClonar-indicator").dxLoadIndicator("option", "visible", true);
    var args = {};
    args.FechaInicial = $("#PopupClonar-FechaInicial").dxDateBox("instance").option("value");
    args.FechaFinal = $("#PopupClonar-FechaFinal").dxDateBox("instance").option("value");
    args.alarmasb = $("#PopupClonar-TipoAlerta").dxSelectBox("instance").option("value");
    args.MatriculaBarcob = $("#PopupClonar-MatriculaBarco").dxAutocomplete("instance").option("value");
    args.RazonSocialb = $("#PopupClonar-RazonSocial").dxAutocomplete("instance").option("value");
    args.PuertoBaseb = $("#PopupClonar-PuertoBase").dxAutocomplete("instance").option("value");
    args.TipoPermisob = $("#PopupClonar-TipoPermiso").dxAutocomplete("instance").option("value");
    args.modobusqueda = $("#PopupClonar-modobusqueda").dxSelectBox("instance").option("value");
    args.Embarcacionb = $("#PopupClonar-NombreBarco").dxAutocomplete("instance").option("value");
    args.Rnpb = $("#PopupClonar-RNP").dxAutocomplete("instance").option("value");
    args.MostrarObtieneInformacion = $("#PopupClonar-criteriodedesconexion").dxSelectBox("instance").option("value");

    $.ajax({
        url: urlReloadGridEmbarcaciones,
        type: "POST",
        dataType: "json",
        data: JSON.stringify(args),
        contentType: "application/json",
        success: function (result) {
            console.log(result);
            $("#gridEmbarcaciones").dxDataGrid("instance").option({ dataSource: result.ResultadoMensajesEmbarcacionesSeleccionar });
            $("#PopupClonar-indicator").dxLoadIndicator("option", "visible", false);
        },
        error: function (err) {
            console.log(err);
            $("#PopupClonar-indicator").dxLoadIndicator("option", "visible", false);
        },
        timeout: 5000
    });
}

function UsarEmbarcacion(id) {
    var url = "../Cedula/Create/" + id + "?alarmas=" + $("#PopupClonar-TipoAlerta").dxSelectBox("instance").option("value") + "&MostrarObtieneInformacion=NO";
    comback(url);
}

function UsarEmbarcacionEdicion(id) {
    var url = "../Cedula/Editar?id=" + id + "&alarmas=" + $("#PopupClonar-TipoAlerta").dxSelectBox("instance").option("value") + "&MostrarObtieneInformacion=NO&idCedula=" + $("#idCedula").val() + "&NoCedula=" + $("#nocedula").dxTextBox("instance").option("value");
    comback(url);
}

function dateBox_valueChanged(data) {
    $("#horario").dxTextBox("instance").option({ "value": $("#myDatepicker3").dxDateBox("instance").option("text") });
}
