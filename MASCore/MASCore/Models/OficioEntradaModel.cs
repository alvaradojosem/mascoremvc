﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using MASCore.ModelReference;

namespace MASCore.Models
{
    public class OficioEntradaModelBusqueda
    {
        private BDMasContextEnt db = new BDMasContextEnt();

        #region "PARAMETROS PARA LA BUSQUEDA"
        public string       NoOficio            { get; set; }
	    public string       Estatus		        { get; set; }
	    public string       Turno	            { get; set; }
	    public string       TipoOficio	        { get; set; }
	    public string       Volante	            { get; set; }
	    public string       Asunto	            { get; set; }
	    public string       Minutario	        { get; set; }
	    public string       Origen	            { get; set; }
        [DisplayName("Recibido DGIV:")]
        public Boolean      FechaDGIV	        { get; set; }
        [DataType(DataType.Date)]
        [DisplayName("Del")]
	    public DateTime     FechaInicioDGIV	    { get; set; }
        [DisplayName("Al")]
	    public DateTime     FechaFinDGIV	    { get; set; }
        [DisplayName("Recibido Monitoreo:")]
	    public Boolean      FechaMon	        { get; set; }
        [Display(Name = "Del")]
        public DateTime     FechaInicioMon	    { get; set; }
        [DisplayName("Al")]
	    public DateTime     FechaFinMon	        { get; set; }
        public string       Barco               { get; set; }
        #endregion

        public int Total { get; set; }
        public int Concluidos { get; set; }
        public int Pendientes { get; set; }

        ////TMasOfEntrada OficioEntrada { get; set; }
        public List<BuscaOficioEntrada_Result> ResultadoListaOficioEntradaSP { get; set; }
    }

    public class OficioEntradaModel
    {
        #region "USADOS EN MODULO DETALLE"
        //public TMasOfEntrada TMASOFENTRADA { get; set; }        
        #endregion

        #region "USADOS PARA AGREGAR EMBARCACION"
        public string embarcacion_barco { get; set; }
        public string embarcacion_rnp { get; set; }
        public string embarcacion_razonsocial { get; set; }
        public string embarcacion_matricula { get; set; }
        //public string antena { get; set; }
        public string ErroresAgregarEmbarcacion { get; set; }

        #region "USADOS EN MODULO DETALLE"
        public List<string> ResultadoListaRelacionEntradaSP { get; set; }
        #endregion

        #region "USADOS EN MODULO NUEVO/CREAR"
        public List<string> ResultadoListaRelacionEntradaTMPSP { get; set; }
        #endregion
        #endregion

        //// VARIABLES USADAS PARA LA BUSQUEDA DE INFORMACION DE EMBARCACION
        #region "BUSQUEDA DE INFORMACION DE EMBARCACION"
            #region "USADOS PARA MOSTRAR DIVS"
                public Boolean MostrandoBusquedaInformacionEmbarcacion { get; set; }
            #endregion

            public List<BuscaInformacionDeEmbarcacion_Result> InformacionDeEmbarcacion { get; set; }
        #endregion

        #region "INFORMACION GENERAL"
        public int          IdEntrada           {get;set;}
        [Required]
        public string       Volante             { get; set; } //30
        [Required]
        public string       NoOficio            { get; set; } //30
        public string       TipoOficio          { get; set; } //50
        public DateTime     FechaDGIV           { get; set; }
        public string       HoraDGIV            { get; set; } //20
        public DateTime     FechaMon            { get; set; }
        public string       HoraMon             { get; set; } //20
        [Required]
        public string       Asunto              { get; set; } //100
        [Required]
        public string       Turno               { get; set; } //50
        [Required]
        public string       Origen              { get; set; } //50
        public string       Minutario           { get; set; } //50
        public string       Estatus             { get; set; } //10
        [Required]
        public string       Comentarios         { get; set; } //500
        public string       Relacion            { get; set; } //30  
        public string       OFP                 { get; set; } //30  
        #endregion
    }
}