﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MASCore.DataModel
{
    public partial class SolInfoActualizar_Result
    {
        public int IdInfoSol { get; set; }
        public string Fecha { get; set; }
        public string Hora { get; set; }
        public string Solicitante { get; set; }
        public string Cargo { get; set; }
        public string Telefono { get; set; }
        public string Email { get; set; }
        public string Ciudad { get; set; }
        public string Estado { get; set; }
        public string Asunto { get; set; }
        public string Dependencia { get; set; }
        public string Medio { get; set; }
        public string Descripcion { get; set; }
        public string OFP { get; set; }
        public string Folio { get; set; }
    }
}
