﻿using System;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.AspNetCore.Routing;
using System.Web.Http.Filters;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Logging;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

namespace MASCore.Components
{
    public class RequiredAuthenticationAttribute : FilterAttribute, Microsoft.AspNetCore.Mvc.Filters.IAuthorizationFilter
    {

        /// <summary>
        /// Determina si va a realizar la comprobación de autenticación para un método de acción o 
        /// controlador. Por defecto es true.
        /// </summary>
        public bool Check { get; set; }

        public string Controller { get; set; }
        public string Action { get; set; }

        public RequiredAuthenticationAttribute()
        {
            Check = true;
        }

        //public void OnAuthorization(AuthorizationContext filterContext)
        //{
        //    //Si es false no se realiza la comprobación de autenticación
        //    if (!Check) return;

        //    var routeValueDictionary = new RouteValueDictionary(
        //        new
        //        {
        //            //El operador doble interrogación se invierte por validaciones de script en este blog
        //            action = Action ?? "Login",  //LogOn
        //            controller = Controller ?? "User"  //Account
        //        });

        //    string UserName = "Admin";
        //    string cookieValueFromContext = _httpContextAccessor.HttpContext.Request.Cookies["key"];


        //    if (filterContext.Context.Request.Cookies["userName"] != null)
        //    {
        //        UserName = filterContext.Context.Request.Cookies["userName"];
        //    }

        //    if (UserName == "" || UserName == null)
        //        filterContext.Result = new RedirectToRouteResult(routeValueDictionary);
        //    //UserName = filterContext.HttpContext.Request.Cookies["userName"].Value;

        //    if (!filterContext.Context.Request.IsAuthenticated)
        //        filterContext.Result = new RedirectToRouteResult(routeValueDictionary);
        //}

        public void OnAuthorization(AuthorizationFilterContext context)
        {
            throw new NotImplementedException();
        }
    }

    public class AuthorizationContext
    {
        public RedirectToRouteResult Result { get; internal set; }
        public object Controller { get; internal set; }
        public object Context { get; internal set; }
    }
}