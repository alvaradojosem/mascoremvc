﻿var now = new Date();

var URLBase = 'https://10.215.71.239:44350/'; // Net Core

$(document).ready(function () {
    //Inicio de Tabs
    $("#generales").show();
    $("#alertas").hide();
    $("#comentarios").hide();

    // botones Salvar y regresar
    Controles.Boton.obj("#save", "save","default");
    Controles.Boton.obj("#regresar", "revert","default");
    // tooltips
    Controles.tooltips.obj("#ToolTip1", "#save");
    Controles.tooltips.obj("#ToolTip2", "#regresar");
    Controles.tooltips.obj("#toolTip3", "#buscarembarcaciones");
    // Calendario
    Controles.Calendarios.obj("#FechaCedula");
    // DropDown Actividad
    Controles.SelectBox.obj("api/LoadControls/GetActividades", "#Actividad", "-- Seleccione un Tipo de Actividad --");

    //Tabs
    //Controles Generales
    Controles.TextBox.obj("#Barco", "", "", false, "", "");
    Controles.TextBox.obj("#RazonSocial", "", "", false, "", "");
    Controles.TextBox.obj("#Matricula", "", "", false, "", "");
    Controles.TextBox.obj("#RNP", "", "", false, "", "");
    Controles.TextBox.obj("#PuertoBase", "", "", false, "", "");

    //Controles Persona(Alertas)
    Controles.TextBox.obj("#Empresa", "", "", false, "", "");
    Controles.TextBox.obj("#Atendio", "", "", false, "", "");
    Controles.TextBox.obj("#Lugar", "", "", false, "", "");
    Controles.SelectBox.obj("api/LoadControls/GetResponsablesOFP", "#OFP", "-- Seleccione un Responsable OFP --");

    // Controles Comentarios
    Controles.TextArea.obj("#Descripcion", "", 120);
    Controles.Calendarios.obj("#Fecha");
    Controles.CajaHora.obj("#Hora");
    Controles.SelectBox.obj("api/LoadControls/GetBTTipos", "#Tipo", "-- Seleccione un Tipo --");
    Controles.TextBox.obj("#Numero", "", "", true, "(00) 0000-0000", "");
    Controles.SelectBox.obj("api/LoadControls/GetRespuestas", "#Respuesta", "NO");
   
  

    //Fin Tabs


    // boton Buscar
    Controles.Boton.obj("#buscarembarcaciones", "search");
    Controles.Boton.obj("#button", "fas fa-plus-circle");

    Tabs();

    $("#small-indicator").dxLoadIndicator({
        height: 30,
        width: 30,
        visible:false
    });

    $("#button").dxButton({
        onClick: function (e) {
           
            DevExpress.ui.notify("The Done button was clicked");
            $("#small-indicator").visible=true;
        }
    });

    GridLlamadas();

});

function GridLlamadas() {
    $("#gridContainer").dxDataGrid({
        dataSource: null,
        showBorders: true,
        ColumnHidingEnabled: true,
        ShowColumnLines: true,
        ShowRowLines: true,
        RowAlternationEnabled: true,
        ShowBorders: true,

        paging: {
            pageSize: 5,
            ShowColumHeader: true,
            WordWrapEnabled: true
        },
        pager: {
            showPageSizeSelector: true,
            allowedPageSizes: [5, 8, 15, 30],
            showInfo: true,
            ShowNavigationButtons: true,
            ShowPageSizeSelector: true,
            visible: true
        },
        columns: ["Tipo", "Numero", "Fecha", "Hora", "Respuesta"]

    });





}


function Tabs() {
  
var tabs = [
    { id: "generales", text: "Datos de la Embarcación", icon: "fa fa-ship", content: "generales" },
    { id: "alertas", text: "Datos de la Persona", icon: "fas fa-user", content: "alertas"},
    { id: "comentarios", text: "Datos de Llamadas", icon: "fas fa-phone", content: "comentarios"}
];
    
    var tabsInstance = $("#tabs > .tabs-container").dxTabs({
        dataSource: tabs,
        selectedIndex: 0,
        onItemClick: function (e) {
            if (e.itemData.id === "generales") {
                $("#generales").show();
                $("#alertas").hide();
                $("#comentarios").hide();
            } else if (e.itemData.id === "alertas") {
                $("#generales").hide();
                $("#comentarios").hide();
                $("#alertas").show();
            }
            else if (e.itemData.id === "comentarios") {
                $("#generales").hide();
                $("#alertas").hide();
                $("#comentarios").show();
            }
        }
        
    }).dxTabs("instance");

}