﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace MASCore.Models
{
    public class SolicitantesModel
    {
        public string solicitante { get; set; }
        public List<TMasSolicitantes> ResultadoListaSolicitantes { get; set; }
        public TMasSolicitantes Solicitantes { get; set; }
        [DataType(DataType.Password)]
        public string Clave { get; set; }
    }
}