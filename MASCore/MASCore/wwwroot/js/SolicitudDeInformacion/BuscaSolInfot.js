﻿var now = new Date();
var URLBase = 'https://10.215.71.239:44350/'; // Net Core
var ArrayItems = [];

$(document).ready(function () {
    
   //Calendarios
    Controles.Calendarios.obj("#FechaInicial");
    Controles.Calendarios.obj("#FechaFinal");

    //CtrlToolTip,CtrlDiv
    Controles.tooltips.obj("#ToolTip1", "#FechaInicial");
    Controles.tooltips.obj("#ToolTip2", "#FechaFinal");
    Controles.tooltips.obj("#ToolTip4", "#exportToExcel");
    Controles.tooltips.obj("#ToolTip5", "#exportToPDF");

    //url, CtrlDiv, PlaceHolder
    Controles.SelectBox.obj("api/LoadControls/GetActividades", "#Asunto", "-- Seleccione el Asunto --");
    Controles.SelectBox.obj("api/LoadControls/GetResponsablesOFP", "#OFP", "-- Seleccione el OFP --");
    Controles.SelectBox.obj("api/LoadControls/GetResponsablesOFP", "#Medio", "-- Seleccione el Medio --");
    Controles.SelectBox.obj("api/LoadControls/GetResponsablesOFP", "#Dependencia", "-- Seleccione la Dependencia --");
    Controles.SelectBox.obj("api/LoadControls/GetResponsablesOFP", "#Estado", "-- Seleccione el Estado --");
    Controles.SelectBox.obj("api/LoadControls/GetResponsablesOFP", "#Ciudad", "-- Seleccione la Ciudad --");

    //AutoCompletes
    Controles.CajaDeTexto.obj("#Solicitante", "Solicitante", "");
    Controles.CajaDeTexto.obj("#Barco", "Barco", "");
    Controles.CajaDeTexto.obj("#Folio", "Folio", "");

    //Botones
    Controles.Boton.obj("#exportToExcel", "fa fa-file-excel", "default");
    Controles.Boton.obj("#exportToPDF", "fa fa-file-pdf", "danger");



    //ArrayItems = ObjCajaDeTexto("api/SolicitudDeInformacion/SearchResultadoGenerico_matriculabarco","HA");
  
    
    $("#Barco").dxAutocomplete({
        dataSource: ObjCajaDeTexto("api/SolicitudDeInformacion/SearchResultadoGenerico_matriculabarco", "a"),       
        onValueChanged: function (data) {
            Barco = data.value;
            ObjCajaDeTexto("api/SolicitudDeInformacion/SearchResultadoGenerico_matriculabarco","a");
        }
    });

});

function getDataSource() {
    
    // I just emulate long request to a server-side here
   var names = ["James", "John", "Robert", "Michael", "William", "David", "Richard", "Charles", "Joseph", "Thomas", "Christopher", "Daniel", "Paul", "Mark", "Donald", "George", "Kenneth", "Steven", "Edward", "Brian", "Ronald", "Anthony", "Kevin", "Jason", "Jeff", "Mary", "Patricia", "Linda", "Barbara", "Elizabeth", "Jennifer", "Maria", "Susan", "Margaret", "Dorothy", "Lisa", "Nancy", "Karen", "Betty", "Helen", "Sandra", "Donna", "Carol", "Ruth", "Sharon", "Michelle", "Laura", "Sarah", "Kimberly", "Deborah"];


    return names;
}



function ObjCajaDeTexto(url, q) {

    var intro = '';

    $.ajax({
        type: "POST",
        url: "https://10.215.71.239:44350/api/SolicitudDeInformacion/SearchResultadoGenerico_matriculabarco?q="+q+"&limit=15",
        //data: { "q": q, "limit": "15" },
        success: function (data) {
           
            $.each(data, function (key, val) {

              
                ArrayItems.push(val);

            });

            return ArrayItems;
        },
        error: function (response) {
            console.log(response);
        }

    });


}

