#pragma checksum "C:\Users\Equipo\Documents\00-Proyectos 2019\MASCore\MASCore\Views\Llamadas\Create.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "ef7c2964a98fc23311214a146d578335960cf78a"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Llamadas_Create), @"mvc.1.0.view", @"/Views/Llamadas/Create.cshtml")]
[assembly:global::Microsoft.AspNetCore.Mvc.Razor.Compilation.RazorViewAttribute(@"/Views/Llamadas/Create.cshtml", typeof(AspNetCore.Views_Llamadas_Create))]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#line 1 "C:\Users\Equipo\Documents\00-Proyectos 2019\MASCore\MASCore\Views\_ViewImports.cshtml"
using MASCore;

#line default
#line hidden
#line 2 "C:\Users\Equipo\Documents\00-Proyectos 2019\MASCore\MASCore\Views\_ViewImports.cshtml"
using MASCore.Models;

#line default
#line hidden
#line 4 "C:\Users\Equipo\Documents\00-Proyectos 2019\MASCore\MASCore\Views\_ViewImports.cshtml"
using DevExtreme.AspNet.Mvc;

#line default
#line hidden
#line 2 "C:\Users\Equipo\Documents\00-Proyectos 2019\MASCore\MASCore\Views\Llamadas\Create.cshtml"
using Microsoft.AspNetCore.Http.Extensions;

#line default
#line hidden
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"ef7c2964a98fc23311214a146d578335960cf78a", @"/Views/Llamadas/Create.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"a6f9e97209bd19ae69cd611cb55c19dae5d6d315", @"/Views/_ViewImports.cshtml")]
    public class Views_Llamadas_Create : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<MASCore.Models.LlamadaModel>
    {
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_0 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("src", new global::Microsoft.AspNetCore.Html.HtmlString("~/js/Llamadas/CreateLlamada.js"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_1 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("src", new global::Microsoft.AspNetCore.Html.HtmlString("~/Scripts/MAS/LlamadasTelefonicas.js"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        #line hidden
        #pragma warning disable 0169
        private string __tagHelperStringValueBuffer;
        #pragma warning restore 0169
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperExecutionContext __tagHelperExecutionContext;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner __tagHelperRunner = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner();
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __backed__tagHelperScopeManager = null;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __tagHelperScopeManager
        {
            get
            {
                if (__backed__tagHelperScopeManager == null)
                {
                    __backed__tagHelperScopeManager = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager(StartTagHelperWritingScope, EndTagHelperWritingScope);
                }
                return __backed__tagHelperScopeManager;
            }
        }
        private global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.HeadTagHelper __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_HeadTagHelper;
        private global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper;
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            BeginContext(81, 2, true);
            WriteLiteral("\r\n");
            EndContext();
#line 4 "C:\Users\Equipo\Documents\00-Proyectos 2019\MASCore\MASCore\Views\Llamadas\Create.cshtml"
  
    ViewBag.Title = "Nueva Llamada";

#line default
#line hidden
            BeginContext(128, 77, false);
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("head", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "fe8b4a5a69864bddacd84b789b32a21c", async() => {
                BeginContext(134, 8, true);
                WriteLiteral("\r\n\r\n    ");
                EndContext();
                BeginContext(142, 54, false);
                __tagHelperExecutionContext = __tagHelperScopeManager.Begin("script", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "7b74a64f7a2845ec8486c5aa14ba18be", async() => {
                }
                );
                __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper>();
                __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper);
                __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_0);
                await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
                if (!__tagHelperExecutionContext.Output.IsContentModified)
                {
                    await __tagHelperExecutionContext.SetOutputContentAsync();
                }
                Write(__tagHelperExecutionContext.Output);
                __tagHelperExecutionContext = __tagHelperScopeManager.End();
                EndContext();
                BeginContext(196, 2, true);
                WriteLiteral("\r\n");
                EndContext();
            }
            );
            __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_HeadTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.HeadTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_Razor_TagHelpers_HeadTagHelper);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            EndContext();
            BeginContext(205, 69, true);
            WriteLiteral("\r\n\r\n<div style=\"text-align:center; color:Red;font-size:medium\">\r\n    ");
            EndContext();
            BeginContext(275, 19, false);
#line 13 "C:\Users\Equipo\Documents\00-Proyectos 2019\MASCore\MASCore\Views\Llamadas\Create.cshtml"
Write(ViewBag.NoValidoPor);

#line default
#line hidden
            EndContext();
            BeginContext(294, 12, true);
            WriteLiteral("\r\n</div>\r\n\r\n");
            EndContext();
#line 16 "C:\Users\Equipo\Documents\00-Proyectos 2019\MASCore\MASCore\Views\Llamadas\Create.cshtml"
 using (Html.BeginForm("Create", "Llamadas", FormMethod.Post))
{
    

#line default
#line hidden
            BeginContext(378, 28, false);
#line 18 "C:\Users\Equipo\Documents\00-Proyectos 2019\MASCore\MASCore\Views\Llamadas\Create.cshtml"
Write(Html.ValidationSummary(true));

#line default
#line hidden
            EndContext();
            BeginContext(410, 243, true);
            WriteLiteral("    <article>\r\n        <div class=\"row\">\r\n            <div class=\"col-4\">\r\n                <h1>Nueva Llamada</h1>\r\n            </div>\r\n            <div class=\"col-8 text-right\">\r\n                <script>\r\n                    var urlComback = \'");
            EndContext();
            BeginContext(654, 79, false);
#line 27 "C:\Users\Equipo\Documents\00-Proyectos 2019\MASCore\MASCore\Views\Llamadas\Create.cshtml"
                                 Write(Url.Action("Index", "Llamadas", null, UriHelper.GetDisplayUrl(Context.Request)));

#line default
#line hidden
            EndContext();
            BeginContext(733, 53, true);
            WriteLiteral(";\'//\r\n                    var buscarembarcaciones = \'");
            EndContext();
            BeginContext(787, 93, false);
#line 28 "C:\Users\Equipo\Documents\00-Proyectos 2019\MASCore\MASCore\Views\Llamadas\Create.cshtml"
                                          Write(Url.Action("BuscarEmbarcaciones", "Llamadas", null, UriHelper.GetDisplayUrl(Context.Request)));

#line default
#line hidden
            EndContext();
            BeginContext(880, 48, true);
            WriteLiteral(";\'//\r\n                    var agregarllamada = \'");
            EndContext();
            BeginContext(929, 91, false);
#line 29 "C:\Users\Equipo\Documents\00-Proyectos 2019\MASCore\MASCore\Views\Llamadas\Create.cshtml"
                                     Write(Url.Action("AgregarLlamadaTmp", "Llamadas", null, UriHelper.GetDisplayUrl(Context.Request)));

#line default
#line hidden
            EndContext();
            BeginContext(1020, 693, true);
            WriteLiteral(@";'//
                </script>
                <div class=""row"" >
                    <div class=""col-9""></div>
                    <div class=""col-1"" style=""padding:0; display: table-row"">
                        <div id=""save"" style=""display: table-cell""></div>
                        <div id=""ToolTip1"">Guardar</div>
                    
                    </div>
                    <div class=""col-1"" style=""padding:0"">
                        <div id=""regresar"" style=""display: table-cell""></div>
                        <div id=""ToolTip2"">Regresar a la página anterior</div>
             </div>
                </div>
            </div>
        </div>
    </article>
");
            EndContext();
            BeginContext(1720, 36, false);
#line 47 "C:\Users\Equipo\Documents\00-Proyectos 2019\MASCore\MASCore\Views\Llamadas\Create.cshtml"
Write(Html.Hidden("IdentificadorFinal", 0));

#line default
#line hidden
            EndContext();
            BeginContext(1758, 8051, true);
            WriteLiteral(@"    <input type=""hidden"" value=""Guardar"" name=""submitButton"" />
    <div class=""col-lg-12 col-md-12 col-sm-12 col-xs-12"">
        <div class=""x_panel"">
            <div class=""x_title"">
                <div class=""dx-fieldset"">
                    <div class=""dx-field row"">
                        <label class=""dx-field-label col-4"">Fecha de la Cédula:</label>
                        <div class=""dx-field-value col-8"">

                            <div id=""FechaCedula""></div>

                        </div>
                    </div>
                    <div class=""dx-field row"">
                        <label class=""dx-field-label col-4"">Seleccione el tipo de actividad que corresponda:</label>
                        <div class=""dx-field-value col-8"">
                            <div id=""Actividad""></div>

                        </div>
                    </div>
                </div>
                <div class=""clearfix""></div>
            </div>
            <div class=""demo-container");
            WriteLiteral(@""">
                <div id=""tabs"">
                    <div class=""tabs-container""></div>
                    <div class=""content  dx-fieldset"">
                    </div>
                </div>
                <div id=""generales"">
                    <div class=""dx-fieldset"">
                        <div class=""dx-field row"">
                            <label class=""dx-field-label col-3"">Embarcación:</label>
                            <div class=""dx-field-value"">
                                <div id=""Barco""></div>

                            </div>
                        </div>
                        <div class=""dx-field row"">
                            <label class=""dx-field-label col-3"">Razón Social:</label>
                            <div class=""dx-field-value"">
                                <div id=""RazonSocial""></div>

                            </div>
                        </div>
                        <div class=""dx-field row"">
                            <label ");
            WriteLiteral(@"class=""dx-field-label col-3"">Matrícula:</label>
                            <div class=""dx-field-value"">
                                <div id=""Matricula""></div>

                            </div>
                        </div>
                        <div class=""dx-field row"">
                            <label class=""dx-field-label col-3"">RNP:</label>
                            <div class=""dx-field-value"">
                                <div id=""RNP""></div>

                            </div>
                        </div>
                        <div class=""dx-field row"">
                            <label class=""dx-field-label col-3"">Puerto Base:</label>
                            <div class=""dx-field-value"">
                                <div id=""PuertoBase""></div>

                            </div>
                        </div>
                        <div class=""row"">
                            <div class=""col-12"">
                                <div id=""buscarembarcac");
            WriteLiteral(@"iones""></div>
                                <div id=""toolTip3"">Buscar embarcaciones</div>

                            </div>
                        </div>
                    </div>
                </div>
                <div id=""alertas"">
                    <div class=""dx-fieldset"">
                        <div class=""dx-field row"">
                            <label class=""dx-field-label col-4"">Persona, Empresa o Dependencia a quien se realizó la llamada:</label>
                            <div class=""dx-field-value col-8"">
                                <div id=""Empresa""></div>

                            </div>
                        </div>
                        <div class=""dx-field row"">
                            <label class=""dx-field-label col-4"">Nombre de la persona que atendió la llamada:</label>
                            <div class=""dx-field-value col-8"">
                                <div id=""Atendio""></div>

                            </div>
                ");
            WriteLiteral(@"        </div>
                        <div class=""dx-field row"">
                            <label class=""dx-field-label col-4"">Lugar que se hizo la llamada:</label>
                            <div class=""dx-field-value col-8"">
                                <div id=""Lugar""></div>

                            </div>
                        </div>
                        <div class=""dx-field row"">
                            <label class=""dx-field-label col-4"">OFP que realizó la llamada:</label>
                            <div class=""dx-field-value col-8"">
                                <div id=""OFP""></div>

                            </div>
                        </div>
                    </div>
                </div>
                <div id=""comentarios"">
                    <div class=""dx-fieldset"">
                        <div class=""dx-field row"">
                            <label class=""dx-field-label col-3"">Descripción de la actividad:</label>
                            <");
            WriteLiteral(@"div class=""dx-field-value col-9"">
                                <div id=""Descripcion""></div>

                            </div>
                        </div>
                        <div class=""dx-field row"">
                            <label class=""dx-field-label col-3"">Fecha</label>
                            <div class=""dx-field-value col-9"">
                                <div id=""Fecha""></div>

                            </div>
                        </div>
                        <div class=""dx-field row"">
                            <label class=""dx-field-label col-3"">Hora (HH:MM:SS)</label>                            
                            <div class=""dx-field-value col-9"">
                                <div id=""Hora""></div>
                            </div>
                        </div>
                        <div class=""dx-field row"">
                            <label class=""dx-field-label col-3"">*Tipo</label>
                            <div class=""dx-field-");
            WriteLiteral(@"value col-9"">
                                <div id=""Tipo""></div>
                            </div>
                        </div>
                        <div class=""dx-field row"">
                            <label class=""dx-field-label col-3"">*Número</label>
                            <div class=""dx-field-value col-9"">
                                <div id=""Numero""></div>
                            </div>
                        </div>
                        <div class=""dx-field row"">
                            <label class=""dx-field-label col-3"">*Respuesta</label>
                            <div class=""dx-field-value col-9"">
                                <div id=""Respuesta""></div>
                            </div>
                        </div>
                        <div class=""row"">
                            <div class=""col-10""></div>
                            <div class=""col-1"" style=""padding-right: 0px;margin-right: 0px;text-align: right;margin-top: auto;"">
       ");
            WriteLiteral(@"                         <div id=""small-indicator""></div>
                            </div>
                            <div class=""col-1"" style=""padding:0"">
                                
                                <div id=""button""></div>
                            </div>
                        </div>
                        <div class=""row col-12"">
                            <div class=""alert alert-danger"" id=""alerta"">
                                <button type=""button"" class=""close"" data-dismiss=""alert"">x</button>
                                <strong>Favor de llenar los Campos requeridos. </strong><br />
                                <span id=""mensaje""></span>
                            </div>
                        </div>
                        <div style=""text-align:center; color:Red;font-size:medium"">
                            ");
            EndContext();
            BeginContext(9810, 19, false);
#line 213 "C:\Users\Equipo\Documents\00-Proyectos 2019\MASCore\MASCore\Views\Llamadas\Create.cshtml"
                       Write(ViewBag.NoValidoPor);

#line default
#line hidden
            EndContext();
            BeginContext(9829, 368, true);
            WriteLiteral(@"
                        </div>
                        <div class=""dx-field row"">
                            <div id=""grid"" class=""col-12"">
                                <div id=""gridContainer""></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

");
            EndContext();
            BeginContext(11004, 20, true);
            WriteLiteral("            </div>\r\n");
            EndContext();
#line 247 "C:\Users\Equipo\Documents\00-Proyectos 2019\MASCore\MASCore\Views\Llamadas\Create.cshtml"
      

}

#line default
#line hidden
            BeginContext(11037, 2, true);
            WriteLiteral("\r\n");
            EndContext();
            BeginContext(11041, 257, false);
#line 251 "C:\Users\Equipo\Documents\00-Proyectos 2019\MASCore\MASCore\Views\Llamadas\Create.cshtml"
Write(Html.DevExtreme().Popup()
        .ID("popup")
        .ElementAttr("class", "popup")
        .ShowTitle(true)
        .Title("Busca Información de Embarcación")
        .Visible(false)
        .DragEnabled(false)
        .CloseOnOutsideClick(true)
);

#line default
#line hidden
            EndContext();
            BeginContext(11299, 4, true);
            WriteLiteral("\r\n\r\n");
            EndContext();
#line 261 "C:\Users\Equipo\Documents\00-Proyectos 2019\MASCore\MASCore\Views\Llamadas\Create.cshtml"
 using (Html.DevExtreme().NamedTemplate("popup-template"))
{

#line default
#line hidden
            BeginContext(11366, 133, true);
            WriteLiteral("    <div class=\"row\">\r\n        <div class=\"col-6\">\r\n            <div class=\"row col-12\" style=\"padding-bottom:5px\">\r\n                ");
            EndContext();
            BeginContext(11501, 384, false);
#line 266 "C:\Users\Equipo\Documents\00-Proyectos 2019\MASCore\MASCore\Views\Llamadas\Create.cshtml"
            Write(Html.DevExtreme().SelectBox().DisplayExpr("Resultado").ValueExpr("Resultado").DeferRendering(false)
                            .DataSource(d => d.Mvc().LoadAction("GetOpcionesBusqueda")).ID("embarcaciones-tipo")
                            .Placeholder("-- Seleccione el Tipo de Alerta --").Width("100%").ElementAttr("class", "form-control").ShowClearButton(true)
                );

#line default
#line hidden
            EndContext();
            BeginContext(11886, 18, true);
            WriteLiteral("\r\n                ");
            EndContext();
            BeginContext(11906, 295, false);
#line 270 "C:\Users\Equipo\Documents\00-Proyectos 2019\MASCore\MASCore\Views\Llamadas\Create.cshtml"
            Write(Html.DevExtreme().Tooltip().Target("#embarcaciones-tipo").ShowEvent("mouseenter").HideEvent("mouseleave")
                            .ContentTemplate(item => new global::Microsoft.AspNetCore.Mvc.Razor.HelperResult(async(__razor_template_writer) => {
    PushWriter(__razor_template_writer);
    BeginContext(12065, 142, true);
    WriteLiteral("Seleccionar en este campo el <b>Tipo de Atributo de la embarcación</b>, dicha información se usuará para realizar la búsqueda de embarcaciones");
    EndContext();
    PopWriter();
}
)));

#line default
#line hidden
            EndContext();
            BeginContext(12216, 148, true);
            WriteLiteral("\r\n            </div>\r\n        </div>\r\n        <div class=\"col-6\">\r\n            <div class=\"row col-12\" style=\"padding-bottom:5px\">\r\n                ");
            EndContext();
            BeginContext(12366, 175, false);
#line 276 "C:\Users\Equipo\Documents\00-Proyectos 2019\MASCore\MASCore\Views\Llamadas\Create.cshtml"
            Write(Html.DevExtreme().TextBox().ID("embarcaciones-valor").Placeholder("-- Indique la palabra a buscar --").Width("100%").ElementAttr("class", "form-control").ShowClearButton(true));

#line default
#line hidden
            EndContext();
            BeginContext(12542, 18, true);
            WriteLiteral("\r\n                ");
            EndContext();
            BeginContext(12562, 266, false);
#line 277 "C:\Users\Equipo\Documents\00-Proyectos 2019\MASCore\MASCore\Views\Llamadas\Create.cshtml"
            Write(Html.DevExtreme().Tooltip().Target("#embarcaciones-valor").ShowEvent("mouseenter").HideEvent("mouseleave").ContentTemplate(item => new global::Microsoft.AspNetCore.Mvc.Razor.HelperResult(async(__razor_template_writer) => {
    PushWriter(__razor_template_writer);
    BeginContext(12692, 142, true);
    WriteLiteral("Indicar en este campo la <b>Palabra a buscar del tipo seleccionado</b>, dicha información se usuará para realizar la búsqueda de embarcaciones");
    EndContext();
    PopWriter();
}
)));

#line default
#line hidden
            EndContext();
            BeginContext(12843, 231, true);
            WriteLiteral("\r\n            </div>\r\n        </div>\r\n    </div>\r\n    <div class=\"row\">\r\n        <div class=\"col-10\"></div>\r\n        <div class=\"col-1\" style=\"padding-right: 0px;margin-right: 0px;text-align: right;margin-top: auto;\">\r\n            ");
            EndContext();
            BeginContext(13076, 99, false);
#line 284 "C:\Users\Equipo\Documents\00-Proyectos 2019\MASCore\MASCore\Views\Llamadas\Create.cshtml"
        Write(Html.DevExtreme().LoadIndicator().ID("embarcaciones-indicator").Height(20).Width(20).Visible(false));

#line default
#line hidden
            EndContext();
            BeginContext(13176, 59, true);
            WriteLiteral("\r\n        </div>\r\n        <div class=\"col-1\">\r\n            ");
            EndContext();
            BeginContext(13237, 220, false);
#line 287 "C:\Users\Equipo\Documents\00-Proyectos 2019\MASCore\MASCore\Views\Llamadas\Create.cshtml"
        Write(Html.DevExtreme().Button().Icon("fas fa-search").ID("embarcaciones-buscar").Type(ButtonType.Default).StylingMode(ButtonStylingMode.Contained).UseSubmitBehavior(false).OnClick("llamdastelefonicas.ReloadGridEmbarcaciones"));

#line default
#line hidden
            EndContext();
            BeginContext(13458, 14, true);
            WriteLiteral("\r\n            ");
            EndContext();
            BeginContext(13474, 145, false);
#line 288 "C:\Users\Equipo\Documents\00-Proyectos 2019\MASCore\MASCore\Views\Llamadas\Create.cshtml"
        Write(Html.DevExtreme().Tooltip().Target("#embarcaciones-buscar").ShowEvent("mouseenter").HideEvent("mouseleave").ContentTemplate("Buscar Información"));

#line default
#line hidden
            EndContext();
            BeginContext(13620, 121, true);
            WriteLiteral("\r\n        </div>\r\n    </div>\r\n    <div class=\"row\" style=\"padding-top:5px\">\r\n\r\n        <div class=\"col-12\">\r\n            ");
            EndContext();
            BeginContext(13743, 2428, false);
#line 294 "C:\Users\Equipo\Documents\00-Proyectos 2019\MASCore\MASCore\Views\Llamadas\Create.cshtml"
        Write(Html.DevExtreme().DataGrid<MASCore.ModelReference.SPMASBuscaInformacionDeEmbarcacionyPangas_Result>()
                        .ID("gridEmbarcaciones")
                        .DataSource(Model.InformacionDeEmbarcacion, new string[] { "Matricula" })
                        .ColumnHidingEnabled(true)
                        .ShowColumnLines(true)
                        .ShowRowLines(true)
                        .RowAlternationEnabled(true)
                        .ShowBorders(true)
                        .Pager(p => p
                            .AllowedPageSizes(new int[] { 3, 5 })
                            .ShowInfo(true)
                            .ShowNavigationButtons(true)
                            .ShowPageSizeSelector(true)
                            .Visible(true))
                        .Paging(p => p.PageSize(5))
                        .ShowColumnHeaders(true)
                        .WordWrapEnabled(true)
                        .Columns(columns =>
                        {
                            columns.AddFor(m => m.Matricula).Caption("Matrícula");
                            columns.AddFor(m => m.Nombre).Caption("Nombre Barco");
                            columns.AddFor(m => m.SerieAntena).Caption("Serie Antena");
                            columns.AddFor(m => m.Localidad).Caption("Localidad");
                            columns.AddFor(m => m.RNP).Caption("RNP");
                            columns.AddFor(m => m.Permiso).Caption("Permiso");
                            columns.AddFor(m => m.TipoPermiso).Caption("Tipo Permiso");
                            columns.AddFor(m => m.RazonSocial).Caption("Razón Social");
                            columns.AddFor(m => m.RepLegal).Caption("Representante Legal");
                            columns.AddFor(m => m.RNPTitular).Caption("RNP Titular");
                            columns.AddFor(m => m.Matricula).Caption("Acción").CellTemplate(item => new global::Microsoft.AspNetCore.Mvc.Razor.HelperResult(async(__razor_template_writer) => {
    PushWriter(__razor_template_writer);
    BeginContext(15722, 392, true);
    WriteLiteral(@"
                                <a title=""Seleccionar"" href=""javascript:llamdastelefonicas.RegresaValoresEmbarcacion('<%= data.Nombre %>','<%= data.RazonSocial %>','<%= value %>','<%= data.RNP %>','<%= data.PuertoBASe %>')"">
                                    <i class=""dx-icon fa fa-hand-pointer tamanoIconoEnGrid""></i>
                                </a>
                            ");
    EndContext();
    PopWriter();
}
));
                                            })
            );

#line default
#line hidden
            EndContext();
            BeginContext(16186, 30, true);
            WriteLiteral("\r\n        </div>\r\n    </div>\r\n");
            EndContext();
#line 332 "C:\Users\Equipo\Documents\00-Proyectos 2019\MASCore\MASCore\Views\Llamadas\Create.cshtml"
}

#line default
#line hidden
            BeginContext(16219, 6, true);
            WriteLiteral("\r\n    ");
            EndContext();
            BeginContext(16225, 60, false);
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("script", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "4a994f26bb4243fd9692b981746abcec", async() => {
            }
            );
            __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_1);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            EndContext();
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<MASCore.Models.LlamadaModel> Html { get; private set; }
    }
}
#pragma warning restore 1591
