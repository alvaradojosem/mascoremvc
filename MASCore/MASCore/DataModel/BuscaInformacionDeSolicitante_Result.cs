﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MASCore.DataModel
{
    public partial class BuscaInformacionDeSolicitante_Result
    {
        public string Solicitante { get; set; }
        public string Cargo { get; set; }
        public string Telefono { get; set; }
        public string email { get; set; }
        public string dependencia { get; set; }
        public string Estado { get; set; }
        public string Ciudad { get; set; }
    }
}
