using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Localization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Razor;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.FileProviders;
using Microsoft.Extensions.Options;
using Microsoft.EntityFrameworkCore;
using MASCore.Components;
using MASCore.Data;

namespace MASCore
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.Configure<CookiePolicyOptions>(options =>
            {
                // This lambda determines whether user consent for non-essential cookies is needed for a given request.
                options.CheckConsentNeeded = context => true;
                options.MinimumSameSitePolicy = SameSiteMode.None;
            });

            // Agregamos los Idiomas en el sistema
            services.AddLocalization(options => options.ResourcesPath = "Resources");
            services.AddMvc().AddJsonOptions(options => options.SerializerSettings.ContractResolver = new Newtonsoft.Json.Serialization.DefaultContractResolver())
                .AddViewLocalization(
                opts => { opts.ResourcesPath = "Resources"; })
                .AddViewLocalization(LanguageViewLocationExpanderFormat.Suffix)
                .AddDataAnnotationsLocalization()
                .SetCompatibilityVersion(CompatibilityVersion.Version_2_1);
            services.Configure<RequestLocalizationOptions>(opts =>
            {
                var SupportedCulture = new List<CultureInfo>
                {
                    new CultureInfo("BG"),
                    new CultureInfo("DK"),
                    new CultureInfo("DE"),
                    new CultureInfo("US"),
                    new CultureInfo("FR"),
                    new CultureInfo("NL"),
                    new CultureInfo("PL"),
                    new CultureInfo("BR"),
                    new CultureInfo("PT"),
                    new CultureInfo("RU"),
                    new CultureInfo("UA"),
                    new CultureInfo("CN"),
                    new CultureInfo("ES")
                };
                opts.DefaultRequestCulture = new RequestCulture("ES");
                opts.SupportedCultures = SupportedCulture;
                opts.SupportedUICultures = SupportedCulture;
            });

            services.AddSession(); // add session

            //services.AddDbContext<ApplicationDbContext>(options =>
            //   //options.UseSqlServer(
            //   //    Configuration.GetConnectionString("DefaultConnection")));
            //services.AddDefaultIdentity<IdentityUser>()
            //    //.AddDefaultUI(UIFramework.Bootstrap4)
            //    .AddEntityFrameworkStores<ApplicationDbContext>());



            //services.AddMvc().AddJsonOptions(options => options.SerializerSettings.ContractResolver = new Newtonsoft.Json.Serialization.DefaultContractResolver()).SetCompatibilityVersion(CompatibilityVersion.Version_2_1);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IConfiguration configuration, IHostingEnvironment env)
        {
            var contentRoot = configuration.GetValue<string>(WebHostDefaults.ContentRootKey);

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                app.UseHsts();
            }

            // Configure localization.
            var locOptions = app.ApplicationServices.GetService<IOptions<RequestLocalizationOptions>>();
            app.UseRequestLocalization(locOptions.Value);

            app.UseStaticFiles();
            app.UseHttpsRedirection();
           
            app.UseCookiePolicy();
            app.UseAuthentication();
            app.UseSession();

           

            app.UseMvc(routes =>
            {

                //routes.MapRoute(
                //    name: "defaultLogin",
                //    template: "{controller=Account}/{action=PageLogin}/{id?}");

                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}"
                    );

            });
        }
    }
}
