﻿var URL = 'https://10.215.71.239:44350/'; // Net Core
var BaseUrl = 'https://localhost:44387';

var xhr = new XMLHttpRequest();

var api = {

        Account: {
        Athenticate: function (obj, callback) {
     
            return POST(obj, 'api/Account/authenticate', callback);
        }
    },
    Cedulas: {  
        GetAlarms: function (id, success, error) {
            return GET(null, "api/LoadControls/GetAlarmas", success, error);
        }

    }
   
};

function GET(obj, url, success, error) {
    return $.ajax({
        type: "GET",
        url: url,
        success: function (data) {
            if (data)
                success(data);
        },
        error: function (response) {
            error(response);
        }

    });

}
function POST(obj, url, success, error) {

    return ajax("POST", obj, url, success, error);
}
function PUT(obj, url, success, error) {
    return ajax("PUT", obj, url, success, error);
}
function DELETE(obj, url, success, error) {
    return ajax("DELETE", obj, url, success, error);
}
//// 'https://10.215.71.239:44350/api/Account/authenticate?username=admin&password=ikaxplir',
function ajax(type, obj, url, success, error) {
    return $.ajax({
        url: URL + url,        
        headers: GetHeaders(),
        type: type,//POST,GET,PUT,DELETE
        data: obj   
    }).done(function (data) {
        if (success)
            success(data);
    }).fail(function (data) {
        LogError(data);
        if (error)
            error(data);
    });
}


function GetHeaders() {
    var header =
    {
        'Accept': 'application/json',
        'Authorization': GetHeaderAuth()
    };

    return header;
}
function GetHeaderAuth() {
    var result = "";
    var uData = JSON.parse(sessionStorage.getItem("uData"));
    if (uData !== null && uData !== undefined) {
        result = uData.token;
    }
    return 'astrum ' + result;
  
}
function LogError(data) {
    if (data !== null && data.responseJSON !== undefined) {
        if (data.status === 401) {
            alert("Acceso no autorizado, Redirigiendo a Login");
            document.location = BaseUrl;
            return;
        }
        console.log("Response data : " + JSON.stringify(data.responseJSON));
    }
    else if (data !== null)
        console.log("Response data : " + JSON.stringify(data));

    var msg = "Error inseperado";
    if (typeof ERR_INESPERADO !== 'undefined') msg = ERR_INESPERADO;

    alert(msg);
}