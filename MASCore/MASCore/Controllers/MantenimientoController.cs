﻿using System;
using System.Collections.Generic;
using MASCore.Models;
using ClosedXML.Excel;
using MASCore.Exportar;
//
using DevExtreme.AspNet.Mvc;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using MASCore.ModelReference;

namespace MASCore.Controllers
{
    public class MantenimientoController : Controller
    {
        FuncionesProcedimientos FunProc = new FuncionesProcedimientos();
        //
        // GET: /Mantenimiento/

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult BuscaMantenimiento()
        {
            MantenimientoBusquedaModel ModeloTmp = new MantenimientoBusquedaModel();
            #region "ASIGNA VALORES, CARGA DDLS Y GRIDS"
            ModeloTmp.UsaFechas = true;
            ModeloTmp.FechaInicio = DateTime.Now;
            ModeloTmp.FechaFin = DateTime.Now;            
            List<BuscaMantenimientos_Result> ResultTmp = new List<BuscaMantenimientos_Result>();
            ModeloTmp.ResultadoListaMantenimientos = null;// ResultTmp;
            
            List<MantenimientoResumen> ResultResumenTmp = new List<MantenimientoResumen>();
            ModeloTmp.ResultadoListaMantenimientosResumen = ResultResumenTmp;

            List<BuscaMantenimientosResumenNegacion_Result> ResultResumenNegacionTmp = new List<BuscaMantenimientosResumenNegacion_Result>();
            ModeloTmp.ResultadoListaMantenimientosResumenNegacion = null;// ResultResumenNegacionTmp;

            ModeloTmp.NoBarcos = true;
            ModeloTmp.MostrandoResumen = false;
            ModeloTmp.MostrandoBusquedaInformacionEmbarcacion = false;

            //ViewBag.zonamantenimientos = new SelectList(FunProc.LlenarResultadoGenerico("zonamantenimientos"), "Resultado", "Resultado");
            //ViewBag.permisomantenimientos = new SelectList(FunProc.LlenarResultadoGenerico("permisomantenimientos"), "Resultado", "Resultado");
            //ViewBag.supervisormantenimientos = new SelectList(FunProc.LlenarResultadoGenerico("supervisormantenimientos"), "Resultado", "Resultado");
            //ViewBag.RazonesSociales = FunProc.LlenarResultadoGenerico("RazonSocialMAS");
            //ViewBag.servicios = FunProc.LlenarResultadoGenerico("servicios");
            #endregion

            #region "SECCION BUSCA INFORMACION DE EMBARCACION"
            List<BuscaInformacionDeEmbarcacion_Result> ModeloInformacionDeEmbarcacion = new List<BuscaInformacionDeEmbarcacion_Result>();
            ModeloTmp.InformacionDeEmbarcacion = null;// ModeloInformacionDeEmbarcacion;

            ViewBag.opcionesbusqueda = null;//new SelectList(FunProc.LlenarResultadoGenerico("opcionesbusqueda"), "Resultado", "Resultado");

            #endregion

            //Session["SessionLista"] = ModeloTmp.ResultadoListaMantenimientos;
            //Session["SessionListaResumen"] = ModeloTmp.ResultadoListaMantenimientosResumen;

            return View(ModeloTmp);
        }
        [HttpPost]
        public ActionResult BuscaMantenimiento(MantenimientoBusquedaModel ModeloTmp, string submitButton, string opcionesbusqueda, string txtvalor)
        {
            ModeloTmp.MostrandoBusquedaInformacionEmbarcacion = false;
            ModeloTmp.MostrandoResumen = false;

            #region "ASIGNA VALORES, CARGA DDLS Y GRIDS"
            List<BuscaMantenimientos_Result> ResultTmp = new List<BuscaMantenimientos_Result>();
            ModeloTmp.ResultadoListaMantenimientos = null;//ResultTmp;
            List<MantenimientoResumen> ResultResumenTmp = new List<MantenimientoResumen>();
            ModeloTmp.ResultadoListaMantenimientosResumen = ResultResumenTmp;
            List<BuscaMantenimientosResumenNegacion_Result> ResultResumenNegacionTmp = new List<BuscaMantenimientosResumenNegacion_Result>();
            ModeloTmp.ResultadoListaMantenimientosResumenNegacion = null;//ResultResumenNegacionTmp;

            //ViewBag.zonamantenimientos = new SelectList(FunProc.LlenarResultadoGenerico("zonamantenimientos"), "Resultado", "Resultado");
            //ViewBag.permisomantenimientos = new SelectList(FunProc.LlenarResultadoGenerico("permisomantenimientos"), "Resultado", "Resultado");
            //ViewBag.supervisormantenimientos = new SelectList(FunProc.LlenarResultadoGenerico("supervisormantenimientos"), "Resultado", "Resultado");
            //ViewBag.servicios = FunProc.LlenarResultadoGenerico("servicios");
            //ViewBag.RazonesSociales = FunProc.LlenarResultadoGenerico("RazonSocialMAS");
            #endregion

            if (ModeloTmp.Servicios != null)
            {
                ModeloTmp.Servicios = ModeloTmp.Servicios.Trim();
                string ServiciosTmp = ModeloTmp.Servicios;
                string UltimoCaracter = ServiciosTmp.Substring(ServiciosTmp.Length - 1, 1);
                if (UltimoCaracter == ",")
                {
                    ServiciosTmp = ServiciosTmp.Substring(0, ServiciosTmp.Length - 1);
                }
                ServiciosTmp = ServiciosTmp.Replace(",", "','");
                ModeloTmp.Servicios = ServiciosTmp;
            }
           
            //List<WebGridColumn> columns = new List<WebGridColumn>();

            #region "VALIDACIONES"
            Boolean Valido = true;
            if (ModeloTmp.FechaInicio.Year < 1753 || ModeloTmp.FechaFin.Year < 1753)
            {
                ViewBag.NoValidoPor = "No es posible realizar la búsqueda. Verificar Fechas.";
                Valido = false;
            }
            if (ModeloTmp.UsaFechas == true)
            {            
                if (ModeloTmp.FechaInicio > ModeloTmp.FechaFin)
                {
                    ViewBag.NoValidoPor = "No es posible realizar la búsqueda, la fecha inicial no puede ser mayor a la fecha final. Verificar.";
                    Valido = false;
                }                
            }

            if (ModeloTmp.NombreBarco != null)
            {
                if (ModeloTmp.NombreBarco != "")
                {
                    ModeloTmp.Embarcacion = null;//FunProc.BuscaResultadoGenericoConParametros("matriculaconnombrebarco", "", ModeloTmp.NombreBarco, 0, 0);
                    if (ModeloTmp.Embarcacion == "")
                    {
                        Valido = false;
                    }
                }                
            }
            #endregion

            switch (submitButton)
            {
                case "Buscar":
                    if (Valido == true)
                    {
                        ModeloTmp.MostrandoResumen = false;

                        #region "BUSCAR CONSULTA PRINCIPAL"
                        //ModeloTmp.ResultadoListaMantenimientos = FunProc.EncuentraMantenimientos(ModeloTmp);
                        //Session["SessionLista"] = ModeloTmp.ResultadoListaMantenimientos;
                        #endregion           
                        #region "BUSCAR EN RESUMEN"           
                        //ModeloTmp.ResultadoListaMantenimientosResumen = FunProc.EncuentraMantenimientosResumen(ModeloTmp);
                        //Session["SessionListaResumen"] = ModeloTmp.ResultadoListaMantenimientosResumen;

                        //// CREA LAS COLUMNAS DINAMICAMENTE       //List<WebGridColumn> columns = new List<WebGridColumn>();                    
                        //if (ModeloTmp.ServicioSel == true)
                        //    columns.Add(new WebGridColumn() { ColumnName = "Servicio", Header = "Servicio" });
                        //if (ModeloTmp.PuertoBaseSel == true)
                        //    columns.Add(new WebGridColumn() { ColumnName = "Localidad", Header = "Localidad" });
                        //if (ModeloTmp.RazonSocialSel == true)
                        //    columns.Add(new WebGridColumn() { ColumnName = "RazonSocial", Header = "Razon Social" });
                        //if (ModeloTmp.SupervisorSel == true)
                        //    columns.Add(new WebGridColumn() { ColumnName = "Supervisor", Header = "Supervisor" });
                        //if (ModeloTmp.ZonaSel == true)
                        //    columns.Add(new WebGridColumn() { ColumnName = "Zona", Header = "Zona" });
                        //if (ModeloTmp.BarcoSel == true)
                        //    columns.Add(new WebGridColumn() { ColumnName = "NombreBarco", Header = "NombreBarco" });
                        //if (ModeloTmp.ServicioSel == false && ModeloTmp.PuertoBaseSel == false && ModeloTmp.RazonSocialSel == false && ModeloTmp.SupervisorSel == false && ModeloTmp.ZonaSel == false && ModeloTmp.BarcoSel == false)
                        //{
                        //    columns.Add(new WebGridColumn() { ColumnName = "Servicio", Header = "Servicio" });
                        //}

                        //if (ModeloTmp.NoBarcos == true)
                        //    columns.Add(new WebGridColumn() { ColumnName = "NoBarcos", Header = "No Barcos" });
                        //if (ModeloTmp.NoServicios == true)
                        //    columns.Add(new WebGridColumn() { ColumnName = "NoServicios", Header = "No Servicios" });
                        //if (ModeloTmp.NoAmbos == true)
                        //{
                        //    columns.Add(new WebGridColumn() { ColumnName = "NoBarcos", Header = "No Barcos" });
                        //    columns.Add(new WebGridColumn() { ColumnName = "NoServicios", Header = "No Servicios" });
                        //}


                        ////columns.Add(new WebGridColumn() { ColumnName = "Total", Header = "Total" });
                        //Session["SessionColumnas"] = columns;
                        //ViewBag.Columns = columns;

                        #endregion
                        #region"BUSCAR TOTAL BARCOS / FOLIOS"
                        ModeloTmp = null;//FunProc.EncuentraMantenimientosTotalBarcosFolio(ModeloTmp);
                        #endregion
                        #region "BUSCAR LA NEGACION"
                        //ModeloTmp.ResultadoListaMantenimientosResumenNegacion = FunProc.EncuentraMantenimientosResumenNegacion(ModeloTmp);
                        //Session["SessionListaNegacion"] = ModeloTmp.ResultadoListaMantenimientosResumenNegacion;
                        #endregion
                    }
                    break;
                case "Buscar Resumen":
                    if (Valido == true)
                    {
                        ModeloTmp.MostrandoResumen = true;

                        #region "BUSCAR CONSULTA PRINCIPAL"
                        ModeloTmp.ResultadoListaMantenimientos = null;//FunProc.EncuentraMantenimientos(ModeloTmp);
                        #endregion
                        #region "BUSCAR EN RESUMEN"
                        ModeloTmp.ResultadoListaMantenimientosResumen = null;//FunProc.EncuentraMantenimientosResumen(ModeloTmp);
                        #endregion
                        #region"BUSCAR TOTAL BARCOS / FOLIOS"
                        ModeloTmp = null;//FunProc.EncuentraMantenimientosTotalBarcosFolio(ModeloTmp);
                        #endregion
                        #region "BUSCAR LA NEGACION"
                        ModeloTmp.ResultadoListaMantenimientosResumenNegacion = null;//FunProc.EncuentraMantenimientosResumenNegacion(ModeloTmp);
                        #endregion
                    }
                    break;
                case "Buscar Información":
                    ModeloTmp.MostrandoBusquedaInformacionEmbarcacion = true;
                    break;
                default :
                    break;
            }

            #region "SECCION BUSCA INFORMACION DE EMBARCACION"
            //List<BuscaInformacionDeEmbarcacion_Result> ModeloInformacionDeEmbarcacion = new List<BuscaInformacionDeEmbarcacion_Result>();
            //ModeloInformacionDeEmbarcacion = FunProc.EncuentraInformacionDeEmbarcacionSP(opcionesbusqueda, txtvalor);
            //ModeloTmp.InformacionDeEmbarcacion = ModeloInformacionDeEmbarcacion;
            //ViewBag.opcionesbusqueda = new SelectList(FunProc.LlenarResultadoGenerico("opcionesbusqueda"), "Resultado", "Resultado");
            #endregion

            string ParametrosDeBusqueda = "";
            if (ModeloTmp.UsaFechas == true)
            {
                ParametrosDeBusqueda += "Desde " + ModeloTmp.FechaInicio.ToShortDateString() + " Hasta " + ModeloTmp.FechaFin.ToShortDateString() + " |";
            }
            if (ModeloTmp.Embarcacion != null && ModeloTmp.Embarcacion.Length > 0)
                ParametrosDeBusqueda += " Embarcacion: " + ModeloTmp.Embarcacion + " |";
            if (ModeloTmp.Antena != null && ModeloTmp.Antena.Length > 0)
                ParametrosDeBusqueda += " Antena: " + ModeloTmp.Antena + " |";
            if (ModeloTmp.RazonSocial != null && ModeloTmp.RazonSocial.Length > 0)
                ParametrosDeBusqueda += " Razon Social: " + ModeloTmp.RazonSocial + " |";
            if (ModeloTmp.PuertoBase != null && ModeloTmp.PuertoBase.Length > 0)
                ParametrosDeBusqueda += " Puerto Base: " + ModeloTmp.PuertoBase + " |";
            if (ModeloTmp.Zona != null && ModeloTmp.Zona.Length > 0)
                ParametrosDeBusqueda += " Zona: " + ModeloTmp.Zona + " |";
            if (ModeloTmp.Permiso != null && ModeloTmp.Permiso.Length > 0)
                ParametrosDeBusqueda += " Permiso: " + ModeloTmp.Permiso + " |";
           
            if (ParametrosDeBusqueda.Length > 0)
                ParametrosDeBusqueda = ParametrosDeBusqueda.Substring(0, ParametrosDeBusqueda.Length - 1);

            //Session["SessionParametrosDeBusqueda"] = ParametrosDeBusqueda;

            return View(ModeloTmp);
        }


        #region "NO USADOS POR AHORA"
        //
        // GET: /Mantenimiento/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        //
        // GET: /Mantenimiento/Create
        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /Mantenimiento/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /Mantenimiento/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        //
        // POST: /Mantenimiento/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /Mantenimiento/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        //
        // POST: /Mantenimiento/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
        #endregion

        #region "AUTOCOMPLETE"
        public string SearchResultadoGenerico_puertobase(string q, int limit)
        {
            var result = "";// FunProc.FindRsultadoGenerico(q, limit, "puertobase");
            return String.Join(Environment.NewLine, result);
        }

        public string SearchResultadoGenerico_razonsocial(string q, int limit)
        {
            var result = "";//FunProc.FindRsultadoGenerico(q, limit, "razonsocial");
            return String.Join(Environment.NewLine, result);
        }

        public string SearchResultadoGenerico_servicios(string q, int limit)
        {
            var result = "";//FunProc.FindRsultadoGenerico(q, limit, "servicios");
            return String.Join(Environment.NewLine, result);
        }

        public string SearchResultadoGenerico_muelle(string q, int limit)
        {
            var result = "";//FunProc.FindRsultadoGenerico(q, limit, "muelles");
            return String.Join(Environment.NewLine, result);
        }

        public string SearchResultadoGenerico_antena(string q, int limit)
        {
            var result = "";//FunProc.FindRsultadoGenerico(q, limit, "antenas");
            return String.Join(Environment.NewLine, result);
        }

        public string SearchResultadoGenerico_nombrebarco(string q, int limit)
        {
            var result = "";//FunProc.FindRsultadoGenerico(q, limit, "nombrebarco");
            return String.Join(Environment.NewLine, result);
        } 
        
        #endregion

        #region "EXPORTAR A PDF"
        public ActionResult GeneratePdfMantenimiento()
        {
            List<BuscaMantenimientos_Result> Lista = new List<BuscaMantenimientos_Result>();
            Lista = null;//Session["SessionLista"] as List<BuscaMantenimientos_Result>;
            var pdf = "";// new PdfResult(Lista, "GeneratePdfMantenimiento");
            return null;//pdf;
        }
        #endregion

        #region "EXPORTAR A EXCEL"
        public ActionResult GeneraReporteMantenimientosExportarBusqueda()
        {
            string UserName = "";
            if (HttpContext.Request.Cookies["userName"] != null)
                UserName = HttpContext.Request.Cookies["userName"].ToString();
            List<BuscaMantenimientos_Result> Lista = new List<BuscaMantenimientos_Result>();
            Lista = null;//Session["SessionLista"] as List<BuscaMantenimientos_Result>;

            List<MantenimientoResumen> ListaResumen = new List<MantenimientoResumen>();
            ListaResumen = null;//Session["SessionListaResumen"] as List<MantenimientoResumen>;

            List<BuscaMantenimientosResumenNegacion_Result> ListaNegacion = new List<BuscaMantenimientosResumenNegacion_Result>();
            ListaNegacion = null;//Session["SessionListaNegacion"] as List<BuscaMantenimientosResumenNegacion_Result>;

            //List<WebGridColumn> columns = new List<WebGridColumn>();
            //columns = Session["SessionColumnas"] as List<WebGridColumn>;

            //      Obtiene la ruta de la plantilla
            string ruta = null;//Server.MapPath("~/Templates/Mantenimientos/ExportarTodo.xlsx");
            //      Abre el libro de excel..
            var workbook = new XLWorkbook(ruta, XLEventTracking.Disabled);
            //      Selecciona la hoja 1 del libro
            var worksheet = workbook.Worksheet(1);
            worksheet.Cell("A8").Value = "Impreso por: " + UserName + " | Fecha: " + DateTime.Now.ToShortDateString();
            string ParametrosDeBusqueda = "";
            ParametrosDeBusqueda = null;//Session["SessionParametrosDeBusqueda"] as string;
            worksheet.Cell("A9").Value = ParametrosDeBusqueda;
            if (Lista != null)
            {
                #region "AGREGA VALORES"
                worksheet.Cell("A12").Value = Lista;
                //worksheet.Cell(5, 1).Value = Lista;
                //worksheet.Columns().AdjustToContents();
                #endregion
            }

            var worksheet2 = workbook.Worksheet(2);
            if (ListaResumen != null)
            {
                #region "AGREGA VALORES"
                int i = 1;
                //foreach (WebGridColumn Column in columns)
                //{
                //    if (Column.ColumnName != "")
                //    {
                //        worksheet2.Cell(4, i).Value = Column.ColumnName;
                //        int ii = 5;
                //        foreach (var Resum in ListaResumen)
                //        {
                //            if (Column.ColumnName == "Servicio")
                //            {
                //                worksheet2.Cell(ii, i).Value = Resum.Servicio;
                //            }
                //            if (Column.ColumnName == "Localidad")
                //            {
                //                worksheet2.Cell(ii, i).Value = Resum.Localidad;
                //            }
                //            if (Column.ColumnName == "RazonSocial")
                //            {
                //                worksheet2.Cell(ii, i).Value = Resum.RazonSocial;
                //            }
                //            if (Column.ColumnName == "Supervisor")
                //            {
                //                worksheet2.Cell(ii, i).Value = Resum.Supervisor;
                //            }
                //            if (Column.ColumnName == "Zona")
                //            {
                //                worksheet2.Cell(ii, i).Value = Resum.Zona;
                //            }
                //            if (Column.ColumnName == "NombreBarco")
                //            {
                //                worksheet2.Cell(ii, i).Value = Resum.NombreBarco;
                //            }
                //            if (Column.ColumnName == "NoBarcos")
                //            {
                //                worksheet2.Cell(ii, i).Value = Resum.NoBarcos;
                //            }
                //            if (Column.ColumnName == "NoServicios")
                //            {
                //                worksheet2.Cell(ii, i).Value = Resum.NoServicios;
                //            }
                //            ii += 1;
                //        }
                //        i += 1;

                //    }

                //}

                //worksheet2.Cell(5, 1).Value = ListaTmpResumen;
                //worksheet2.Columns().AdjustToContents();

                #endregion
            }


            var worksheet3 = workbook.Worksheet(3);
            if (ListaNegacion != null)
            {
                #region "AGREGA VALORES"
                worksheet3.Cell(5, 1).Value = ListaNegacion;
                worksheet3.Columns().AdjustToContents();
                #endregion
            }
            return new ExcelResult(workbook, "Exportar Mantenimientos");
        }

        #endregion

        [HttpGet]
        public ActionResult GetBarcos(DataSourceLoadOptions loadOptions)
        {
            return null;//Content(JsonConvert.SerializeObject(DataSourceLoader.Load(FunProc.LlenarResultadoGenerico("nombrebarco2"), loadOptions)), "application/json");
        }

        public ActionResult GetRazonesSociales(DataSourceLoadOptions loadOptions)
        {
            return null;// Content(JsonConvert.SerializeObject(DataSourceLoader.Load(FunProc.LlenarResultadoGenerico("RazonSocialMAS"), loadOptions)), "application/json");
        }

        public ActionResult GetServicios(DataSourceLoadOptions loadOptions)
        {
            return null;//Content(JsonConvert.SerializeObject(DataSourceLoader.Load(FunProc.LlenarResultadoGenerico("servicios"), loadOptions)), "application/json");
        }
    }
}
