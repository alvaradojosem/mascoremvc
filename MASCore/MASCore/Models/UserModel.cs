﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication.Cookies;
using System.Security.Claims;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Http;

namespace MASCore.Models
{



    public class UserModel
    {
        [BindProperty]
        public UserModel User { get; set; }

        [TempData]
        public string ErrorMessage { get; set; }



        public int IdUsuario { get; set; }
        [Required]
        [StringLength(20)]

        [Display(Name = "Usuario : ")]
        public string NombreCorto { get; set; }

        // [Required]
        [StringLength(100, MinimumLength = 10)]
        [Display(Name = "Nombre completo : ")]
        public string Nombre { get; set; }

        [Required]
        [StringLength(10)] //[StringLength(10,MinimumLength=6)] para validar tambien el minimo requerido de caracteres
        [DataType(DataType.Password)]
        [Display(Name = "Contraseña  : ")]
        public string Contraseña { get; set; }
        public string ContraseñaSalt { get; set; }
        //public int Nivel { get; set; }
    }
}