﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MASCore.ModelReference
{
    public class OficioSalidaReference
    {
    }
    public partial class RelacionSalidaSeleccionarTmp_Result
    {
        public int IdSalida { get; set; }
        public string RNP { get; set; }
        public string Barco { get; set; }
        public string Matricula { get; set; }
        public string RazonSocial { get; set; }
        public string UltimaTx { get; set; }
        public string RepLegal { get; set; }
    }
}
