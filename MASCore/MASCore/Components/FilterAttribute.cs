﻿#region Assembly System.Web.Http, Version=5.2.7.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35
// C:\Users\Equipo\.nuget\packages\microsoft.aspnet.webapi.core\5.2.7\lib\net45\System.Web.Http.dll
#endregion

namespace System.Web.Http.Filters
{
    //
    // Summary:
    //     Represents the base class for action-filter attributes.
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, Inherited = true, AllowMultiple = true)]
    public abstract class FilterAttribute : Attribute, IFilter
    {
        //
        // Summary:
        //     Initializes a new instance of the System.Web.Http.Filters.FilterAttribute class.
        //protected FilterAttribute();

        //
        // Summary:
        //     Gets a value that indicates whether multiple filters are allowed.
        //
        // Returns:
        //     true if multiple filters are allowed; otherwise, false.
        public virtual bool AllowMultiple { get; }
    }
}