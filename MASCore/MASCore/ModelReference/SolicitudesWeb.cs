﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;

namespace MASCore.ModelReference
{
    public class SolicitudesWeb
    {
    }

    public partial class SPMasBuscaInformacionDeEmbarcacionExacta_Result
    {
        public string Matricula { get; set; }
        public string Nombre { get; set; }
        public string SerieAntena { get; set; }
        public string Localidad { get; set; }
        public string RNP { get; set; }
        public string Permiso { get; set; }
        public string TipoPermiso { get; set; }
        public string RazonSocial { get; set; }
        public string RepLegal { get; set; }
        public string RNPTitular { get; set; }
        public string PuertoBase { get; set; }
        public string Zona { get; set; }
    }

    public partial class ReporteCedulasExportarBusqueda_Result
    {
        public int idcedula { get; set; }
        public string NoCedula { get; set; }
        public string Causas { get; set; }
        public string TipoAlerta { get; set; }
        public Nullable<System.DateTime> FechaCedula { get; set; }
        public string Latitud { get; set; }
        public string Longitud { get; set; }
        public string Velocidad { get; set; }
        public string Profundidad { get; set; }
        public string DistanciaCosta { get; set; }
        public string Zona { get; set; }
        public string Referencia { get; set; }
        public string Estatus { get; set; }
        public string UltimaTx { get; set; }
        public Nullable<System.DateTime> FechaAlerta { get; set; }
        public string HoraAlerta { get; set; }
        public string MatriculaBarco { get; set; }
        public string NombreBarco { get; set; }
        public string RNP { get; set; }
        public string TipoPermiso { get; set; }
        public string PuertoBase { get; set; }
        public string RazonSocial { get; set; }
        public string Litoral { get; set; }
        public string Domicilio { get; set; }
        public string Contacto { get; set; }
        public string Puesto { get; set; }
        public string Telefono { get; set; }
        public string Anexos { get; set; }
        public string OFP { get; set; }
        public string Turno { get; set; }
        public string NombreContactada { get; set; }
        public string PuestoContactada { get; set; }
        public string TelefonoContactada { get; set; }
        public int CedulaLlamada { get; set; }
        public string FechaLlamada { get; set; }
        public string Estado { get; set; }
    }















}
