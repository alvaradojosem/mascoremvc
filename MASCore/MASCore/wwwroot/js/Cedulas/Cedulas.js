﻿var now = new Date();
var ArrayItems = [];

//var URLIni = 'https://10.215.71.239:44350/'; // Net Core 
var urlFuncGenerico = 'api/FunProc/LlenarResultadoGenerico';

$(document).ready(function () {
    //Calendarios.obj(CtrlDiv)
    Fechainicial= Controles.Calendarios.obj("#FechaInicial");
    Controles.Calendarios.obj("#FechaFinal");
   
    Controles.tooltips.obj("#tooltip1", "#FechaInicial");
    Controles.tooltips.obj("#tooltip2", "#FechaFinal");
    Controles.tooltips.obj("#tooltip3", "#searchRows");
    Controles.tooltips.obj("#tooltip4", "#exportToExcel");
    Controles.tooltips.obj("#tooltip5", "#exportToPDF");
   

    Controles.SelectBox.obj("api/LoadControls/GetAlarmas", "#TipoCedula", "--Seleccione un Tipo de Cedula--");
    Controles.SelectBox.obj("api/LoadControls/GetCausasAlarmas", "#TipoEmergencia", "-- Seleccione una Causa de la Emergencia  --");
    Controles.SelectBox.obj("api/LoadControls/GetLitorales", "#litorales", "-- Seleccione un Litoral--");

    Controles.CajaDeTexto.obj(urlFuncGenerico, "#numerocedula", "Numero de Cedula", "nocedula");
    Controles.CajaDeTexto.obj(urlFuncGenerico, "#responsableofp", "Responsable", "responsableofp");
    Controles.CajaDeTexto.obj(urlFuncGenerico, "#tipopermiso", "Tipo de Permiso", "tipopermiso");
    Controles.CajaDeTexto.obj(urlFuncGenerico, "#nombredelbarco", "Nombre del Barco", "nombrebarco");
    Controles.CajaDeTexto.obj(urlFuncGenerico, "#rnpdelbarco", "RNP del Barco", "rnpdelbarco");
    Controles.CajaDeTexto.obj(urlFuncGenerico, "#razonsocial", "Razón Social","razonsocial","");
    Controles.CajaDeTexto.obj(urlFuncGenerico, "#puertobase", "Puerto Base", "puertobase");

   

    $("#searchRows").dxButton({
        width: 50,
        type: "default",
        icon: "search" ,     
        onClick: function (e) {
            var result = e.validationGroup.validate();
            if (result.isValid) {
                getJson();
                DevExpress.ui.notify("Informacion Genrada Correctamente.", "Success");
            } else {
                DevExpress.ui.notify("Favor de ingresar los filtros del reporte.", "error");
            }
        }
    });


   

});

function getJson() {
    var fi = new Date($("#FechaInicial").val()),
        ff = new Date( $("#FechaFinal").val()),
        fc='';//fecha Compuesta

    var yy = now.getFullYear(),
        mm = (now.getMonth() + 1),
        dd = now.getDate();

    if (mm <= 9) { mm = '0'+(now.getMonth() + 1)}
    fc = mm + '/' + dd  + '/' + yy;


    if (fi === null || fi === '' || fi ==='Invalid Date') { fi = fc; } else { fi = fi.getMonth() + 1 + '-' + fi.getDate() + '-' + fi.getFullYear();}
    if (ff === null || ff === '' || ff ==='Invalid Date') { ff = fc; } else { ff = ff.getMonth() + 1 + '-' + ff.getDate() + '-' + ff.getFullYear(); }
    if (ff === 'NaN-NaN-NaN' ) { ff = fc; }
    if (fi === 'NaN-NaN-NaN') { fi = fc; }

    var responsable = $("#responsableofp").val(),
        numerodecedula = $("#numerocedula").val(),
        tipopermiso = $("#tipopermiso").val(),
        nombredelbarco = $("#nombredelbarco").val(),
        rnpdelbarco = $("#rnpdelbarco").val(),
        razonsocial = $("#razonsocial").val(),
        puertobase = $("#puertobase").val(),
        litorales = $("#litorales").val();


    var ObjVariables = {
        "fechaInicial": fi.toString().replace('/','-'),
        "fechaFinal": ff.toString().replace('/', '-'),
        "numerodecedula": numerodecedula,
        "responsable": responsable,
        "tipopermiso": tipopermiso,
        "nombredelbarco": nombredelbarco,
        "rnpdelbarco": rnpdelbarco,
        "razonsocial": razonsocial,
        "puertobase": puertobase,
        "alarmas": "",
        "causas": "",
        "litorales": litorales
    };

    ArrayItems = Controles.POSTGridCedulas.obj("api/Cedula/BuscaCedulas", ObjVariables);
    
    GridCedulas();
}



function GridCedulas() {

    $("#exportToExcel").dxButton({
        width: 50,
        type: "success",
        icon: "fa fa-file-excel"

    });

    $("#exportToPDF").dxButton({
        width: 50,
        type: "danger",
        icon: "fa fa-file-pdf"

    });
   
   
    $("#gridContainer").dxDataGrid({
        dataSource: ArrayItems,
        showBorders: true,
        ColumnHidingEnabled: true,
        ShowColumnLines: true,
        ShowRowLines: true,
        RowAlternationEnabled: true,
        ShowBorders: true,


        paging: {
            pageSize: 5,
            ShowColumHeader: true,
            WordWrapEnabled:true
        },
        pager: {
            showPageSizeSelector: true,
            allowedPageSizes: [5, 8, 15, 30],
            showInfo: true,
            ShowNavigationButtons: true,
            ShowPageSizeSelector: true,
            visible:true
        },
        columns: [ {
            caption: "Número de cédula",
            dataField: "noCedula",
            width: 150
        }, {
                caption: "Causa",
                dataField: "Causa",
                width: 200
        }, {
                caption: "Tipo de alerta",
                dataField: "tipoAlerta",
                 width: 200
        }, {
           caption: "Fecha cédula",
                dataField: "fechaCedula",
            dataType: "date",
                format: "dd-MM-yyyy",
            width: 100
            }, {
                caption: "Fecha alerta",
                dataField: "fechaAlerta",
                dataType: "date",
                format: "dd-MM-yyyy", 
                width: 100
            },{
                caption: "Nombre barco",
                dataField: "nombreBarco" 
            }, {
                caption: "RNP",
                dataField: "rnp"  
            }, {
                caption: "Tipo permiso",
                dataField: "tipoPermiso"
            }
            , {
                caption: "Razón Social",
                dataField: "razonSocial"
            }
            , {
                caption: "Acción",
                dataField: "null"
            }


        ]

    });



}
    