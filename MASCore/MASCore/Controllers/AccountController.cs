﻿using System;
using Microsoft.AspNetCore.Mvc;
using MASCore.Models;
using MASCore.Components;
using System.Text;
using System.Security.Cryptography;
using MASCore.ModelReference;
using System.Diagnostics;

namespace MASCore.Controllers
{
    public class AccountController : Controller
    {

        #region "ATRIBUTOS"
        private Byte[] _salt;
        #endregion
        //public System.Security.Claims.ClaimsPrincipal User { get; }
        //[HttpPost("Login")]
        //[AllowAnonymous]
        //[ProducesResponseType(204)]
        //[ProducesResponseType(400)]
        //public IActionResult Login()
        //{
        //    return PartialView("~/Views/Shared/_LoginPartialNew.cshtml");
        //}

        public IActionResult PageLogin()
        {
            return PartialView("~/Views/Shared/_LoginPartialNew.cshtml");
        }

        public IActionResult Login()
        {

            return View("~/Views/Home/Home.cshtml");
        }

        //    [HttpGet]
        //    [RequiredAuthentication(Check = false)]
        //    public ActionResult LogIn()
        //    {

        //        if (Request.IsAuthenticated)
        //        {

        //            //Si ve la pantalla de login y se encuentra autenticado, lo mando a la pantalla de bienvenida !!!
        //            return RedirectToAction("Index", "Home");
        //        }

        //        return View();
        //    }

        //    [HttpPost]
        //    [RequiredAuthentication(Check = false)]
        //    public ActionResult LogIn(Models.UserModel User)
        //    {
        //        if (ModelState.IsValid)
        //        {
        //            if (EsValido(User.NombreCorto, User.Contraseña))
        //            {
        //                if (EsValidoMAS(User.NombreCorto))
        //                {
        //                    //                        TiempodeSesionHoras
        //                    string ConfigTiempoSessionHoras = ConfigurationManager.ConnectionStrings["TiempodeSesionHoras"].ConnectionString;
        //                    int TiempoSessionHoras = 0; // Convert.ToInt32(ConfigTiempoSessionHoras);
        //                    int.TryParse(ConfigTiempoSessionHoras, out TiempoSessionHoras);

        //                    Response.Cookies["userName"].Value = User.NombreCorto;
        //                    Response.Cookies["userName"].Expires = DateTime.Now.AddHours(TiempoSessionHoras);
        //                    return RedirectToAction("Index", "Home");
        //                }
        //                else
        //                {
        //                    Response.Cookies["userName"].Value = "";
        //                    Response.Cookies["userName"].Expires = DateTime.Now.AddSeconds(1);
        //                    ModelState.AddModelError("", "El usuario no tiene permisos para acceder al sistema.");
        //                }
        //            }
        //            else
        //            {
        //                Response.Cookies["userName"].Value = "";
        //                Response.Cookies["userName"].Expires = DateTime.Now.AddSeconds(1);
        //                ModelState.AddModelError("", "Los datos son Incorrectos.");
        //            }
        //        }
        //        return View();
        //    }


        //    private bool EsValido(String username, String password)
        //    {
        //        Boolean isPasswordCorrect;
        //        String passwdFromDB;
        //        //String password;
        //        Int32 passwordFormat = 0;
        //        String salt = "";
        //        GetPasswordWithFormat(username, out passwdFromDB, out passwordFormat, out salt);


        //        String encodedPasswd = EncodePassword(password, passwordFormat, salt);

        //        isPasswordCorrect = passwdFromDB.Equals(encodedPasswd);

        //        if (isPasswordCorrect)
        //        {
        //            return true;
        //        }
        //        else
        //        {
        //            return false;
        //        }
        //    }

        //    private void GetPasswordWithFormat(String username, out String password, out Int32 passwordFormat, out String passwordSalt)
        //    {
        //        password = "";
        //        passwordFormat = 0;
        //        passwordSalt = "";
        //        try
        //        {
        //            FuncionesProcedimientos FunProc = new FuncionesProcedimientos();
        //            GetPasswordSISMEP_Result DatosDeUsuario = FunProc.BuscaDatosDeUsuario(username);
        //            if (DatosDeUsuario != null)
        //            {
        //                password = DatosDeUsuario.Password;
        //                passwordFormat = DatosDeUsuario.PasswordFormat;
        //                passwordSalt = DatosDeUsuario.PasswordSalt;
        //            }


        //        }
        //        catch (Exception ex)
        //        {
        //            Debug.Write("GetPasswordWithFormat:" + ex.Message);
        //            throw ex;
        //        }
        //    }

        //    internal String EncodePassword(String pass, Int32 passwordFormat, String salt)
        //    {
        //        if (passwordFormat == 0) // MembershipPasswordFormat.Clear
        //            return pass;
        //        Byte[] bIn = Encoding.Unicode.GetBytes(pass);
        //        _salt = Convert.FromBase64String(salt);
        //        Byte[] bAll = new Byte[_salt.Length + bIn.Length];
        //        Byte[] bRet = null;

        //        Buffer.BlockCopy(_salt, 0, bAll, 0, _salt.Length);
        //        Buffer.BlockCopy(bIn, 0, bAll, _salt.Length, bIn.Length);
        //        if (passwordFormat == 1)
        //        { // MembershipPasswordFormat.Hashed
        //            HashAlgorithm s = HashAlgorithm.Create(Membership.HashAlgorithmType);
        //            bRet = s.ComputeHash(bAll);
        //        }
        //        else
        //            bRet = EncryptPassword(bAll);
        //        //bRet = EncryptPassword(Encoding.Unicode.GetBytes(pass));
        //        return Convert.ToBase64String(bRet);
        //    }

        //    private Byte[] EncryptPassword(Byte[] password)
        //    {
        //        TripleDESCryptoServiceProvider tdes = new TripleDESCryptoServiceProvider();
        //        tdes.Key = _salt;
        //        tdes.Mode = CipherMode.ECB;
        //        tdes.Padding = PaddingMode.PKCS7;

        //        ICryptoTransform cTransform = tdes.CreateEncryptor();
        //        //transform the specified region of bytes array to resultArray
        //        byte[] resultArray =
        //          cTransform.TransformFinalBlock(password, 0,
        //          password.Length);
        //        //Release resources held by TripleDes Encryptor
        //        tdes.Clear();
        //        return resultArray;
        //        //Return the encrypted data into unreadable string format
        //        //return Convert.ToBase64String(resultArray, 0, resultArray.Length);
        //    }

        //    private bool IsValid(string NombreCorto, string Contraseña)
        //    {
        //        var crypto = new SimpleCrypto.PBKDF2();
        //        bool isValid = false;

        //        using (var db = new BDMasContextEnt())
        //        {
        //            var user = db.TMasUsuarios.FirstOrDefault(u => u.NombreCorto == NombreCorto);
        //            if (user != null)
        //            {
        //                if (user.Contraseña == Contraseña)
        //                {
        //                    isValid = true;
        //                }
        //            }
        //        }
        //        return isValid;
        //    }

        //}

    }
}
