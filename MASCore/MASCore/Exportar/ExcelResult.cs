﻿using ClosedXML.Excel;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Options;
using System.IO;


namespace MASCore.Exportar
{
    public class ExcelResult : ActionResult
    {
        private readonly XLWorkbook _workbook;
        private readonly string _fileName;

        public void ExecuteResult(ControllerContext context)
        {
            var response = context.HttpContext.Response;
            response.Clear();
            response.ContentType = "application/vnd.openxmlformats-officedocument."
                                 + "spreadsheetml.sheet";
            response.Headers.Add("content-disposition",
                               "attachment;filename=\"" + _fileName + ".xlsx\"");



            //using (var memoryStream = new MemoryStream())
            //{
            //    _workbook.SaveAs(memoryStream);

            //    memoryStream.WriteTo(response.OutputStream);
            //}

            //HttpContext.Response.end();

        }


        public ExcelResult(XLWorkbook workbook, string fileName)
        {
            _workbook = workbook;
            _fileName = fileName;
        }

      
    
    }
}