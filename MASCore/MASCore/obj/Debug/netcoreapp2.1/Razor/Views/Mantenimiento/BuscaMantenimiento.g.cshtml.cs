#pragma checksum "C:\Users\Equipo\Documents\00-Proyectos 2019\MASCore\MASCore\Views\Mantenimiento\BuscaMantenimiento.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "8dd2afb5ccca373b1dc812590163e08b0577c2d5"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Mantenimiento_BuscaMantenimiento), @"mvc.1.0.view", @"/Views/Mantenimiento/BuscaMantenimiento.cshtml")]
[assembly:global::Microsoft.AspNetCore.Mvc.Razor.Compilation.RazorViewAttribute(@"/Views/Mantenimiento/BuscaMantenimiento.cshtml", typeof(AspNetCore.Views_Mantenimiento_BuscaMantenimiento))]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#line 1 "C:\Users\Equipo\Documents\00-Proyectos 2019\MASCore\MASCore\Views\_ViewImports.cshtml"
using MASCore;

#line default
#line hidden
#line 3 "C:\Users\Equipo\Documents\00-Proyectos 2019\MASCore\MASCore\Views\Mantenimiento\BuscaMantenimiento.cshtml"
using MASCore.Models;

#line default
#line hidden
#line 4 "C:\Users\Equipo\Documents\00-Proyectos 2019\MASCore\MASCore\Views\_ViewImports.cshtml"
using DevExtreme.AspNet.Mvc;

#line default
#line hidden
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"8dd2afb5ccca373b1dc812590163e08b0577c2d5", @"/Views/Mantenimiento/BuscaMantenimiento.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"a6f9e97209bd19ae69cd611cb55c19dae5d6d315", @"/Views/_ViewImports.cshtml")]
    public class Views_Mantenimiento_BuscaMantenimiento : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<dynamic>
    {
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_0 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("src", new global::Microsoft.AspNetCore.Html.HtmlString("~/js/Mantenimiento/BuscaMtto.js"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        #line hidden
        #pragma warning disable 0169
        private string __tagHelperStringValueBuffer;
        #pragma warning restore 0169
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperExecutionContext __tagHelperExecutionContext;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner __tagHelperRunner = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner();
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __backed__tagHelperScopeManager = null;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __tagHelperScopeManager
        {
            get
            {
                if (__backed__tagHelperScopeManager == null)
                {
                    __backed__tagHelperScopeManager = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager(StartTagHelperWritingScope, EndTagHelperWritingScope);
                }
                return __backed__tagHelperScopeManager;
            }
        }
        private global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.HeadTagHelper __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_HeadTagHelper;
        private global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper;
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            BeginContext(0, 4, true);
            WriteLiteral("\r\n\r\n");
            EndContext();
            BeginContext(27, 2, true);
            WriteLiteral("\r\n");
            EndContext();
#line 5 "C:\Users\Equipo\Documents\00-Proyectos 2019\MASCore\MASCore\Views\Mantenimiento\BuscaMantenimiento.cshtml"
  
    ViewBag.Title = "Busca Mantenimientos";


#line default
#line hidden
            BeginContext(83, 71, true);
            WriteLiteral("\r\n\r\n\r\n<div style=\"text-align:center; color:Red;font-size:medium\">\r\n    ");
            EndContext();
            BeginContext(155, 19, false);
#line 13 "C:\Users\Equipo\Documents\00-Proyectos 2019\MASCore\MASCore\Views\Mantenimiento\BuscaMantenimiento.cshtml"
Write(ViewBag.NoValidoPor);

#line default
#line hidden
            EndContext();
            BeginContext(174, 12, true);
            WriteLiteral("\r\n</div>\r\n\r\n");
            EndContext();
            BeginContext(186, 79, false);
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("head", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "376068c791f7499e8a1c2c6af5b27330", async() => {
                BeginContext(192, 9, true);
                WriteLiteral("\r\n \r\n    ");
                EndContext();
                BeginContext(201, 55, false);
                __tagHelperExecutionContext = __tagHelperScopeManager.Begin("script", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "9029b2ba7fa24bb8b31cba0547274d0c", async() => {
                }
                );
                __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper>();
                __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper);
                __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_0);
                await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
                if (!__tagHelperExecutionContext.Output.IsContentModified)
                {
                    await __tagHelperExecutionContext.SetOutputContentAsync();
                }
                Write(__tagHelperExecutionContext.Output);
                __tagHelperExecutionContext = __tagHelperScopeManager.End();
                EndContext();
                BeginContext(256, 2, true);
                WriteLiteral("\r\n");
                EndContext();
            }
            );
            __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_HeadTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.HeadTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_Razor_TagHelpers_HeadTagHelper);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            EndContext();
            BeginContext(265, 4, true);
            WriteLiteral("\r\n\r\n");
            EndContext();
            BeginContext(270, 28, false);
#line 21 "C:\Users\Equipo\Documents\00-Proyectos 2019\MASCore\MASCore\Views\Mantenimiento\BuscaMantenimiento.cshtml"
Write(Html.ValidationSummary(true));

#line default
#line hidden
            EndContext();
            BeginContext(298, 5163, true);
            WriteLiteral(@"
<article>
    <h1>Buscar Mantenimiento</h1>
</article>

<div id=""tabs"">

    <div class=""tabs-container""></div>
    <div class=""content  dx-fieldset"">
        <div id=""consulta"">
            <div>
                <div class=""col-md-12 col-sm-12 col-xs-12"">
                    <div class=""x_panel"">
                        <div class=""x_title"">
                            <h4>Criterios de filtrado</h4>
                            <div class=""clearfix""></div>
                        </div>
                        <div class=""x_content"">
                            <div class=""row"">

                                <table style=""width: 100%; margin-left: 15px; margin-right: 15px;"">
                                    <tr style=""height: 40px;"">
                                        <td style=""text-align: left;"">
                                            <div id=""DivSwitch"" style=""font-weight: normal; padding-left: 5px; vertical-align: middle;""></div>
                                   ");
            WriteLiteral(@"         <div style=""font-weight: normal; padding-left: 5px; vertical-align: middle;"">Rango de Fechas</div>

                                        </td>
                                        <td style=""width: 50px; text-align: center;"">
                                            <label id=""lblDel"" style=""font-weight: normal;"">Del:</label>
                                        </td>
                                        <td>
                                            <div id=""FechaInicial""></div>

                                        </td>
                                        <td style=""width: 50px; text-align: center;"">
                                            <label id=""lblAl"" style=""font-weight: normal;"">Al:</label>
                                        </td>
                                        <td>
                                            <div id=""FechaFinal""></div>

                                        </td>
                                    </tr>
       ");
            WriteLiteral(@"                         </table>
                                <table style=""width: 100%; margin-left: 15px; margin-right: 15px; margin-top: 10px;"">
                                    <tr style=""height: 30px;"">
                                        <td style=""width: 100%; text-align: left; vertical-align: bottom;"" colspan=""2"">
                                            <label id=""lblDatosBarco"" style=""font-weight: normal;"">Datos del barco:</label>
                                        </td>
                                    </tr>
                                    <tr style=""height: 40px;"">
                                        <td style=""width: 50%; padding-right: 10px;"">
                                            <div id=""Barco""></div>


                                        </td>
                                        <td style=""width: 50%; padding-left: 10px;"">
                                            <div id=""RazonSocial""></div>


                                    ");
            WriteLiteral(@"    </td>
                                    </tr>
                                </table>
                                <table style=""width: 100%; margin-left: 15px; margin-right: 15px; margin-top: 10px; margin-bottom: 10px;"">
                                    <tr style=""height: 30px;"">
                                        <td style=""width: 100%; text-align: left; vertical-align: bottom;"" colspan=""2"">
                                            <label id=""lblDatosMantenimiento"" style=""font-weight: normal;"">Datos del mantenimiento:</label>
                                        </td>
                                    </tr>
                                    <tr style=""height: 40px;"">
                                        <td style=""width: 50%; padding-right: 10px;"">
                                            <div id=""Servicio""></div>

                                        </td>
                                        <td style=""width: 50%; padding-left: 10px;"">
               ");
            WriteLiteral(@"                             <div id=""NumeroFolio""></div>

                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <br />
            <table style=""width: calc(100% - 60px); margin-left: 35px;"">
                <tr style=""height: 40px;"">
                    <td style=""width: 100%; text-align: right; padding-top: 10px;"">

                        <button type=""submit"" name=""submitButton"" class=""btn-primary form-control"" id=""Buscar"" value=""Buscar"" style=""width:60px; margin-left:-15px;"" data-toggle=""tooltip"" data-placement=""top"" title=""Buscar Mantenimiento"">
                            <i class=""fas fa-search"" style=""width:24px""></i>
                        </button>
                    </td>
                </tr>
            </table>
            <br />
            <section>");
            WriteLiteral("\n                <div id=\"DivGrid\"></div>\r\n");
            EndContext();
            BeginContext(8380, 7403, true);
            WriteLiteral(@"            </section>
        </div>
        <div id=""resumen"">
            <div>
                <div class=""col-md-12 col-sm-12 col-xs-12"">
                    <div class=""x_panel"">
                        <div class=""x_title"">
                            <h4>Seleccione los campos y el total a mostrar</h4>
                            <div class=""clearfix""></div>
                        </div>
                        <div class=""x_content"">
                            <div class=""row"">
                                <table style=""width: 100%;"">
                                    <tr style=""height: 40px;"">
                                        <td style=""width:60%;color:#000; vertical-align:middle; text-align: center;"">
                                            <label id=""lblCampos"" style=""font-weight: normal;"">Campos</label>
                                        </td>
                                        <td style=""width:40%; vertical-align:middle; text-align: center;"">
        ");
            WriteLiteral(@"                                    <label id=""lblSumarizacion"" style=""font-weight: normal;"">Sumarización</label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style=""width:60%;border-top-style: solid; border-top-color: black; border-top-width: thin;"">
                                            <div class=""row"">
                                                <div class=""col-6"">
                                                    <div id=""Switch1"" class=""col-5"" style=""font-weight: normal; padding-left: 5px; vertical-align: middle;""></div>
                                                    <label style=""font-weight: normal; padding-left: 5px;"">Servicio</label>


                                                </div>
                                                <div class=""col-6"">
                                                    <div id=""Switch2"" class=""col-5"" style");
            WriteLiteral(@"=""font-weight: normal; padding-left: 5px; vertical-align: middle;""></div>
                                                    <label style=""font-weight: normal; padding-left: 5px;"">Puerto Base</label>
                                                </div>
                                            </div>
                                            <div class=""row"">
                                                <div class=""col-6"">
                                                    <div id=""Switch3"" class=""col-5"" style=""font-weight: normal; padding-left: 5px; vertical-align: middle;""></div>
                                                    <label style=""font-weight: normal; padding-left: 5px;"">Raz&oacute;n Social</label>
                                                </div>
                                                <div class=""col-6"">
                                                    <div id=""Switch4"" class=""col-5"" style=""font-weight: normal; padding-left: 5px; vertical-align: middle;"">");
            WriteLiteral(@"</div>
                                                    <label style=""font-weight: normal; padding-left: 5px;"">Supervisor</label>
                                                </div>
                                            </div>
                                            <div class=""row"">
                                                <div class=""col-6"">
                                                    <div id=""Switch5"" class=""col-5"" style=""font-weight: normal; padding-left: 5px; vertical-align: middle;""></div>
                                                    <label style=""font-weight: normal; padding-left: 5px;"">Zona</label>
                                                </div>
                                                <div class=""col-6"">
                                                    <div id=""Switch6"" class=""col-5"" style=""font-weight: normal; padding-left: 5px; vertical-align: middle;""></div>
                                                    <label style=""font-weig");
            WriteLiteral(@"ht: normal; padding-left: 5px;"">Barco</label>
                                                </div>
                                            </div>
                                        </td>
                                        <td style=""width: 40%; border-left-style: solid; border-left-color: black; border-left-width: thin; border-top-style: solid; border-top-color: black; border-top-width: thin;"">
                                            <div id=""divTotal"" style=""margin-left: 20px; margin-top: 20px;"">
                                                <div id=""Switch7"" class=""col-5"" style=""font-weight: normal; padding-left: 5px; vertical-align: middle;""></div>
                                                <label style=""font-weight: normal; padding-left: 5px;"">Contar Barcos</label>
                                                <br />
                                                <div id=""Switch8"" class=""col-5"" style=""font-weight: normal; padding-left: 5px; vertical-align: middle;""><");
            WriteLiteral(@"/div>
                                                <label style=""font-weight: normal; padding-left: 5px;"">Contar Servicios</label>
                                                <br />
                                                <div id=""Switch9"" class=""col-5"" style=""font-weight: normal; padding-left: 5px; vertical-align: middle;""></div>
                                                <label style=""font-weight: normal; padding-left: 5px;"">Contar Ambos</label>
                                                <br />
                                                <br />
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <br />
            <table style=""width: calc(100% - 30px); margin-left: 15px;"">
                <");
            WriteLiteral(@"tr style=""height: 40px;"">
                    <td style=""width: 100%; text-align: right; padding-top: 10px;"">


                        <button type=""submit"" name=""submitButton"" class=""btn-primary form-control"" id=""BuscarResumen"" value=""Buscar"" style=""width:60px; margin-left:-15px;"" data-toggle=""tooltip"" data-placement=""top"" title=""Buscar Resumen"">
                            <i class=""fas fa-search"" style=""width:24px""></i>
                        </button>
                    </td>
                </tr>
            </table>
            <br />
            <section></section>
        </div>


    </div>
    <div id=""negacion"">
        <div>
            <div class=""col-md-12 col-sm-12 col-xs-12"" style=""margin-bottom: 20px;"">
                <div class=""x_panel"">
                    <div class=""x_title"">
                        <div>Los datos aquí mostrados representan las embarcaciones que no cumplieron con los criterios de la consulta ejecutada.</div>
                        <div class=""c");
            WriteLiteral("learfix\"></div>\r\n                    </div>\r\n                    <div class=\"x_content\">\r\n                        <div class=\"row\">\r\n                            <section>\r\n                                <div id=\"GridNegacion\"></div>\r\n");
            EndContext();
            BeginContext(18932, 196, true);
            WriteLiteral("                            </section>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </div>\r\n    </div>\r\n\r\n\r\n\r\n</div>\r\n\r\n\r\n\r\n\r\n\r\n");
            EndContext();
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<dynamic> Html { get; private set; }
    }
}
#pragma warning restore 1591
