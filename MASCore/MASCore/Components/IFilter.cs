﻿#region Assembly System.Web.Http, Version=5.2.7.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35
// C:\Users\Equipo\.nuget\packages\microsoft.aspnet.webapi.core\5.2.7\lib\net45\System.Web.Http.dll
#endregion

namespace System.Web.Http.Filters
{
    //
    // Summary:
    //     Defines the methods that are used in a filter.
    public interface IFilter
    {
        //
        // Summary:
        //     Gets or sets a value indicating whether more than one instance of the indicated
        //     attribute can be specified for a single program element.
        //
        // Returns:
        //     true if more than one instance is allowed to be specified; otherwise, false.
        //     The default is false.
        bool AllowMultiple { get; }
    }
}