﻿using MASCore.ModelReference;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Linq;

namespace MASCore.Models
{
    public class LlamadaBusquedaModel
    {
        //private BDMasContextEnt db = new BDMasContextEnt();

        public string Actividad { get; set; }

        public string Ofp { get; set; }

        public string Embarcacion { get; set; }

        public string Filtro { get; set; }

        [DataType(DataType.DateTime)]
        public DateTime FechaInicial { get; set; }

        [DataType(DataType.DateTime)]
        [Required(ErrorMessage = "Si no selecciona, toma la fecha actual.")]
        public DateTime FechaFinal { get; set; }

        public List<BuscaLlamadas_Result> ResultadoListaLlamadasSP { get; set; }

        public List<BuscaLlamadas_Result> EncuentraLasLlamadasSP(string Filtro, DateTime FechIni, DateTime FechFin, global::System.String actividad, string barco, string ofp)
        {
            string FechaFormateadaIni = FechIni.ToString("d", CultureInfo.CreateSpecificCulture("en-US"));
            string FechaFormateadaFin = FechFin.ToString("d", CultureInfo.CreateSpecificCulture("en-US"));
            if (barco != null) barco = barco.Replace("'", "");
            if (Filtro == null)
            {
                Filtro = "";
            };
            return null;// db.BuscaLlamadas(Filtro, FechaFormateadaIni, FechaFormateadaFin, actividad, barco, ofp).ToList();
        }
    }

    public class LlamadaModel
    {
        //private BDMasContextEnt db = new BDMasContextEnt();
        public Boolean MostrandoBusquedaInformacionEmbarcacion { get; set; }
        public string Identificador { get; set; }
        public TMasBTBitaCel TMASBTBITACEL { get; set; }
        public TMasBTControlTelefonos TMASBTCONTROLTELEFONOS { get; set; }
        public List<TMasBTControlTelefonos> TMASBTCONTROLTELEFONOS_LISTA { get; set; }
        public List<SPMASBuscaInformacionDeEmbarcacionyPangas_Result> InformacionDeEmbarcacion { get; set; }

        public List<BuscaInformacionDeEmbarcacion_Result> EncuentraInformacionDeEmbarcacionSP(string Opcion, string Valor)
        {
            if (Valor != null) Valor = Valor.Replace("'", "");
            return null;// db.BuscaInformacionDeEmbarcacion(Opcion, Valor).ToList();
        }

        public List<SPMASBuscaInformacionDeEmbarcacionyPangas_Result> EncuentraInformacionDeEmbarcacionPangasSP(string Opcion, string Valor)
        {
            if (Valor != null) Valor = Valor.Replace("'", "");
            return null;//db.SPMASBuscaInformacionDeEmbarcacionyPangas(Opcion, Valor).ToList();
        }

        public int ActualizaBTBitaCel(TMasBTBitaCel FinalTMasBTBitaCel_)
        {
            int ResultadoInsert = 0;

            BTBitaCelActualizar_Result DatosDeBtBitaCelActualizados = null;// db.BTBitaCelActualizar(FinalTMasBTBitaCel_.IdBitaCel, FinalTMasBTBitaCel_.Actividad, FinalTMasBTBitaCel_.Barco, FinalTMasBTBitaCel_.RNP, FinalTMasBTBitaCel_.Matricula, FinalTMasBTBitaCel_.RazonSocial, FinalTMasBTBitaCel_.PuertoBase, FinalTMasBTBitaCel_.Fecha, FinalTMasBTBitaCel_.Empresa, FinalTMasBTBitaCel_.Atendio, FinalTMasBTBitaCel_.Lugar, FinalTMasBTBitaCel_.Descripcion, FinalTMasBTBitaCel_.OFP, FinalTMasBTBitaCel_.Respuesta).FirstOrDefault();

            if (DatosDeBtBitaCelActualizados != null)
            {
                ResultadoInsert = DatosDeBtBitaCelActualizados.IdBitaCel;
            }

            return ResultadoInsert;
        }

        public int InsertaBTBitaCel(TMasBTBitaCel FinalTMasBTBitaCel_)
        {
            int ResultadoInsert = 0;

            BTBitaCelInsertar_Result DatosDeBtBitaCelInsertada = null;//db.BTBitaCelInsertar(FinalTMasBTBitaCel_.Actividad, FinalTMasBTBitaCel_.Barco, FinalTMasBTBitaCel_.RNP, FinalTMasBTBitaCel_.Matricula, FinalTMasBTBitaCel_.RazonSocial, FinalTMasBTBitaCel_.PuertoBase, FinalTMasBTBitaCel_.Fecha, FinalTMasBTBitaCel_.Empresa, FinalTMasBTBitaCel_.Atendio, FinalTMasBTBitaCel_.Lugar, FinalTMasBTBitaCel_.Descripcion, FinalTMasBTBitaCel_.OFP, FinalTMasBTBitaCel_.Respuesta).FirstOrDefault();

            if (DatosDeBtBitaCelInsertada != null)
            {
                ResultadoInsert = DatosDeBtBitaCelInsertada.IdBitaCel;
            }

            return ResultadoInsert;
        }

        public int InsertaLlamada(int IdBTBitacelFinal, int IdTemporal)
        {
            int ResultadoInsert = 0;
            LlamadasInsertar_Result DatosDeLlamadaInsertada = null;// db.LlamadasInsertar(IdBTBitacelFinal, IdTemporal).FirstOrDefault();

            if (DatosDeLlamadaInsertada != null)
            {
                ResultadoInsert = Convert.ToInt32(DatosDeLlamadaInsertada.Cantidad);
            }

            return ResultadoInsert;
        }

        public int InsertaLlamadaTmp(TMasBTControlTelefonos FinalTMasBTControlTelefonos_)
        {
            int ResultadoInsert = 0;

            LlamadasInsertarTMP_Result DatosDeLlamadaTmpInsertada = null;// db.LlamadasInsertarTMP(FinalTMasBTControlTelefonos_.IdBitaCel, FinalTMasBTControlTelefonos_.IdControl,
                //FinalTMasBTControlTelefonos_.Fecha, FinalTMasBTControlTelefonos_.Hora, FinalTMasBTControlTelefonos_.Tipo, FinalTMasBTControlTelefonos_.Numero, FinalTMasBTControlTelefonos_.Respuesta).FirstOrDefault();

            if (DatosDeLlamadaTmpInsertada != null)
            {
                ResultadoInsert = 0;// DatosDeLlamadaTmpInsertada.IdBitaCel;
            }

            return ResultadoInsert;
        }

        public int InsertaLlamadaEnEditar(TMasBTControlTelefonos FinalTMasBTControlTelefonos_)
        {
            int ResultadoInsert = 0;

            LlamadasInsertarEnEditar_Result DatosDeLlamadaEnEditarInsertada = null;// db.LlamadasInsertarEnEditar(FinalTMasBTControlTelefonos_.IdBitaCel, FinalTMasBTControlTelefonos_.IdControl,
                //FinalTMasBTControlTelefonos_.Fecha, FinalTMasBTControlTelefonos_.Hora, FinalTMasBTControlTelefonos_.Tipo, FinalTMasBTControlTelefonos_.Numero, FinalTMasBTControlTelefonos_.Respuesta).FirstOrDefault();

            if (DatosDeLlamadaEnEditarInsertada != null)
            {
                ResultadoInsert = 1;
            }

            return ResultadoInsert;
        }

        public int BorraLlamadaEnEditar(TMasBTControlTelefonos FinalTMasBTControlTelefonos_)
        {
            int ResultadoInsert = 0;
            LlamadasBorrarEnEditar_Result DatosDeLlamadaEnEditarBorrados = null;// db.LlamadasBorrarEnEditar(FinalTMasBTControlTelefonos_.IdBitaCel, FinalTMasBTControlTelefonos_.IdControl,
                //FinalTMasBTControlTelefonos_.Fecha, FinalTMasBTControlTelefonos_.Hora, FinalTMasBTControlTelefonos_.Tipo, FinalTMasBTControlTelefonos_.Numero).FirstOrDefault();

            if (DatosDeLlamadaEnEditarBorrados != null)
            {
                ResultadoInsert = 1;
            }

            return ResultadoInsert;
        }

        public List<TMasBTControlTelefonos> EncuentraLasLlamadasEnDBSP(int IdBitaCel)
        {
            List<TMasBTControlTelefonos> ListadeTelefonos = new List<TMasBTControlTelefonos>();
            List<BuscaLlamadasEnDB_Result> LasLlamadasEnDB = null;// db.BuscaLlamadasEnDB(IdBitaCel).ToList();

            foreach (BuscaLlamadasEnDB_Result Llamada in LasLlamadasEnDB)
            {
                TMasBTControlTelefonos TelefonoInicial = new TMasBTControlTelefonos();
                TelefonoInicial.IdBitaCel = Llamada.IdBitaCel;
                TelefonoInicial.IdControl = Llamada.IdControl;
                TelefonoInicial.Tipo = Llamada.Tipo;
                TelefonoInicial.Numero = Llamada.Numero;
                TelefonoInicial.Fecha = Llamada.Fecha;
                TelefonoInicial.Hora = Llamada.Hora;
                TelefonoInicial.Respuesta = Llamada.Respuesta;

                ListadeTelefonos.Add(TelefonoInicial);
            }

            return ListadeTelefonos;
        }

        public List<TMasBTControlTelefonos> EncuentraLasLlamadasTMPSP(TMasBTControlTelefonos FinalTMasBTControlTelefonos_)
        {
            List<TMasBTControlTelefonos> ListadeTelefonos = new List<TMasBTControlTelefonos>();
            List<BuscaLlamadasTMP_Result> LasLlamadasTMP = null;// db.BuscaLlamadasTMP(FinalTMasBTControlTelefonos_.IdBitaCel).ToList();

            foreach (BuscaLlamadasTMP_Result Llamada in LasLlamadasTMP)
            {
                TMasBTControlTelefonos TelefonoInicial = new TMasBTControlTelefonos();
                TelefonoInicial.IdBitaCel = Llamada.IdBitaCel;
                TelefonoInicial.IdControl = Llamada.IdControl;
                TelefonoInicial.Tipo = Llamada.Tipo;
                TelefonoInicial.Numero = Llamada.Numero;
                TelefonoInicial.Fecha = Llamada.Fecha;
                TelefonoInicial.Hora = Llamada.Hora;
                TelefonoInicial.Respuesta = Llamada.Respuesta;

                ListadeTelefonos.Add(TelefonoInicial);
            }

            return ListadeTelefonos;
        }

        public List<SPMasBuscaLlamadasDesdeSismep_Result> EncuentraLlamadasDesdeSismep(string rnp)
        {
            if (rnp != null) rnp = rnp.Replace("'", "");
            return null;// db.SPMasBuscaLlamadasDesdeSismep(rnp).ToList();
        }
    }

    public partial class BuscaResultadoGenerico_Result
    {
        public string Resultado { get; set; }
    }
}