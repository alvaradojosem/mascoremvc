#pragma checksum "C:\Users\Equipo\Documents\00-Proyectos 2019\MASCore\MASCore\Views\OficioSalida\Delete.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "faefae437c2c343c8012c0bdd43028074710fdd2"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_OficioSalida_Delete), @"mvc.1.0.view", @"/Views/OficioSalida/Delete.cshtml")]
[assembly:global::Microsoft.AspNetCore.Mvc.Razor.Compilation.RazorViewAttribute(@"/Views/OficioSalida/Delete.cshtml", typeof(AspNetCore.Views_OficioSalida_Delete))]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#line 1 "C:\Users\Equipo\Documents\00-Proyectos 2019\MASCore\MASCore\Views\_ViewImports.cshtml"
using MASCore;

#line default
#line hidden
#line 2 "C:\Users\Equipo\Documents\00-Proyectos 2019\MASCore\MASCore\Views\_ViewImports.cshtml"
using MASCore.Models;

#line default
#line hidden
#line 4 "C:\Users\Equipo\Documents\00-Proyectos 2019\MASCore\MASCore\Views\_ViewImports.cshtml"
using DevExtreme.AspNet.Mvc;

#line default
#line hidden
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"faefae437c2c343c8012c0bdd43028074710fdd2", @"/Views/OficioSalida/Delete.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"a6f9e97209bd19ae69cd611cb55c19dae5d6d315", @"/Views/_ViewImports.cshtml")]
    public class Views_OficioSalida_Delete : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<MASCore.DataModel.TMasOfSalida>
    {
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            BeginContext(39, 2, true);
            WriteLiteral("\r\n");
            EndContext();
#line 3 "C:\Users\Equipo\Documents\00-Proyectos 2019\MASCore\MASCore\Views\OficioSalida\Delete.cshtml"
  
    ViewBag.Title = "Delete";

#line default
#line hidden
            BeginContext(79, 160, true);
            WriteLiteral("\r\n<h2>Delete</h2>\r\n\r\n<h3>Are you sure you want to delete this?</h3>\r\n<fieldset>\r\n    <legend>TMasOfSalida</legend>\r\n\r\n    <div class=\"display-label\">\r\n         ");
            EndContext();
            BeginContext(240, 44, false);
#line 14 "C:\Users\Equipo\Documents\00-Proyectos 2019\MASCore\MASCore\Views\OficioSalida\Delete.cshtml"
    Write(Html.DisplayNameFor(model => model.NoOficio));

#line default
#line hidden
            EndContext();
            BeginContext(284, 55, true);
            WriteLiteral("\r\n    </div>\r\n    <div class=\"display-field\">\r\n        ");
            EndContext();
            BeginContext(340, 40, false);
#line 17 "C:\Users\Equipo\Documents\00-Proyectos 2019\MASCore\MASCore\Views\OficioSalida\Delete.cshtml"
   Write(Html.DisplayFor(model => model.NoOficio));

#line default
#line hidden
            EndContext();
            BeginContext(380, 58, true);
            WriteLiteral("\r\n    </div>\r\n\r\n    <div class=\"display-label\">\r\n         ");
            EndContext();
            BeginContext(439, 41, false);
#line 21 "C:\Users\Equipo\Documents\00-Proyectos 2019\MASCore\MASCore\Views\OficioSalida\Delete.cshtml"
    Write(Html.DisplayNameFor(model => model.Fecha));

#line default
#line hidden
            EndContext();
            BeginContext(480, 55, true);
            WriteLiteral("\r\n    </div>\r\n    <div class=\"display-field\">\r\n        ");
            EndContext();
            BeginContext(536, 37, false);
#line 24 "C:\Users\Equipo\Documents\00-Proyectos 2019\MASCore\MASCore\Views\OficioSalida\Delete.cshtml"
   Write(Html.DisplayFor(model => model.Fecha));

#line default
#line hidden
            EndContext();
            BeginContext(573, 58, true);
            WriteLiteral("\r\n    </div>\r\n\r\n    <div class=\"display-label\">\r\n         ");
            EndContext();
            BeginContext(632, 42, false);
#line 28 "C:\Users\Equipo\Documents\00-Proyectos 2019\MASCore\MASCore\Views\OficioSalida\Delete.cshtml"
    Write(Html.DisplayNameFor(model => model.Asunto));

#line default
#line hidden
            EndContext();
            BeginContext(674, 55, true);
            WriteLiteral("\r\n    </div>\r\n    <div class=\"display-field\">\r\n        ");
            EndContext();
            BeginContext(730, 38, false);
#line 31 "C:\Users\Equipo\Documents\00-Proyectos 2019\MASCore\MASCore\Views\OficioSalida\Delete.cshtml"
   Write(Html.DisplayFor(model => model.Asunto));

#line default
#line hidden
            EndContext();
            BeginContext(768, 58, true);
            WriteLiteral("\r\n    </div>\r\n\r\n    <div class=\"display-label\">\r\n         ");
            EndContext();
            BeginContext(827, 44, false);
#line 35 "C:\Users\Equipo\Documents\00-Proyectos 2019\MASCore\MASCore\Views\OficioSalida\Delete.cshtml"
    Write(Html.DisplayNameFor(model => model.Dirigido));

#line default
#line hidden
            EndContext();
            BeginContext(871, 55, true);
            WriteLiteral("\r\n    </div>\r\n    <div class=\"display-field\">\r\n        ");
            EndContext();
            BeginContext(927, 40, false);
#line 38 "C:\Users\Equipo\Documents\00-Proyectos 2019\MASCore\MASCore\Views\OficioSalida\Delete.cshtml"
   Write(Html.DisplayFor(model => model.Dirigido));

#line default
#line hidden
            EndContext();
            BeginContext(967, 58, true);
            WriteLiteral("\r\n    </div>\r\n\r\n    <div class=\"display-label\">\r\n         ");
            EndContext();
            BeginContext(1026, 42, false);
#line 42 "C:\Users\Equipo\Documents\00-Proyectos 2019\MASCore\MASCore\Views\OficioSalida\Delete.cshtml"
    Write(Html.DisplayNameFor(model => model.dCargo));

#line default
#line hidden
            EndContext();
            BeginContext(1068, 55, true);
            WriteLiteral("\r\n    </div>\r\n    <div class=\"display-field\">\r\n        ");
            EndContext();
            BeginContext(1124, 38, false);
#line 45 "C:\Users\Equipo\Documents\00-Proyectos 2019\MASCore\MASCore\Views\OficioSalida\Delete.cshtml"
   Write(Html.DisplayFor(model => model.dCargo));

#line default
#line hidden
            EndContext();
            BeginContext(1162, 58, true);
            WriteLiteral("\r\n    </div>\r\n\r\n    <div class=\"display-label\">\r\n         ");
            EndContext();
            BeginContext(1221, 44, false);
#line 49 "C:\Users\Equipo\Documents\00-Proyectos 2019\MASCore\MASCore\Views\OficioSalida\Delete.cshtml"
    Write(Html.DisplayNameFor(model => model.dEmpresa));

#line default
#line hidden
            EndContext();
            BeginContext(1265, 55, true);
            WriteLiteral("\r\n    </div>\r\n    <div class=\"display-field\">\r\n        ");
            EndContext();
            BeginContext(1321, 40, false);
#line 52 "C:\Users\Equipo\Documents\00-Proyectos 2019\MASCore\MASCore\Views\OficioSalida\Delete.cshtml"
   Write(Html.DisplayFor(model => model.dEmpresa));

#line default
#line hidden
            EndContext();
            BeginContext(1361, 58, true);
            WriteLiteral("\r\n    </div>\r\n\r\n    <div class=\"display-label\">\r\n         ");
            EndContext();
            BeginContext(1420, 41, false);
#line 56 "C:\Users\Equipo\Documents\00-Proyectos 2019\MASCore\MASCore\Views\OficioSalida\Delete.cshtml"
    Write(Html.DisplayNameFor(model => model.Envia));

#line default
#line hidden
            EndContext();
            BeginContext(1461, 55, true);
            WriteLiteral("\r\n    </div>\r\n    <div class=\"display-field\">\r\n        ");
            EndContext();
            BeginContext(1517, 37, false);
#line 59 "C:\Users\Equipo\Documents\00-Proyectos 2019\MASCore\MASCore\Views\OficioSalida\Delete.cshtml"
   Write(Html.DisplayFor(model => model.Envia));

#line default
#line hidden
            EndContext();
            BeginContext(1554, 58, true);
            WriteLiteral("\r\n    </div>\r\n\r\n    <div class=\"display-label\">\r\n         ");
            EndContext();
            BeginContext(1613, 42, false);
#line 63 "C:\Users\Equipo\Documents\00-Proyectos 2019\MASCore\MASCore\Views\OficioSalida\Delete.cshtml"
    Write(Html.DisplayNameFor(model => model.eCargo));

#line default
#line hidden
            EndContext();
            BeginContext(1655, 55, true);
            WriteLiteral("\r\n    </div>\r\n    <div class=\"display-field\">\r\n        ");
            EndContext();
            BeginContext(1711, 38, false);
#line 66 "C:\Users\Equipo\Documents\00-Proyectos 2019\MASCore\MASCore\Views\OficioSalida\Delete.cshtml"
   Write(Html.DisplayFor(model => model.eCargo));

#line default
#line hidden
            EndContext();
            BeginContext(1749, 58, true);
            WriteLiteral("\r\n    </div>\r\n\r\n    <div class=\"display-label\">\r\n         ");
            EndContext();
            BeginContext(1808, 44, false);
#line 70 "C:\Users\Equipo\Documents\00-Proyectos 2019\MASCore\MASCore\Views\OficioSalida\Delete.cshtml"
    Write(Html.DisplayNameFor(model => model.eEmpresa));

#line default
#line hidden
            EndContext();
            BeginContext(1852, 55, true);
            WriteLiteral("\r\n    </div>\r\n    <div class=\"display-field\">\r\n        ");
            EndContext();
            BeginContext(1908, 40, false);
#line 73 "C:\Users\Equipo\Documents\00-Proyectos 2019\MASCore\MASCore\Views\OficioSalida\Delete.cshtml"
   Write(Html.DisplayFor(model => model.eEmpresa));

#line default
#line hidden
            EndContext();
            BeginContext(1948, 58, true);
            WriteLiteral("\r\n    </div>\r\n\r\n    <div class=\"display-label\">\r\n         ");
            EndContext();
            BeginContext(2007, 42, false);
#line 77 "C:\Users\Equipo\Documents\00-Proyectos 2019\MASCore\MASCore\Views\OficioSalida\Delete.cshtml"
    Write(Html.DisplayNameFor(model => model.Copias));

#line default
#line hidden
            EndContext();
            BeginContext(2049, 55, true);
            WriteLiteral("\r\n    </div>\r\n    <div class=\"display-field\">\r\n        ");
            EndContext();
            BeginContext(2105, 38, false);
#line 80 "C:\Users\Equipo\Documents\00-Proyectos 2019\MASCore\MASCore\Views\OficioSalida\Delete.cshtml"
   Write(Html.DisplayFor(model => model.Copias));

#line default
#line hidden
            EndContext();
            BeginContext(2143, 58, true);
            WriteLiteral("\r\n    </div>\r\n\r\n    <div class=\"display-label\">\r\n         ");
            EndContext();
            BeginContext(2202, 47, false);
#line 84 "C:\Users\Equipo\Documents\00-Proyectos 2019\MASCore\MASCore\Views\OficioSalida\Delete.cshtml"
    Write(Html.DisplayNameFor(model => model.Comentarios));

#line default
#line hidden
            EndContext();
            BeginContext(2249, 55, true);
            WriteLiteral("\r\n    </div>\r\n    <div class=\"display-field\">\r\n        ");
            EndContext();
            BeginContext(2305, 43, false);
#line 87 "C:\Users\Equipo\Documents\00-Proyectos 2019\MASCore\MASCore\Views\OficioSalida\Delete.cshtml"
   Write(Html.DisplayFor(model => model.Comentarios));

#line default
#line hidden
            EndContext();
            BeginContext(2348, 58, true);
            WriteLiteral("\r\n    </div>\r\n\r\n    <div class=\"display-label\">\r\n         ");
            EndContext();
            BeginContext(2407, 46, false);
#line 91 "C:\Users\Equipo\Documents\00-Proyectos 2019\MASCore\MASCore\Views\OficioSalida\Delete.cshtml"
    Write(Html.DisplayNameFor(model => model.TipoSalida));

#line default
#line hidden
            EndContext();
            BeginContext(2453, 55, true);
            WriteLiteral("\r\n    </div>\r\n    <div class=\"display-field\">\r\n        ");
            EndContext();
            BeginContext(2509, 42, false);
#line 94 "C:\Users\Equipo\Documents\00-Proyectos 2019\MASCore\MASCore\Views\OficioSalida\Delete.cshtml"
   Write(Html.DisplayFor(model => model.TipoSalida));

#line default
#line hidden
            EndContext();
            BeginContext(2551, 58, true);
            WriteLiteral("\r\n    </div>\r\n\r\n    <div class=\"display-label\">\r\n         ");
            EndContext();
            BeginContext(2610, 44, false);
#line 98 "C:\Users\Equipo\Documents\00-Proyectos 2019\MASCore\MASCore\Views\OficioSalida\Delete.cshtml"
    Write(Html.DisplayNameFor(model => model.Contesta));

#line default
#line hidden
            EndContext();
            BeginContext(2654, 55, true);
            WriteLiteral("\r\n    </div>\r\n    <div class=\"display-field\">\r\n        ");
            EndContext();
            BeginContext(2710, 40, false);
#line 101 "C:\Users\Equipo\Documents\00-Proyectos 2019\MASCore\MASCore\Views\OficioSalida\Delete.cshtml"
   Write(Html.DisplayFor(model => model.Contesta));

#line default
#line hidden
            EndContext();
            BeginContext(2750, 58, true);
            WriteLiteral("\r\n    </div>\r\n\r\n    <div class=\"display-label\">\r\n         ");
            EndContext();
            BeginContext(2809, 45, false);
#line 105 "C:\Users\Equipo\Documents\00-Proyectos 2019\MASCore\MASCore\Views\OficioSalida\Delete.cshtml"
    Write(Html.DisplayNameFor(model => model.Minutario));

#line default
#line hidden
            EndContext();
            BeginContext(2854, 55, true);
            WriteLiteral("\r\n    </div>\r\n    <div class=\"display-field\">\r\n        ");
            EndContext();
            BeginContext(2910, 41, false);
#line 108 "C:\Users\Equipo\Documents\00-Proyectos 2019\MASCore\MASCore\Views\OficioSalida\Delete.cshtml"
   Write(Html.DisplayFor(model => model.Minutario));

#line default
#line hidden
            EndContext();
            BeginContext(2951, 58, true);
            WriteLiteral("\r\n    </div>\r\n\r\n    <div class=\"display-label\">\r\n         ");
            EndContext();
            BeginContext(3010, 43, false);
#line 112 "C:\Users\Equipo\Documents\00-Proyectos 2019\MASCore\MASCore\Views\OficioSalida\Delete.cshtml"
    Write(Html.DisplayNameFor(model => model.Estatus));

#line default
#line hidden
            EndContext();
            BeginContext(3053, 55, true);
            WriteLiteral("\r\n    </div>\r\n    <div class=\"display-field\">\r\n        ");
            EndContext();
            BeginContext(3109, 39, false);
#line 115 "C:\Users\Equipo\Documents\00-Proyectos 2019\MASCore\MASCore\Views\OficioSalida\Delete.cshtml"
   Write(Html.DisplayFor(model => model.Estatus));

#line default
#line hidden
            EndContext();
            BeginContext(3148, 58, true);
            WriteLiteral("\r\n    </div>\r\n\r\n    <div class=\"display-label\">\r\n         ");
            EndContext();
            BeginContext(3207, 43, false);
#line 119 "C:\Users\Equipo\Documents\00-Proyectos 2019\MASCore\MASCore\Views\OficioSalida\Delete.cshtml"
    Write(Html.DisplayNameFor(model => model.TipoRel));

#line default
#line hidden
            EndContext();
            BeginContext(3250, 55, true);
            WriteLiteral("\r\n    </div>\r\n    <div class=\"display-field\">\r\n        ");
            EndContext();
            BeginContext(3306, 39, false);
#line 122 "C:\Users\Equipo\Documents\00-Proyectos 2019\MASCore\MASCore\Views\OficioSalida\Delete.cshtml"
   Write(Html.DisplayFor(model => model.TipoRel));

#line default
#line hidden
            EndContext();
            BeginContext(3345, 27, true);
            WriteLiteral("\r\n    </div>\r\n</fieldset>\r\n");
            EndContext();
#line 125 "C:\Users\Equipo\Documents\00-Proyectos 2019\MASCore\MASCore\Views\OficioSalida\Delete.cshtml"
 using (Html.BeginForm()) {

#line default
#line hidden
            BeginContext(3401, 67, true);
            WriteLiteral("    <p>\r\n        <input type=\"submit\" value=\"Delete\" /> |\r\n        ");
            EndContext();
            BeginContext(3469, 40, false);
#line 128 "C:\Users\Equipo\Documents\00-Proyectos 2019\MASCore\MASCore\Views\OficioSalida\Delete.cshtml"
   Write(Html.ActionLink("Back to List", "Index"));

#line default
#line hidden
            EndContext();
            BeginContext(3509, 12, true);
            WriteLiteral("\r\n    </p>\r\n");
            EndContext();
#line 130 "C:\Users\Equipo\Documents\00-Proyectos 2019\MASCore\MASCore\Views\OficioSalida\Delete.cshtml"
}

#line default
#line hidden
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<MASCore.DataModel.TMasOfSalida> Html { get; private set; }
    }
}
#pragma warning restore 1591
