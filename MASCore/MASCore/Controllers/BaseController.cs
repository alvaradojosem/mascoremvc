﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using System.Collections.Generic;

using MASCore.Models;

namespace MASCore.Controllers
{
    public class BaseController : Controller
    {
      
        protected configuracion conf;
        protected string token;
      
        public BaseController() { }
        public BaseController(IOptions<configuracion> settings)
        {
            conf = settings.Value;
        }

       

        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
        }
    }
}