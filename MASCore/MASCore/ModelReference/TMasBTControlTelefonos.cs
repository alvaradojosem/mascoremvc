﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MASCore.ModelReference
{
    public partial class TMasBTControlTelefonos
    {
        public int IdBitaCel { get; set; }
        public int IdControl { get; set; }
        public Nullable<System.DateTime> Fecha { get; set; }
        public string Hora { get; set; }
        public string Tipo { get; set; }
        public string Numero { get; set; }
        public Nullable<bool> Respuesta { get; set; }
    }
}
