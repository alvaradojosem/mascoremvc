#pragma checksum "C:\Users\Equipo\Documents\00-Proyectos 2019\MASCore\MASCore\Views\Llamadas\GeneratePdfLlamadas.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "62b62f8d0d54f058930d2f6ecd8cd78c6de2bf25"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Llamadas_GeneratePdfLlamadas), @"mvc.1.0.view", @"/Views/Llamadas/GeneratePdfLlamadas.cshtml")]
[assembly:global::Microsoft.AspNetCore.Mvc.Razor.Compilation.RazorViewAttribute(@"/Views/Llamadas/GeneratePdfLlamadas.cshtml", typeof(AspNetCore.Views_Llamadas_GeneratePdfLlamadas))]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#line 1 "C:\Users\Equipo\Documents\00-Proyectos 2019\MASCore\MASCore\Views\_ViewImports.cshtml"
using MASCore;

#line default
#line hidden
#line 2 "C:\Users\Equipo\Documents\00-Proyectos 2019\MASCore\MASCore\Views\_ViewImports.cshtml"
using MASCore.Models;

#line default
#line hidden
#line 4 "C:\Users\Equipo\Documents\00-Proyectos 2019\MASCore\MASCore\Views\_ViewImports.cshtml"
using DevExtreme.AspNet.Mvc;

#line default
#line hidden
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"62b62f8d0d54f058930d2f6ecd8cd78c6de2bf25", @"/Views/Llamadas/GeneratePdfLlamadas.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"a6f9e97209bd19ae69cd611cb55c19dae5d6d315", @"/Views/_ViewImports.cshtml")]
    public class Views_Llamadas_GeneratePdfLlamadas : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<IEnumerable<MASCore.DataModel.BuscaLlamadas_Result>>
    {
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_0 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("src", new global::Microsoft.AspNetCore.Html.HtmlString("~/images/Logo1.jpg"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_1 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("src", new global::Microsoft.AspNetCore.Html.HtmlString("~/images/Logo2.jpg"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        #line hidden
        #pragma warning disable 0169
        private string __tagHelperStringValueBuffer;
        #pragma warning restore 0169
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperExecutionContext __tagHelperExecutionContext;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner __tagHelperRunner = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner();
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __backed__tagHelperScopeManager = null;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __tagHelperScopeManager
        {
            get
            {
                if (__backed__tagHelperScopeManager == null)
                {
                    __backed__tagHelperScopeManager = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager(StartTagHelperWritingScope, EndTagHelperWritingScope);
                }
                return __backed__tagHelperScopeManager;
            }
        }
        private global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper;
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            BeginContext(60, 2, true);
            WriteLiteral("\r\n");
            EndContext();
#line 3 "C:\Users\Equipo\Documents\00-Proyectos 2019\MASCore\MASCore\Views\Llamadas\GeneratePdfLlamadas.cshtml"
  
    Layout = "~/Views/Shared/_PdfLayout.cshtml";

#line default
#line hidden
            BeginContext(119, 116, true);
            WriteLiteral("\r\n<table width=\"100%\" cellpadding=\"1.0\" cellspacing=\"1.0\"   widths=\"1;4;1\">\r\n    <row>\r\n        <cell>\r\n            ");
            EndContext();
            BeginContext(235, 32, false);
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("img", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.SelfClosing, "e74568158e904a0e8e8093647fee3f67", async() => {
            }
            );
            __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_0);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            EndContext();
            BeginContext(267, 560, true);
            WriteLiteral(@" />
        </cell>
        <cell horizontalalign=""Default"" verticalalign=""Default"">
            <paragraph style=""font-size: 13;font-weight:normal;"" red=""60"" green=""60"" blue=""60"" align=""Center"">
                COMISION NACIONAL DE ACUACULTURA Y PESCA <br />
                DIRECCIÓN GENERAL DE INSPECCION Y VIGILANCIA <br />
            </paragraph>
            <chunk style=""font-size: 13;font-weight:normal"" red=""60"" green=""60"" blue=""60"" align=""Center"">  DIRECCIÓN DE MONITOREO Y COORDINACIÓN</chunk> 
        </cell>
        <cell>
            ");
            EndContext();
            BeginContext(827, 32, false);
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("img", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.SelfClosing, "d40830b67f4245cd9612b92ccdd85cb8", async() => {
            }
            );
            __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_1);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            EndContext();
            BeginContext(859, 358, true);
            WriteLiteral(@" 
        </cell>
    </row>
</table>

<paragraph style=""font-family: Helvetica; font-size: 16; ""  align=""Center"">
	<chunk style=""font-weight:normal;"" red=""60"" green=""60"" blue=""60"">REPORTE - LLAMADAS </chunk>
</paragraph>

<table width=""100%"" cellpadding=""1.0"" cellspacing=""1.0""   widths=""3;3;2"">
    <row>
        <cell colspan=""2""> Impreso por: ");
            EndContext();
            BeginContext(1218, 69, false);
#line 31 "C:\Users\Equipo\Documents\00-Proyectos 2019\MASCore\MASCore\Views\Llamadas\GeneratePdfLlamadas.cshtml"
                                   Write(Html.Encode(Context.Request.Cookies["userName"].ToString().ToUpper()));

#line default
#line hidden
            EndContext();
            BeginContext(1287, 10, true);
            WriteLiteral(" </cell>\r\n");
            EndContext();
            BeginContext(1324, 22, true);
            WriteLiteral("        <cell> Fecha: ");
            EndContext();
            BeginContext(1347, 37, false);
#line 33 "C:\Users\Equipo\Documents\00-Proyectos 2019\MASCore\MASCore\Views\Llamadas\GeneratePdfLlamadas.cshtml"
                 Write(DateTime.Now.Date.ToShortDateString());

#line default
#line hidden
            EndContext();
            BeginContext(1384, 35, true);
            WriteLiteral("  </cell>\r\n    </row>\r\n</table>\r\n\r\n");
            EndContext();
#line 37 "C:\Users\Equipo\Documents\00-Proyectos 2019\MASCore\MASCore\Views\Llamadas\GeneratePdfLlamadas.cshtml"
 if (Model != null)
{


foreach (var item in Model) 
{

#line default
#line hidden
            BeginContext(1480, 507, true);
            WriteLiteral(@"    <table width=""100%"" cellpadding=""1.0"" cellspacing=""1.0""  border=""2"" widths="".7;1;1;.7;.7;1;1"" red=""255"" green=""255"" blue=""255"" align=""left"">
        <row>
            <cell colspan=""3"" borderwidth=""0.5"" left=""true"" right=""true"" top=""true"" bottom=""true"" header=""true"" leading=""18.0"">            	         
                <chunk style=""font-family: Helvetica; font-size: 10;font-weight:bold;text-align:center"">
                    LLAMADA
                </chunk>             
            </cell>
");
            EndContext();
            BeginContext(2006, 359, true);
            WriteLiteral(@"            <cell colspan=""4"" borderwidth=""0.5"" left=""false"" right=""false"" top=""false"" bottom=""false"" header=""true"" leading=""18.0"">            	         
                <chunk style=""font-family: Helvetica; font-size: 10;font-weight:bold;text-align:center"">
                    
                </chunk>             
            </cell>
        </row>
");
            EndContext();
            BeginContext(2474, 1800, true);
            WriteLiteral(@"        <row>
	        <cell borderwidth=""0.5"" left=""true"" right=""true"" top=""true"" bottom=""true"" header=""true"" leading=""18.0"">            	         
                <chunk style=""font-family: Helvetica; font-size: 10;font-weight:bold;""  red=""10"" green=""100"" blue=""100"">
                        Id
                </chunk>             
            </cell>	   
	        <cell borderwidth=""0.5"" left=""true"" right=""true"" top=""true"" bottom=""true"" >
                <chunk style=""font-family:Helvetica;font-size: 10;font-weight:bold;""  red=""10"" green=""100"" blue=""100"">
                        Fecha
                </chunk>
            </cell>
            <cell borderwidth=""0.5"" left=""true"" right=""true"" top=""true"" bottom=""true"" >
                <chunk style=""font-family:Helvetica;font-size: 10;font-weight:bold;""  red=""10"" green=""100"" blue=""100"">
                        Hora
                </chunk>
            </cell>
            <cell  borderwidth=""0.5"" left=""true"" right=""true"" top=""true"" bottom=""true"">
");
            WriteLiteral(@"                <chunk style=""font-family:Helvetica;font-size: 10;font-weight:bold;"" red=""10"" green=""100"" blue=""100"">
                    Respuesta
                </chunk>
            </cell>  
            <cell colspan=""3"" borderwidth=""0.5"" left=""true"" right=""true"" top=""true"" bottom=""true"" >
                <chunk style=""font-family:Helvetica;font-size: 10;font-weight:bold;""  red=""10"" green=""100"" blue=""100"">
                       Barco
                </chunk>
            </cell>                         
        </row>
        <row>
            <cell borderwidth=""0.5"" left=""true"" right=""true"" top=""true"" bottom=""true"">
                <chunk style=""font-family:Helvetica;font-size: 10;font-weight:bold;"">                        
                        ");
            EndContext();
            BeginContext(4275, 14, false);
#line 88 "C:\Users\Equipo\Documents\00-Proyectos 2019\MASCore\MASCore\Views\Llamadas\GeneratePdfLlamadas.cshtml"
                   Write(item.IdBitaCel);

#line default
#line hidden
            EndContext();
            BeginContext(4289, 240, true);
            WriteLiteral("\r\n                </chunk>\r\n            </cell>\t        \r\n            <cell borderwidth=\"0.5\" left=\"true\" right=\"true\" top=\"true\" bottom=\"true\" >\r\n                <chunk style=\"font-family:Helvetica;font-size: 9;\">\r\n                        ");
            EndContext();
            BeginContext(4530, 36, false);
#line 93 "C:\Users\Equipo\Documents\00-Proyectos 2019\MASCore\MASCore\Views\Llamadas\GeneratePdfLlamadas.cshtml"
                   Write(item.Fecha.Value.ToShortDateString());

#line default
#line hidden
            EndContext();
            BeginContext(4566, 231, true);
            WriteLiteral("\r\n                </chunk>\r\n            </cell> \r\n            <cell borderwidth=\"0.5\" left=\"true\" right=\"true\" top=\"true\" bottom=\"true\">\r\n                <chunk style=\"font-family:Helvetica;font-size: 9;\">\r\n                        ");
            EndContext();
            BeginContext(4798, 9, false);
#line 98 "C:\Users\Equipo\Documents\00-Proyectos 2019\MASCore\MASCore\Views\Llamadas\GeneratePdfLlamadas.cshtml"
                   Write(item.Hora);

#line default
#line hidden
            EndContext();
            BeginContext(4807, 228, true);
            WriteLiteral("\r\n                </chunk>\r\n            </cell> \r\n            <cell  borderwidth=\"0.5\" left=\"true\" right=\"true\" top=\"true\" bottom=\"true\">\r\n                <chunk style=\"font-family:Helvetica;font-size: 9;\">\r\n                    ");
            EndContext();
            BeginContext(5036, 14, false);
#line 103 "C:\Users\Equipo\Documents\00-Proyectos 2019\MASCore\MASCore\Views\Llamadas\GeneratePdfLlamadas.cshtml"
               Write(item.Respuesta);

#line default
#line hidden
            EndContext();
            BeginContext(5050, 243, true);
            WriteLiteral("\r\n                </chunk>\r\n            </cell> \r\n            <cell colspan=\"3\" borderwidth=\"0.5\" left=\"true\" right=\"true\" top=\"true\" bottom=\"true\">\r\n                <chunk style=\"font-family:Helvetica;font-size: 9;\">\r\n                        ");
            EndContext();
            BeginContext(5294, 10, false);
#line 108 "C:\Users\Equipo\Documents\00-Proyectos 2019\MASCore\MASCore\Views\Llamadas\GeneratePdfLlamadas.cshtml"
                   Write(item.Barco);

#line default
#line hidden
            EndContext();
            BeginContext(5304, 99, true);
            WriteLiteral("\r\n                </chunk>\r\n            </cell>                                  \r\n        </row>\r\n");
            EndContext();
            BeginContext(5513, 1512, true);
            WriteLiteral(@"        <row>
	        <cell borderwidth=""0.5"" left=""true"" right=""true"" top=""true"" bottom=""true"" header=""true"" leading=""18.0"">            	         
                <chunk style=""font-family: Helvetica; font-size: 10;font-weight:bold;""  red=""10"" green=""100"" blue=""100"">
                        RNP
                </chunk>             
            </cell>	   
	        <cell borderwidth=""0.5"" left=""true"" right=""true"" top=""true"" bottom=""true"" >
                <chunk style=""font-family:Helvetica;font-size: 10;font-weight:bold;""  red=""10"" green=""100"" blue=""100"">
                        Matricula
                </chunk>
            </cell>
            <cell colspan=""3"" borderwidth=""0.5"" left=""true"" right=""true"" top=""true"" bottom=""true"" >
                <chunk style=""font-family:Helvetica;font-size: 10;font-weight:bold;"" red=""10"" green=""100"" blue=""100"">
                    Razón Social
                </chunk>
            </cell>
            <cell colspan=""2"" borderwidth=""0.5"" left=""true"" right=""tr");
            WriteLiteral(@"ue"" top=""true"" bottom=""true"" >
                <chunk style=""font-family:Helvetica;font-size: 10;font-weight:bold;""  red=""10"" green=""100"" blue=""100"">
                       OFP
                </chunk>
            </cell>                         
        </row>
        <row>
            <cell borderwidth=""0.5"" left=""true"" right=""true"" top=""true"" bottom=""true"">
                <chunk style=""font-family:Helvetica;font-size: 9;"">                        
                        ");
            EndContext();
            BeginContext(7026, 8, false);
#line 138 "C:\Users\Equipo\Documents\00-Proyectos 2019\MASCore\MASCore\Views\Llamadas\GeneratePdfLlamadas.cshtml"
                   Write(item.RNP);

#line default
#line hidden
            EndContext();
            BeginContext(7034, 240, true);
            WriteLiteral("\r\n                </chunk>\r\n            </cell>\t        \r\n            <cell borderwidth=\"0.5\" left=\"true\" right=\"true\" top=\"true\" bottom=\"true\" >\r\n                <chunk style=\"font-family:Helvetica;font-size: 9;\">\r\n                        ");
            EndContext();
            BeginContext(7275, 14, false);
#line 143 "C:\Users\Equipo\Documents\00-Proyectos 2019\MASCore\MASCore\Views\Llamadas\GeneratePdfLlamadas.cshtml"
                   Write(item.Matricula);

#line default
#line hidden
            EndContext();
            BeginContext(7289, 243, true);
            WriteLiteral("\r\n                </chunk>\r\n            </cell> \r\n            <cell colspan=\"3\" borderwidth=\"0.5\" left=\"true\" right=\"true\" top=\"true\" bottom=\"true\">\r\n                <chunk style=\"font-family:Helvetica;font-size: 9;\">\r\n                        ");
            EndContext();
            BeginContext(7533, 16, false);
#line 148 "C:\Users\Equipo\Documents\00-Proyectos 2019\MASCore\MASCore\Views\Llamadas\GeneratePdfLlamadas.cshtml"
                   Write(item.RazonSocial);

#line default
#line hidden
            EndContext();
            BeginContext(7549, 243, true);
            WriteLiteral("\r\n                </chunk>\r\n            </cell> \r\n            <cell colspan=\"2\" borderwidth=\"0.5\" left=\"true\" right=\"true\" top=\"true\" bottom=\"true\">\r\n                <chunk style=\"font-family:Helvetica;font-size: 9;\">\r\n                        ");
            EndContext();
            BeginContext(7793, 8, false);
#line 153 "C:\Users\Equipo\Documents\00-Proyectos 2019\MASCore\MASCore\Views\Llamadas\GeneratePdfLlamadas.cshtml"
                   Write(item.OFP);

#line default
#line hidden
            EndContext();
            BeginContext(7801, 120, true);
            WriteLiteral("\r\n                </chunk>\r\n            </cell>                                  \r\n        </row>\r\n\r\n\r\n    </table>   \r\n");
            EndContext();
#line 160 "C:\Users\Equipo\Documents\00-Proyectos 2019\MASCore\MASCore\Views\Llamadas\GeneratePdfLlamadas.cshtml"
}

}







#line default
#line hidden
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<IEnumerable<MASCore.DataModel.BuscaLlamadas_Result>> Html { get; private set; }
    }
}
#pragma warning restore 1591
