﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MASCore.ModelReference
{

    public partial class BTBitaCelInsertar_Result
    {
        public int IdBitaCel { get; set; }
        public string Actividad { get; set; }
        public string Barco { get; set; }
        public string RNP { get; set; }
        public string Matricula { get; set; }
        public string RazonSocial { get; set; }
        public string PuertoBase { get; set; }
        public Nullable<System.DateTime> Fecha { get; set; }
        public string Empresa { get; set; }
        public string Atendio { get; set; }
        public string Lugar { get; set; }
        public string Descripcion { get; set; }
        public string OFP { get; set; }
    }
}
