﻿using System;


namespace MASCore.DataModel
{
    public partial class BuscaInformacionDeEmbarcacion_Result
    {
        public string Matricula { get; set; }
        public string Nombre { get; set; }
        public string SerieAntena { get; set; }
        public string Localidad { get; set; }
        public string RNP { get; set; }
        public string Permiso { get; set; }
        public string TipoPermiso { get; set; }
        public string RazonSocial { get; set; }
        public string RepLegal { get; set; }
        public string RNPTitular { get; set; }
        public string PuertoBase { get; set; }
        public string Zona { get; set; }
    }
}
