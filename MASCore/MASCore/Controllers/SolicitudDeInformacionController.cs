﻿using ClosedXML.Excel;
using MASCore.Exportar;
using MASCore.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Xceed.Words.NET;
using DevExtreme.AspNet.Mvc;
using Newtonsoft.Json;
using DevExtreme.AspNet.Data;
using Newtonsoft.Json.Linq;
using MASCore.DataModel;
using Microsoft.AspNetCore.Hosting;


namespace MASCore.Controllers
{
    public class SolicitudDeInformacionController : BaseController
    {
        //private BDMasContextEnt db = new BDMasContextEnt();
        FuncionesProcedimientos FunProc = new FuncionesProcedimientos();

        //
        // GET: /SolicitudDeInformacion/
        public ActionResult Index()
        {
            TMasSolInfoCedulas TMSol = new TMasSolInfoCedulas();
            return View(TMSol);
        }

        public ActionResult BuscaSolInfo() // carga la pantalla para buscar
        {
            SolicitudDeInformacionEntradaDatosBusqueda ModeloTmp = new SolicitudDeInformacionEntradaDatosBusqueda();
            ModeloTmp.FechaInicial = DateTime.Now;
            ModeloTmp.FechaFinal = DateTime.Now;

            //ViewBag.ofp = FunProc.LlenarResultadoGenerico("responsableofp");
            //ViewBag.asunto = FunProc.LlenarResultadoGenerico("asunto");
            //ViewBag.medio = FunProc.LlenarResultadoGenerico("medio");
            //ViewBag.estado = FunProc.LlenarResultadoGenerico("estadosolinfo");
            //ViewBag.ciudad = FunProc.BuscaResultadoGenericoConParametrosLista("CIUDAdsolinfo", "", "");
            //ViewBag.Dependencias = FunProc.LlenarResultadoGenerico("dependencia");
            
            List<BuscaSolicitudesDeInformacion_Result> ResultadoListaSolInfoSP = new List<BuscaSolicitudesDeInformacion_Result>();
            ModeloTmp.ResultadoListaSolInfoSP = ResultadoListaSolInfoSP;
            //Session.Remove("SessionResultadoListaSolInfoSPTmp");
            //Session["SessionResultadoListaSolInfoSPTmp"] = ModeloTmp.ResultadoListaSolInfoSP;
            return View(ModeloTmp);
        }

        [HttpPost]
        public ActionResult BuscaSolInfo(SolicitudDeInformacionEntradaDatosBusqueda ModeloTmp) //Accion del botón par buscar
        {
            //ViewBag.ofp = FunProc.LlenarResultadoGenerico("responsableofp");
            //ViewBag.asunto = FunProc.LlenarResultadoGenerico("asunto");
            //ViewBag.medio = FunProc.LlenarResultadoGenerico("medio");
            //ViewBag.estado = FunProc.LlenarResultadoGenerico("estadosolinfo");
            //ViewBag.ciudad = FunProc.BuscaResultadoGenericoConParametrosLista("CIUDAdsolinfo", "", "");
            //ViewBag.Dependencias = FunProc.LlenarResultadoGenerico("dependencia");
            List<BuscaSolicitudesDeInformacion_Result> ResultadoListaSolInfoSPTmp = new List<BuscaSolicitudesDeInformacion_Result>();
            ModeloTmp.ResultadoListaSolInfoSP = ResultadoListaSolInfoSPTmp;
            //Session["SessionResultadoListaSolInfoSPTmp"] = ModeloTmp.ResultadoListaSolInfoSP;
            if (ModelState.IsValid)
            {
                #region "VALIDACIONES"
                Boolean Valido = true;
                if (ModeloTmp.FechaInicial > ModeloTmp.FechaFinal)
                {
                    ViewBag.NoValidoPor = "No es posible realizar la búsqueda, la fecha inicial no puede ser mayor a la fecha final. Verificar.";
                    Valido = false;
                }
                #endregion
                if (Valido == true)
                {
                    //Aquí separamos el valor de Embarcación, quitamos la matricula y el RNP.

                    //Session["embarcacion"] = "";

                    if (ModeloTmp.Embarcacion != null)
                    {
                        string barco = ModeloTmp.Embarcacion.Replace(" | ", "|");
                        String[] arrayString = barco.Split('|');
                        //MODIFICADO POR SZ EL 09/01/2017
                        String nombreBarco = arrayString[0];
                        String matriculaBarco = arrayString[1];
                        String RNPBarco = arrayString[2];

                        //MODIFICADO EN VEZ DE TRAER EL NOMBRE DEL BARCO TRAEMOS EL RNP
                        //Session["embarcacion"] = RNPBarco;
                    }
                    //VER VALIDACIÓN DE BARCO PARA CUANDO SEA NULO NO TRUENE.

                    ModeloTmp.ResultadoListaSolInfoSP = ModeloTmp.EncuentraLasSolInfoSP(ModeloTmp.FechaInicial, ModeloTmp.FechaFinal,
                        ModeloTmp.medio, ModeloTmp.asunto, ModeloTmp.dependencia, ModeloTmp.OFP, ModeloTmp.solicitante, ModeloTmp.folio, "",// HttpContext.Session["embarcacion"],
                        ModeloTmp.Estado, ModeloTmp.Ciudad);
                }

                //Session["SessionResultadoListaSolInfoSPTmp"] = ModeloTmp.ResultadoListaSolInfoSP;

                //No se usa SessionResultadoListaSolInfo
                List<BuscaSolicitudesDeInformacion_Result> ResultadoListaSolInfo = ModeloTmp.ResultadoListaSolInfoSP;
                //Session["SessionResultadoListaSolInfo"] = ResultadoListaSolInfo;
            }

            string ParametrosDeBusqueda = "";

            ParametrosDeBusqueda += "Desde " + ModeloTmp.FechaInicial.ToShortDateString() + " Hasta " + ModeloTmp.FechaFinal.ToShortDateString() + " |";

            if (ModeloTmp.solicitante != null && ModeloTmp.solicitante.Length > 0)
                ParametrosDeBusqueda += " Solicitante: " + ModeloTmp.solicitante + " |";
            if (ModeloTmp.folio != null && ModeloTmp.folio.Length > 0)
                ParametrosDeBusqueda += " Folio: " + ModeloTmp.folio + " |";
            if (ModeloTmp.asunto != null && ModeloTmp.asunto.Length > 0)
                ParametrosDeBusqueda += " Asunto: " + ModeloTmp.asunto + " |";
            if (ModeloTmp.OFP != null && ModeloTmp.OFP.Length > 0)
                ParametrosDeBusqueda += " OFP: " + ModeloTmp.OFP + " |";
            if (ModeloTmp.medio != null && ModeloTmp.medio.Length > 0)
                ParametrosDeBusqueda += " Medio: " + ModeloTmp.medio + " |";
            if (ModeloTmp.dependencia != null && ModeloTmp.dependencia.Length > 0)
                ParametrosDeBusqueda += " Dependencia: " + ModeloTmp.dependencia + " |";

            if (ParametrosDeBusqueda.Length > 0)
                ParametrosDeBusqueda = ParametrosDeBusqueda.Substring(0, ParametrosDeBusqueda.Length - 1);

            //HttpContext.Session["SessionParametrosDeBusqueda"] = ParametrosDeBusqueda;

            return View(ModeloTmp);
        }

        //
        // GET: /SolicitudDeInformacion/Details/5
        public ActionResult Details(int id = 0)
        {
            SolicitudesDeInformacionModel ModeloTmp = new SolicitudesDeInformacionModel();

            TMasSolInfoCedulas tmassolinfocedulas = null;// db.TMasSolInfoCedulas.Single(t => t.IdInfoSol == id);
            ModeloTmp.TMASSOLINFOCEDULAS = tmassolinfocedulas;
            //db.SPMasSolInfoRelacionNormalizar(id);
            ModeloTmp.ResultadoListaSolInfoRelacionSP = ModeloTmp.SolInfoRelacionInsertarSP_Result(id);

            if (tmassolinfocedulas == null)
            {
                return ViewBag("HttpNotFound");
            }

            List<BuscaCargaImagenesTipo_Result> ListaImagenesTmp = new List<BuscaCargaImagenesTipo_Result>();
            ListaImagenesTmp = null;// FunProc.ObtieneListaCargaImagenesTipo(tmassolinfocedulas.IdInfoSol.ToString(), "", "SOLICITUDES DE INFORMACION");
            //Session["SessionImagenesSolInfo"] = ListaImagenesTmp;

            return View(ModeloTmp);
        }

        //
        // GET: /SolicitudDeInformacion/Create
        public ActionResult Create()
        {
            //if (Session["idsvehi"] is null)
            //{
            //    Session["idsvehi"] = db.BuscaResultadoGenericoSP("nombre_rnpBarco").Select(c => c.Resultado).ToList<string>();
            //}

            //Session["Identificador"] = 0;

            //ViewBag.ofp = FunProc.LlenarResultadoGenerico("responsableofp");
            //ViewBag.asunto = FunProc.LlenarResultadoGenerico("asunto");
            //ViewBag.medio = FunProc.LlenarResultadoGenerico("medioMAS");
            //ViewBag.estado = FunProc.LlenarResultadoGenerico("estadosolinfo");
            //ViewBag.ciudad = FunProc.BuscaResultadoGenericoConParametrosLista("CIUDAdsolinfo", "", "");
            ViewBag.EmbarcacionesNombreRNP = ObtenerEmbarcacionesNombreRNP();

            SolicitudesDeInformacionModel ModeloTmp = new SolicitudesDeInformacionModel();
            ModeloTmp.fecha = DateTime.Now;
            ModeloTmp.hora = DateTime.Now.TimeOfDay.ToString().Substring(0, 8);
            ModeloTmp.folio = "PENDIENTE";
            ModeloTmp.ErroresAgregarEmbarcacion = "";

            ModeloTmp.ResultadoListaSolInfoRelacionSPTMP = ModeloTmp.SolInfoRelacionInsertarTMPSP_Result(ModeloTmp.identrada);

            return View(ModeloTmp);
        }

        //
        // POST: /SolicitudDeInformacion/Create
        [HttpPost]
        public ActionResult Create(SolicitudesDeInformacionModel ModeloTmp, string submitButton, string Estado, string opcionesbusqueda, string txtvalor, string txtvalorsolicitante, string txtciudad, string txtestado, string txtdependencia, string txtIdSel, string txtRnpSel)
        {
            //ViewBag.ofp = FunProc.LlenarResultadoGenerico("responsableofp");
            //ViewBag.asunto = FunProc.LlenarResultadoGenerico("asunto");
            //ViewBag.medio = FunProc.LlenarResultadoGenerico("medioMAS");
            //ViewBag.estado = FunProc.LlenarResultadoGenerico("estadosolinfo");
            //ViewBag.ciudad = FunProc.BuscaResultadoGenericoConParametrosLista("CIUDADsolinfo", "", Estado);
            ViewBag.EmbarcacionesNombreRNP = ObtenerEmbarcacionesNombreRNP();
            ModeloTmp.ResultadoListaSolInfoRelacionSPTMP = ModeloTmp.SolInfoRelacionInsertarTMPSP_Result(ModeloTmp.identrada);

            switch (submitButton)
            {
                case "Guardar":
                    ModeloTmp.ResultadoListaSolInfoRelacionSPTMP = ModeloTmp.SolInfoRelacionInsertarTMPSP_Result(ModeloTmp.identrada);
                    if (ModeloTmp.ResultadoListaSolInfoRelacionSPTMP.Count == 0)
                    {
                        ModeloTmp.ErroresAgregarEmbarcacion = "Debe ingresar al menos una Embarcación";
                    }
                    else
                    {
                        if (ModelState.IsValid)
                        {
                            #region "VALIDACIONES"
                            Boolean Valido = true;
                            if (ModeloTmp.fecha > DateTime.Now.Date)
                            {
                                ViewBag.NoValidoPor = "La fecha no puede ser mayor a la fecha actual, Verificar Fechas.";
                                Valido = false;
                            }
                            if (ModeloTmp.fecha.Year < 1753 || ModeloTmp.fecha.Year > 2030)
                            {
                                ViewBag.NoValidoPor = "No es posible completar la acción, Verificar Fechas.";
                                Valido = false;
                            }
                            //AGREGADO POR SZ EL 14/12/2016
                            //VALIDA QUE NO SE GUARDE UNA SOLICITUD DE INFORMACIÓN SI NO LLEVA ESTADO O CIUDAD.
                            if (ModeloTmp.estado == null || ModeloTmp.ciudad == null)
                            {
                                ViewBag.NoValidoPor = "El estado y la ciudad son requeridos.";
                                Valido = false;
                            }
                            #endregion
                            if (Valido == true)
                            {
                                //Guarda la informacion en la tabla
                                ModeloTmp.IdInfoSol = ModeloTmp.SolInfoInsertarSP_returnID(ModeloTmp);
                                if (ModeloTmp.IdInfoSol > 0)
                                {
                                    //Inserta la relacion de barcos en su tabla correspondiente
                                    int Cantidad = ModeloTmp.InsertaRelacionSolInfo(ModeloTmp.IdInfoSol, ModeloTmp.identrada);
                                    if (Cantidad > 0)
                                    {
                                        return RedirectToAction("Details", "SolicitudDeInformacion", new { id = ModeloTmp.IdInfoSol });
                                    }
                                }
                            }
                        }
                    }
                    break;
                default:
                    break;
            }

            return View(ModeloTmp);
        }

        public ActionResult CrearSolInfo()
        {
            //Session["Identificador"] = 0;

            //ViewBag.ofp = new SelectList(FunProc.LlenarResultadoGenerico("responsableofp"), "Resultado", "Resultado");
            //ViewBag.dependencia = new SelectList(FunProc.LlenarResultadoGenerico("dependencia"), "Resultado", "Resultado");
            //ViewBag.asunto = new SelectList(FunProc.LlenarResultadoGenerico("asunto"), "Resultado", "Resultado");
            //ViewBag.medio = new SelectList(FunProc.LlenarResultadoGenerico("medio"), "Resultado", "Resultado");

            //ViewBag.solicitante = new SelectList(FunProc.LlenarResultadoGenerico("solicitante"), "Resultado", "Resultado");
            //ViewBag.estado = new SelectList(FunProc.LlenarResultadoGenerico("estadosolinfo"), "Resultado", "Resultado");
            //// PENDIENTE , EL COMBO DE CIUDAD DEBE DE CAMBIAR AL SELECCIONAR EL ESTADO
            //ViewBag.ciudad = new SelectList(FunProc.BuscaResultadoGenericoConParametrosLista("CIUDAdsolinfo", "", ""), "Resultado", "Resultado");

            SolicitudesDeInformacionModel ModeloTmp = new SolicitudesDeInformacionModel();
            ModeloTmp.fecha = DateTime.Now;
            ModeloTmp.hora = DateTime.Now.TimeOfDay.ToString().Substring(0, 8);
            ModeloTmp.folio = "PENDIENTE";
            ModeloTmp.ErroresAgregarEmbarcacion = "";

            #region "SECCION BUSCA INFORMACION DE EMBARCACION"
            List<BuscaInformacionDeEmbarcacion_Result> ModeloInformacionDeEmbarcacion = new List<BuscaInformacionDeEmbarcacion_Result>();
            ModeloTmp.InformacionDeEmbarcacion = ModeloInformacionDeEmbarcacion;
            //ViewBag.opcionesbusqueda = new SelectList(FunProc.LlenarResultadoGenerico("opcionesbusqueda"), "Resultado", "Resultado");
            #endregion
            List<BuscaInformacionDeSolicitante_Result> ModeloInformacionDeSolcitante = new List<BuscaInformacionDeSolicitante_Result>();
            ModeloTmp.InformacionDeSolicitante = ModeloInformacionDeSolcitante;

            //List<SolInfoRelacionInsertarTMPResult_Result> ResultadoListaSolInfoRelacionSPTmp = new List<SolInfoRelacionInsertarTMPResult_Result>();
            //ModeloTmp.ResultadoListaSolInfoRelacionSP = ResultadoListaSolInfoRelacionSPTmp;
            ModeloTmp.ResultadoListaSolInfoRelacionSPTMP = ModeloTmp.SolInfoRelacionInsertarTMPSP_Result(ModeloTmp.identrada);

            return View(ModeloTmp);
        }

        public String ValidaCamposAgregarBarco(SolicitudesDeInformacionModel ModeloTmp)
        {
            String Errores = "";
            if (ModeloTmp.barco == "" || ModeloTmp.barco == null)
                Errores += " Barco,";
            if (ModeloTmp.rnp == "" || ModeloTmp.rnp == null)
                Errores += " RNP,";
            if (ModeloTmp.razonsocial == "" || ModeloTmp.razonsocial == null)
                Errores += " Razon Social,";
            if (ModeloTmp.antena == "" || ModeloTmp.antena == null)
                Errores += " Antena,";
            if (ModeloTmp.matricula == "" || ModeloTmp.matricula == null)
                Errores += " Matricula,";
            if (Errores.Length > 0)
            {
                Errores = Errores.Substring(0, Errores.Length - 1);
                Errores = "*Campos Requeridos :" + Errores;
            }


            return Errores;
        }

        //
        // GET: /SolicitudDeInformacion/Edit/5
        public ActionResult Edit(int id = 0)
        {
            //if (Session["idsvehi"] is null)
            //{
            //    Session["idsvehi"] = db.BuscaResultadoGenericoSP("nombre_rnpBarco").Select(c => c.Resultado).ToList<string>();
            //}

            //ViewBag.ofp = FunProc.LlenarResultadoGenerico("responsableofp");
            ////ViewBag.dependencia = FunProc.LlenarResultadoGenerico("dependencia");
            //ViewBag.asunto = FunProc.LlenarResultadoGenerico("asunto");
            //ViewBag.medio = FunProc.LlenarResultadoGenerico("medio");

            //ViewBag.estado = FunProc.LlenarResultadoGenerico("estadosolinfo");
            //ViewBag.ciudad = FunProc.BuscaResultadoGenericoConParametrosLista("CIUDADsolinfo", "", "");
            //ViewBag.EmbarcacionesNombreRNP = ObtenerEmbarcacionesNombreRNP();
            ////AGREGADO POR SZ EL 05/12/2016
            ////ViewBag.barco = FunProc.LlenarResultadoGenerico("nombre_rnp_matriculabarco");
            ////ViewBag.barcoUi = FunProc.LlenarResultadoGenerico("nombre_rnp_matriculabarcoUi");

            SolicitudesDeInformacionModel ModeloTmp = new SolicitudesDeInformacionModel();
            ModeloTmp.ErroresAgregarEmbarcacion = "";
            #region "ASIGNA INFORMACION A VIEWMODEL"

            TMasSolInfoCedulas tmassolinfocedulas = null;// db.TMasSolInfoCedulas.Single(t => t.IdInfoSol == id);
            //Session["Identificador"] = tmassolinfocedulas.IdInfoSol;
            ModeloTmp.identrada = tmassolinfocedulas.IdInfoSol;
            ModeloTmp.IdInfoSol = tmassolinfocedulas.IdInfoSol;
            ModeloTmp.fecha = DateTime.Parse(tmassolinfocedulas.Fecha);
            ModeloTmp.hora = tmassolinfocedulas.Hora;
            ModeloTmp.solicitante = tmassolinfocedulas.Solicitante;
            ModeloTmp.cargo = tmassolinfocedulas.Cargo;
            ModeloTmp.telefono = tmassolinfocedulas.Telefono;
            ModeloTmp.email = tmassolinfocedulas.Email;
            ModeloTmp.ciudad = tmassolinfocedulas.Ciudad;
            ModeloTmp.estado = tmassolinfocedulas.Estado;
            ModeloTmp.asunto = tmassolinfocedulas.Asunto;
            ModeloTmp.dependencia = tmassolinfocedulas.Dependencia;
            ModeloTmp.medio = tmassolinfocedulas.Medio;
            ModeloTmp.descripcion = tmassolinfocedulas.Descripcion;
            ModeloTmp.ofp = tmassolinfocedulas.OFP;
            ModeloTmp.folio = tmassolinfocedulas.Folio;

            //COMENTADO POR PRUEBAS...
            ModeloTmp.ResultadoListaSolInfoRelacionSPTMP = ModeloTmp.SolInfoRelacionInsertarTMPSP_Result(id);
            #endregion

            //#region "SECCION BUSCA INFORMACION DE EMBARCACION"
            //List<BuscaInformacionDeEmbarcacion_Result> ModeloInformacionDeEmbarcacion = new List<BuscaInformacionDeEmbarcacion_Result>();
            //ModeloTmp.InformacionDeEmbarcacion = ModeloInformacionDeEmbarcacion;
            ////ViewBag.opcionesbusqueda = FunProc.LlenarResultadoGenerico("opcionesbusqueda");
            //#endregion
            //List<BuscaInformacionDeSolicitante_Result> ModeloInformacionDeSolcitante = new List<BuscaInformacionDeSolicitante_Result>();

            //ModeloTmp.InformacionDeSolicitante = ModeloInformacionDeSolcitante;

            //if (tmassolinfocedulas == null)
            //{
            //    return StatusCode(404);
            //}
            return View(ModeloTmp);
        }

        //
        // POST: /SolicitudDeInformacion/Edit/5
        //[HttpPost]
        public ActionResult Edit(SolicitudesDeInformacionModel ModeloTmp, string submitButton, string Estado, string opcionesbusqueda, string txtvalor, string txtvalorsolicitante, string txtIdSel, string txtRnpSel)
        {
            string UserName = "";
            if (HttpContext.Request.Cookies["userName"] != null)
                UserName = HttpContext.Request.Cookies["userName"].ToString();
            //#region "MOSTRAR DIVS"
            //ModeloTmp.MostrandoBusquedaInformacionEmbarcacion = false;     // Inicializa falso y al ir al boton define si mostrar o no 
            //ModeloTmp.MostrandoBusquedaInformacionSolicitante = false;
            //#endregion
            //#region "SECCION BUSCA INFORMACION DE EMBARCACION"
            //List<BuscaInformacionDeEmbarcacion_Result> ModeloInformacionDeEmbarcacion = new List<BuscaInformacionDeEmbarcacion_Result>();
            //ModeloInformacionDeEmbarcacion = ModeloTmp.EncuentraInformacionDeEmbarcacionSP(opcionesbusqueda, txtvalor);
            //ModeloTmp.InformacionDeEmbarcacion = ModeloInformacionDeEmbarcacion;
            //#endregion
            //List<BuscaInformacionDeSolicitante_Result> ModeloInformacionDeSolcitante = new List<BuscaInformacionDeSolicitante_Result>();
            //ModeloTmp.InformacionDeSolicitante = db.BuscaInformacionDeSolicitante(txtvalorsolicitante).ToList();

            ModeloTmp.ErroresAgregarEmbarcacion = "";
            //ViewBag.ofp = FunProc.LlenarResultadoGenerico("responsableofp");
            //ViewBag.asunto = FunProc.LlenarResultadoGenerico("asunto");
            //ViewBag.medio = FunProc.LlenarResultadoGenerico("medio");
            //ViewBag.estado = FunProc.LlenarResultadoGenerico("estadosolinfo");
            //ViewBag.ciudad = FunProc.BuscaResultadoGenericoConParametrosLista("CIUDADsolinfo", "", "");
            ViewBag.EmbarcacionesNombreRNP = ObtenerEmbarcacionesNombreRNP();

            ModeloTmp.ResultadoListaSolInfoRelacionSPTMP = ModeloTmp.SolInfoRelacionInsertarTMPSP_Result(ModeloTmp.identrada);

            switch (submitButton)
            {
                case "Guardar":
                    // Valida que exista al menos una embarcacion agregada.
                    if (ModeloTmp.ResultadoListaSolInfoRelacionSPTMP.Count == 0 || (ModeloTmp.ciudad == "" || ModeloTmp.ciudad == null) )
                    {
                        if (ModeloTmp.ResultadoListaSolInfoRelacionSPTMP.Count == 0)
                            ModeloTmp.ErroresAgregarEmbarcacion = "Debe ingresar al menos una Embarcación";
                        if (ModeloTmp.ciudad == "" || ModeloTmp.ciudad == null)
                            ModeloTmp.ErroresAgregarEmbarcacion = "Debe ingresar la Ciudad";
                    }
                    else
                    {
                        // Si el modelo es valido ( se ingresaron los campos requeridos y los formateados)
                        if (ModelState.IsValid)
                        {
                            #region "VALIDACIONES"
                            Boolean Valido = true;
                            if (ModeloTmp.fecha > DateTime.Now.Date)
                            {
                                ViewBag.NoValidoPor = "La fecha no puede ser mayor a la fecha actual, Verificar Fechas.";
                                Valido = false;
                            }
                            if (ModeloTmp.fecha.Year < 1753 || ModeloTmp.fecha.Year > 2030)
                            {
                                ViewBag.NoValidoPor = "No es posible completar la acción, Verificar Fechas.";
                                Valido = false;
                            }
                            #endregion
                            if (Valido == true)
                            {
                                //Actualiza la informacion en la tabla
                                ModeloTmp.IdInfoSol = ModeloTmp.SolInfoActualizarSP_returnID(ModeloTmp);
                                if (ModeloTmp.IdInfoSol > 0)
                                {
                                    return RedirectToAction("Details", "SolicitudDeInformacion", new { id = ModeloTmp.IdInfoSol });
                                }
                            }
                        }
                    }
                    break;
                default:
                    break;
            }

            return View(ModeloTmp);
        }

        #region Nuevos Metodos del Edit.Post => Switch, para tener un codigo mas limpio y que permita un mejor mantenimiento
        [HttpPost]
        public ActionResult EliminarBarcoNew(SolicitudesDeInformacionModel ModeloTmp)
        {
            ModeloTmp.SolInfoRelacionBorrarTMPSP(ModeloTmp.identrada, ModeloTmp.embarcacion_RNP);

            return Content(JsonConvert.SerializeObject(new { status = true, modelo = ModeloTmp.SolInfoRelacionInsertarTMPSP_Result(ModeloTmp.identrada) }), "application/json");
        }

        [HttpPost]
        public ActionResult EliminarBarco(SolicitudesDeInformacionModel ModeloTmp)
        {
            var status = "";// FunProc.EliminaSolInfoRel(ModeloTmp.embarcacion_idEntrada, ModeloTmp.embarcacion_RNP);

            return Content(JsonConvert.SerializeObject(new { status = status, modelo = ModeloTmp.SolInfoRelacionInsertarTMPSP_Result(ModeloTmp.identrada) }), "application/json");
        }

        [HttpPost]
        public ActionResult ActualizarBarco(SolicitudesDeInformacionModel ModeloTmp)
        {
            actualizaRegistro(Int32.Parse(ModeloTmp.embarcacion_idEntrada), ModeloTmp.embarcacion_RNP);
            return Content(JsonConvert.SerializeObject(ModeloTmp.SolInfoRelacionInsertarTMPSP_Result(ModeloTmp.identrada)), "application/json");
        }

        [HttpPost]
        public ActionResult Agregar(SolicitudesDeInformacionModel ModeloTmp)
        {
            string UserName = "";
            if (HttpContext.Request.Cookies["userName"] != null)
                UserName = HttpContext.Request.Cookies["userName"].ToString();

            List<string> ids = new List<string>();
            //if (Session["idsvehi"] is null) {
            //    ids = db.BuscaResultadoGenericoSP("nombre_rnpBarco").Select(c => c.Resultado).ToList<string>();
            //    Session["idsvehi"] = ids;
            //} else { ids = (List<string>)Session["idsvehi"]; }

            ////var v = (from x in ids
            ////         where x.Split('|')[1] == ModeloTmp.busbarco_rnp_matricula
            ////         select x.Split('|')[0]).FirstOrDefault();
            ////ModeloTmp.busbarco_rnp_matricula_idVehiculo = v;

            if (ModeloTmp.busbarco_rnp_matricula_idVehiculo == "" || ModeloTmp.busbarco_rnp_matricula_idVehiculo == null)
            {
                ModeloTmp.ErroresAgregarEmbarcacion = "Debe ingresar al menos una Embarcación";
            }
            else
            {
                bool busqXfiltro = false;
                string DatosaBuscar = "";
                if (ModeloTmp.busbarco_rnp_matricula != null && ModeloTmp.busbarco_rnp_matricula.Length > 0)
                {
                    string[] elements = ModeloTmp.busbarco_rnp_matricula.Split('/');
                    if (busqXfiltro == false) DatosaBuscar += " V.IdVehiculo in ('" + FunProc.SplitCadenaAplicaTrim(ModeloTmp.busbarco_rnp_matricula_idVehiculo, ',').Replace(",", "','") + "') ";
                    if (busqXfiltro == true) DatosaBuscar += " AND V.IdVehiculo in ('" + FunProc.SplitCadenaAplicaTrim(ModeloTmp.busbarco_rnp_matricula_idVehiculo, ',').Replace(",", "','") + "') ";
                    busqXfiltro = true;
                }
                if (ModeloTmp.busbarco != null && ModeloTmp.busbarco.Length > 0)
                {
                    if (busqXfiltro == false) DatosaBuscar += " V.Nombre in ('" + FunProc.SplitCadenaAplicaTrim(ModeloTmp.busbarco, ',').Replace(",", "','") + "') ";
                    if (busqXfiltro == true) DatosaBuscar += " AND V.Nombre in ('" + FunProc.SplitCadenaAplicaTrim(ModeloTmp.busbarco, ',').Replace(",", "','") + "') ";
                    busqXfiltro = true;
                }
                if (ModeloTmp.busmatricula != null && ModeloTmp.busmatricula.Length > 0)    //busmatricula
                {
                    if (busqXfiltro == false) DatosaBuscar += " Matricula in ('" + FunProc.SplitCadenaAplicaTrim(ModeloTmp.busmatricula, ',').Replace(",", "','") + "') ";
                    if (busqXfiltro == true) DatosaBuscar += " AND Matricula in ('" + FunProc.SplitCadenaAplicaTrim(ModeloTmp.busmatricula, ',').Replace(",", "','") + "') ";
                    busqXfiltro = true;
                }
                if (ModeloTmp.busrnp != null && ModeloTmp.busrnp.Length > 0)//busmatricula
                {
                    if (busqXfiltro == false) DatosaBuscar += " RNP in ('" + FunProc.SplitCadenaAplicaTrim(ModeloTmp.busrnp, ',').Replace(",", "','") + "') ";
                    if (busqXfiltro == true) DatosaBuscar += " AND RNP in ('" + FunProc.SplitCadenaAplicaTrim(ModeloTmp.busrnp, ',').Replace(",", "','") + "') ";
                    busqXfiltro = true;
                }
                if (busqXfiltro == true)
                {
                    DatosaBuscar = " AND ( " + DatosaBuscar + " ) ";
                    ModeloTmp.identrada = ModeloTmp.SolInfoRelacionInsertarTMPSP_returnID2(DatosaBuscar, ModeloTmp.identrada, UserName);
                    ModeloTmp.ResultadoListaSolInfoRelacionSPTMP = ModeloTmp.SolInfoRelacionInsertarTMPSP_Result(ModeloTmp.identrada);
                }

                ModelState.Clear();
                ModeloTmp.busbarco = "";
                ModeloTmp.busrnp = "";
                ModeloTmp.busmatricula = "";
                ModeloTmp.busbarco_rnp_matricula = "";
            }
            return Content(JsonConvert.SerializeObject(ModeloTmp), "application/json");
        }
        #endregion

        //
        // GET: /SolicitudDeInformacion/Delete/5
        public ActionResult Delete(int id = 0)
        {
            TMasSolInfoCedulas tmassolinfocedulas = null;// db.TMasSolInfoCedulas.Single(t => t.IdInfoSol == id);
            //if (tmassolinfocedulas == null)
            //{
            //    return StatusCode(404);
            //}
            return View(tmassolinfocedulas);
        }

        //
        // POST: /SolicitudDeInformacion/Delete/5
        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            TMasSolInfoCedulas tmassolinfocedulas = null;// db.TMasSolInfoCedulas.Single(t => t.IdInfoSol == id);
            //db.TMasSolInfoCedulas.Remove(tmassolinfocedulas);
            //db.SaveChanges();
            return RedirectToAction("Index");
        }

        #region "EXPORTAR A WORD"
        //

    
        private readonly IHostingEnvironment _env;

        public SolicitudDeInformacionController(IHostingEnvironment env)
        {
            _env = env;
        }

        public ActionResult GeneraSolicitudDeInformacionWord(int id)
        {
            SolicitudesDeInformacionModel ModeloTmp = new SolicitudesDeInformacionModel();
            TMasSolInfoCedulas Datos = null;// db.TMasSolInfoCedulas.Single(t => t.IdInfoSol == id);
            //ModeloTmp.TMASSOLINFOCEDULAS = tmassolinfocedulas;
            List<SolInfoRelacionInsertarResult_Result> ListaEmbarcaciones = new List<SolInfoRelacionInsertarResult_Result>();
            ListaEmbarcaciones = ModeloTmp.SolInfoRelacionInsertarSP_Result(id);
            DocX doc;

            var webRoot = _env.WebRootPath;
            

            string Plantilla = "FormatoInformacionTemp";
            var fileTemp = System.IO.Path.Combine(webRoot, "/Templates/SolicitudDeInformacion/" + Plantilla + ".docx");
            string ruta = fileTemp;//Server.MapPath("~/Templates/SolicitudDeInformacion/" + Plantilla + ".docx");
            var fileDestino = System.IO.Path.Combine(webRoot, "Templates/GENERADOS/" + Plantilla + " " + FunProc.ObtieneIdentificadorFechaHora() + ".docx");
            string rutadestino = fileDestino; // Server.MapPath("~/Templates/GENERADOS/" + Plantilla + " " + FunProc.ObtieneIdentificadorFechaHora() + ".docx");
            if (System.IO.File.Exists(ruta) == true)
            {
                doc = DocX.Load(ruta);
                if (Datos != null)
                {
                    #region "ASIGNA VALOR A LOS CAMPOS CUANDO ES NULO"
                    if (Datos.Folio == null) Datos.Folio = "";
                    if (Datos.Asunto == null) Datos.Asunto = "";
                    if (Datos.Fecha == null) Datos.Fecha = "";
                    if (Datos.Hora == null) Datos.Hora = "";
                    if (Datos.Solicitante == null) Datos.Solicitante = "";
                    if (Datos.Cargo == null) Datos.Cargo = "";
                    if (Datos.Dependencia == null) Datos.Dependencia = "";
                    if (Datos.Ciudad == null) Datos.Ciudad = "";
                    if (Datos.Estado == null) Datos.Estado = "";
                    if (Datos.Telefono == null) Datos.Telefono = "";
                    if (Datos.Email == null) Datos.Email = "";
                    if (Datos.Medio == null) Datos.Medio = "";
                    if (Datos.Descripcion == null) Datos.Descripcion = "";
                    if (Datos.OFP == null) Datos.OFP = "";
                    #endregion

                    #region "LLENA LOS CAMPOS EN EL DOCUMENTO WORD"
                    doc.ReplaceText("«FOLIO»", Datos.Folio);
                    doc.ReplaceText("«ASUNTO»", Datos.Asunto);
                    doc.ReplaceText("«FECHA»", Datos.Fecha);
                    doc.ReplaceText("«HORA»", Datos.Hora);
                    doc.ReplaceText("«SOLICITANTE»", Datos.Solicitante);
                    doc.ReplaceText("«CARGO»", Datos.Cargo);
                    doc.ReplaceText("«DEPENDENCIA»", Datos.Dependencia);
                    doc.ReplaceText("«CIUDAD»", Datos.Ciudad);
                    doc.ReplaceText("«ESTADO»", Datos.Estado);
                    doc.ReplaceText("«TELEFONO»", Datos.Telefono);
                    doc.ReplaceText("«EMAIL»", Datos.Email);
                    doc.ReplaceText("«MEDIO»", Datos.Medio);
                    doc.ReplaceText("«DESCRIPCION»", Datos.Descripcion);
                    doc.ReplaceText("«OFP»", Datos.OFP);

                    //TOTALBARCOS    "SELECT Count(RNP) FROM MAS_SolInfoRelacion WHERE IdEntrada = " & varPubGral & ""
                    //BARCOSTX       "SELECT Count(RNP) FROM MAS_SolInfoRelacion WHERE Transmitiendo = 'Si' AND IdEntrada = " & varPubGral & ""
                    //BARCOSNOTX     TOTAL - TX
                    int TOTAL = 0;
                    int TX = 0;
                    int NOTX = 0;
                    ReporteSolInfoTotalBarcos_Result DatosExtras = new ReporteSolInfoTotalBarcos_Result();
                    DatosExtras = null;// db.ReporteSolInfoTotalBarcos(id).FirstOrDefault();
                    if (DatosExtras != null)
                    {
                        TOTAL = Convert.ToInt32(DatosExtras.TOTAL);
                        TX = Convert.ToInt32(DatosExtras.TX);
                        NOTX = Convert.ToInt32(DatosExtras.NOTX);
                    }
                    doc.ReplaceText("«TOTALBARCOS»", TOTAL.ToString());
                    doc.ReplaceText("«BARCOSTX»", TX.ToString());
                    doc.ReplaceText("«BARCOSNOTX»", NOTX.ToString());

                    #endregion

                    #region "CREA E INSERTA LA TABLA EN EL DOCUMENTO WORD"
                    //      LLENA LA RELACION DE EMBARCACIONES
                    if (ListaEmbarcaciones.Count > 0)
                    {
                        int totaltablas = doc.Tables.Count;
                        // Empty table
                        Table TablaVacia = doc.Tables[1];
                        // New table
                        Table TablaNueva = doc.AddTable(ListaEmbarcaciones.Count + 1, 6);
                        TablaNueva.Alignment = Alignment.center;
                        TablaNueva.Design = TableDesign.TableGrid;


                        //      CREA LA CABECERA                          Embarcacion
                        TablaNueva.Rows[0].Cells[0].Paragraphs.First().Append("Embarcacion").Font(("Arial")).FontSize(9).Bold();
                        TablaNueva.Rows[0].Cells[1].Paragraphs.First().Append("Tx").Font(("Arial")).FontSize(9).Bold();
                        TablaNueva.Rows[0].Cells[2].Paragraphs.First().Append("Fecha Ultima Tx").Font(("Arial")).FontSize(9).Bold();
                        TablaNueva.Rows[0].Cells[3].Paragraphs.First().Append("En Puerto").Font(("Arial")).FontSize(9).Bold();
                        TablaNueva.Rows[0].Cells[4].Paragraphs.First().Append("Nombre Puerto").Font(("Arial")).FontSize(9).Bold();
                        TablaNueva.Rows[0].Cells[5].Paragraphs.First().Append("Permiso(s) Vigencia(s)").Font(("Arial")).FontSize(9).Bold();
                        //      CREA EL CUERPO DE LA TABLA
                        int cont = 1;
                        foreach (var Rel in ListaEmbarcaciones)
                        {
                            #region "ASIGNA VALOR A LOS CAMPOS CUANDO ES NULO"
                            if (Rel.Barco == null) Rel.Barco = "";
                            if (Rel.Transmitiendo == null) Rel.Transmitiendo = "";
                            if (Rel.EnPuerto == null) Rel.EnPuerto = "";
                            if (Rel.NombrePuerto == null) Rel.NombrePuerto = "";
                            if (Rel.Permisos == null) Rel.Permisos = "";
                            #endregion

                            TablaNueva.Rows[cont].Cells[0].Paragraphs.First().Append(Rel.Barco).Font(("Arial")).FontSize(8);
                            TablaNueva.Rows[cont].Cells[1].Paragraphs.First().Append(Rel.Transmitiendo.Trim()).Font(("Arial")).FontSize(8);   //DateTime.Now.ToString("dd-MM-yyyy") 
                            TablaNueva.Rows[cont].Cells[2].Paragraphs.First().Append(Rel.UltimaTx).Font(("Arial")).FontSize(8);
                            TablaNueva.Rows[cont].Cells[3].Paragraphs.First().Append(Rel.EnPuerto).Font(("Arial")).FontSize(8);
                            TablaNueva.Rows[cont].Cells[4].Paragraphs.First().Append(Rel.NombrePuerto).Font(("Arial")).FontSize(8);

                            Rel.Permisos = Rel.Permisos.Replace("<font color=red>", "");
                            Rel.Permisos = Rel.Permisos.Replace("</font>", "");
                            Rel.Permisos = Rel.Permisos.Replace("</BR></BR>", "\r\n\r\n");
                            Rel.Permisos = Rel.Permisos.Replace("</BR>", "\n");


                            TablaNueva.Rows[cont].Cells[5].Paragraphs.First().Append(Rel.Permisos).Font(("Arial")).FontSize(8);

                            cont += 1;
                        }
                        TablaNueva.AutoFit = AutoFit.Contents;
                        //TablaNueva.Alignment = Alignment.both;
                        //TablaNueva.Rows[1].Cells[1].Width
                        Table newTable = TablaVacia.InsertTableAfterSelf(TablaNueva);
                        TablaVacia.Remove();
                    }
                    else
                    {//     SI NO HAY ELEMENTOS EN LA LISTA , SE BORRA
                        Table TablaVacia = doc.Tables[1];
                        TablaVacia.Remove();
                    }
                    #endregion                    
                }

                doc.SaveAs(rutadestino);
            }
            else
            {
                doc = DocX.Create(ruta);
                string NoExisteLaPlantilla = "";
                //CREA UN DOCUMENTO NUEVO
                doc.Save();
            }

            return new WordResult(doc, "Reporte " + Plantilla + " " + FunProc.ObtieneIdentificadorFechaHora());

        }
        #endregion

        #region "EXPORTAR A PDF"
        public ActionResult GeneratePdfSolicitudDeInformacion()
        {
            List<BuscaSolicitudesDeInformacion_Result> ListaTmp = new List<BuscaSolicitudesDeInformacion_Result>();
            ListaTmp = null;// HttpContext.Session["SessionResultadoListaSolInfoSPTmp"] as List<BuscaSolicitudesDeInformacion_Result>;
            var pdf = "";// new PdfResult(ListaTmp, "GeneratePdfSolicitudDeInformacion");
            return null;// pdf;
        }
        #endregion

        #region "EXPORTAR A EXCEL"

        public ActionResult GeneraReporteSolicitudDeInformacionExportarBusqueda()
        {
            string UserName = "";
            if (HttpContext.Request.Cookies["userName"] != null)
                UserName = HttpContext.Request.Cookies["userName"].ToString();
            //      Recupera los datos que se insertaran en el archivo excel
            List<BuscaSolicitudesDeInformacion_Result> ListaTmp = new List<BuscaSolicitudesDeInformacion_Result>();
            ListaTmp = null;// Session["SessionResultadoListaSolInfoSPTmp"] as List<BuscaSolicitudesDeInformacion_Result>;




            //      Obtiene la ruta de la plantilla
            string ruta = "";// Server.MapPath("~/Templates/SolicitudDeInformacion/ExportarTodo.xlsx");
            //      Abre el libro de excel..
            var workbook = new XLWorkbook(ruta);
            //      Selecciona la hoja 1 del libro
            var worksheet = workbook.Worksheet(1);
            worksheet.Cell("A8").Value = "Impreso por: " + UserName + " | Fecha: " + DateTime.Now.ToShortDateString();
            string ParametrosDeBusqueda = "";
            ParametrosDeBusqueda = "";//Session["SessionParametrosDeBusqueda"] as string;
            worksheet.Cell("A9").Value = ParametrosDeBusqueda;
            if (ListaTmp != null)
            {

                #region "CREA LA CABECERA"
                ////////////List<string> Cabecera = new List<string>();
                ////////////Cabecera.Add("Folio");
                ////////////Cabecera.Add("Fecha");
                ////////////Cabecera.Add("Solicitante");
                ////////////Cabecera.Add("Cargo");
                ////////////Cabecera.Add("No. Total de Barcos");
                ////////////Cabecera.Add("Telefono");

                //////////////Crea la cabecera
                ////////////int RenglonIniciaCabecera = 5;
                ////////////int ColumnaIniciaCabecera = 1;
                ////////////for (var i = 0 ; i <= Cabecera.Count -1; i++)
                ////////////{

                ////////////    worksheet.Cell(RenglonIniciaCabecera, ColumnaIniciaCabecera + i).Value = Cabecera[i].ToString();
                ////////////}

                ////////////var rngHeaders = worksheet.Range(RenglonIniciaCabecera, ColumnaIniciaCabecera, RenglonIniciaCabecera, Cabecera.Count);

                ////////////rngHeaders.Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
                ////////////rngHeaders.Style.Font.Bold = true;
                ////////////rngHeaders.Style.Font.FontColor = XLColor.DarkBlue;
                ////////////rngHeaders.Style.Fill.BackgroundColor = XLColor.Aqua;
                #endregion

                #region "AGREGA VALORES"
                worksheet.Cell("A12").Value = ListaTmp;
                ////////worksheet.Cell("A5").Value = ListaTmp;
                //worksheet.Cell(5, 1).Value = ListaTmp;
                //worksheet.Columns().AdjustToContents();
                #endregion
            }



            return new ExcelResult(workbook, "Exportar Búsqueda Solicitudes de Informacion");
        }

        #endregion

        #region "AUTOCOMPLETE Y LLAMADAS ASYNC DESDE PAGINA"

        public string SearchResultadoGenerico_matriculabarco(string q, int limit)
        {
            var result = "";// FunProc.FindRsultadoGenerico(q, limit, "matriculabarco");
            return String.Join(Environment.NewLine, result);
        }

        //public ActionResult SearchResultadoGenerico_rnpdelbarco(DataSourceLoadOptions loadOptions)
        //{
        //    var criterio = (from opciones in ((JArray)loadOptions.Filter[0]).ToList<dynamic>()
        //                    where opciones.Value != "this" && opciones.Value != "contains"
        //                    select opciones.Value).FirstOrDefault();

        //    return null;// Content(JsonConvert.SerializeObject(DataSourceLoader.Load(FunProc.FindRsultadoGenerico(criterio, 0, "rnpdelbarco"), loadOptions)), "application/json");
        //}

        //public ActionResult SearchResultadoGenerico_nombrebarco(DataSourceLoadOptions loadOptions)
        //{
        //    var criterio = (from opciones in ((JArray)loadOptions.Filter[0]).ToList<dynamic>()
        //                    where opciones.Value != "this" && opciones.Value != "contains"
        //                    select opciones.Value).FirstOrDefault();

        //    return null;//Content(JsonConvert.SerializeObject(DataSourceLoader.Load(FunProc.FindRsultadoGenerico(criterio, 0, "nombrebarco"), loadOptions)), "application/json");
        //}

        public String SearchResultadoGenerico_nombre_rnp_matriculabarco(string q, int limit)
        {
            List<string> barcos = new List<string>();
            var result = "";//FunProc.FindRsultadoGenerico(q, limit, "nombre_rnp_matriculabarco");
            foreach (var item in result)
            {
                //barcos.Add(item.Split('|')[1].ToString());
            }
            return String.Join(Environment.NewLine, barcos);

        }

        public List<string> ObtenerEmbarcacionesNombreRNP()
        {
            var result = "";//FunProc.FindRsultadoGenerico("nombre_rnpBarco");
            List<string> barcos = new List<string>();
            foreach (var item in result)
            {
                //barcos.Add(item.Split('|')[1].ToString());
            }
            return barcos;
        }

        //[HttpGet]
        //public ActionResult SearchResultadoGenerico_nombre_rnpbarco(DataSourceLoadOptions loadOptions)
        //{
        //    if (loadOptions.Filter != null)
        //    {
        //        List<string> barcos = new List<string>();
        //        var criterio = (from opciones in ((JArray)loadOptions.Filter[0]).ToList<dynamic>()
        //            where opciones.Value != "this" && opciones.Value != "contains"
        //            select opciones.Value).FirstOrDefault();
        //        var result = "";//FunProc.FindRsultadoGenerico(criterio, 0, "nombre_rnpBarco");
        //        foreach (var item in result)
        //        {
        //            //barcos.Add(item.Split('|')[1].ToString());
        //        }
        //        return Content(JsonConvert.SerializeObject(DataSourceLoader.Load(barcos, loadOptions)), "application/json");
        //    }
        //    else
        //    {
        //        return Content(string.Empty);
        //    }
        //}

        [HttpGet]
        public ActionResult SearchResultadoGenerico_idVehiculo_nombre_rnpbarcoJSON()
        {
            List<barco> barcos = new List<barco>();
            var result = "";// db.BuscaResultadoGenericoSP("nombre_rnpBarco").Select(c => c.Resultado).ToList();
            //var result = FunProc.FindRsultadoGenerico("", 0, "nombre_rnp_matriculabarco");
            foreach (var item in result)
            {
                barco b = new barco();
                //b.idVehiculo = item.Split('|')[0].ToString();
                //b.Descripcion = item.Split('|')[1].ToString();
                barcos.Add(b);
            }

            return null;//Json(barcos, JsonRequestBehavior.AllowGet);
        }

        private class barco
        {
            public string idVehiculo { get; set; }
            public string Descripcion { get; set; }
        }

        //[HttpGet]
        //public ActionResult SearchResultadoGenerico_solicitante(DataSourceLoadOptions loadOptions)
        //{
        //    var criterio = (from opciones in ((JArray)loadOptions.Filter[0]).ToList<dynamic>()
        //                    where opciones.Value != "this" && opciones.Value != "contains"
        //                    select opciones.Value).FirstOrDefault();

        //    return null;//Content(JsonConvert.SerializeObject(DataSourceLoader.Load(FunProc.FindRsultadoGenerico(criterio, 0, "solicitante"), loadOptions)), "application/json");
        //}

        [HttpPost]
        public ActionResult CIUDADES(string idCiudad)
        {
            //var ciudades = autoRepositorio.GetModelos(id);
            //var ciudades = new SelectList(FunProc.BuscaResultadoGenericoConParametrosLista("CIUDAD", "", id), "Resultado", "Resultado");
            var ciudades2 = "";//FunProc.BuscaResultadoGenericoConParametrosLista("CIUDADsolinfo", "", idCiudad);
            return null;//Json(new SelectList(ciudades2, "Resultado", "Resultado"));
        }

        public JsonResult getinfosolicitantes(string NomSolicitante)
        {
            List<BuscaInformacionDeSolicitante_Result> ListaInformacionDeSolcitante = new List<BuscaInformacionDeSolicitante_Result>();
            ListaInformacionDeSolcitante = null;//db.BuscaInformacionDeSolicitante(NomSolicitante).ToList();
            return null;//Json(ListaInformacionDeSolcitante, JsonRequestBehavior.AllowGet);
        }

        //Agregado por SZ el 06/12/2016
        [HttpPost]
        public ActionResult eliminaRegistros(string idEntrada, string RNP)
        {
            bool status = false;
            status = false;//FunProc.EliminaSolInfoRel(idEntrada, RNP);
            return RedirectToAction("Edit", "SolicitudDeInformacion", new { id = idEntrada });
            //return RedirectToAction("Details", "SolicitudDeInformacion", new { id = ModeloTmp.IdInfoSol });
        }

        //AGREGADO POR SZ EL 09/01/2016
        //PERMITE ELIMINAR REGISTROS AL DAR CLICK EN EL ENLACE DE REGRESAR.
        public ActionResult eliminarRegistrosxIdEntrada()
        {
            int idEntrada = 0;//Int32.Parse(Session["Identificador"].ToString());
            List<String> lista = null;//db.TMasSolInfoRelacionTmp.Where(x => x.IdEntrada == idEntrada).Select(x => x.RNP).ToList<String>();
            foreach (string s in lista)
            {
               // FunProc.EliminaSolInfoRel(idEntrada.ToString(), s);
            }
            return RedirectToAction("Index", "SolicitudDeInformacion");

        }
        //Fin de Agregado

        //public JsonResult ObtieneMarcadoresEarthAsyncJson(string Litoral, string Reporte, string Permiso)
        //{            
        //    List<MarcadoresEarth> ListaMarcadores = new List<MarcadoresEarth>();
        //    ListaMarcadores = FunProc.ObtieneMarcadoresDetalleAsync(Litoral, Reporte, Permiso);
        //    return Json(ListaMarcadores, JsonRequestBehavior.AllowGet);
        //}
        #endregion

        protected override void Dispose(bool disposing)
        {
            //db.Dispose();
            base.Dispose(disposing);
        }

        public void actualizaRegistro(int idEntrada, string RNP)
        {
            string idVehiculo = getIdVehiculo(RNP.Trim());
            
            if(idVehiculo != "")
            {
                string Usuario = "";
                string DatosaBuscar = " AND V.IdVehiculo = '" + idVehiculo + "'";

                if (HttpContext.Request.Cookies["userName"] != null)
                    Usuario = HttpContext.Request.Cookies["userName"].ToString();

                SolicitudesDeInformacionModel sim = new SolicitudesDeInformacionModel();
                sim.SolInfoRelacionInsertarDefinitivoSP_returnID2(DatosaBuscar, idEntrada, Usuario);
            }
        }

        private string getIdVehiculo(string rnp)
        {
            string idVehiculo = "";
            var Vehiculos = "";//FunProc.LlenarResultadoGenerico("nombre_rnp_matriculabarco");
            foreach (var item in Vehiculos)
            {
                //string _idVehiculo = item.Resultado.Split('|')[0];
                //string _rnp = item.Resultado.Split('|')[1].Split('/')[2].ToString().Trim();

                //if (_rnp == rnp)
                //    idVehiculo = _idVehiculo;
            }
            return idVehiculo;
        }

        //[HttpGet]
        //public ActionResult GetAsuntos(DataSourceLoadOptions loadOptions)
        //{
        //    return null;//Content(JsonConvert.SerializeObject(DataSourceLoader.Load(FunProc.LlenarResultadoGenerico("asunto"), loadOptions)), "application/json");

        //}

        //[HttpGet]
        //public ActionResult GetOFPs(DataSourceLoadOptions loadOptions)
        //{
        //    return null;//Content(JsonConvert.SerializeObject(DataSourceLoader.Load(FunProc.LlenarResultadoGenerico("responsableofp"), loadOptions)), "application/json");

        //}

        //[HttpGet]
        //public ActionResult GetDependencias(DataSourceLoadOptions loadOptions)
        //{
        //    return null;//Content(JsonConvert.SerializeObject(DataSourceLoader.Load(FunProc.LlenarResultadoGenerico("dependencia"), loadOptions)), "application/json");

        //}

        //[HttpGet]
        //public ActionResult GetMedios(DataSourceLoadOptions loadOptions)
        //{
        //    return null;//Content(JsonConvert.SerializeObject(DataSourceLoader.Load(FunProc.LlenarResultadoGenerico("medio"), loadOptions)), "application/json");

        //}

        //[HttpGet]
        //public ActionResult GetEstados(DataSourceLoadOptions loadOptions)
        //{
        //    return null;//Content(JsonConvert.SerializeObject(DataSourceLoader.Load(FunProc.LlenarResultadoGenerico("estadosolinfo"), loadOptions)), "application/json");
        //}

        //[HttpGet]
        //public ActionResult GetBarcos(DataSourceLoadOptions loadOptions)
        //{
        //    return null;//Content(JsonConvert.SerializeObject(DataSourceLoader.Load(FunProc.LlenarResultadoGenerico("embarcacion"), loadOptions)), "application/json");
        //}

        //[HttpGet]
        //public ActionResult GetCiudades(DataSourceLoadOptions loadOptions)
        //{
        //    return null;//Content(JsonConvert.SerializeObject(DataSourceLoader.Load(FunProc.BuscaResultadoGenericoConParametrosLista("CIUDAdsolinfo", "", ""), loadOptions)), "application/json");
        //}
    }
}