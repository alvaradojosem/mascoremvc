﻿using System;


namespace MASCore.ModelReference
{

    //
    // Summary:
    //     Represents a column in a System.Web.Helpers.WebGrid instance.

  
    public partial class BuscaMantenimientosResumenNegacion_Result
    {
        public string MatBarco { get; set; }
        public string NombreBarco { get; set; }
        public string RNP { get; set; }
        public string Permiso { get; set; }
        public string TipoPermiso { get; set; }
        public string RNPTitular { get; set; }
        public string PuertoBase { get; set; }
        public string SerieAntena { get; set; }
    }
    public partial class BuscaMantenimientos_Result
    {
        public string Folio { get; set; }
        public Nullable<System.DateTime> Fecha_Mantto { get; set; }
        public string Servicio { get; set; }
        public string NombreBarco { get; set; }
        public string Antena { get; set; }
        public string Matricula { get; set; }
        public string RNP { get; set; }
        public string PuertoBase { get; set; }
        public string RazonSocial { get; set; }
        public string Zona { get; set; }
        public string Supervisor { get; set; }
        public string Recibe { get; set; }
    }



}
