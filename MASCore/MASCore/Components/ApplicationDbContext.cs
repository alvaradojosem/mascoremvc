﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace MASCore.Data
{
    public class ApplicationDbContext : IdentityDbContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }
    }

    //
    // Summary:
    //     The list of supported presentation frameworks for the default UI
    public enum UIFramework
    {
        //
        // Summary:
        //     Bootstrap 3
        Bootstrap3 = 0,
        //
        // Summary:
        //     Bootstrap 4
        Bootstrap4 = 1
    }
}
