﻿namespace MASCore.Models
{
    public class configuracion
    {
        public double timeoutCookie { get; set; }
        public string urlMiPerfil { get; set; }
        public string urlMisPermisos { get; set; }
        public string urlMisFiltros { get; set; }
        public string urlMisGrupos { get; set; }
        public string urlAdministracionUsuarios { get; set; }
        public string urlGuardarMiPerfil { get; set; }
        public string urlGuardarMisFiltros { get; set; }
        public string urlAdministracionGuardarGrupoMedidoresUsuario { get; set; }
        public string urlAdministracionGuardarUsuarioRol { get; set; }
        public string urlAdministracionGuardarUsuario { get; set; }
        public string tokenConf { get; set; }
        public string urlConfiguracion { get; set; }
        public string urlAdministracionConfs { get; set; }
        public string IV { get; set; }
        public string Simetric { get; set; }
        public string urlUsuarioIntentoRecuperacion { get; set; }
    }
}