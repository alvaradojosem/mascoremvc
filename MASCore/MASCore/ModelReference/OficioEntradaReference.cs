﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MASCore.ModelReference
{
    public class OficioEntradaReference
    {
    }

    public partial class BuscaOficioEntrada_Result
    {
        public int IdEntrada { get; set; }
        public string Estatus { get; set; }
        public string NoOficio { get; set; }
        public string TipoOficio { get; set; }
        public Nullable<System.DateTime> FechaDGIV { get; set; }
        public string HoraDGIV { get; set; }
        public Nullable<System.DateTime> FechaMon { get; set; }
        public string HoraMon { get; set; }
        public string Volante { get; set; }
        public string Asunto { get; set; }
        public string Turno { get; set; }
        public string Origen { get; set; }
        public string Minutario { get; set; }
        public string Comentarios { get; set; }
        public string Respuesta { get; set; }
    }
}
