﻿using System.IO;
using System.Runtime.CompilerServices;

namespace MASCore.ModelReference
{
 //
    // Summary:
    //     Serves as the base class for classes that provide access to individual files
    //     that have been uploaded by a client.
    [TypeForwardedFrom("System.Web.Abstractions, Version=3.5.0.0, Culture=Neutral, PublicKeyToken=31bf3856ad364e35")]
public abstract class HttpPostedFileBase
{
    //
    // Summary:
    //     Initializes the class for use by an inherited class instance. This constructor
    //     can only be called by an inherited class.
    //protected HttpPostedFileBase();

    //
    // Summary:
    //     When overridden in a derived class, gets the size of an uploaded file, in bytes.
    //
    // Returns:
    //     The length of the file, in bytes.
    //
    // Exceptions:
    //   T:System.NotImplementedException:
    //     Always.
    public virtual int ContentLength { get; }
    //
    // Summary:
    //     When overridden in a derived class, gets the MIME content type of an uploaded
    //     file.
    //
    // Returns:
    //     The MIME content type of the file.
    //
    // Exceptions:
    //   T:System.NotImplementedException:
    //     Always.
    public virtual string ContentType { get; }
    //
    // Summary:
    //     When overridden in a derived class, gets the fully qualified name of the file
    //     on the client.
    //
    // Returns:
    //     The name of the file on the client, which includes the directory path.
    //
    // Exceptions:
    //   T:System.NotImplementedException:
    //     Always.
    public virtual string FileName { get; }
    //
    // Summary:
    //     When overridden in a derived class, gets a System.IO.Stream object that points
    //     to an uploaded file to prepare for reading the contents of the file.
    //
    // Returns:
    //     An object for reading a file.
    //
    // Exceptions:
    //   T:System.NotImplementedException:
    //     Always.
    public virtual Stream InputStream { get; }

    //
    // Summary:
    //     When overridden in a derived class, saves the contents of an uploaded file.
    //
    // Parameters:
    //   filename:
    //     The name of the file to save.
    //
    // Exceptions:
    //   T:System.NotImplementedException:
    //     Always.
    //public virtual void SaveAs(string filename);
}
}
