﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MASCore.ParaBorrar
{
    public partial class BuscaCargaImagenes_Result
    {
        public int Id { get; set; }
        public string Identificador { get; set; }
        public string Rnp { get; set; }
        public string RutaBase { get; set; }
        public string Tipo { get; set; }
        public string Subtipo1 { get; set; }
        public string Subtipo2 { get; set; }
        public Nullable<System.DateTime> Fecha { get; set; }
        public string Consecutivo { get; set; }
    }
}
