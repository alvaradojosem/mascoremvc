﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace MASCore.Models
{
    public class SolicitudesWebVMBusqueda
    {
        public SolicitudesWebEntidadBusqueda EntidadBusqueda { get; set; }
        public List<SolicitudesWebEntidadResultadoBusqueda> ListaSolicitudesWeb { get; set; }
    }

    public class SolicitudesWebEntidadBusqueda
    {
        //a)	Rango de fechas
        public DateTime fechainicial { get; set; }
        public DateTime fechafinal { get; set; }
        //[RegularExpression(@"^\d*$:", ErrorMessage = "Solo numeros")]   
        //"^[0-9]+$"
        [Range(1, 10000000, ErrorMessage = "Solo numeros")]  
        public string NumerodeReporte { get; set; }               // IDReporte
                
        public string OficioCNP { get; set; }                   //NoOficio
        public string FoliodeMantenimiento { get; set; }        //Folio Pendiente de checar cual es ese
        public string Estado  { get; set; }                    //Estatus
        public string Barco { get; set; }
        public string Localidad { get; set; }
        public string Tipo { get; set; }                       //TIPO DE SOLICITUD
        public string RazonSocial { get; set; }
        public string Supervisor { get; set; }
    
    }
    public class SolicitudesWebEntidadResultadoBusqueda
    {
        public string NumerodeReporte { get; set; }               // IDReporte
        public string Fecha { get; set; }
        public string Hora { get; set; }       
        public string OficioCNP { get; set; }                   //NoOficio
        public string FoliodeMantenimiento { get; set; }        //Folio Pendiente de checar cual es ese
        public string Estado { get; set; }                    //Estatus
        public string Barco { get; set; }
        public string Localidad { get; set; }
        public string Tipo { get; set; }                       //TIPO DE SOLICITUD
        public string RazonSocial { get; set; }
        public string Supervisor { get; set; }
    }

    public class ResultadoDropDown
    {
        public string ResultadoId { get; set; }
        public string Resultado { get; set; }
    }

    public class SolicitudesWeb_Entidad 
    {
        public int Id_Reporte { get; set; }

        [Required(ErrorMessage = "* Requerido")]
        [DisplayName("Fecha")]
        [DataType(DataType.DateTime)]
        public DateTime Fecha { get; set; }
                
        [Required(ErrorMessage = "* Requerido")]
        //[RegularExpression(@"^([0-9]|0[0-9]|1[0-9]|2[0-3]):[0-5][0-9]$", ErrorMessage = "Formato a 24 Hrs. HH:MM")]
        public string Hora { get; set; }

        [Required(ErrorMessage = "* Requerido")]
        public string Tipo { get; set; } //(opción de lista o dropdown)  //TIPO DE SOLICITUD
        [Required(ErrorMessage = "* Requerido")]
        public string OficioCNP { get; set; }
        //[Required(ErrorMessage = "*")]
        //[RegularExpression(@"^\+?\d{1,3}?[- .]?\(?(?:\d{2,3})\)?[- .]?\d\d\d[- .]?\d\d\d\d$", ErrorMessage = "Numero de telefono no es valido")]
        public string Telefono { get; set; }
        //[Required(ErrorMessage = "*")]
        public string Contacto { get; set; }
        //[Required(ErrorMessage = "*")]
        public string Solicitante { get; set; }
        //[Required(ErrorMessage = "*")]
        public string Recibio { get; set; }
        [Required(ErrorMessage = "* Requerido")]
        public string Nombredelaembarcacion { get; set; }
        [Required(ErrorMessage = "* Requerido")]
        public string RNP { get; set; }
        [Required(ErrorMessage = "* Requerido")]
        public string RazonSocial { get; set; }
        //[Required(ErrorMessage = "*")]
        public string Localidad { get; set; }
        //[Required(ErrorMessage = "*")]
        public string Supervisor { get; set; }
        [Required(ErrorMessage = "* Requerido")]
        public string Comentarios { get; set; }
        public string datoJustificacion { get; set; } //V16
        public List<CargaImagenesVariasSolWeb> ListaImagenesCarga { get; set; }
    }

#region Cargar Imagenes Varias
    public class CargaImagenesVariasSolWeb
    {
        public int   Id	{ get; set; }
        public string Identificador { get; set; }    // IDReporte
        public string RutaBase	{ get; set; } 
        public string Carpeta	{ get; set; } 
        public DateTime  Fecha	{ get; set; } 
        public string Consecutivo	{ get; set; }
        public string Modulo { get; set; } 

    }
#endregion

#region ValidacionFechaHora 
    //V16
    public static class FechaHoraValidacion
    {
        public static ValidationResult validaFecha(DateTime fecha)
        {
            if (fecha <= DateTime.Now)
            {
                return ValidationResult.Success;
            }
            return new ValidationResult ("La fecha no debe ser mayor a la actual");
        }
    }
#endregion
}