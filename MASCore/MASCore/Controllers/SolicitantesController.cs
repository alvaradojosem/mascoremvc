﻿using DevExtreme.AspNet.Data;
using DevExtreme.AspNet.Mvc;
using MASCore.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using Microsoft.AspNetCore.Mvc;
using MASCore.DataModel;
using Microsoft.AspNetCore.Http;


namespace MASCore.Controllers
{

  
    public class SolicitantesController : Controller
    {
        //private BDMasContextEnt db = new BDMasContextEnt();
        FuncionesProcedimientos FunProc = new FuncionesProcedimientos();

      

        #region "ATRIBUTOS"
        private Byte[] _salt;
        #endregion 
        
        // GET: /Solicitantes/
        public ActionResult Index()
        {
            //return View(db.TMasSolicitantes.ToList()); // codigo Original

            TMasSolInfoCedulas TMSol = new TMasSolInfoCedulas();

            return View(TMSol);
        }

        public ActionResult BuscaSolicitantes()
        {
            SolicitantesModel ModeloTmp = new SolicitantesModel();
            //ModeloTmp.ResultadoListaSolicitantes = db.TMasSolicitantes.ToList();
            return View(ModeloTmp);
        }

        [HttpPost]
        public ActionResult BuscaSolicitantes(SolicitantesModel ModeloTmp)
        {
            List<TMasSolicitantes> ListaSolicitantes = new List<TMasSolicitantes>();
            TMasSolicitantes tmassolicitantes = new TMasSolicitantes();
            if (ModeloTmp.solicitante != null)
                //tmassolicitantes = db.TMasSolicitantes.Single(t => t.Nombre == ModeloTmp.solicitante);

            if (tmassolicitantes.Nombre != null)
                ListaSolicitantes.Add(tmassolicitantes);

            ModeloTmp.ResultadoListaSolicitantes = ListaSolicitantes;

            return View(ModeloTmp);
        }

        // GET: /Solicitantes/Details/5
        public ActionResult Details(int id = 0)
        {
            string UserName = "";
            if (HttpContext.Request.Cookies["userName"] != null)
                UserName = HttpContext.Request.Cookies["userName"];
            ViewBag.Valida7X24 = FunProc.ValidaPermisoUsuario7X24(UserName);
            TMasSolicitantes tmassolicitantes=null;// = db.TMasSolicitantes.Single(t => t.Id_Reg == id);
            if (tmassolicitantes == null)
            {
                return StatusCode(404); 
            }

            return View(tmassolicitantes); 
        }
        
        // GET: /Solicitantes/Create
        public ActionResult Create()
        {
            string UserName = "";
            if (HttpContext.Request.Cookies["userName"] != null)
            UserName = HttpContext.Request.Cookies["userName"];
            ViewBag.Valida7X24 = FunProc.ValidaPermisoUsuario7X24(UserName);
            //ViewBag.estado = FunProc.LlenarResultadoGenerico("estadosolinfo");
            //ViewBag.ciudad = FunProc.BuscaResultadoGenericoConParametrosLista("CIUDADsolinfo", "", "");
            //ViewBag.Litoral = FunProc.LlenarResultadoGenerico("litorales");

            List<TMasSolicitantes> ListaSolicitantes = new List<TMasSolicitantes>();
            TMasSolicitantes Solicitantes = new TMasSolicitantes();
            SolicitantesModel ModeloTmp = new SolicitantesModel();
            ModeloTmp.ResultadoListaSolicitantes = ListaSolicitantes;
            ModeloTmp.Solicitantes = Solicitantes;
            return View(ModeloTmp);
        }
        
        // POST: /Solicitantes/Create
        [HttpPost]
        public ActionResult Create(SolicitantesModel ModeloTmp, string Clave, string Estado, string Ciudad, string estatussolicitante, string estatusweb, string Litoral)
        {
            string UserName = "";
            ViewBag.NoValidoPor = string.Empty;
            if (HttpContext.Request.Cookies["userName"] != null)
                UserName = HttpContext.Request.Cookies["userName"];
            string permiso7x24 = FunProc.ValidaPermisoUsuario7X24(UserName);
            ViewBag.Valida7X24 = permiso7x24;
            //ViewBag.estado = FunProc.LlenarResultadoGenerico("estadosolinfo");
            //ViewBag.ciudad = FunProc.BuscaResultadoGenericoConParametrosLista("CIUDADsolinfo", "", "");
            //ViewBag.Litoral = FunProc.LlenarResultadoGenerico("litorales");

            #region "VALIDACIONES"
            Boolean Valido = true;
            //Agregado por SZ el 08/03/2017
            if (ModeloTmp.Solicitantes.Nombre == null || ModeloTmp.Solicitantes.Nombre.Length == 0)
            {
                ViewBag.NoValidoPor = "El Nombre es Requerido.";
                Valido = false;
            }
            else
            {
                //Agregado por SZ el 17/03/2017
                string nombre = ModeloTmp.Solicitantes.Nombre.ToUpper().Trim();
                //var listNombres = FunProc.LlenarResultadoGenerico("nombres").Where(b=> b.Resultado.Trim() == nombre).ToList();
                //if (listNombres.Count > 0) {
                //    ViewBag.NoValidoPor = "Ese Nombre ya existe, ingrese otro porfavor.";
                //    Valido = false;
                //} 
            }
            //Agregado por SZ el 08/03/2017
            if ((ModeloTmp.Solicitantes.UserName == null || ModeloTmp.Solicitantes.UserName.Length == 0))
            {
                if ((ViewBag.Valida7X24 == "1"))
                {
                    ViewBag.NoValidoPor = "El User Name es Requerido.";
                    Valido = false;
                }
            }
            else
            {
                //Agregado por SZ el 17/03/2017
                string usuario = ModeloTmp.Solicitantes.UserName.ToUpper().Trim();
                //var listUsers = FunProc.LlenarResultadoGenerico("usuarios").Where(b => b.Resultado.Trim() == usuario).ToList();
                //if (listUsers.Count > 0)
                //{
                //    ViewBag.NoValidoPor += "Ese User Name ya existe, ingrese otro porfavor.#-#";
                //    Valido = false;
                //}
            }
            //Agregado por SZ el 08/03/2017
            if ((ModeloTmp.Solicitantes.Password == null || ModeloTmp.Solicitantes.Password.Length == 0)&&(ViewBag.Valida7X24 == "1"))
            {
                ViewBag.NoValidoPor += "El Password es Requerido.#-#";
                Valido = false;
            }
            if (ModeloTmp.Solicitantes.Ciudad == null || ModeloTmp.Solicitantes.Ciudad.Length == 0)
            {
                ViewBag.NoValidoPor += "Seleccione Ciudad.#-#";
                Valido = false;
            }
            //AGREGADO POR SZ EL 26/12/2016
            if (ModeloTmp.Solicitantes.Telefono1 != null)
            {
                Regex Val = new Regex(@"^\d+(\.\d+)?$");
                if (Val.IsMatch(ModeloTmp.Solicitantes.Telefono1))
                {
                    if (ModeloTmp.Solicitantes.Telefono1.Length > 10)
                    {
                        ViewBag.NoValidoPor += "Ingrese solo 10 digitos para Telefono 1.#-#";
                    }
                }
                else
                {
                    ViewBag.NoValidoPor += "El Telefono 1 solo acepta números.#-#";
                    Valido = false;
                }
            }
            if (ModeloTmp.Solicitantes.Telefono2 != null)
            {
                Regex Val = new Regex(@"^\d+(\.\d+)?$");
                if (Val.IsMatch(ModeloTmp.Solicitantes.Telefono2))
                {
                    if (ModeloTmp.Solicitantes.Telefono2.Length > 10)
                    {
                        ViewBag.NoValidoPor += "Ingrese solo 10 digitos para Telefono 2.#-#";
                    }
                }
                else
                {
                    ViewBag.NoValidoPor += "El Telefono 2 solo acepta números.#-#";
                    Valido = false;
                }
            }
            if (ModeloTmp.Solicitantes.Celular != null)
            {
                Regex Val = new Regex(@"^\d+(\.\d+)?$");
                if (Val.IsMatch(ModeloTmp.Solicitantes.Celular))
                {
                    if (ModeloTmp.Solicitantes.Celular.Length > 10)
                    {
                        ViewBag.NoValidoPor += "Ingrese solo 10 digitos para Celular.#-#";
                    }
                }
                else
                {
                    ViewBag.NoValidoPor += "El Celular solo acepta números.#-#";
                    Valido = false;
                }
            }
            if (ModeloTmp.Solicitantes.CorreoInstitucional != null)
            {
                Regex Val = new Regex(@"^[_a-zA-Z0-9-]+(\.[_a-zA-Z0-9-]+)*@[a-zA-Z0-9-]+(\.[a-zA-Z0-9-]+)*(\.[a-zA-Z]{2,4})$");
                if (Val.IsMatch(ModeloTmp.Solicitantes.CorreoInstitucional)) { } else {
                    ViewBag.NoValidoPor += "El Correo Institucional no tiene el formato requerido.#-#";
                    Valido = false;
                }
            }
            if (ModeloTmp.Solicitantes.CorreoAlterno != null)
            {
                Regex Val = new Regex(@"^[_a-zA-Z0-9-]+(\.[_a-zA-Z0-9-]+)*@[a-zA-Z0-9-]+(\.[a-zA-Z0-9-]+)*(\.[a-zA-Z]{2,4})$");
                if (Val.IsMatch(ModeloTmp.Solicitantes.CorreoAlterno)) { } else {
                    ViewBag.NoValidoPor += "El Correo Alterno no tiene el formato requerido.#-#";
                    Valido = false;
                }
            }
            if (ModeloTmp.Solicitantes.Dependencia == null || ModeloTmp.Solicitantes.Dependencia.Length == 0)
            {
                ViewBag.NoValidoPor += "Ingrese la dependencia.#-#";
                Valido = false;
            }
            #endregion

            if (ModelState.IsValid && Valido)
            {
                if (ModeloTmp.Solicitantes.Dependencia != null || ModeloTmp.Solicitantes.Dependencia.Length > 0)
                {
                    ModeloTmp.Solicitantes.Dependencia = ModeloTmp.Solicitantes.Dependencia.ToUpper();
                }
                ModeloTmp.Solicitantes.PasswordFormat = 2;
                ModeloTmp.Solicitantes.PasswordSalt = "Y/HheFw59N+w6dk1wn7t4w==";
                //Aqui le decimos que nos convierta la clave o password a su formateado.
                if (ViewBag.Valida7X24 == "1")
                {
                    ModeloTmp.Solicitantes.Password = EncodePassword(ModeloTmp.Solicitantes.Password, ModeloTmp.Solicitantes.PasswordFormat.Value, ModeloTmp.Solicitantes.PasswordSalt);
                }
                else
                {
                    //si es un operador ponlo null.
                    ModeloTmp.Solicitantes.Password = null;
                }
                
                //db.TMasSolicitantes.Add(ModeloTmp.Solicitantes);
                //db.SaveChanges();
                return RedirectToAction("BuscaSolicitantes");
            }
            return View(ModeloTmp);
        }

        public ActionResult Edit(int id = 0)
        {
            string UserName = "";
            if (HttpContext.Request.Cookies["userName"] != null)
                UserName = HttpContext.Request.Cookies["userName"];
            ViewBag.Valida7X24 = FunProc.ValidaPermisoUsuario7X24(UserName);
            //ViewBag.estado = FunProc.LlenarResultadoGenerico("estadosolinfo");
            //ViewBag.ciudad = FunProc.BuscaResultadoGenericoConParametrosLista("CIUDADsolinfo", "", "");
            //ViewBag.Litoral = FunProc.LlenarResultadoGenerico("litorales");

            List<TMasSolicitantes> ListaSolicitantes = new List<TMasSolicitantes>();
            TMasSolicitantes Solicitantes = new TMasSolicitantes();
            SolicitantesModel ModeloTmp = new SolicitantesModel();
            ModeloTmp.ResultadoListaSolicitantes = ListaSolicitantes;
            Solicitantes =null; //db.TMasSolicitantes.Single(t => t.Id_Reg == id);
            //Session["Id"] = id;
            HttpContext.Session.SetString("Id",id.ToString());
           
            ModeloTmp.Solicitantes = Solicitantes;
            ModeloTmp.Solicitantes.Red1 = ModeloTmp.Solicitantes.Red1.Trim();
            ModeloTmp.Solicitantes.Red2 = ModeloTmp.Solicitantes.Red2.Trim();
            return View(ModeloTmp);
        }

        [HttpPost]
        public ActionResult Edit(SolicitantesModel ModeloTmp, Boolean CambiaPass, string Clave, string Estado, string Ciudad, string estatussolicitante, string estatusweb, string Litoral)
        {
            string UserName = "";
            if (HttpContext.Request.Cookies["userName"] != null)
                UserName = HttpContext.Request.Cookies["userName"];
            ViewBag.Valida7X24 = FunProc.ValidaPermisoUsuario7X24(UserName);
            //ViewBag.estado = FunProc.LlenarResultadoGenerico("estadosolinfo");
            //ViewBag.ciudad = FunProc.BuscaResultadoGenericoConParametrosLista("CIUDADsolinfo", "", "");
            //var nombres = FunProc.LlenarResultadoGenerico("nombresEdit");
            //var usuarios = FunProc.LlenarResultadoGenerico("usuariosEdit");
            //ViewBag.Litoral = FunProc.LlenarResultadoGenerico("litorales");

            #region "VALIDACIONES"
            Boolean Valido = true;
            ViewBag.NoValidoPor = string.Empty;

            if (ModeloTmp.Solicitantes.Nombre == null || ModeloTmp.Solicitantes.Nombre.Length == 0)
            {
                ViewBag.NoValidoPor = "El Nombre es Requerido.";
                Valido = false;
            }

            //var repetidoNom = (from n in nombres
            //                   where n.Resultado.Split('|')[1].Trim().ToUpper().Contains(ModeloTmp.Solicitantes.Nombre.Trim().ToUpper())
            //                   select n.Resultado).ToList();
            var repetidoNom = "";
            if (!(repetidoNom is null)) {
                var ids = (from n in repetidoNom
                               //where !n.Split('|')[0].Trim().Equals(ModeloTmp.Solicitantes.Id_Reg.ToString())
                           select n).ToList();

                var nombre = (from n in ids
                               //where n.Split('|')[1].Trim().Equals(ModeloTmp.Solicitantes.Nombre.Trim().ToUpper().ToString())
                           select n).ToList();


                if (nombre.Count > 0)
                {
                    ViewBag.NoValidoPor += "Ese Nombre ya existe, ingrese otro porfavor.#-#";
                    Valido = false;
                }
            }

            if (ModeloTmp.Solicitantes.UserName == null || ModeloTmp.Solicitantes.UserName.Length == 0)
            {
                if (ViewBag.Valida7X24 == "1")
                {
                    ViewBag.NoValidoPor += "El User Name es Requerido.#-#";
                    Valido = false;
                }
            }

            //var repetidoUsr = (from n in usuarios
            //                   where n.Resultado.Split('|')[1].Trim().ToUpper().Contains(ModeloTmp.Solicitantes.UserName.Trim().ToUpper())
            //                   select n.Resultado).ToList();
            var repetidoUsr = "";
            if (!(repetidoUsr is null))
            {
                var ids = (from n in repetidoUsr
                           //where !n.Split('|')[0].Trim().Equals(ModeloTmp.Solicitantes.Id_Reg.ToString())
                           select n).ToList();
                if (ids.Count > 0)
                {
                    ViewBag.NoValidoPor += "Ese User Name ya existe, ingrese otro porfavor.#-#";
                    Valido = false;
                }
            }

            if (ModeloTmp.Solicitantes.Ciudad == null || ModeloTmp.Solicitantes.Ciudad.Length == 0) {
                ViewBag.NoValidoPor += "Seleccione Ciudad.#-#";
                Valido = false;
            }

            if (ModeloTmp.Solicitantes.Dependencia == null || ModeloTmp.Solicitantes.Dependencia.Length == 0) {
                ViewBag.NoValidoPor += "Ingrese la dependencia.#-#";
                Valido = false;
            }

            if (CambiaPass == true && Clave.Length == 0)
            {
                ViewBag.NoValidoPor += "El password no puede ser vacio.#-#";
                Valido = false;
            }
            ModeloTmp.Solicitantes.Red1 = ModeloTmp.Solicitantes.Red1.Trim();
            ModeloTmp.Solicitantes.Red2 = ModeloTmp.Solicitantes.Red2.Trim();
            if (ModeloTmp.Solicitantes.Red1 != null && ModeloTmp.Solicitantes.Red1.Length > 10)
            {
                ViewBag.NoValidoPor += "Red 1 no puede tener mas de 10 digitos.#-#";
                Valido = false;
            }
            if (ModeloTmp.Solicitantes.Red2 != null && ModeloTmp.Solicitantes.Red2.Length > 10)
            {
                ViewBag.NoValidoPor += "Red 2 no puede tener mas de 10 digitos.#-#";
                Valido = false;
            }
            if (ModeloTmp.Solicitantes.Celular != null && ModeloTmp.Solicitantes.Celular.Length > 15)
            {
                ViewBag.NoValidoPor += "Celular no puede tener mas de 15 digitos.#-#";
                Valido = false;
            }
            if (ModeloTmp.Solicitantes.Telefono1 != null && ModeloTmp.Solicitantes.Telefono1.Length > 15)
            {
                ViewBag.NoValidoPor += "Telefono 1 no puede tener mas de 15 digitos.#-#";
                Valido = false;
            }
            if (ModeloTmp.Solicitantes.Telefono2 != null && ModeloTmp.Solicitantes.Telefono2.Length > 15)
            {
                ViewBag.NoValidoPor += "Telefono 2 no puede tener mas de 15 digitos.#-#";
                Valido = false;
            }
            #endregion

            if (ModelState.IsValid && Valido == true)
            {
                //GUARDA LA INFORMACION
                ModeloTmp.Solicitantes.PasswordFormat = 2;
                ModeloTmp.Solicitantes.PasswordSalt = "Y/HheFw59N+w6dk1wn7t4w==";
                if (ModeloTmp.Solicitantes.Dependencia != null && ModeloTmp.Solicitantes.Dependencia.Length > 0)
                {
                    ModeloTmp.Solicitantes.Dependencia = ModeloTmp.Solicitantes.Dependencia.ToUpper();
                }
                if (CambiaPass == true)
                {
                    if (Clave.Length > 0)
                    {
                        String passwdFromDB = ModeloTmp.Solicitantes.Password;
                        Int32 passwordFormat = 0;
                        Int32.TryParse(ModeloTmp.Solicitantes.PasswordFormat.ToString(), out passwordFormat);
                        String salt = ModeloTmp.Solicitantes.PasswordSalt;
                        String encodedPasswd = EncodePassword(Clave, passwordFormat, salt);
                        ModeloTmp.Solicitantes.Password = encodedPasswd;
                    }
                }
                //db.TMasSolicitantes.Attach(ModeloTmp.Solicitantes);
                ////Se cambió SZ 19/1072017
                //db.Entry(ModeloTmp.Solicitantes).State = EntityState.Modified;
                //db.SaveChanges();
                return RedirectToAction("Details", "Solicitantes", new { id = ModeloTmp.Solicitantes.Id_Reg });
            }
            return View(ModeloTmp);
        }
        
        // GET: /Solicitantes/Delete/5
        public ActionResult Delete(int id = 0)
        {
            TMasSolicitantes tmassolicitantes = null;// db.TMasSolicitantes.Single(t => t.Id_Reg == id);
            if (tmassolicitantes == null)
            {
                return StatusCode(404);
            }
            return View(tmassolicitantes);
        }
        
        // POST: /Solicitantes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            TMasSolicitantes tmassolicitantes = null;// db.TMasSolicitantes.Single(t => t.Id_Reg == id);
            //db.TMasSolicitantes.Remove(tmassolicitantes);
            //db.SaveChanges();
            return RedirectToAction("Index");
        }

        #region "CONVIERTE PASS"
        internal String EncodePassword(String pass, Int32 passwordFormat, String salt)
        {
            if (passwordFormat == 0) // MembershipPasswordFormat.Clear
                return pass;
            Byte[] bIn = Encoding.Unicode.GetBytes(pass);
            _salt = Convert.FromBase64String(salt);
            Byte[] bAll = new Byte[_salt.Length + bIn.Length];
            Byte[] bRet = null;

            Buffer.BlockCopy(_salt, 0, bAll, 0, _salt.Length);
            Buffer.BlockCopy(bIn, 0, bAll, _salt.Length, bIn.Length);
            //if (passwordFormat == 1)
            //{ // MembershipPasswordFormat.Hashed
            //    HashAlgorithm s = HashAlgorithm.Create(Membership.HashAlgorithmType);
            //    bRet = s.ComputeHash(bAll);
            //}
            //else
                bRet = EncryptPassword(bAll);
            //bRet = EncryptPassword(Encoding.Unicode.GetBytes(pass));
            return Convert.ToBase64String(bRet);
        }

   

        private Byte[] EncryptPassword(Byte[] password)
        {
            TripleDESCryptoServiceProvider tdes = new TripleDESCryptoServiceProvider();
            tdes.Key = _salt;
            tdes.Mode = CipherMode.ECB;
            tdes.Padding = PaddingMode.PKCS7;

            ICryptoTransform cTransform = tdes.CreateEncryptor();
            //transform the specified region of bytes array to resultArray
            byte[] resultArray =
            cTransform.TransformFinalBlock(password, 0,
            password.Length);
            //Release resources held by TripleDes Encryptor
            tdes.Clear();
            return resultArray;
            //Return the encrypted data into unreadable string format
            //return Convert.ToBase64String(resultArray, 0, resultArray.Length);
        }

        #endregion

        #region Autocompletar
        [HttpGet]
        public ActionResult SearchResultadoGenerico_dependencia(DevExtreme.AspNet.Mvc.DataSourceLoadOptions loadOptions)
        {
            var criterio = (from opciones in ((JArray)loadOptions.Filter[0]).ToList<dynamic>()
                            where opciones.Value != "this" && opciones.Value != "contains"
                            select opciones.Value).FirstOrDefault();

            return null;//Content(JsonConvert.SerializeObject(DataSourceLoader.Load(FunProc.FindRsultadoGenerico(criterio, 0, "dependencia"), loadOptions)), "application/json");
        }
        #endregion

        protected override void Dispose(bool disposing)
        {
            //db.Dispose();
            base.Dispose(disposing);
        }
    }
}