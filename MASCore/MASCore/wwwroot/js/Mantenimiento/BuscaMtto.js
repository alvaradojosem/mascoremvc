﻿var now = new Date();

var URLBase = 'https://10.215.71.239:44350/'; // Net Core

$(document).ready(function () {
    $("#consulta").show();
    $("#resumen").hide();
    $("#negacion").hide();
    Tabs();
    //// tooltips
    Controles.tooltips.obj("#tooltip1", "#exportToExcel");
    Controles.tooltips.obj("#tooltip2", "#exportToPDF");
  
    ////Botones
    Controles.Boton.obj("#exportToExcel", "fa fa-file-excel","default");
    Controles.Boton.obj("#exportToPDF", "fa fa-file-pdf", "danger");

    //Control Switch CtrlDiv,offtext,ontext,width
    Controles.Ctrlswitch.obj("#DivSwitch", "NO", "SI", "55%",true);
    Controles.Ctrlswitch.obj("#Switch1", "NO", "SI", "55%",true);
    Controles.Ctrlswitch.obj("#Switch2", "NO", "SI", "55%",false);
    Controles.Ctrlswitch.obj("#Switch3", "NO", "SI", "55%", false);
    Controles.Ctrlswitch.obj("#Switch4", "NO", "SI", "55%", false);
    Controles.Ctrlswitch.obj("#Switch5", "NO", "SI", "55%", false);
    Controles.Ctrlswitch.obj("#Switch6", "NO", "SI", "55%", false);
    Controles.Ctrlswitch.obj("#Switch7", "NO", "SI", "55%", true);
    Controles.Ctrlswitch.obj("#Switch8", "NO", "SI", "55%", false);
    Controles.Ctrlswitch.obj("#Switch9", "NO", "SI", "55%", false);

    // Calendarios
    Controles.Calendarios.obj("#FechaInicial");
    Controles.Calendarios.obj("#FechaFinal");
    $("#FechaInicial").dxDateBox({
        width: "70%",
        ShowClearButton:true
    });
    $("#FechaFinal").dxDateBox({
        width: "70%",
        ShowClearButton: true
    });

    //Autocomplete
    Controles.CajaDeTexto.obj("#Barco", "-- Busque la embarcación --", "");
    Controles.CajaDeTexto.obj("#NumeroFolio", "Número de folio", "");

    //url, CtrlDiv, PlaceHolder
    Controles.SelectBox.obj("api/LoadControls/GetActividades", "#RazonSocial", "-- Seleccione la Razón Social --");
    Controles.SelectBox.obj("api/LoadControls/GetActividades", "#Servicio", "-- Seleccione el Servicio --");
    


});

function Tabs() {

   
    var tabs = [
        { id: "consulta", text: "Consulta",  content: "consulta" },
        { id: "resumen", text: "Resúmen", content: "resumen" },
        { id: "negacion", text: "Negación",  content: "negacion" }
    ];

    var tabsInstance = $("#tabs > .tabs-container").dxTabs({
        dataSource: tabs,
        selectedIndex: 0,
        onItemClick: function (e) {
            if (e.itemData.id === "consulta") {
                $("#consulta").show();
                $("#resumen").hide();
                $("#negacion").hide();
            } else if (e.itemData.id === "resumen") {
                $("#consulta").hide();
                $("#resumen").show();
                $("#negacion").hide();
            }
            else if (e.itemData.id === "negacion") {
                $("#consulta").hide();
                $("#resumen").hide();
                $("#negacion").show();
            }
        }

    }).dxTabs("instance");

}