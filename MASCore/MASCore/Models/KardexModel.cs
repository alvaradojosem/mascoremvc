﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using MASCore.ModelReference;
using static MASCore.ModelReference.KardexModelReference;

namespace MASCore.Models
{
    public class KardexModelBusqueda
    {
        private BDMasContextEnt db = new BDMasContextEnt();

        #region "PARAMETROS PARA LA BUSQUEDA"
        public Boolean MostrandoBusquedaInformacionEmbarcacion { get; set; }

        public DateTime fechainicial { get; set; }
        public DateTime fechafinal { get; set; }
        public string tramite { get; set; }
        public String barco { get; set; }
        public String rnp { get; set; }
        public String razon { get; set; }
        public String puerto { get; set; }
        #endregion

        #region "VARIABLES USADAS PARA LA BUSQUEDA DE INFORMACION DE EMBARCACION"
        //public List<BuscaInformacionDeEmbarcacion_Result> InformacionDeEmbarcacion { get; set; }
        //public List<BuscaInformacionDeEmbarcacion_Result> EncuentraInformacionDeEmbarcacionSP(string Opcion, string Valor)
        //{
        //    if (Valor != null) Valor = Valor.Replace("'", ""); 
        //    return db.BuscaInformacionDeEmbarcacion(Opcion, Valor).ToList();
        //}

        public List<SPMASBuscaInformacionDeEmbarcacionyPangas_Result> InformacionDeEmbarcacion { get; set; }
        public List<SPMASBuscaInformacionDeEmbarcacionyPangas_Result> EncuentraInformacionDeEmbarcacionSP(string Opcion, string Valor)
        {
            if (Valor != null) Valor = Valor.Replace("'", "");
            return null;// db.SPMASBuscaInformacionDeEmbarcacionyPangas(Opcion, Valor).ToList();
        }
        
        #endregion

        public List<BuscaKardex_Result> ResultadoListaKardexSP { get; set; }

        public List<BuscaKardex_Result> EncuentraKardexSP(KardexModelBusqueda ModeloTemporal)
        {
            if (ModeloTemporal.barco != null) ModeloTemporal.barco = ModeloTemporal.barco.Replace("'", "");
            if (ModeloTemporal.rnp != null) ModeloTemporal.rnp = ModeloTemporal.rnp.Replace("'", "");
            if (ModeloTemporal.razon != null) ModeloTemporal.razon = ModeloTemporal.razon.Replace("'", "");
            if (ModeloTemporal.puerto != null) ModeloTemporal.puerto = ModeloTemporal.puerto.Replace("'", "");

            //string FechaFormateadaIni = FechaInicial.ToString("d", CultureInfo.CreateSpecificCulture("en-US"));
            string FechaFormateadaIni = ModeloTemporal.fechainicial.Year.ToString() + '-' + ModeloTemporal.fechainicial.Month.ToString().PadLeft(2, '0') + '-' + ModeloTemporal.fechainicial.Day.ToString().PadLeft(2, '0') + " 00:00:00";
            //string FechaFormateadaFin = FechaFinal.ToString("d", CultureInfo.CreateSpecificCulture("en-US"));
            string FechaFormateadaFin = ModeloTemporal.fechafinal.Year.ToString() + '-' + ModeloTemporal.fechafinal.Month.ToString().PadLeft(2, '0') + '-' + ModeloTemporal.fechafinal.Day.ToString().PadLeft(2, '0') + " 23:59:59";
            return null;// db.BuscaKardex(FechaFormateadaIni, FechaFormateadaFin, ModeloTemporal.tramite, ModeloTemporal.barco, ModeloTemporal.rnp, ModeloTemporal.razon, ModeloTemporal.puerto).ToList();
            
        }

    }

    public class KardexServiciosModel
    {
        // Informacion del Servicio
        //public KardexDetalleServicio_Result KardexDetalleServiciosInfoGeneral { get; set; }
        ////Lista de Servicios
        //public List<KardexDetalleServiciosLista_Result> KardexDetalleServiciosLista { get; set; }

    }

    public class KardexJustificacionModel
    {
        private BDMasContextEnt db = new BDMasContextEnt();


        public Boolean MostrandoBusquedaInformacionEmbarcacion { get; set; }
        public string MostrarLlamadas { get; set; }

        #region "INFORMACION GENERAL"
        // SE OBTIENEN DE LA TABLA TMasJustificaciones
        public int IdJustificacion { get; set; }
        public DateTime FechaEscrito { get; set; }
        public DateTime FechaRegistro { get; set; }
        [Required]
        public string Embarcacion { get; set; }
        [Required]
        public string RazonSocial { get; set; }
        [Required]
        public string RNP { get; set; }
        [Required]
        public string Permiso { get; set; }
        [Required]
        public string PuertoBase { get; set; }
        [Required]
        public DateTime FechaDesconexion { get; set; }
        //[Required]
        public string Estatus { get; set; }
        [Required]
        public string Motivo { get; set; }
        //[Required]
        public int LlamadaTelefonica { get; set; }
        //[Required]
        public string Matricula { get; set; }
        #endregion

        #region "VARIABLES USADAS PARA LA BUSQUEDA DE INFORMACION DE EMBARCACION"
        public List<BuscaInformacionDeEmbarcacion_Result> InformacionDeEmbarcacion { get; set; }
        public List<BuscaInformacionDeEmbarcacion_Result> EncuentraInformacionDeEmbarcacionSP(string Opcion, string Valor)
        {
            if (Valor != null) Valor = Valor.Replace("'", "");
            return null;// db.BuscaInformacionDeEmbarcacion(Opcion, Valor).ToList();
        }
        #endregion

        //public LlamadaBusquedaModel ModeloDeLLamada { get; set; }
        #region "VARIABLES USADAS PARA MOSTRAR LAS LLAMADAS QUE SE VINCULAN"
        public LlamadaBusquedaModel ModeloDeLLamada { get; set; }
    //    public List<BuscaLlamadas_Result> ResultadoListaLlamadasSP { get; set; }

    //    public List<BuscaLlamadas_Result> EncuentraLasLlamadasSP(string Filtro, DateTime FechIni, DateTime FechFin, global::System.String actividad, string barco,
    //string ofp)
    //    {
    //        string FechaFormateadaIni = FechIni.ToString("d", CultureInfo.CreateSpecificCulture("en-US"));
    //        string FechaFormateadaFin = FechFin.ToString("d", CultureInfo.CreateSpecificCulture("en-US"));
    //        return db.BuscaLlamadas(Filtro, FechaFormateadaIni, FechaFormateadaFin, actividad, barco, ofp).ToList();
    //    }
        #endregion

        public int ActualizaKardexJustificacionSP(KardexJustificacionModel ModeloTmp)
        {
            int id = 0;
            //KardexJustificacionActualizar_Result Resultado = db.KardexJustificacionActualizar(ModeloTmp.IdJustificacion.ToString(), ModeloTmp.FechaEscrito, ModeloTmp.FechaRegistro, ModeloTmp.Embarcacion, ModeloTmp.RazonSocial, ModeloTmp.RNP, ModeloTmp.Permiso, ModeloTmp.PuertoBase, ModeloTmp.FechaDesconexion, "NO APLICA", ModeloTmp.Motivo, ModeloTmp.LlamadaTelefonica, "NO APLICA").FirstOrDefault();
            //if (Resultado != null)
            //{
            //    id = Resultado.IdJustificacion;
            //}
            return id;
        }

        public KardexJustificacionModel EncuentraKardexJustificacionSP(string IdJustificacionTmp)
        {
            KardexJustificacionModel ModeloTemporal = new KardexJustificacionModel();
            //KardexDetalleJustificacion_Result Resultado = db.KardexDetalleJustificacion(IdJustificacionTmp).FirstOrDefault();

            //if (Resultado != null)
            //{
            //    ModeloTemporal.IdJustificacion = Resultado.IdJustificacion;
            //    ModeloTemporal.FechaEscrito = Convert.ToDateTime(Resultado.FechaEscrito);
            //    ModeloTemporal.FechaRegistro = Convert.ToDateTime(Resultado.FechaRegistro);
            //    ModeloTemporal.Embarcacion = Resultado.Embarcacion;
            //    ModeloTemporal.RazonSocial = Resultado.RazonSocial;
            //    ModeloTemporal.RNP = Resultado.RNP;
            //    ModeloTemporal.Permiso = Resultado.Permiso;
            //    ModeloTemporal.PuertoBase = Resultado.PuertoBase;
            //    ModeloTemporal.FechaDesconexion = Convert.ToDateTime(Resultado.FechaDesconexion);
            //    ModeloTemporal.Motivo = Resultado.Motivo;
            //    ModeloTemporal.LlamadaTelefonica = Convert.ToInt32( Resultado.LlamadaTelefonica);
            //    ModeloTemporal.Matricula = Resultado.Matricula;
            //    ModeloTemporal.Estatus = Resultado.Estatus;

            //}
            //string FechaFormateadaIni = FechaInicial.ToString("d", CultureInfo.CreateSpecificCulture("en-US"));
            //string FechaFormateadaIni = ModeloTemporal.fechainicial.Year.ToString() + '-' + ModeloTemporal.fechainicial.Month.ToString().PadLeft(2, '0') + '-' + ModeloTemporal.fechainicial.Day.ToString().PadLeft(2, '0') + " 00:00:00";
            ////string FechaFormateadaFin = FechaFinal.ToString("d", CultureInfo.CreateSpecificCulture("en-US"));
            //string FechaFormateadaFin = ModeloTemporal.fechafinal.Year.ToString() + '-' + ModeloTemporal.fechafinal.Month.ToString().PadLeft(2, '0') + '-' + ModeloTemporal.fechafinal.Day.ToString().PadLeft(2, '0') + " 23:59:59";
            //return db.BuscaKardex(FechaFormateadaIni, FechaFormateadaFin, ModeloTemporal.tramite, ModeloTemporal.barco, ModeloTemporal.rnp, ModeloTemporal.razon, ModeloTemporal.puerto).ToList();
            return ModeloTemporal;
        }

    }


}