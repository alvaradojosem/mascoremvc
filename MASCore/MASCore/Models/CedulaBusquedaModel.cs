﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using DevExtreme.AspNet.Mvc;
using DevExtreme.AspNet.Data;
using System.Globalization;
using System.Linq;



namespace MASCore.Models
{
    public class InfoMensajesEmbarcacionesModel
    {
        //private BDMasContextEnt db = new BDMasContextEnt();
        public TMasCedula TMASCEDULA { get; set; }

        //public List<MensajesEmbarcacionesSeleccionar_Result> ResultadoMensajesEmbarcacionesSeleccionar { get; set; }
        #region "ELEMENTOS DE LA CLASE para la busqueda de mensajes de embarcaciones"
        [DataType(DataType.DateTime)]
        public DateTime FechaInicial { get; set; }                          //[DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:yyyy-MM-dd}")]  //[DisplayFormat(DataFormatString = "{0:dd/mm/yyyy}")]  
        [DataType(DataType.DateTime)]                                       //[DataType(DataType.Date)]
        [Required(ErrorMessage = "Si no selecciona, toma la fecha actual.")]
        public DateTime FechaFinal { get; set; }                             //[DisplayFormat(DataFormatString = "{0:d}")]
        public string NoValidoPor_ { get; set; }
        public LlamadaBusquedaModel ModeloDeLLamada { get; set; }
        public string MostrarObtieneInformacion { get; set; }
        public string MostrarLlamadas { get; set; }
        public string alertaseleccionada { get; set; }
        public string alarmasb { get; set; }
        public string MatriculaBarcob { get; set; }
        public string RazonSocialb { get; set; }
        public string PuertoBaseb { get; set; }
        public string TipoPermisob { get; set; }
        public string modobusqueda { get; set; }
        //------->   AGREGADO EN FASE IV 27-03-2015
        public string Embarcacionb { get; set; }
        public string Rnpb { get; set; }
        //<-------   AGREGADO EN FASE IV 27-03-2015
        #endregion
        public string IdMensaje { get; set; } //VZ1611_RPZPP
        #region "ELEMENTOS PARA REPORTE TABULAR RUTA"
        [DisplayName(" De ")]
        public DateTime FechaInicioRep { get; set; }
        [DisplayName(" A ")]
        public DateTime FechaFinRep { get; set; }
        #endregion

        //public List<MensajesEmbarcacionesSeleccionar_Result> EncuentraMensajesEmbarcacionesSeleccionarVacio(string FechIni, string FechFin, global::System.String alarmaselec, string matricula,
        //    string razonsocial, string puertobase, string tipopermiso, string ver, string Usuario, string Embarcacion_, string RNP_)
        //{
        //    return db.MensajesEmbarcacionesSeleccionar(FechIni, FechFin, alarmaselec, matricula, razonsocial, puertobase,
        //        tipopermiso, ver, "", Usuario, Embarcacion_, RNP_).ToList();
        //}

        //public List<MensajesEmbarcacionesSeleccionar_Result> EncuentraMensajesEmbarcacionesSeleccionar(string Usuario, DateTime FechIni, DateTime FechFin, global::System.String alarmaselec, string matricula,
        //    string razonsocial, string puertobase, string tipopermiso, string ver, string Embarcacion_, string RNP_, string _criteriodedesconexionS = "")
        //{
        //    // Se cambió el 19/10/2017 actualización de entity framework 5
        //    //db.CommandTimeout = 200;
        //    ((IObjectContextAdapter)this.db).ObjectContext.CommandTimeout = 200;
        //    string FechaFormateadaIni = FechIni.Year.ToString() + '-' + FechIni.Month.ToString().PadLeft(2, '0') + '-' + FechIni.Day.ToString().PadLeft(2, '0') + " 00:00:00";
        //    string FechaFormateadaFin = FechFin.Year.ToString() + '-' + FechFin.Month.ToString().PadLeft(2, '0') + '-' + FechFin.Day.ToString().PadLeft(2, '0') + " 23:59:59";

        //    return db.MensajesEmbarcacionesSeleccionar(FechaFormateadaIni, FechaFormateadaFin, alarmaselec, matricula, razonsocial, puertobase,
        //        tipopermiso, ver, _criteriodedesconexionS, Usuario, Embarcacion_, RNP_).ToList();
        //}

        public string PermisosPorMatricula(string Matricula_)
        {
            string Permisos = "";
            try
            {
                //ICollection<BuscaPermisosPorMatricula_Result> DatosPermisosPorMatricula = db.BuscaPermisosPorMatricula(Matricula_).ToList();
                //foreach (BuscaPermisosPorMatricula_Result Permiso in DatosPermisosPorMatricula)
                //{
                //    Permisos = Permisos + "," + Permiso.Permiso;
                //}
                //if (Permisos.Length > 1)
                //    Permisos = Permisos.Substring(1, Permisos.Length - 1);
            }
            catch (Exception e)
            {
                string erroMsg = e.StackTrace + e.Message + e.InnerException;
            }
            return Permisos;
        }

        //public string Estatus(BuscaUltimaTx_Result DatosUltimaTx_)
        //{

        //    String EstatusTmp = "";
        //    try
        //    {
        //        DateTime FechaDeUltimaTransmision = DateTime.Parse(DatosUltimaTx_.UltimaTx.ToString());

        //        TimeSpan diff = DateTime.Now.Subtract(FechaDeUltimaTransmision);
        //        long HorasDeDiferencia = (long)diff.TotalHours;

        //        if (HorasDeDiferencia >= 24)
        //        {
        //            EstatusTmp = "DESCONECTADO POR MAS DE 24 HORAS";
        //            EstatusTmp = "DESCONECTADO POR " + HorasDeDiferencia + " HORAS";
        //        }
        //        else
        //        {
        //            EstatusTmp = "TRANSMITIENDO";
        //        }

        //        EstatusTmp = EstatusTmp + ", " + DatosUltimaTx_.Lugar;  /////SE OMITIO EL 30 DE ABRIL 
        //    }
        //    catch (Exception e)
        //    {
        //        string erroMsg = e.StackTrace + e.Message + e.InnerException;
        //    }
        //    return EstatusTmp;
        //}

        public TMasCedula ObtieneCedulaSeleccionadaSP(string IdCedula)
        {
            TMasCedula Cedula = new TMasCedula();
            try
            {
                //ObtieneCedulaSeleccionada_Result ResultadoCedulaSeleccionada = db.ObtieneCedulaSeleccionada(IdCedula).FirstOrDefault();

                //if (ResultadoCedulaSeleccionada != null)
                //{
                //    Cedula.IdCedula = ResultadoCedulaSeleccionada.IdCedula;
                //    Cedula.NoCedula = ResultadoCedulaSeleccionada.NoCedula;
                //    Cedula.TipoAlerta = ResultadoCedulaSeleccionada.TipoAlerta;
                //    Cedula.FechaCedula = Cedula.FechaCedula;
                //    Cedula.Latitud = ResultadoCedulaSeleccionada.Latitud;
                //    Cedula.Longitud = ResultadoCedulaSeleccionada.Longitud;
                //    Cedula.Velocidad = ResultadoCedulaSeleccionada.Velocidad;
                //    Cedula.Profundidad = ResultadoCedulaSeleccionada.Profundidad;
                //    Cedula.DistanciaCosta = ResultadoCedulaSeleccionada.DistanciaCosta;
                //    Cedula.Zona = ResultadoCedulaSeleccionada.Zona;
                //    Cedula.Referencia = ResultadoCedulaSeleccionada.Referencia;
                //    Cedula.Causas = ResultadoCedulaSeleccionada.Causas;
                //    Cedula.Estatus = ResultadoCedulaSeleccionada.Estatus;
                //    Cedula.UltimaTx = ResultadoCedulaSeleccionada.UltimaTx;
                //    Cedula.FechaAlerta = ResultadoCedulaSeleccionada.FechaAlerta;
                //    Cedula.HoraAlerta = ResultadoCedulaSeleccionada.HoraAlerta;
                //    Cedula.MatriculaBarco = ResultadoCedulaSeleccionada.MatriculaBarco;
                //    Cedula.NombreBarco = ResultadoCedulaSeleccionada.NombreBarco;
                //    Cedula.RNP = ResultadoCedulaSeleccionada.RNP;
                //    Cedula.TipoPermiso = ResultadoCedulaSeleccionada.TipoPermiso;
                //    Cedula.PuertoBase = ResultadoCedulaSeleccionada.PuertoBase;
                //    Cedula.RazonSocial = ResultadoCedulaSeleccionada.RazonSocial;
                //    Cedula.Litoral = ResultadoCedulaSeleccionada.Litoral;
                //    Cedula.Domicilio = ResultadoCedulaSeleccionada.Domicilio;
                //    Cedula.Contacto = ResultadoCedulaSeleccionada.Contacto;
                //    Cedula.Puesto = ResultadoCedulaSeleccionada.Puesto;
                //    Cedula.Telefono = ResultadoCedulaSeleccionada.Telefono;
                //    Cedula.Comentarios = ResultadoCedulaSeleccionada.Comentarios;
                //    Cedula.Anexos = ResultadoCedulaSeleccionada.Anexos;
                //    Cedula.OFP = ResultadoCedulaSeleccionada.OFP;
                //    Cedula.Turno = ResultadoCedulaSeleccionada.Turno;
                //    Cedula.NombreContactada = ResultadoCedulaSeleccionada.NombreContactada;
                //    Cedula.PuestoContactada = ResultadoCedulaSeleccionada.PuestoContactada;
                //    Cedula.TelefonoContactada = ResultadoCedulaSeleccionada.TelefonoContactada;
                //    Cedula.Llamada = ResultadoCedulaSeleccionada.Llamada;
                //    Cedula.Estado = ResultadoCedulaSeleccionada.Estado;
                //    Cedula.Vigencias = ResultadoCedulaSeleccionada.Vigencias;
                //    Cedula.Periodo = ResultadoCedulaSeleccionada.Periodo;
                //    Cedula.UsuarioAstrum = ResultadoCedulaSeleccionada.UsuarioAstrum;
                //}
            }
            catch (Exception e)
            {
                string erroMsg = e.StackTrace + e.Message + e.InnerException;
            }
            return Cedula;
        }

        public TMasCedula AsingaResultadoMensajeEmbarcacionSeleccionada(string Id, TMasCedula CedulaOld, string Usuario, string AlarmasDesdeObtieneInfo)
        {
            FuncionesProcedimientos FunProc = new FuncionesProcedimientos();
            Guid IdM = Guid.Parse(Id);
            TMasCedula Cedula = new TMasCedula();
            try
            {
                if (CedulaOld != null)
                {
                    Cedula = CedulaOld;
                }

                //MensajeEmbarcacionSeleccionada_Result DatosdeMensajeEmbarcacionSeleccionada = db.MensajeEmbarcacionSeleccionada(IdM, Usuario).FirstOrDefault();

                //if (DatosdeMensajeEmbarcacionSeleccionada != null)
                //{

                //    #region "ASIGNA VALORES"
                //    Cedula.MatriculaBarco = DatosdeMensajeEmbarcacionSeleccionada.MatBarco;
                //    Cedula.TipoPermiso = PermisosPorMatricula(Cedula.MatriculaBarco);                                //Obtiene Permisos basados en la matricula
                //    Cedula.TipoPermiso = FunProc.BuscaResultadoGenericoConParametros("permisos", Cedula.MatriculaBarco);                                //Obtiene Permisos basados en la matricula
                //    Cedula.Vigencias = FunProc.BuscaResultadoGenericoConParametros("vigenciapermisos", Cedula.MatriculaBarco);

                //    //BuscaUltimaTx_Result DatosUltimaTx = db.BuscaUltimaTx(Cedula.MatriculaBarco, Usuario).FirstOrDefault(); // SE CAMBIO EL 30 DE ABRIL PARA TRAER LA LISTA   //Obtiene UltimaTX y el lugar 
                //    List<BuscaUltimaTx_Result> DatosUltimaTx = db.BuscaUltimaTx(Cedula.MatriculaBarco, Usuario).ToList();

                //    #region "OBTIENE LA ULTIMA TX Y EL ESTATUS"
                //    string Lugar = "";
                //    string NombrePuerto = "";
                //    foreach (BuscaUltimaTx_Result Res in DatosUltimaTx)
                //    {
                //        Cedula.UltimaTx = Res.UltimaTx.ToString();
                //        if (Res.Lugar.ToUpper() == "EN PUERTO")
                //        {
                //            Lugar = "EN PUERTO";
                //            NombrePuerto = Res.NombrePuerto.ToString();
                //            break;
                //        }
                //    }

                //    DateTime FechaDeUltimaTransmision = DateTime.Parse(Cedula.UltimaTx.ToString());
                //    TimeSpan diff = DateTime.Now.Subtract(FechaDeUltimaTransmision);
                //    long HorasDeDiferencia = (long)diff.TotalHours;
                //    if (HorasDeDiferencia >= 24)
                //    {
                //        Cedula.Estatus = "DESCONECTADO POR MAS DE 24 HORAS";
                //        Cedula.Estatus = "DESCONECTADO POR " + HorasDeDiferencia + " HORAS";
                //    }
                //    else
                //    {
                //        Cedula.Estatus = "TRANSMITIENDO";
                //    }

                //    if (AlarmasDesdeObtieneInfo.ToUpper().Replace('Ó', 'O') == "SIN TRANSMISION")
                //    {
                //        Cedula.Estatus = "SIN TRANSMISIÓN POR " + HorasDeDiferencia + " HORAS";
                //    }

                //    if (Lugar.ToUpper() == "EN PUERTO")
                //    {
                //        Cedula.Estatus = Cedula.Estatus + ", " + "EN PUERTO ";  //ModeloTmp_EnPuerto = "SI";
                //    }
                //    else
                //    {
                //        Cedula.Estatus = Cedula.Estatus + ", " + "EN ALTAMAR"; //ModeloTmp_EnPuerto = "NO";
                //    }
                //    #endregion
                //    //Cedula.UltimaTx = DatosUltimaTx.UltimaTx.ToString();   // SE CAMBIO EL 30 DE ABRIL
                //    //Cedula.Estatus = Estatus(DatosUltimaTx);              // SE CAMBIO EL 30 DE ABRIL

                //    if (Cedula.Estatus.Length > 0)
                //        Cedula.Estatus = Cedula.Estatus.ToUpper().Replace("SEGUNDA", "");

                //    Cedula.NombreBarco = DatosdeMensajeEmbarcacionSeleccionada.NombreBarco;
                //    Cedula.RNP = DatosdeMensajeEmbarcacionSeleccionada.RNP;
                //    Cedula.RazonSocial = DatosdeMensajeEmbarcacionSeleccionada.RazonSocial;
                //    Cedula.PuertoBase = DatosdeMensajeEmbarcacionSeleccionada.PuertoBase;
                //    Cedula.Estado = DatosdeMensajeEmbarcacionSeleccionada.Estado;
                //    Cedula.Zona = DatosdeMensajeEmbarcacionSeleccionada.Zona;
                //    Cedula.Litoral = DatosdeMensajeEmbarcacionSeleccionada.Zona;
                //    Cedula.Contacto = DatosdeMensajeEmbarcacionSeleccionada.RepLegal;

                //    if (DatosdeMensajeEmbarcacionSeleccionada.RepLegal == "")
                //    {
                //        Cedula.Puesto = "NO INFO";
                //    }
                //    else
                //    {
                //        Cedula.Puesto = "REPRESENTANTE LEGAL";
                //    }

                //    Cedula.Telefono = DatosdeMensajeEmbarcacionSeleccionada.Telefono;
                //    Cedula.Domicilio = DatosdeMensajeEmbarcacionSeleccionada.Domicilio;
                //    Cedula.FechaAlerta = DatosdeMensajeEmbarcacionSeleccionada.Fecha;
                //    Cedula.HoraAlerta = DatosdeMensajeEmbarcacionSeleccionada.Hora.ToString();

                //    FuncionesProcedimientos FuncProc = new FuncionesProcedimientos();
                //    if (DatosdeMensajeEmbarcacionSeleccionada.Latitud > 0)
                //        Cedula.Latitud = FuncProc.ConvertirCoordenada(DatosdeMensajeEmbarcacionSeleccionada.Latitud.ToString());
                //    if (DatosdeMensajeEmbarcacionSeleccionada.Longitud != null)
                //        Cedula.Longitud = FuncProc.ConvertirCoordenada(DatosdeMensajeEmbarcacionSeleccionada.Longitud.ToString());
                //    Cedula.Velocidad = DatosdeMensajeEmbarcacionSeleccionada.Velocidad.ToString();
                //    Cedula.TipoAlerta = DatosdeMensajeEmbarcacionSeleccionada.TipoAlerta.ToUpper();
                //    if (AlarmasDesdeObtieneInfo == "SIN TRANSMISION" || AlarmasDesdeObtieneInfo == "SIN TRANSMISIÓN")
                //    {
                //        Cedula.TipoAlerta = AlarmasDesdeObtieneInfo;
                //    }
                //    if (Cedula.TipoAlerta.Length > 0)
                //    {
                //        Cedula.TipoAlerta = Cedula.TipoAlerta.Replace("SEGUNDA", "").Trim();
                //        if (Cedula.TipoAlerta == "PROXIMIDAD A EMBARCACIONES" || Cedula.TipoAlerta == "MÚLTIPLES PERMISOS" || Cedula.TipoAlerta == "PERMISO VENCIDO" || Cedula.TipoAlerta == "TÉCNICAMENTE PESCANDO" || Cedula.TipoAlerta == "DESCONECTADO")
                //        {
                //            Cedula.TipoAlerta = "TRANSMISION IRREGULAR";
                //        }
                //    }

                //    // Inizializados 
                //    Cedula.Profundidad = "";
                //    Cedula.Llamada = 0;  // Como se esta creando la cedula no hay llamadas vinculadas, al editar si se necesita cargar lo que tenga en TMasCedulas
                //    #endregion

                //}
            }
            catch (Exception e)
            {
                string erroMsg = e.StackTrace + e.Message + e.InnerException;
            }
            return Cedula;

        }

        public string ValidayActualizaCedula(InfoMensajesEmbarcacionesModel ModeloTmp, string alarmas_, string causas_, string turnos_, string responsableofp_,
            string Usuario, Guid idMsj)
        {
            #region "ASIGNACION DE VALORES"
            //Asigno los Valores
            TMasCedula FinalTMasCedula = new TMasCedula();
            string RequeridosTmp = "";
            try
            {
                //FinalTMasCedula.UsuarioAstrum = ModeloTmp.TMASCEDULA.UsuarioAstrum;
                //FinalTMasCedula.TipoAlerta = ModeloTmp.TMASCEDULA.TipoAlerta;
                ////FinalTMasCedula.TipoAlerta = alarmas_;
                //FinalTMasCedula.FechaCedula = ModeloTmp.TMASCEDULA.FechaCedula;
                //FinalTMasCedula.Latitud = ModeloTmp.TMASCEDULA.Latitud;
                //FinalTMasCedula.Longitud = ModeloTmp.TMASCEDULA.Longitud;
                //FinalTMasCedula.Velocidad = ModeloTmp.TMASCEDULA.Velocidad;
                //FinalTMasCedula.Profundidad = ModeloTmp.TMASCEDULA.Profundidad;
                //FinalTMasCedula.DistanciaCosta = ModeloTmp.TMASCEDULA.DistanciaCosta;
                //FinalTMasCedula.Zona = ModeloTmp.TMASCEDULA.Zona;
                //FinalTMasCedula.Referencia = ModeloTmp.TMASCEDULA.Referencia;
                ////FinalTMasCedula.Causas = causas_;
                //FinalTMasCedula.Causas = ModeloTmp.TMASCEDULA.Causas;
                //FinalTMasCedula.Estatus = ModeloTmp.TMASCEDULA.Estatus;
                //FinalTMasCedula.UltimaTx = ModeloTmp.TMASCEDULA.UltimaTx;
                //FinalTMasCedula.FechaAlerta = ModeloTmp.TMASCEDULA.FechaAlerta;
                //FinalTMasCedula.HoraAlerta = ModeloTmp.TMASCEDULA.HoraAlerta;
                //FinalTMasCedula.MatriculaBarco = ModeloTmp.TMASCEDULA.MatriculaBarco;
                //FinalTMasCedula.NombreBarco = ModeloTmp.TMASCEDULA.NombreBarco;
                //FinalTMasCedula.RNP = ModeloTmp.TMASCEDULA.RNP;
                //FinalTMasCedula.TipoPermiso = ModeloTmp.TMASCEDULA.TipoPermiso;
                //FinalTMasCedula.PuertoBase = ModeloTmp.TMASCEDULA.PuertoBase;
                //FinalTMasCedula.RazonSocial = ModeloTmp.TMASCEDULA.RazonSocial;
                //FinalTMasCedula.Litoral = ModeloTmp.TMASCEDULA.Litoral;
                //FinalTMasCedula.Domicilio = ModeloTmp.TMASCEDULA.Domicilio;
                //FinalTMasCedula.Contacto = ModeloTmp.TMASCEDULA.Contacto;
                //FinalTMasCedula.Puesto = ModeloTmp.TMASCEDULA.Puesto;
                //FinalTMasCedula.Telefono = ModeloTmp.TMASCEDULA.Telefono;
                //FinalTMasCedula.Comentarios = ModeloTmp.TMASCEDULA.Comentarios;
                //FinalTMasCedula.Anexos = ModeloTmp.TMASCEDULA.Anexos;
                //FinalTMasCedula.OFP = ModeloTmp.TMASCEDULA.OFP;
                ////FinalTMasCedula.OFP = responsableofp_;
                ////FinalTMasCedula.Turno = turnos_;
                //FinalTMasCedula.Turno = ModeloTmp.TMASCEDULA.Turno;
                //FinalTMasCedula.NombreContactada = ModeloTmp.TMASCEDULA.NombreContactada;
                //FinalTMasCedula.PuestoContactada = ModeloTmp.TMASCEDULA.PuestoContactada;
                //FinalTMasCedula.TelefonoContactada = ModeloTmp.TMASCEDULA.TelefonoContactada;
                //FinalTMasCedula.Llamada = ModeloTmp.TMASCEDULA.Llamada;
                //FinalTMasCedula.Estado = ModeloTmp.TMASCEDULA.Estado;
                //FinalTMasCedula.Vigencias = ModeloTmp.TMASCEDULA.Vigencias;
                //FinalTMasCedula.Periodo = ModeloTmp.TMASCEDULA.Periodo;
                //FinalTMasCedula.NoCedula = ModeloTmp.TMASCEDULA.NoCedula;
                //FinalTMasCedula.IdCedula = ModeloTmp.TMASCEDULA.IdCedula;
                ////Agregado por SZ el 15/03/2017
                //FinalTMasCedula.idMensaje = idMsj;
                #endregion

                if (FinalTMasCedula.MatriculaBarco != null && FinalTMasCedula.MatriculaBarco.Trim().Length != 0)
                {
                    //if (FinalTMasCedula.UltimaTx == null || FinalTMasCedula.UltimaTx.Trim().Length == 0)
                    //{
                    //    BuscaUltimaTx_Result DatosUltimaTx = db.BuscaUltimaTx(FinalTMasCedula.MatriculaBarco, Usuario).FirstOrDefault();   //Obtiene UltimaTX y el lugar
                    //    FinalTMasCedula.UltimaTx = DatosUltimaTx.UltimaTx.ToString();
                    //    FinalTMasCedula.Estatus = ModeloTmp.Estatus(DatosUltimaTx);
                    //}
                }

                #region "REGION DE CAMPOS REQUERIDOS"
                //Valida que no falten campos y la integridad de los datos

                if (FinalTMasCedula.FechaCedula == null || FinalTMasCedula.MatriculaBarco == null || FinalTMasCedula.Comentarios == null ||
                    FinalTMasCedula.RNP == null || FinalTMasCedula.NombreBarco == null || FinalTMasCedula.MatriculaBarco == "" ||
                    FinalTMasCedula.Comentarios == "" || FinalTMasCedula.RNP == "" || FinalTMasCedula.NombreBarco == "")
                {
                    RequeridosTmp = "Son requeridos los campos: ";
                    if (FinalTMasCedula.MatriculaBarco == null || FinalTMasCedula.MatriculaBarco == "")
                        RequeridosTmp += "Matricula de Barco,";
                    if (FinalTMasCedula.Comentarios == null || FinalTMasCedula.Comentarios == "")
                        RequeridosTmp += "Comentarios,";
                    if (FinalTMasCedula.RNP == null || FinalTMasCedula.RNP == "")
                        RequeridosTmp += "RNP,";
                    if (FinalTMasCedula.NombreBarco == null || FinalTMasCedula.NombreBarco == "")
                        RequeridosTmp += "Nombre Barco,";
                    if (FinalTMasCedula.FechaCedula == null)
                    {
                        RequeridosTmp += "Fecha Cedula,";
                    }
                    RequeridosTmp = RequeridosTmp.Substring(0, RequeridosTmp.Length - 1);
                }

                #endregion

                if (RequeridosTmp.Length == 0)
                {

                    // Esta bien y se hace el insert en la DB
                    // ANTES DE HACER EL INSERT OBTIENE MAS DATOS COMO EL NUMERO DE CEDULA, PERIODO, ETC
                    //periodo
                    if (FinalTMasCedula.Periodo == null || FinalTMasCedula.Periodo.Trim().Length == 0)
                    {
                        FinalTMasCedula.Periodo = ObtenPeriodo(FinalTMasCedula.FechaCedula);
                    }

                    RequeridosTmp = ActualizaCedula(FinalTMasCedula);
                }

            }
            catch (Exception e)
            {
                string erroMsg = e.StackTrace + e.Message + e.InnerException;
            }
            return RequeridosTmp;

        }

        public TMasCedula AsignaLeyendaNOAPLICA(TMasCedula FinalTMasCedula)  //InfoMensajesEmbarcacionesModel ModeloTmp
        {
            try
            {
                //string Leyenda = "NO APLICA";

                //if (FinalTMasCedula.TipoAlerta == "SIN TRANSMISIÓN" || FinalTMasCedula.TipoAlerta == "TRANSMISION IRREGULAR")
                //{
                //    FinalTMasCedula.HoraAlerta = Leyenda;
                //    FinalTMasCedula.Velocidad = Leyenda;
                //    FinalTMasCedula.Profundidad = Leyenda;
                //    FinalTMasCedula.DistanciaCosta = Leyenda;
                //    FinalTMasCedula.Zona = Leyenda;
                //    FinalTMasCedula.Referencia = Leyenda;
                //    FinalTMasCedula.Causas = Leyenda;
                //    FinalTMasCedula.NombreContactada = Leyenda;
                //    FinalTMasCedula.PuestoContactada = Leyenda;
                //    FinalTMasCedula.TelefonoContactada = Leyenda;
                //}
                //else if (FinalTMasCedula.TipoAlerta == "PESCA PROHIBIDA" || FinalTMasCedula.TipoAlerta == "AREA NATURAL PROTEGIDA" || FinalTMasCedula.TipoAlerta == "ZONA PROHIBIDA" || FinalTMasCedula.TipoAlerta == "ZONA DECRETO")
                //{
                //    FinalTMasCedula.Estatus = Leyenda;
                //    FinalTMasCedula.Causas = Leyenda;
                //}
                //else if (FinalTMasCedula.TipoAlerta == "EMERGENCIA" || FinalTMasCedula.TipoAlerta == "FALSA ALARMA")
                //{
                //    FinalTMasCedula.Velocidad = Leyenda;
                //    FinalTMasCedula.Estatus = Leyenda;
                //    FinalTMasCedula.Profundidad = Leyenda;
                //    FinalTMasCedula.DistanciaCosta = Leyenda;
                //    FinalTMasCedula.Referencia = Leyenda;
                //}
            }
            catch (Exception e)
            {
                string erroMsg = e.StackTrace + e.Message + e.InnerException;
            }
            return FinalTMasCedula;
        }
        //Comentado por SZ el 14/03/2017
        //public string ValidaeInsertaCedula(InfoMensajesEmbarcacionesModel ModeloTmp, string alarmas_, string causas_, string turnos_, string responsableofp_, string Usuario)
        public string ValidaeInsertaCedula(InfoMensajesEmbarcacionesModel ModeloTmp, string alarmas_, string causas_, string turnos_, string responsableofp_,
            string Usuario, Guid idMsj)
        {
            string RequeridosTmp = ""; //NoEsValidoPor(FinalTMasCedula);
            //try
            //{
            //    #region "ASIGNACION DE VALORES"
            //    //Asigno los Valores
            //    TMasCedula FinalTMasCedula = new TMasCedula();
            //    FinalTMasCedula.UsuarioAstrum = ModeloTmp.TMASCEDULA.UsuarioAstrum;
            //    FinalTMasCedula.TipoAlerta = ModeloTmp.TMASCEDULA.TipoAlerta;
            //    if (FinalTMasCedula.TipoAlerta == null || FinalTMasCedula.TipoAlerta.Trim().Length == 0)
            //        FinalTMasCedula.TipoAlerta = alarmas_;
            //    FinalTMasCedula.FechaCedula = ModeloTmp.TMASCEDULA.FechaCedula;
            //    FinalTMasCedula.Latitud = ModeloTmp.TMASCEDULA.Latitud;
            //    FinalTMasCedula.Longitud = ModeloTmp.TMASCEDULA.Longitud;
            //    FinalTMasCedula.Velocidad = ModeloTmp.TMASCEDULA.Velocidad;
            //    FinalTMasCedula.Profundidad = ModeloTmp.TMASCEDULA.Profundidad;
            //    FinalTMasCedula.DistanciaCosta = ModeloTmp.TMASCEDULA.DistanciaCosta;
            //    FinalTMasCedula.Zona = ModeloTmp.TMASCEDULA.Zona;
            //    FinalTMasCedula.Referencia = ModeloTmp.TMASCEDULA.Referencia;
            //    FinalTMasCedula.Causas = ModeloTmp.TMASCEDULA.Causas;
            //    if (FinalTMasCedula.Causas == null || FinalTMasCedula.Causas.Trim().Length == 0)
            //        FinalTMasCedula.Causas = causas_;
            //    FinalTMasCedula.Estatus = ModeloTmp.TMASCEDULA.Estatus;
            //    FinalTMasCedula.UltimaTx = ModeloTmp.TMASCEDULA.UltimaTx;
            //    FinalTMasCedula.FechaAlerta = ModeloTmp.TMASCEDULA.FechaAlerta;
            //    FinalTMasCedula.HoraAlerta = ModeloTmp.TMASCEDULA.HoraAlerta;
            //    FinalTMasCedula.MatriculaBarco = ModeloTmp.TMASCEDULA.MatriculaBarco;
            //    FinalTMasCedula.NombreBarco = ModeloTmp.TMASCEDULA.NombreBarco;
            //    FinalTMasCedula.RNP = ModeloTmp.TMASCEDULA.RNP;
            //    FinalTMasCedula.TipoPermiso = ModeloTmp.TMASCEDULA.TipoPermiso;
            //    FinalTMasCedula.PuertoBase = ModeloTmp.TMASCEDULA.PuertoBase;
            //    FinalTMasCedula.RazonSocial = ModeloTmp.TMASCEDULA.RazonSocial;
            //    FinalTMasCedula.Litoral = ModeloTmp.TMASCEDULA.Litoral;
            //    FinalTMasCedula.Domicilio = ModeloTmp.TMASCEDULA.Domicilio;
            //    FinalTMasCedula.Contacto = ModeloTmp.TMASCEDULA.Contacto;
            //    FinalTMasCedula.Puesto = ModeloTmp.TMASCEDULA.Puesto;
            //    FinalTMasCedula.Telefono = ModeloTmp.TMASCEDULA.Telefono;
            //    FinalTMasCedula.Comentarios = ModeloTmp.TMASCEDULA.Comentarios;
            //    FinalTMasCedula.Anexos = ModeloTmp.TMASCEDULA.Anexos;
            //    FinalTMasCedula.OFP = ModeloTmp.TMASCEDULA.OFP;
            //    if (FinalTMasCedula.OFP == null || FinalTMasCedula.OFP.Trim().Length == 0)
            //        FinalTMasCedula.OFP = responsableofp_;
            //    FinalTMasCedula.Turno = ModeloTmp.TMASCEDULA.Turno;
            //    if (FinalTMasCedula.Turno == null || FinalTMasCedula.Turno.Trim().Length == 0)
            //        FinalTMasCedula.Turno = turnos_;
            //    FinalTMasCedula.NombreContactada = ModeloTmp.TMASCEDULA.NombreContactada;
            //    FinalTMasCedula.PuestoContactada = ModeloTmp.TMASCEDULA.PuestoContactada;
            //    FinalTMasCedula.TelefonoContactada = ModeloTmp.TMASCEDULA.TelefonoContactada;
            //    FinalTMasCedula.Llamada = ModeloTmp.TMASCEDULA.Llamada;
            //    FinalTMasCedula.Estado = ModeloTmp.TMASCEDULA.Estado;
            //    FinalTMasCedula.Vigencias = ModeloTmp.TMASCEDULA.Vigencias;
            //    FinalTMasCedula.Periodo = ModeloTmp.TMASCEDULA.Periodo;
            //    //Agregado por SZ el 14/03/2017
            //    if (idMsj.ToString() == "00000000-0000-0000-0000-000000000000")
            //    {
            //        FinalTMasCedula.idMensaje = null;
            //    }
            //    else
            //    {
            //        FinalTMasCedula.idMensaje = idMsj;
            //    }
            //    #endregion

            //    if (FinalTMasCedula.MatriculaBarco != null && FinalTMasCedula.MatriculaBarco.Trim().Length != 0)
            //    {
            //        if (FinalTMasCedula.UltimaTx == null || FinalTMasCedula.UltimaTx.Trim().Length == 0)
            //        {
            //            BuscaUltimaTx_Result DatosUltimaTx = db.BuscaUltimaTx(FinalTMasCedula.MatriculaBarco, Usuario).FirstOrDefault();   //Obtiene UltimaTX y el lugar
            //            if (DatosUltimaTx != null)
            //            {
            //                FinalTMasCedula.UltimaTx = DatosUltimaTx.UltimaTx.ToString();
            //                FinalTMasCedula.Estatus = ModeloTmp.Estatus(DatosUltimaTx);
            //            }
            //        }
            //    }

            //    #region "REGION DE CAMPOS REQUERIDOS"
            //    //Valida que no falten campos y la integridad de los datos
            //    //string RequeridosTmp = ""; //NoEsValidoPor(FinalTMasCedula);
            //    if (FinalTMasCedula.FechaCedula == null || FinalTMasCedula.MatriculaBarco == null || FinalTMasCedula.Comentarios == null ||
            //        FinalTMasCedula.RNP == null || FinalTMasCedula.NombreBarco == null || FinalTMasCedula.MatriculaBarco == "" ||
            //        FinalTMasCedula.Comentarios == "" || FinalTMasCedula.RNP == "" || FinalTMasCedula.NombreBarco == "")
            //    {
            //        RequeridosTmp = "Son requeridos los campos: ";
            //        if (FinalTMasCedula.MatriculaBarco == null || FinalTMasCedula.MatriculaBarco == "")
            //            RequeridosTmp += "Matricula de Barco,";
            //        if (FinalTMasCedula.Comentarios == null || FinalTMasCedula.Comentarios == "")
            //            RequeridosTmp += "Comentarios,";
            //        if (FinalTMasCedula.RNP == null || FinalTMasCedula.RNP == "")
            //            RequeridosTmp += "RNP,";
            //        if (FinalTMasCedula.NombreBarco == null || FinalTMasCedula.NombreBarco == "")
            //            RequeridosTmp += "Nombre Barco,";
            //        if (FinalTMasCedula.FechaCedula == null)
            //        {
            //            RequeridosTmp += "Fecha Cedula,";
            //        }
            //        RequeridosTmp = RequeridosTmp.Substring(0, RequeridosTmp.Length - 1);
            //    }

            //    #endregion

            //    if (RequeridosTmp.Length == 0)
            //    {
            //        // Esta bien y se hace el insert en la DB
            //        // ANTES DE HACER EL INSERT OBTIENE MAS DATOS COMO EL NUMERO DE CEDULA, PERIODO, ETC
            //        //Periodo
            //        if (FinalTMasCedula.Periodo == null || FinalTMasCedula.Periodo.Trim().Length == 0)
            //        {
            //            FinalTMasCedula.Periodo = ObtenPeriodo(FinalTMasCedula.FechaCedula);
            //        }
            //        //Asigna la leyenda de NO APLICA a los campos correspondientes
            //        FinalTMasCedula = AsignaLeyendaNOAPLICA(FinalTMasCedula);
            //        //Hace el insert
            //        RequeridosTmp = InsertaCedula(FinalTMasCedula);
            //        //RequeridosTmp = "";
            //    }
            //}
            //catch (Exception e)
            //{
            //    string erroMsg = e.StackTrace + e.Message + e.InnerException;
            //}
            return RequeridosTmp;
        }

        public string ActualizaCedula(TMasCedula FinalTMasCedula_)
        {
            string ResultadoUpdate = "";
            //try
            //{

            //    CedulasActualizar_Result DatosDeCedulaActualizada = db.CedulasActualizar(FinalTMasCedula_.IdCedula, FinalTMasCedula_.NoCedula, FinalTMasCedula_.TipoAlerta, FinalTMasCedula_.FechaCedula, FinalTMasCedula_.Latitud,
            //        FinalTMasCedula_.Longitud, FinalTMasCedula_.Velocidad, FinalTMasCedula_.Profundidad, FinalTMasCedula_.DistanciaCosta, FinalTMasCedula_.Zona, FinalTMasCedula_.Referencia,
            //        FinalTMasCedula_.Causas, FinalTMasCedula_.Estatus, FinalTMasCedula_.UltimaTx, FinalTMasCedula_.FechaAlerta, FinalTMasCedula_.HoraAlerta, FinalTMasCedula_.MatriculaBarco,
            //        FinalTMasCedula_.NombreBarco, FinalTMasCedula_.RNP, FinalTMasCedula_.TipoPermiso, FinalTMasCedula_.PuertoBase, FinalTMasCedula_.RazonSocial, FinalTMasCedula_.Litoral,
            //        FinalTMasCedula_.Domicilio, FinalTMasCedula_.Contacto, FinalTMasCedula_.Puesto, FinalTMasCedula_.Telefono, FinalTMasCedula_.Comentarios, FinalTMasCedula_.Anexos,
            //        FinalTMasCedula_.OFP, FinalTMasCedula_.Turno, FinalTMasCedula_.NombreContactada, FinalTMasCedula_.PuestoContactada, FinalTMasCedula_.TelefonoContactada,
            //        FinalTMasCedula_.Llamada, FinalTMasCedula_.Estado, FinalTMasCedula_.Vigencias, FinalTMasCedula_.Periodo, FinalTMasCedula_.UsuarioAstrum).FirstOrDefault();

            //    if (DatosDeCedulaActualizada != null)
            //    {
            //        ResultadoUpdate = DatosDeCedulaActualizada.IdCedula.ToString();
            //    }
            //}
            //catch (Exception e)
            //{
            //    string erroMsg = e.StackTrace + e.Message + e.InnerException;
            //}
            return ResultadoUpdate;
        }

        public string InsertaCedula(TMasCedula FinalTMasCedula_)
        {
            string ResultadoInsert = "";
            //try
            //{
            //    CedulasInsertar_Result DatosDeCedulaInsertada = db.CedulasInsertar(
            //      FinalTMasCedula_.TipoAlerta,
            //      FinalTMasCedula_.FechaCedula,
            //      FinalTMasCedula_.Latitud,
            //      FinalTMasCedula_.Longitud,
            //      FinalTMasCedula_.Velocidad,
            //      FinalTMasCedula_.Profundidad,
            //      FinalTMasCedula_.DistanciaCosta,
            //      FinalTMasCedula_.Zona,
            //      FinalTMasCedula_.Referencia,
            //      FinalTMasCedula_.Causas,
            //      FinalTMasCedula_.Estatus,
            //      FinalTMasCedula_.UltimaTx,
            //      FinalTMasCedula_.FechaAlerta,
            //      FinalTMasCedula_.HoraAlerta,
            //      FinalTMasCedula_.MatriculaBarco,
            //      FinalTMasCedula_.NombreBarco,
            //      FinalTMasCedula_.RNP,
            //      FinalTMasCedula_.TipoPermiso,
            //      FinalTMasCedula_.PuertoBase,
            //      FinalTMasCedula_.RazonSocial,
            //      FinalTMasCedula_.Litoral,
            //      FinalTMasCedula_.Domicilio,
            //      FinalTMasCedula_.Contacto,
            //      FinalTMasCedula_.Puesto,
            //      FinalTMasCedula_.Telefono,
            //      FinalTMasCedula_.Comentarios,
            //      FinalTMasCedula_.Anexos,
            //      FinalTMasCedula_.OFP,
            //      FinalTMasCedula_.Turno,
            //      FinalTMasCedula_.NombreContactada,
            //      FinalTMasCedula_.PuestoContactada,
            //      FinalTMasCedula_.TelefonoContactada,
            //      FinalTMasCedula_.Llamada,
            //      FinalTMasCedula_.Estado,
            //      FinalTMasCedula_.Vigencias,
            //      FinalTMasCedula_.Periodo,
            //      FinalTMasCedula_.UsuarioAstrum,
            //      FinalTMasCedula_.idMensaje).FirstOrDefault(); //VZ1611_RPZPP se agrgego IdMesaje

            //    if (DatosDeCedulaInsertada != null)
            //    {
            //        ResultadoInsert = DatosDeCedulaInsertada.IdCedula.ToString();
            //    }
            //}
            //catch (Exception e)
            //{
            //    string erroMsg = e.StackTrace + e.Message + e.InnerException;
            //}
            return ResultadoInsert;
        }

        public string ObtenPeriodo(DateTime? FechaX)
        {
            string Periodo_ = FechaX.Value.Year.ToString();
            try
            {
                int Mes = FechaX.Value.Month;

                switch (Mes)
                {
                    case 1:
                        Periodo_ = "ENE-" + Periodo_;
                        break;
                    case 2:
                        Periodo_ = "FEB-" + Periodo_;
                        break;
                    case 3:
                        Periodo_ = "MAR-" + Periodo_;
                        break;
                    case 4:
                        Periodo_ = "ABR-" + Periodo_;
                        break;
                    case 5:
                        Periodo_ = "MAY-" + Periodo_;
                        break;
                    case 6:
                        Periodo_ = "JUN-" + Periodo_;
                        break;
                    case 7:
                        Periodo_ = "JUL-" + Periodo_;
                        break;
                    case 8:
                        Periodo_ = "AGO-" + Periodo_;
                        break;
                    case 9:
                        Periodo_ = "SEP-" + Periodo_;
                        break;
                    case 10:
                        Periodo_ = "OCT-" + Periodo_;
                        break;
                    case 11:
                        Periodo_ = "NOV-" + Periodo_;
                        break;
                    case 12:
                        Periodo_ = "DIC-" + Periodo_;
                        break;
                }
            }
            catch (Exception e)
            {
                string erroMsg = e.StackTrace + e.Message + e.InnerException;
            }

            return Periodo_;

        }
    }

    public class CedulaBusquedaModel
    {
        //private BDMasContextEnt db = new BDMasContextEnt();

        #region "ELEMENTOS DE LA CLASE para la busqueda de cedulas"
        [DataType(DataType.DateTime)]
        public DateTime FechaInicial { get; set; }                          //[DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:yyyy-MM-dd}")]  //[DisplayFormat(DataFormatString = "{0:dd/mm/yyyy}")]  

        [DataType(DataType.DateTime)]                                       //[DataType(DataType.Date)]
        [Required(ErrorMessage = "Si no selecciona, toma la fecha actual.")]
        public DateTime FechaFinal { get; set; }                             //[DisplayFormat(DataFormatString = "{0:d}")]

        public string numerodecedula { get; set; }
        public string responsable { get; set; }
        public string tipopermiso { get; set; }
        public string nombredelbarco { get; set; }
        public string rnpdelbarco { get; set; }
        public string razonsocial { get; set; }
        public string puertobase { get; set; }

        public string alarmas { get; set; }
        public string causas { get; set; }
        public string litorales { get; set; }

        public CedulaEntity CedulaEnt { get; set; }
        public List<CedulaEntity> ResultadoListaCedulas { get; set; }
        //public List<CedulasSeleccionar_Result> ResultadoListaCedulasSP { get; set; }

            //paso temporal
        public List<CedulasSeleccionar_Result> ResultadoListaCedulasSP { get; set; }
        //borrar no corresponde aqui
        public partial class CedulasSeleccionar_Result
        {
            public int IdCedula { get; set; }
            public string NoCedula { get; set; }
            public string TipoAlerta { get; set; }
            public string Causas { get; set; }
            public Nullable<System.DateTime> FechaCedula { get; set; }
            public Nullable<System.DateTime> FechaAlerta { get; set; }
            public string NombreBarco { get; set; }
            public string RNP { get; set; }
            public string OFP { get; set; }
            public string TipoPermiso { get; set; }
            public string RazonSocial { get; set; }
        }

        #endregion

        //public List<CedulaEntity> TodasLasCedulas()
        //{
        //    List<CedulaEntity> listacedulas = (from l in db.TMasCedulas
        //                                       select new CedulaEntity { IdCedula = l.IdCedula, NoCedula = l.NoCedula, TipoAlerta = l.TipoAlerta, FechaCedula = l.FechaCedula, FechaAlerta = l.FechaAlerta, NombreBarco = l.NombreBarco, RNP = l.RNP, TipoPermiso = l.TipoPermiso, RazonSocial = l.RazonSocial }).Distinct().ToList();
        //    if (listacedulas != null)
        //    {
        //        var olistalitorales = from Ced in listacedulas
        //                              orderby Ced.IdCedula
        //                              select Ced;
        //        return olistalitorales.ToList();
        //    }
        //    else
        //    {
        //        return null;
        //    }

        //}

        //public List<CedulasSeleccionar_Result> TodasLasCedulasSP(DateTime FechIni, DateTime FechFin)
        //{
        //    string FechaFormateada = FechIni.Month.ToString() + FechIni.Day.ToString() + FechIni.Year.ToString();

        //    string FechaFormateadaIni = FechIni.ToString("d", CultureInfo.CreateSpecificCulture("en-US"));
        //    string FechaFormateadaFin = FechFin.ToString("d", CultureInfo.CreateSpecificCulture("en-US"));

        //    return db.CedulasSeleccionar(FechaFormateadaIni, FechaFormateadaFin, "", "", "", "", "", "", "", "", "", "").ToList();
        //}

        //public List<CedulasSeleccionar_Result> EncuentraLasCedulasSP(DateTime FechIni, DateTime FechFin, global::System.String alarmaselec, string causaselec,
        //    string numerocedula, string responsable, string tipopermiso, string nombredelbarco, string rnpdelbarco, string razonsocial, string puertobase, string litoralselec)
        //{
        //    if (alarmaselec != null) alarmaselec = alarmaselec.Replace("'", "");
        //    if (causaselec != null) causaselec = causaselec.Replace("'", "");
        //    if (numerocedula != null) numerocedula = numerocedula.Replace("'", "");
        //    if (responsable != null) responsable = responsable.Replace("'", "");
        //    if (tipopermiso != null) tipopermiso = tipopermiso.Replace("'", "");

        //    if (nombredelbarco != null) nombredelbarco = nombredelbarco.Replace("'", "");
        //    if (rnpdelbarco != null) rnpdelbarco = rnpdelbarco.Replace("'", "");
        //    if (razonsocial != null) razonsocial = razonsocial.Replace("'", "");
        //    if (puertobase != null) puertobase = puertobase.Replace("'", "");
        //    if (litoralselec != null) litoralselec = litoralselec.Replace("'", "");

        //    string FechaFormateadaIni = FechIni.ToString("d", CultureInfo.CreateSpecificCulture("en-US"));
        //    string FechaFormateadaFin = FechFin.ToString("d", CultureInfo.CreateSpecificCulture("en-US"));
        //    return db.CedulasSeleccionar(FechaFormateadaIni, FechaFormateadaFin, alarmaselec, numerocedula, responsable, nombredelbarco,
        //        rnpdelbarco, razonsocial, litoralselec, puertobase, tipopermiso, causaselec).ToList();
        //}

        //public List<CedulaEntity> EncuentraLasCedulas(DateTime FechIni, DateTime FechFin, string alarmaselec, string causaselec,
        //    string numerocedula, string responsable, string tipopermiso, string nombredelbarco, string rnpdelbarco, string razonsocial, string puertobase, string litoralselec)
        //{
        //    var data = from l in db.TMasCedulas
        //               select new CedulaEntity
        //               {
        //                   IdCedula = l.IdCedula,
        //                   NoCedula = l.NoCedula,
        //                   TipoAlerta = l.TipoAlerta,
        //                   Causas = l.Causas,
        //                   FechaCedula = l.FechaCedula,
        //                   FechaAlerta = l.FechaAlerta,
        //                   NombreBarco = l.NombreBarco,
        //                   RNP = l.RNP,
        //                   OFP = l.OFP,
        //                   TipoPermiso = l.TipoPermiso,
        //                   RazonSocial = l.RazonSocial
        //               };
        //    if (FechaFinal == new DateTime(001, 1, 1))
        //    {
        //        FechaFinal = DateTime.Now;
        //        FechFin = DateTime.Now;
        //    }
        //    if (!String.IsNullOrEmpty(FechIni.ToString()) && !String.IsNullOrEmpty(FechFin.ToString()))
        //    {
        //        data = data.Where(l => l.FechaCedula >= FechIni && l.FechaCedula <= FechFin);
        //    }
        //    if (!String.IsNullOrEmpty(alarmaselec))
        //    {
        //        data = data.Where(l => l.TipoAlerta == alarmaselec);
        //        if (!String.IsNullOrEmpty(causaselec) && alarmaselec.ToUpper() == "EMERGENCIA")
        //        {
        //            data = data.Where(l => l.Causas == causaselec);
        //        }
        //    }
        //    if (!String.IsNullOrEmpty(numerocedula))
        //    {
        //        data = data.Where(l => l.NoCedula == numerocedula);
        //    }
        //    if (!String.IsNullOrEmpty(responsable))
        //    {
        //        data = data.Where(l => l.OFP.ToUpper().Contains(responsable.ToUpper()));
        //    }

        //    if (data != null)
        //    {
        //        return data.ToList();
        //    }
        //    else
        //    {
        //        return null;
        //    }
        //}

    }// Aqui tenia agregada la clase CedulaEntity
}


public class CedulaEntity
{
    public int IdCedula { get; set; }
    public string NoCedula { get; set; }
    public DateTime? FechaCedula { get; set; }
    public DateTime? FechaAlerta { get; set; }
    public string TipoAlerta { get; set; }
    public string NombreBarco { get; set; }
    public string RNP { get; set; }
    public string TipoPermiso { get; set; }
    public string RazonSocial { get; set; }
    public string Causas { get; set; }
    public string OFP { get; set; }
}
