#pragma checksum "C:\Users\Equipo\Documents\00-Proyectos 2019\MASCore\MASCore\Views\Kardex\DetailsJustificacion.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "0d59c35e169cdcd26a554e36cfecc950f1a5d138"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Kardex_DetailsJustificacion), @"mvc.1.0.view", @"/Views/Kardex/DetailsJustificacion.cshtml")]
[assembly:global::Microsoft.AspNetCore.Mvc.Razor.Compilation.RazorViewAttribute(@"/Views/Kardex/DetailsJustificacion.cshtml", typeof(AspNetCore.Views_Kardex_DetailsJustificacion))]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#line 1 "C:\Users\Equipo\Documents\00-Proyectos 2019\MASCore\MASCore\Views\_ViewImports.cshtml"
using MASCore;

#line default
#line hidden
#line 2 "C:\Users\Equipo\Documents\00-Proyectos 2019\MASCore\MASCore\Views\_ViewImports.cshtml"
using MASCore.Models;

#line default
#line hidden
#line 4 "C:\Users\Equipo\Documents\00-Proyectos 2019\MASCore\MASCore\Views\_ViewImports.cshtml"
using DevExtreme.AspNet.Mvc;

#line default
#line hidden
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"0d59c35e169cdcd26a554e36cfecc950f1a5d138", @"/Views/Kardex/DetailsJustificacion.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"a6f9e97209bd19ae69cd611cb55c19dae5d6d315", @"/Views/_ViewImports.cshtml")]
    public class Views_Kardex_DetailsJustificacion : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<MASCore.ModelReference.KardexModelReference>
    {
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            BeginContext(52, 2, true);
            WriteLiteral("\r\n");
            EndContext();
#line 3 "C:\Users\Equipo\Documents\00-Proyectos 2019\MASCore\MASCore\Views\Kardex\DetailsJustificacion.cshtml"
  
    ViewBag.Title = "Detalles Justificacion";

#line default
#line hidden
            BeginContext(108, 133, true);
            WriteLiteral("\r\n<div id=\"titulo\">\r\n    <h3 style=\"color:#0F2C48;text-align:center\" ><u>-  Módulo Detalle Justificación -</u></h3>        \r\n</div>\r\n");
            EndContext();
            BeginContext(487, 33, true);
            WriteLiteral("\r\n<div class=\"div-comentarios\">\r\n");
            EndContext();
            BeginContext(577, 168, true);
            WriteLiteral("    <table  align=\"center\" >\r\n        <tr>\r\n            <td align=\"right\">Puerto Base :</td>\r\n            <td style=\"background-color:White;border:1px solid #848484\">\r\n");
            EndContext();
            BeginContext(810, 149, true);
            WriteLiteral("            </td>\r\n            <td align=\"right\">Fecha del escrito :</td>\r\n            <td style=\"background-color:White;border:1px solid #848484\">\r\n");
            EndContext();
            BeginContext(1022, 186, true);
            WriteLiteral("            </td>\r\n            \r\n        </tr>\r\n        <tr>\r\n            <td align=\"right\">Embarcación :</td>\r\n            <td style=\"background-color:White;border:1px solid #848484\">\r\n");
            EndContext();
            BeginContext(1270, 149, true);
            WriteLiteral("            </td>\r\n            <td align=\"right\">Fecha de registro :</td>\r\n            <td style=\"background-color:White;border:1px solid #848484\">\r\n");
            EndContext();
            BeginContext(1483, 178, true);
            WriteLiteral("            </td>\r\n        </tr>\r\n        <tr>       \r\n            <td align=\"right\" >Matricula:</td>\r\n            <td style=\"background-color:White;border:1px solid #848484\"> \r\n");
            EndContext();
            BeginContext(1721, 141, true);
            WriteLiteral("            </td>     \r\n            <td align=\"right\" >RNP :</td>\r\n            <td style=\"background-color:White;border:1px solid #848484\">\r\n");
            EndContext();
            BeginContext(1916, 192, true);
            WriteLiteral("            </td>            \r\n        </tr>\r\n        <tr>\r\n            <td align=\"right\">Permiso :</td>\r\n            <td colspan=\"3\" style=\"background-color:White;border:1px solid #848484\">\r\n");
            EndContext();
            BeginContext(2166, 195, true);
            WriteLiteral("            </td>          \r\n        </tr>\r\n        <tr>\r\n            <td align=\"right\">Razón Social :</td>\r\n            <td colspan=\"3\" style=\"background-color:White;border:1px solid #848484\">\r\n");
            EndContext();
            BeginContext(2423, 328, true);
            WriteLiteral(@"            </td>            
        </tr>
        
        <tr>
            <td colspan=""4"">
                <hr />
            </td>            
        </tr>
        <tr>            
            <td align=""right"">Fecha de reconexión :</td>
            <td style=""background-color:White;border:1px solid #848484"">
");
            EndContext();
            BeginContext(2818, 158, true);
            WriteLiteral("            </td>\r\n            <td align=\"right\">Estatus Justificación : </td>\r\n            <td style=\"background-color:White;border:1px solid #848484\"> <b>\r\n");
            EndContext();
            BeginContext(3038, 233, true);
            WriteLiteral("                </b>\r\n            </td>\r\n        </tr>\r\n        <tr>            \r\n            <td align=\"right\">Motivo de la desconexión :</td>\r\n            <td colspan = \"3\" style=\"background-color:White;border:1px solid #848484\">\r\n");
            EndContext();
            BeginContext(3328, 217, true);
            WriteLiteral("            </td>            \r\n        </tr>\r\n        <tr>            \r\n            <td align=\"right\">Llamada Telefonica :</td>\r\n            <td colspan = \"1\" style=\"background-color:White;border:1px solid #848484\">\r\n");
            EndContext();
            BeginContext(3613, 68, true);
            WriteLiteral("            </td>  \r\n            <td colspan = \"2\" align=\"right\"> \r\n");
            EndContext();
            BeginContext(3786, 213, true);
            WriteLiteral("   type=\"submit\" \r\n   id=\"runReport\" \r\n   target=\"_blank\"\r\n   class=\"button Secondary\">\r\n   Ver Llamada\r\n</a>\r\n            </td>           \r\n        </tr>\r\n    </table>\r\n     <br />\r\n</div>\r\n\r\n<br /> \r\n\r\n\r\n\r\n<p>\r\n");
            EndContext();
            BeginContext(4295, 219, true);
            WriteLiteral("</p>\r\n\r\n\r\n\r\n<script type=\"text/javascript\">\r\n    function ImprimirJustificacion() {\r\n        alert(\'Imprimir Justificacion........\');\r\n    }\r\n\r\n\r\n</script>\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n");
            EndContext();
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<MASCore.ModelReference.KardexModelReference> Html { get; private set; }
    }
}
#pragma warning restore 1591
