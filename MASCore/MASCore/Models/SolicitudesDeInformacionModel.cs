﻿using MASCore.DataModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace MASCore.Models
{
    public class SolicitudesDeInformacionModel
    {
        //private BDMasContextEnt db = new BDMasContextEnt();

        //// USADO PARA MOSTRAR DETALLES DE LA SOLICITUD DE INFORMACION 
        public TMasSolInfoCedulas TMASSOLINFOCEDULAS { get; set; }
        //public SolicitudDeInformacionEntradaDatosBusqueda EntradaDatosBusquedaSolInfo { get; set; }
        ////  VARIABLES USADAS PARA MOSTRAR DIVS
        public Boolean MostrandoBusquedaInformacionEmbarcacion { get; set; }
        public Boolean MostrandoBusquedaInformacionSolicitante { get; set; }
        //// VARIABLES USADAS PARA LA BUSQUEDA DE INFORMACION DE EMBARCACION
        public List<BuscaInformacionDeEmbarcacion_Result> InformacionDeEmbarcacion { get; set; }
        public List<BuscaInformacionDeSolicitante_Result> InformacionDeSolicitante { get; set; }
        public List<BuscaInformacionDeEmbarcacion_Result> EncuentraInformacionDeEmbarcacionSP(string Opcion, string Valor)
        {
            if (Valor != null) Valor = Valor.Replace("'", "");
            return null;// db.BuscaInformacionDeEmbarcacion(Opcion, Valor).ToList();
        }
        //// VARIABLES USADAS PARA AGREGAR EMBARCACION     
        public int identrada { get; set; }
        public string barco { get; set; }
        public string rnp { get; set; }
        public string razonsocial { get; set; }
        public string matricula { get; set; }
        public string antena { get; set; }
        public string ErroresAgregarEmbarcacion { get; set; }

        public string busbarco { get; set; }
        public string busmatricula  { get; set; }
        public string busrnp { get; set; }
        
        //Agregados por SZ el 05/12/2016
        public string busbarco_rnp_matricula { get; set; }
        public string busbarco_rnp_matriculaUi { get; set; }
        public string busbarco_rnp_matricula_idVehiculo { get; set; }

        public string embarcacion_idEntrada { get; set; }
        public string embarcacion_RNP { get; set; }
        public string embarcacion_Nombre { get; set; }

        /// CAMPOS USADOS PARA MOSTRAR EL GRID CON LAS EMBARCACIONES AGREGADAS 
        public List<SolInfoRelacionInsertarTMPResult_Result> ResultadoListaSolInfoRelacionSPTMP { get; set; }
        public List<SolInfoRelacionInsertarResult_Result>    ResultadoListaSolInfoRelacionSP { get; set; }
        
        /// CAMPOS BASICOS DE LA SOLICITUD DE INFORMACION       
        public int IdInfoSol { get; set; }

        [Required(ErrorMessage = "* Campo Requerido")]
        public DateTime fecha { get; set; }
        [Required(ErrorMessage = "* Campo Requerido")]
        public string hora { get; set; }
        [Required(ErrorMessage = "* Campo Requerido")]
        public string solicitante { get; set; }
        [Required(ErrorMessage = "* Campo Requerido")]
        public string cargo { get; set; }
        [Required(ErrorMessage = "* Campo Requerido")]
        public string telefono { get; set; }
        [Required(ErrorMessage = "* Campo Requerido")]
        public string email { get; set; }
        [Required(ErrorMessage = "* Campo Requerido")]
        public string ciudad { get; set; }
        [Required(ErrorMessage = "* Campo Requerido")]
        public string estado { get; set; }
        public string asunto { get; set; }
        public string dependencia { get; set; }
        [Required(ErrorMessage = "* Campo Requerido")]
        public string medio { get; set; }
        [Required(ErrorMessage = "* Campo Requerido")]
        public string descripcion { get; set; }
        [Required(ErrorMessage = "* Campo Requerido")]
        public string ofp { get; set; }
        [Required(ErrorMessage = "* Campo Requerido")]
        [StringLength(13, ErrorMessage = "Ingresar maximo 13 caracteres")]
        public string folio { get; set; }

        public int InsertaRelacionSolInfo(int IdEntrada, int IdEntradaTemporal)
        {
            int ResultadoInsert = 0;
            SolInfoRelacionInsertar_Result DatosDeSolInfoRelacion = null;// db.SolInfoRelacionInsertar(IdEntrada, IdEntradaTemporal).FirstOrDefault();     

            if (DatosDeSolInfoRelacion != null)
            {
                ResultadoInsert = Convert.ToInt32(DatosDeSolInfoRelacion.Cantidad);
            }


            return ResultadoInsert;
        }

        public int SolInfoActualizarSP_returnID(SolicitudesDeInformacionModel ModeloTmp_)
        {
            int IdSolInfo = 0;

            //string FechaString = ModeloTmp_.fecha.ToString();
            string FechaString = ModeloTmp_.fecha.Year.ToString() + '-' + ModeloTmp_.fecha.Month.ToString().PadLeft(2, '0') + '-' + ModeloTmp_.fecha.Day.ToString().PadLeft(2, '0') + " 00:00:00";


            SolInfoActualizar_Result Resultado = null;// db.SolInfoActualizar(ModeloTmp_.IdInfoSol,FechaString, ModeloTmp_.hora, ModeloTmp_.solicitante, ModeloTmp_.cargo, ModeloTmp_.telefono,
               // ModeloTmp_.email, ModeloTmp_.ciudad, ModeloTmp_.estado, ModeloTmp_.asunto, ModeloTmp_.dependencia, ModeloTmp_.medio, ModeloTmp_.descripcion, ModeloTmp_.ofp,ModeloTmp_.folio).FirstOrDefault();

            if (Resultado != null)
                IdInfoSol = Resultado.IdInfoSol;           

            return IdInfoSol;
        }
        
        public int SolInfoInsertarSP_returnID(SolicitudesDeInformacionModel ModeloTmp_)
        {
            int IdSolInfo = 0;
            //string FechaString = ModeloTmp_.fecha.ToString();
            string FechaString = ModeloTmp_.fecha.Year.ToString() + '-' + ModeloTmp_.fecha.Month.ToString().PadLeft(2, '0') + '-' + ModeloTmp_.fecha.Day.ToString().PadLeft(2, '0') + " 00:00:00";

            SolInfoInsertar_Result Resultado = null;// db.SolInfoInsertar(FechaString, ModeloTmp_.hora, ModeloTmp_.solicitante, ModeloTmp_.cargo, ModeloTmp_.telefono,
                //ModeloTmp_.email, ModeloTmp_.ciudad, ModeloTmp_.estado, ModeloTmp_.asunto, ModeloTmp_.dependencia, ModeloTmp_.medio, ModeloTmp_.descripcion, ModeloTmp_.ofp).FirstOrDefault();

            if (Resultado != null)
                IdInfoSol = Resultado.IdInfoSol;

            ////  ACTUALIZA LA INFORMACION MODIFICADA DEL SOLICITANTE EN LA TABLA DE SOLICITANTES
            //TMasSolicitantes tmassolicitantes = new TMasSolicitantes();
            //tmassolicitantes = db.TMasSolicitantes.Single(t => t.Nombre == ModeloTmp_.solicitante);
            //if (tmassolicitantes != null)
            //{
            //    TMasSolicitantes NuevoSolicitante = new TMasSolicitantes();
            //    NuevoSolicitante = tmassolicitantes;
            //    NuevoSolicitante.Telefono1 = ModeloTmp_.telefono;
            //    NuevoSolicitante.CorreoInstitucional = ModeloTmp_.email;
            //    db.TMasSolicitantes.Attach(NuevoSolicitante);
            //    db.ObjectStateManager.ChangeObjectState(NuevoSolicitante, EntityState.Modified);
            //    db.SaveChanges();
            //}


            return IdInfoSol;
        }

        public int SolInfoRelacionInsertarDefinitivoSP_returnID(SolicitudesDeInformacionModel ModeloTmp_, string Usuario)
        {
            ////////// int IdEntrada_, string RNP_, string Matricula_, string Barco_, string RazonSocial_,
            //////////string Transmitiendo_, string UltimaTx_, string EnPuerto_, string NombrePuerto_, string Permisos_
            //Valida si el RNP ya se encuentra en los agregados para este ID
            int IDENTRADA = ModeloTmp_.identrada;
            FuncionesProcedimientos FunProc = new FuncionesProcedimientos();
            //Si no Existe Busca los datos faltantes:
            //UltimaTX
            //EnPuerto
            //Nombre Puerto
            //Permisos
            string ModeloTmp_Transmitiendo = "NO INFO";
            string ModeloTmp_UltimaTX = "NO INFO";
            string ModeloTmp_EnPuerto = "NO INFO";
            string ModeloTmp_NombrePuerto = "";
            string ModeloTmp_Permisos = "NO INFO";


            //ModeloTmp_Permisos = FunProc.BuscaResultadoGenericoConParametrosPermisos("permisos", ModeloTmp_.matricula);  comentado el 20 de agosto
            ModeloTmp_Permisos = null;// FunProc.BuscaResultadoGenericoConParametrosPermisos("permisossolinfo", ModeloTmp_.matricula);
            if (ModeloTmp_Permisos == "")
                ModeloTmp_Permisos = "NO INFO";


            //string ModeloTmp_Tx = "NO INFO";
            /////////////////////////////////////////////////////////////////////    
            ////// DateTime oldTime = new DateTime(2011, 11, 16, 17, 41, 45);
            ////// TimeZoneInfo timeZone1 = TimeZoneInfo.FindSystemTimeZoneById("Central Standard Time (Mexico)");//Mazatlan
            ////// TimeZoneInfo timeZone2 = TimeZoneInfo.FindSystemTimeZoneById("Mountain Standard Time (Mexico)");//Monterrey
            ////// DateTime newTime = TimeZoneInfo.ConvertTime(oldTime, timeZone1, timeZone2);
            //////List< string> deeededede = new List<string>();
            //////deeededede.Add(date1.ToUniversalTime().ToString());
            //////deeededede.Add(TimeZoneInfo.ConvertTimeToUtc(date1).ToString());
            //////deeededede.Add(TimeZoneInfo.ConvertTimeToUtc(date1, TimeZoneInfo.Local).ToString());
            //////deeededede.Add(TimeZoneInfo.ConvertTimeToUtc(date1, tz).ToString());           
            //////ICollection<TimeZoneInfo> timeZones = TimeZoneInfo.GetSystemTimeZones(); 
            //////Console.WriteLine(date1.ToUniversalTime());
            //////Console.WriteLine(TimeZoneInfo.ConvertTimeToUtc(date1));
            //////Console.WriteLine(TimeZoneInfo.ConvertTimeToUtc(date1, TimeZoneInfo.Local));
            //////Console.WriteLine(TimeZoneInfo.ConvertTimeToUtc(date1, tz));   
            //DateTime date1 = new DateTime(2011, 11, 16, 17, 41, 45);
            //TimeZoneInfo tz = TimeZoneInfo.FindSystemTimeZoneById("Mountain Standard Time (Mexico)");
            //string TiempoMazatlan = TimeZoneInfo.ConvertTimeFromUtc(date1, tz).ToString();                       


            List<BuscaUltimaTx_Result> DatosUltimaTx = null;// db.BuscaUltimaTx(ModeloTmp_.matricula,Usuario).ToList();   //Obtiene UltimaTX y el lugar

            string Lugar = "";
            string NombrePuerto = "";
            if (DatosUltimaTx != null)
            {
                foreach (BuscaUltimaTx_Result Res in DatosUltimaTx)
                {
                    ModeloTmp_UltimaTX = Res.UltimaTx.ToString();
                    if (Res.Lugar.ToUpper() == "EN PUERTO")
                    {
                        Lugar = "EN PUERTO";
                        //ModeloTmp_UltimaTX = Res.UltimaTx.ToString();
                        NombrePuerto = Res.NombrePuerto.ToString();
                        break;
                    }
                }
                //ModeloTmp_UltimaTX = DatosUltimaTx.UltimaTx.ToString();
                if (Lugar.ToUpper() == "EN PUERTO")
                {
                    ModeloTmp_EnPuerto = "SI";
                }
                else
                {
                    ModeloTmp_EnPuerto = "NO";
                }
                //DateTime FechaDeUltimaTransmision = DateTime.Parse(ModeloTmp_UltimaTX.ToString());

                //TimeSpan diff = DateTime.Now.Subtract(FechaDeUltimaTransmision);
                //long HorasDeDiferencia = (long)diff.TotalHours;
                long HorasDeDiferencia = 0;
                if (ModeloTmp_UltimaTX != "NO INFO")
                {
                    DateTime FechaDeUltimaTransmision = DateTime.Parse(ModeloTmp_UltimaTX.ToString());
                    TimeSpan diff = DateTime.Now.Subtract(FechaDeUltimaTransmision);
                    HorasDeDiferencia = (long)diff.TotalHours;
                }
                //Boolean isTx = false;
                // obtiene el tiempo de gracia sin transmision de la DB
                int tiempogracia = 72;
                //var Unknow = null;// FunProc.LlenarResultadoGenerico("tiempograciasintx");
                //foreach (var item in Unknow)
                //{
                //    tiempogracia = Convert.ToInt32(item.Resultado);
                //}
                if (HorasDeDiferencia < tiempogracia)
                {
                    //isTx = false;
                    ModeloTmp_Transmitiendo = "SI";
                }
                else
                {
                    //isTx = true;
                    ModeloTmp_Transmitiendo = "NO";
                }
                if (HorasDeDiferencia == 0 && ModeloTmp_UltimaTX == "NO INFO") ModeloTmp_Transmitiendo = "NO INFO";
                if (ModeloTmp_EnPuerto == "SI")
                {
                    //Busca el nombre del puerto por las coordenadas
                    ModeloTmp_NombrePuerto = NombrePuerto;
                }
                else
                {
                    ModeloTmp_NombrePuerto = "";// SE QUITO "EN MAR"
                }
            }

            //date1 = DateTime.Parse(ModeloTmp_UltimaTX);
            //ModeloTmp_UltimaTX = TimeZoneInfo.ConvertTimeFromUtc(date1, tz).ToString();



            // Saca si la fecha corresponde es en horario de verano o no 
            //Obtiene primer dia del horario de verano del año en curso
            //Obtiene el ultimo dia del horario de verano del año en curso
            //Compara si la fecha de la ultima TX esta dentro del rango de estas dos fechas significa que es horario de verano

            //ModeloTmp_EnPuerto = ModeloTmp_.Estatus(DatosUltimaTx);





            //Ya que tiene toda la informacion se Inserta
            //List<SolInfoRelacionInsertarTMP_Result>
            List<SolInfoRelacionInsertarDefinitivo_Result> Resultado = null;// db.SolInfoRelacionInsertarDefinitivo(ModeloTmp_.identrada, ModeloTmp_.rnp, ModeloTmp_.matricula, ModeloTmp_.barco, ModeloTmp_.razonsocial, ModeloTmp_Transmitiendo, ModeloTmp_UltimaTX, ModeloTmp_EnPuerto, ModeloTmp_NombrePuerto, ModeloTmp_Permisos).ToList();
            //List<SolInfoRelacionInsertarTMP_Result> Resultado = db.SolInfoRelacionInsertarTMP(ModeloTmp_.identrada, ModeloTmp_.rnp, ModeloTmp_.matricula, ModeloTmp_.barco, ModeloTmp_.razonsocial, ModeloTmp_Transmitiendo, ModeloTmp_UltimaTX, ModeloTmp_EnPuerto, ModeloTmp_NombrePuerto, ModeloTmp_Permisos).ToList();
            if (Resultado.Count > 0)
            {
                IDENTRADA = Convert.ToInt32(Resultado[0].IdEntrada);
            }
            //Ya insertado , hace la busqueda de embarcaciones que se tiene contemplado agregar a la cedula
            //List<SolInfoRelacionEntity> ResultadoListaSolInfoRelacionSP = new List<SolInfoRelacionEntity>();
            //ResultadoListaSolInfoRelacionSP = db.BuscaSolicitudesDeInformacion(FechaFormateadaIni, FechaFormateadaFin, medio, asunto, dependencia, ofp, solicitante, folio).ToList();

            return IDENTRADA;
        }

        public int SolInfoRelacionInsertarDefinitivoSP_returnID2(string DatosaBuscar_, int identrada_, string Usuario)
        {
            FuncionesProcedimientos FunProc = new FuncionesProcedimientos();
            int IDENTRADA = identrada_;

            List<ObtieneUltimasTX2_Result> NuevaListaDeEmbarcaciones = new List<ObtieneUltimasTX2_Result>();
            //obtiene la lista de transmisiones
            List<ObtieneUltimasTX2_Result> ListaDeEmbarcaciones = new List<ObtieneUltimasTX2_Result>();
            #region Genera Lista de Embarcaciones con permisos

            ListaDeEmbarcaciones = null;// db.ObtieneUltimasTX2(Usuario, DatosaBuscar_).ToList();
            int Contador = 0;
            string MatriculaAnterior = "";
            foreach (ObtieneUltimasTX2_Result Registro in ListaDeEmbarcaciones)
            {
                ObtieneUltimasTX2_Result EmbarcacionMod = new ObtieneUltimasTX2_Result();
                Contador += 1;
                if (MatriculaAnterior == "")
                {
                    //es el primer registro 
                    MatriculaAnterior = Registro.Matricula;
                    EmbarcacionMod = Registro;
                    EmbarcacionMod.TipoPermiso = "";// FunProc.BuscaResultadoGenericoConParametrosPermisos("permisossolinfo", Registro.Matricula);
                    NuevaListaDeEmbarcaciones.Add(EmbarcacionMod);
                }
                else
                {
                    if (MatriculaAnterior != Registro.Matricula)
                    {
                        //la matricula es otra
                        MatriculaAnterior = Registro.Matricula;
                        EmbarcacionMod = Registro;
                        EmbarcacionMod.TipoPermiso = "";// FunProc.BuscaResultadoGenericoConParametrosPermisos("permisossolinfo", Registro.Matricula);
                        NuevaListaDeEmbarcaciones.Add(EmbarcacionMod);
                    }
                    else if (MatriculaAnterior == Registro.Matricula)
                    {
                        //No hace nada por que ya se obtubieron los permisos con la funcion  BuscaResultadoGenericoConParametrosPermisos
                    }
                }
            }
            #endregion
            // RECORRE LA NUEVA LISTA DE EMBARCACIONES CON LOS PERMISOS ACOMODADOS E INSERTA EN DB
            foreach (ObtieneUltimasTX2_Result NuevaEmbarcacion in NuevaListaDeEmbarcaciones)
            {
                string ModeloTmp_Transmitiendo = "NO INFO";
                string ModeloTmp_UltimaTX = "NO INFO";
                string ModeloTmp_EnPuerto = "NO INFO";
                string ModeloTmp_NombrePuerto = "";

                #region Obtiene datos de Transmitiendo, UltimaTx, En Puerto, Nombre Puerto

                List<BuscaUltimaTx_Result> DatosUltimaTx = null;// db.BuscaUltimaTx(NuevaEmbarcacion.Matricula, Usuario).ToList();   //Obtiene UltimaTX y el lugar
                string Lugar = "";
                string NombrePuerto = "";
                if (DatosUltimaTx != null)
                {
                    foreach (BuscaUltimaTx_Result Res in DatosUltimaTx)
                    {
                        ModeloTmp_UltimaTX = Res.UltimaTx.ToString();
                        if (Res.Lugar.ToUpper() == "EN PUERTO")
                        {
                            Lugar = "EN PUERTO";
                            //ModeloTmp_UltimaTX = Res.UltimaTx.ToString();
                            NombrePuerto = Res.NombrePuerto.ToString();
                            break;
                        }
                    }
                    //ModeloTmp_UltimaTX = DatosUltimaTx.UltimaTx.ToString();
                    if (Lugar.ToUpper() == "EN PUERTO")
                    {
                        ModeloTmp_EnPuerto = "SI" + " - " + NombrePuerto;
                    }
                    else
                    {
                        ModeloTmp_EnPuerto = "NO";
                    }

                    long HorasDeDiferencia = 0;
                    if (ModeloTmp_UltimaTX != "NO INFO")
                    {
                        DateTime FechaDeUltimaTransmision = DateTime.Parse(ModeloTmp_UltimaTX.ToString());
                        TimeSpan diff = DateTime.Now.Subtract(FechaDeUltimaTransmision);
                        HorasDeDiferencia = (long)diff.TotalHours;
                    }
                    // obtiene el tiempo de gracia sin transmision de la DB
                    int tiempogracia = 72;
                    //var Unknow = FunProc.LlenarResultadoGenerico("tiempograciasintx");
                    //foreach (var item in Unknow)
                    //{
                    //    tiempogracia = Convert.ToInt32(item.Resultado);
                    //}
                    if (HorasDeDiferencia < tiempogracia)
                    {
                        //isTx = false;
                        ModeloTmp_Transmitiendo = "SI";
                    }
                    else
                    {
                        //isTx = true;
                        ModeloTmp_Transmitiendo = "NO";
                    }
                    if (HorasDeDiferencia == 0 && ModeloTmp_UltimaTX == "NO INFO") ModeloTmp_Transmitiendo = "NO INFO";
                    //if (HorasDeDiferencia == 0) ModeloTmp_Transmitiendo = "NO INFO";
                    if (ModeloTmp_EnPuerto == "SI")
                    {
                        //Busca el nombre del puerto por las coordenadas
                        ModeloTmp_NombrePuerto = NombrePuerto;
                    }
                    else
                    {
                        ModeloTmp_NombrePuerto = "";
                    }
                }

                #endregion
                //SolInfoRelacionInsertarDefinitivo
                List<SolInfoRelacionInsertarDefinitivo_Result> Resultado = null;// db.SolInfoRelacionInsertarDefinitivo(IDENTRADA, NuevaEmbarcacion.RNP, NuevaEmbarcacion.Matricula, NuevaEmbarcacion.Embarcacion, NuevaEmbarcacion.RazonSocial, ModeloTmp_Transmitiendo, ModeloTmp_UltimaTX, ModeloTmp_EnPuerto, NuevaEmbarcacion.PuertoBase, NuevaEmbarcacion.TipoPermiso).ToList();
                //List<SolInfoRelacionInsertarTMP_Result> Resultado = db.SolInfoRelacionInsertarTMP(IDENTRADA, NuevaEmbarcacion.RNP, NuevaEmbarcacion.Matricula, NuevaEmbarcacion.Embarcacion, NuevaEmbarcacion.RazonSocial, ModeloTmp_Transmitiendo, ModeloTmp_UltimaTX, ModeloTmp_EnPuerto, NuevaEmbarcacion.PuertoBase, NuevaEmbarcacion.TipoPermiso).ToList();
                if (Resultado.Count > 0)
                {
                    IDENTRADA = Convert.ToInt32(Resultado[0].IdEntrada);
                }
            }

            return IDENTRADA;
        }

        public int SolInfoRelacionInsertarTMPSP_returnID2(string DatosaBuscar_, int identrada_, string Usuario)
        {
            FuncionesProcedimientos FunProc = new FuncionesProcedimientos();
            int IDENTRADA = identrada_;

            List<ObtieneUltimasTX2_Result> NuevaListaDeEmbarcaciones = new List<ObtieneUltimasTX2_Result>();
            //obtiene la lista de transmisiones
            List<ObtieneUltimasTX2_Result> ListaDeEmbarcaciones = new List<ObtieneUltimasTX2_Result>();
            #region Genera Lista de Embarcaciones con permisos
            
            ListaDeEmbarcaciones = null;//db.ObtieneUltimasTX2(Usuario, DatosaBuscar_).ToList();
            int Contador = 0;
            string MatriculaAnterior = "";
            foreach (ObtieneUltimasTX2_Result Registro in ListaDeEmbarcaciones)
            {
                ObtieneUltimasTX2_Result EmbarcacionMod = new ObtieneUltimasTX2_Result();
                Contador += 1;
                if (MatriculaAnterior == "")
                {
                    //es el primer registro 
                    MatriculaAnterior = Registro.Matricula;
                    EmbarcacionMod = Registro;
                    EmbarcacionMod.TipoPermiso = "";// FunProc.BuscaResultadoGenericoConParametrosPermisos("permisossolinfo", Registro.Matricula);
                    NuevaListaDeEmbarcaciones.Add(EmbarcacionMod);
                }
                else
                {
                    if (MatriculaAnterior != Registro.Matricula)
                    {
                        //la matricula es otra
                        MatriculaAnterior = Registro.Matricula;
                        EmbarcacionMod = Registro;
                        EmbarcacionMod.TipoPermiso = "";// FunProc.BuscaResultadoGenericoConParametrosPermisos("permisossolinfo", Registro.Matricula);
                        NuevaListaDeEmbarcaciones.Add(EmbarcacionMod);
                    }
                    else if (MatriculaAnterior == Registro.Matricula)
                    {
                        //No hace nada por que ya se obtubieron los permisos con la funcion  BuscaResultadoGenericoConParametrosPermisos
                    }
                }
            }
            #endregion
            // RECORRE LA NUEVA LISTA DE EMBARCACIONES CON LOS PERMISOS ACOMODADOS E INSERTA EN DB
            foreach( ObtieneUltimasTX2_Result NuevaEmbarcacion in NuevaListaDeEmbarcaciones)
            {
                string ModeloTmp_Transmitiendo = "NO INFO";
                string ModeloTmp_UltimaTX = "NO INFO";
                string ModeloTmp_EnPuerto = "NO INFO";
                string ModeloTmp_NombrePuerto = "";

                #region Obtiene datos de Transmitiendo, UltimaTx, En Puerto, Nombre Puerto

                List<BuscaUltimaTx_Result> DatosUltimaTx = null;// db.BuscaUltimaTx(NuevaEmbarcacion.Matricula, Usuario).ToList();   //Obtiene UltimaTX y el lugar
                string Lugar = "";
                string NombrePuerto = "";
                if (DatosUltimaTx != null)
                {
                    foreach (BuscaUltimaTx_Result Res in DatosUltimaTx)
                    {
                        ModeloTmp_UltimaTX = Res.UltimaTx.ToString();
                        if (Res.Lugar.ToUpper() == "EN PUERTO")
                        {
                            Lugar = "EN PUERTO";
                            //ModeloTmp_UltimaTX = Res.UltimaTx.ToString();
                            NombrePuerto = Res.NombrePuerto.ToString();
                            break;
                        }
                    }
                    //ModeloTmp_UltimaTX = DatosUltimaTx.UltimaTx.ToString();
                    if (Lugar.ToUpper() == "EN PUERTO")
                    {
                        ModeloTmp_EnPuerto = "SI" + " - " + NombrePuerto;
                    }
                    else
                    {
                        ModeloTmp_EnPuerto = "NO";
                    }

                    long HorasDeDiferencia = 0;
                    if (ModeloTmp_UltimaTX != "NO INFO")
                    {
                        DateTime FechaDeUltimaTransmision = DateTime.Parse(ModeloTmp_UltimaTX.ToString());
                        TimeSpan diff = DateTime.Now.Subtract(FechaDeUltimaTransmision);
                        HorasDeDiferencia = (long)diff.TotalHours;
                    }

                    // obtiene el tiempo de gracia sin transmision de la DB
                    int tiempogracia = 72;
                    //var Unknow = FunProc.LlenarResultadoGenerico("tiempograciasintx");
                    //foreach(var item in Unknow)
                    //{
                    //    tiempogracia = Convert.ToInt32( item.Resultado);
                    //}

                    if (HorasDeDiferencia < tiempogracia)
                    {
                        //isTx = false;
                        ModeloTmp_Transmitiendo = "SI";
                    }
                    else
                    {
                        //isTx = true;
                        ModeloTmp_Transmitiendo = "NO";
                    }
                    if (HorasDeDiferencia == 0 && ModeloTmp_UltimaTX == "NO INFO") ModeloTmp_Transmitiendo = "NO INFO";
                    //if (HorasDeDiferencia == 0) ModeloTmp_Transmitiendo = "NO INFO";
                    if (ModeloTmp_EnPuerto == "SI")
                    {
                        //Busca el nombre del puerto por las coordenadas
                        ModeloTmp_NombrePuerto = NombrePuerto;
                    }
                    else
                    {
                        ModeloTmp_NombrePuerto = "";
                    }
                }

                #endregion

                List<SolInfoRelacionInsertarTMP_Result> Resultado = null;
                // db.SolInfoRelacionInsertarTMP(
                    //IDENTRADA, 
                    //NuevaEmbarcacion.RNP, 
                    //NuevaEmbarcacion.Matricula, 
                    //NuevaEmbarcacion.Embarcacion, 
                    //NuevaEmbarcacion.RazonSocial, 
                    //ModeloTmp_Transmitiendo, 
                    //ModeloTmp_UltimaTX, 
                    //ModeloTmp_EnPuerto, 
                    //NuevaEmbarcacion.PuertoBase, 
                    //NuevaEmbarcacion.TipoPermiso).ToList();

                if (Resultado.Count > 0)
                {
                    IDENTRADA = Convert.ToInt32(Resultado[0].IdEntrada);
                }
            }
            return IDENTRADA;
        }

        public int SolInfoRelacionInsertarTMPSP_returnID(SolicitudesDeInformacionModel ModeloTmp_, string Usuario)
        {
           ////////// int IdEntrada_, string RNP_, string Matricula_, string Barco_, string RazonSocial_,
           //////////string Transmitiendo_, string UltimaTx_, string EnPuerto_, string NombrePuerto_, string Permisos_
            //Valida si el RNP ya se encuentra en los agregados para este ID
            int IDENTRADA = ModeloTmp_.identrada;
            FuncionesProcedimientos FunProc = new FuncionesProcedimientos();
            //Si no Existe Busca los datos faltantes:
            //UltimaTX
            //EnPuerto
            //Nombre Puerto
            //Permisos
            string ModeloTmp_Transmitiendo = "NO INFO";
            string ModeloTmp_UltimaTX = "NO INFO";
            string ModeloTmp_EnPuerto = "NO INFO";
            string ModeloTmp_NombrePuerto = "";
            string ModeloTmp_Permisos = "NO INFO";


            ModeloTmp_Permisos = null;// FunProc.BuscaResultadoGenericoConParametrosPermisos("permisossolinfo", ModeloTmp_.matricula);
            if (ModeloTmp_Permisos == "")
                ModeloTmp_Permisos = "NO INFO";


            //string ModeloTmp_Tx = "NO INFO";
            /////////////////////////////////////////////////////////////////////    
            ////// DateTime oldTime = new DateTime(2011, 11, 16, 17, 41, 45);
            ////// TimeZoneInfo timeZone1 = TimeZoneInfo.FindSystemTimeZoneById("Central Standard Time (Mexico)");//Mazatlan
            ////// TimeZoneInfo timeZone2 = TimeZoneInfo.FindSystemTimeZoneById("Mountain Standard Time (Mexico)");//Monterrey
            ////// DateTime newTime = TimeZoneInfo.ConvertTime(oldTime, timeZone1, timeZone2);
            //////List< string> deeededede = new List<string>();
            //////deeededede.Add(date1.ToUniversalTime().ToString());
            //////deeededede.Add(TimeZoneInfo.ConvertTimeToUtc(date1).ToString());
            //////deeededede.Add(TimeZoneInfo.ConvertTimeToUtc(date1, TimeZoneInfo.Local).ToString());
            //////deeededede.Add(TimeZoneInfo.ConvertTimeToUtc(date1, tz).ToString());           
            //////ICollection<TimeZoneInfo> timeZones = TimeZoneInfo.GetSystemTimeZones(); 
            //////Console.WriteLine(date1.ToUniversalTime());
            //////Console.WriteLine(TimeZoneInfo.ConvertTimeToUtc(date1));
            //////Console.WriteLine(TimeZoneInfo.ConvertTimeToUtc(date1, TimeZoneInfo.Local));
            //////Console.WriteLine(TimeZoneInfo.ConvertTimeToUtc(date1, tz));   
            //DateTime date1 = new DateTime(2011, 11, 16, 17, 41, 45);
            //TimeZoneInfo tz = TimeZoneInfo.FindSystemTimeZoneById("Mountain Standard Time (Mexico)");
            //string TiempoMazatlan = TimeZoneInfo.ConvertTimeFromUtc(date1, tz).ToString();                       

            #region Obtiene datos de Transmitiendo, UltimaTx, En Puerto, Nombre Puerto

            List<BuscaUltimaTx_Result> DatosUltimaTx = null;// db.BuscaUltimaTx(ModeloTmp_.matricula,Usuario).ToList();   //Obtiene UltimaTX y el lugar
            string Lugar = "";
            string NombrePuerto = "";
            if (DatosUltimaTx != null)
            {
                foreach (BuscaUltimaTx_Result Res in DatosUltimaTx)
                {
                    ModeloTmp_UltimaTX = Res.UltimaTx.ToString();
                    if (Res.Lugar.ToUpper() == "EN PUERTO")
                    {
                        Lugar = "EN PUERTO";
                        //ModeloTmp_UltimaTX = Res.UltimaTx.ToString();
                        NombrePuerto = Res.NombrePuerto.ToString();
                        break;
                    }
                }
                //ModeloTmp_UltimaTX = DatosUltimaTx.UltimaTx.ToString();
                if (Lugar.ToUpper() == "EN PUERTO")
                {
                    ModeloTmp_EnPuerto = "SI";
                }
                else
                {
                    ModeloTmp_EnPuerto = "NO";
                }

                long HorasDeDiferencia = 0;
                if (ModeloTmp_UltimaTX != "NO INFO")
                {
                    DateTime FechaDeUltimaTransmision = DateTime.Parse(ModeloTmp_UltimaTX.ToString());
                    TimeSpan diff = DateTime.Now.Subtract(FechaDeUltimaTransmision);
                    HorasDeDiferencia = (long)diff.TotalHours;
                }

                // obtiene el tiempo de gracia sin transmision de la DB
                int tiempogracia = 72;
                //var Unknow = FunProc.LlenarResultadoGenerico("tiempograciasintx");
                //foreach (var item in Unknow)
                //{
                //    tiempogracia = Convert.ToInt32(item.Resultado);
                //}

                if (HorasDeDiferencia < tiempogracia)
                {
                    //isTx = false;
                    ModeloTmp_Transmitiendo = "SI";
                }
                else
                {
                    //isTx = true;
                    ModeloTmp_Transmitiendo = "NO";
                }
                if (HorasDeDiferencia == 0 && ModeloTmp_UltimaTX == "NO INFO") ModeloTmp_Transmitiendo = "NO INFO";
                //if (HorasDeDiferencia == 0) ModeloTmp_Transmitiendo = "NO INFO";
                if (ModeloTmp_EnPuerto == "SI")
                {
                    //Busca el nombre del puerto por las coordenadas
                    ModeloTmp_NombrePuerto = NombrePuerto;
                }
                else
                {
                    ModeloTmp_NombrePuerto = "";
                }
            }

            #endregion
            
            //date1 = DateTime.Parse(ModeloTmp_UltimaTX);
            //ModeloTmp_UltimaTX = TimeZoneInfo.ConvertTimeFromUtc(date1, tz).ToString();



            // Saca si la fecha corresponde es en horario de verano o no 
                //Obtiene primer dia del horario de verano del año en curso
                //Obtiene el ultimo dia del horario de verano del año en curso
                //Compara si la fecha de la ultima TX esta dentro del rango de estas dos fechas significa que es horario de verano

            //ModeloTmp_EnPuerto = ModeloTmp_.Estatus(DatosUltimaTx);
            
            



            //Ya que tiene toda la informacion se Inserta
            //List<SolInfoRelacionInsertarTMP_Result>
            List<SolInfoRelacionInsertarTMP_Result> Resultado = null;//db.SolInfoRelacionInsertarTMP(ModeloTmp_.identrada, ModeloTmp_.rnp, ModeloTmp_.matricula, ModeloTmp_.barco, ModeloTmp_.razonsocial, ModeloTmp_Transmitiendo, ModeloTmp_UltimaTX, ModeloTmp_EnPuerto, ModeloTmp_NombrePuerto, ModeloTmp_Permisos).ToList();
            if (Resultado.Count > 0)
            {
                IDENTRADA = Convert.ToInt32(Resultado[0].IdEntrada);
            }
            //Ya insertado , hace la busqueda de embarcaciones que se tiene contemplado agregar a la cedula
            //List<SolInfoRelacionEntity> ResultadoListaSolInfoRelacionSP = new List<SolInfoRelacionEntity>();
            //ResultadoListaSolInfoRelacionSP = db.BuscaSolicitudesDeInformacion(FechaFormateadaIni, FechaFormateadaFin, medio, asunto, dependencia, ofp, solicitante, folio).ToList();

            return IDENTRADA;
        }

        public string Estatus(BuscaUltimaTx_Result DatosUltimaTx_)
        {
            String EstatusTmp = "";
            //int hrsDiff = System.Data.Linq.SqlClient.SqlMethods.DateDiffHour(startDT, endDT);
            //DateTime date1 = DatosUltimaTx_.UltimaTx;  
            //DateTime date2 = DateTime.Now;  
            //int HorasDeDiferencia = ((TimeSpan) (DateTime.Now - DatosUltimaTx_.UltimaTx)).Hours;
            DateTime FechaDeUltimaTransmision = DateTime.Parse(DatosUltimaTx_.UltimaTx.ToString());

            TimeSpan diff = DateTime.Now.Subtract(FechaDeUltimaTransmision);
            long HorasDeDiferencia = (long)diff.TotalHours;



            if (HorasDeDiferencia >= 72)
            {
                EstatusTmp = "DESCONECTADO POR MAS DE 72 HORAS";
                EstatusTmp = "DESCONECTADO POR " + HorasDeDiferencia + " HORAS";
            }
            else
            {
                EstatusTmp = "TRANSMITIENDO";
            }
            EstatusTmp = EstatusTmp + " EN " + DatosUltimaTx_.Lugar;
            //int r = Dat("h", DatosUltimaTx_.UltimaTx, DateTime.Now);
            //DatosUltimaTx_.UltimaTx
            return EstatusTmp;
        }

        public List<SolInfoRelacionInsertarResult_Result> SolInfoRelacionInsertarSP_Result(int ID)
        {
            List<SolInfoRelacionInsertarResult_Result> lista = new List<SolInfoRelacionInsertarResult_Result>();
            //int IDENTRADA = ModeloTmp_.identrada;
            List<SolInfoRelacionInsertarResult_Result> ResultadoListaSolInfoRelacionSP = new List<SolInfoRelacionInsertarResult_Result>();     //var AAA = db.SolInfoRelacionInsertarResult(ID).ToList();            
            ResultadoListaSolInfoRelacionSP = null;//db.SolInfoRelacionInsertarResult(ID).ToList();
            foreach (SolInfoRelacionInsertarResult_Result Resultado in ResultadoListaSolInfoRelacionSP)
            {
                SolInfoRelacionInsertarResult_Result Reg = new SolInfoRelacionInsertarResult_Result();
                Reg = Resultado;
                String Registro = Reg.Permisos;
                string[] Tokens = Registro.Split('|');
                string PermisosEnRenglon = "";
                foreach (string token in Tokens)
                {
                    string Elemento = token;
                    if (Elemento.Contains("VENCIDO"))
                    {
                        Elemento = "<font color=red>" + Elemento + "</font>";    
                    }
                    PermisosEnRenglon += Elemento + "|";
                }
                PermisosEnRenglon = PermisosEnRenglon.Substring(0, PermisosEnRenglon.Length - 1);
                PermisosEnRenglon = PermisosEnRenglon.Replace(",", "</BR>");
                PermisosEnRenglon = PermisosEnRenglon.Replace("|", "</BR></BR>");                  
                Reg.Permisos = PermisosEnRenglon;
                lista.Add(Reg);
            }
            return lista;
        }

        public List<SolInfoRelacionInsertarTMPResult_Result> SolInfoRelacionInsertarTMPSP_Result(int ID)
        {
            List<SolInfoRelacionInsertarTMPResult_Result> lista = new List<SolInfoRelacionInsertarTMPResult_Result>();
            List<SolInfoRelacionInsertarTMPResult_Result> ResultadoListaSolInfoRelacionSP = new List<SolInfoRelacionInsertarTMPResult_Result>();

            ResultadoListaSolInfoRelacionSP = null;//db.SolInfoRelacionInsertarTMPResult(ID).ToList();

            //foreach (SolInfoRelacionInsertarTMPResult_Result Resultado in ResultadoListaSolInfoRelacionSP)
            //{
            //    SolInfoRelacionInsertarTMPResult_Result Reg = new SolInfoRelacionInsertarTMPResult_Result();
            //    Reg = Resultado;
            //    string Registro = Reg.Permisos;
            //    string[] Tokens = Registro.Split('|');
            //    string PermisosEnRenglon = "";
            //    foreach(string token in Tokens)
            //    {
            //        string Elemento = token;
            //        if(Elemento.Contains("VENCIDO"))
            //        {
            //            Elemento = "<font color=red>" + Elemento + "</font>";
            //        }
            //        PermisosEnRenglon += Elemento + "|";
            //    }
            //    PermisosEnRenglon = PermisosEnRenglon.Substring(0, PermisosEnRenglon.Length - 1);
            //    PermisosEnRenglon = PermisosEnRenglon.Replace(",", "</BR>");
            //    PermisosEnRenglon = PermisosEnRenglon.Replace("|", "</BR></BR>");  
            //    Reg.Permisos = PermisosEnRenglon;
            //    lista.Add(Reg);
            //}

            return lista;
        }

        public List<SolInfoRelacionBorrarTMP_Result> SolInfoRelacionBorrarTMPSP(int ID, string RNP)
        {
            //int IDENTRADA = ModeloTmp_.identrada;
            //List<SolInfoRelacionBorrarTMP_Result> ResultadoListaSolInfoRelacionBorrarSP = new List<SolInfoRelacionBorrarTMP_Result>();

            return null;// db.SolInfoRelacionBorrarTMP(ID,RNP).ToList();
        }

        public List<SolInfoRelacionBorrar_Result> SolInfoRelacionBorrarSP(int ID,string RNP)
        {
            //int IDENTRADA = ModeloTmp_.identrada;
            //List<SolInfoRelacionBorrarTMP_Result> ResultadoListaSolInfoRelacionBorrarSP = new List<SolInfoRelacionBorrarTMP_Result>();

            return null;// db.SolInfoRelacionBorrar(ID,RNP).ToList();
        }
    }

    public class SolInfoRelacionEntity
    {
        public int IdEntrada {get; set;}
        public string RNP {get; set;}
        public string Matricula {get; set;}
        public string Barco {get; set;}
        public string RazonSocial {get; set;}
        public string Transmitiendo {get; set;}
        public string UltimaTx {get; set;}
        public string EnPuerto {get; set;}
        public string NombrePuerto {get; set;}
        public string Permisos { get; set; }
    }
    public class SolicitudDeInformacionEntradaDatosBusqueda
    {
       // private BDMasContextEnt db = new BDMasContextEnt();
        //[StringLength(12, ErrorMessage = "Fecha no valida")]
        //[RegularExpression("dd/MM/yyyy",ErrorMessage="Fecha no valida")]
        //[DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd.MM.yyyy}")]
        public DateTime FechaInicial { get; set; }        
        public DateTime FechaFinal { get; set; }

        //public int IdInfoSol { get; set; }
        //public string Fecha { get; set; }
        //public string Hora { get; set; }
        //[Required(ErrorMessageResourceType = "es", ErrorMessageResourceName = "ValidationRequired")]
        //[RegularExpression(@"([A-Za-zäöüÄÖÜßăîâșțĂÎÂȘȚ\.\\/ ])+", ErrorMessageResourceType = typeof("ES"), ErrorMessageResourceName = "ValidationNumbersNotAllowed")]
        //[Required]
        //[RegularExpression(@"([A-Za-zäöüÄÖÜßăîâșțĂÎÂȘȚ\.\\/ ])+")]
        //[DataType(DataType.Text)]
        //[StringLength(100)]
        public string solicitante { get; set; }
        public string OFP { get; set; }
        [StringLength(13,ErrorMessage="Ingresar maximo 13 caracteres")]
        public string folio { get; set; }
        //public string Cargo { get; set; }
        //public string Telefono { get; set; }
        //public string Email { get; set; }
        //AGREGADO POR SZ EL 26/12/2016
        public string Ciudad { get; set; }
        public string Estado { get; set; }
        public string Embarcacion { get; set; }
        //FIN AGREGADO
        public string asunto { get; set; }
        public string dependencia { get; set; }
        public string medio { get; set; }
        //public string Descripcion { get; set; }

        public List<BuscaSolicitudesDeInformacion_Result> ResultadoListaSolInfoSP { get; set; }

        //COMENTADO POR SZ el 26/12/2016
        //public List<BuscaSolicitudesDeInformacion_Result> EncuentraLasSolInfoSP(DateTime FechaInicial,DateTime FechaFinal,string medio,string  asunto, string dependencia,
        //    string ofp,string solicitante,string folio)
        public List<BuscaSolicitudesDeInformacion_Result> EncuentraLasSolInfoSP(DateTime FechaInicial, DateTime FechaFinal, string medio, string asunto, string dependencia,
            string ofp, string solicitante, string folio, string embarcacion, string estado, string ciudad)
        {
            //string FechaFormateadaIni = FechaInicial.ToString("d", CultureInfo.CreateSpecificCulture("en-US"));
            string FechaFormateadaIni = FechaInicial.Year.ToString() + '-' + FechaInicial.Month.ToString().PadLeft(2, '0') + '-' + FechaInicial.Day.ToString().PadLeft(2, '0') + " 00:00:00";
            //string FechaFormateadaFin = FechaFinal.ToString("d", CultureInfo.CreateSpecificCulture("en-US"));
            string FechaFormateadaFin = FechaFinal.Year.ToString() + '-' + FechaFinal.Month.ToString().PadLeft(2, '0') + '-' + FechaFinal.Day.ToString().PadLeft(2, '0') + " 23:59:59";

            //if(medio != null) medio = medio.Replace("'","");
            //if(asunto != null) asunto = asunto.Replace("'","");
            //if(dependencia != null) dependencia = dependencia.Replace("'","");
            //if(ofp != null) ofp = ofp.Replace("'","");
            if(solicitante != null) solicitante = solicitante.Replace("'","");
            if(folio != null) folio = folio.Replace("'","");
            //AGREGADO POR SZ EL 26/12/2016
            if (embarcacion != null) embarcacion = embarcacion.Replace("'", "");
            if (estado != null) estado = estado.Replace("'", "");
            if (ciudad != null) ciudad = ciudad.Replace("'", "");
            //FIN AGREGADO
            //Modificado por SZ el 03/11/2016 se agregaron embarcación, estado y ciudad.
            //return db.BuscaSolicitudesDeInformacion(FechaFormateadaIni,FechaFormateadaFin,medio,asunto,dependencia,ofp,solicitante,folio).ToList();
            return null; //;db.BuscaSolicitudesDeInformacion(FechaFormateadaIni, FechaFormateadaFin, medio, asunto, dependencia, ofp, solicitante, folio, embarcacion, estado, ciudad).ToList();
        }
       
    }
}



   //public int IdInfoSol { get; set; }
   //     public string Fecha { get; set; }
   //     public string Hora { get; set; }
   //     public string Solicitante { get; set; }
   //     public string Cargo { get; set; }
   //     public string Telefono { get; set; }
   //     public string Email { get; set; }
   //     public string Ciudad { get; set; }
   //     public string Estado { get; set; }
   //     public string Asunto { get; set; }
   //     public string Dependencia { get; set; }
   //     public string Medio { get; set; }
   //     public string Descripcion { get; set; }
   //     public string OFP { get; set; }
   //     public string Folio { get; set; }