﻿var llamdastelefonicas = {
    popup: {},

    init: () => {
        $("#alerta").hide();
        llamdastelefonicas.popup = $("#popup").dxPopup("instance");
        llamdastelefonicas.popup.option("contentTemplate", $("#popup-template"));
    },

    AgregarLlamadaTmp: () => {
        $("#button-indicator").dxLoadIndicator("option", "visible", true);
        var args = {};
        args.IdBitaCel = $("#IdentificadorFinal").val();
        args.IdControl = 0;
        args.Fecha = $("#fechaLlamada").dxDateBox("instance").option("value");
        args.Hora = $("#mireloj").dxDateBox("instance").option("text");
        args.Tipo = $("#tipoLlamada").dxSelectBox("instance").option("value");
        args.Numero = $("#telefonoLlamada").dxTextBox("instance").option("text");
        args.Respuesta = $("#respuestallamada").dxSelectBox("instance").option("value");
        if (args.Respuesta === "SI") {
            args.Respuesta = true;
        } else {
            args.Respuesta = false;
        }

        $.ajax({
            url: agregarllamada,
            type: "POST",
            dataType: "json",
            data: JSON.stringify(args),
            contentType: "application/json",
            success: function (result) {
                console.log(result);
                if (result.valido === false) {
                    $("#mensaje").text(result.errores)
                    $("#alerta").fadeTo(2000, 1000).slideUp(1000, function () {
                        $("#alerta").slideUp(1000);
                    });
                }
                $("#gridTelefonos").dxDataGrid("instance").option({ dataSource: result.lista });
                $("#IdentificadorFinal").val(result.identificador);
                $("#button-indicator").dxLoadIndicator("option", "visible", false);
            },
            error: function (err) {
                console.log(err);
                $("#button-indicator").dxLoadIndicator("option", "visible", false);
            },
            timeout: 5000
        });
    },

    showPopUp: () => {
        llamdastelefonicas.popup.show();
    },

    ReloadGridEmbarcaciones: () => {
        $("#embarcaciones-indicator").dxLoadIndicator("option", "visible", true);
        $.ajax({
            url: buscarembarcaciones,
            type: "POST",
            dataType: "json",
            data: JSON.stringify({ opcionesbusqueda: $("#embarcaciones-tipo").dxSelectBox("instance").option("value"), valor: $("#embarcaciones-valor").dxTextBox("instance").option("value") }),
            contentType: "application/json",
            success: function (result) {
                $("#embarcaciones-indicator").dxLoadIndicator("option", "visible", false);
                console.log(result);
                $("#gridEmbarcaciones").dxDataGrid("instance").option({ dataSource: result });
            },
            error: function (err) {
                $("#embarcaciones-indicator").dxLoadIndicator("option", "visible", false);
                console.log(err);
            },
            timeout: 5000
        });
    },

    RegresaValoresEmbarcacion: (Nombre, RazonSocial, Matricula, RNP, PuertoBase) => {
        $("#Barco").dxTextBox("instance").option({ value: Nombre });
        $("#RazonSocial").dxTextBox("instance").option({ value: RazonSocial });
        $("#Matricula").dxTextBox("instance").option({ value: Matricula });
        $("#RNP").dxTextBox("instance").option({ value: RNP });
        $("#PuertoBase").dxTextBox("instance").option({ value: PuertoBase });
        $("#popup").dxPopup("instance").hide();
    },

    dateBox_valueChanged: (data) => {
        $("#horario").dxTextBox("instance").option({ "value": $("#mireloj").dxDateBox("instance").option("text") });
    },

    BorrarLlamada: (id) => {
        var args = {};
        args.IdBitaCel = $("#titIdBitaCel").dxTextBox("instance").option("value");
        args.IdControl = id;

        $.ajax({
            url: borrarllamada,
            type: "POST",
            dataType: "json",
            data: JSON.stringify(args),
            contentType: "application/json",
            success: function (result) {
                console.log(result);
                $("#gridTelefonos").dxDataGrid("instance").option({ dataSource: result });
            },
            error: function (err) {
                console.log(err);
            },
            timeout: 5000
        });
    },

    AgregarLlamada: () => {
        $("#button-indicator").dxLoadIndicator("option", "visible", true);
        var args = {};
        args.IdBitaCel = $("#titIdBitaCel").dxTextBox("instance").option("value");
        args.IdControl = 0;
        args.Fecha = $("#fechaLlamada").dxDateBox("instance").option("value");
        args.Hora = $("#mireloj").dxDateBox("instance").option("text");
        args.Tipo = $("#tipoLlamada").dxSelectBox("instance").option("value");
        args.Numero = $("#telefonoLlamada").dxTextBox("instance").option("text");
        args.Respuesta = $("#respuestallamada").dxSelectBox("instance").option("value");
        if (args.Respuesta === "SI") {
            args.Respuesta = true;
        } else {
            args.Respuesta = false;
        }

        $.ajax({
            url: agregarllamada,
            type: "POST",
            dataType: "json",
            data: JSON.stringify(args),
            contentType: "application/json",
            success: function (result) {
                console.log(result);
                if (result.valido === false) {
                    $("#mensaje").text(result.errores)
                    $("#alerta").fadeTo(2000, 1000).slideUp(1000, function () {
                        $("#alerta").slideUp(1000);
                    });
                }
                $("#gridTelefonos").dxDataGrid("instance").option({ dataSource: result.lista });
                $("#button-indicator").dxLoadIndicator("option", "visible", false);
            },
            error: function (err) {
                console.log(err);
                $("#button-indicator").dxLoadIndicator("option", "visible", false);
            },
            timeout: 5000
        });
    }
};

(() => {
    setTimeout(() => { llamdastelefonicas.init(); }, 500);
})()
