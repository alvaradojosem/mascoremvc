﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MASCore.DataModel
{

    public partial class BuscaLlamadas_Result
    {
        public Nullable<System.DateTime> Fecha { get; set; }
        public string Hora { get; set; }
        public string Respuesta { get; set; }
        public string Barco { get; set; }
        public string RNP { get; set; }
        public string Matricula { get; set; }
        public string RazonSocial { get; set; }
        public string OFP { get; set; }
        public int IdBitaCel { get; set; }
    }
}
