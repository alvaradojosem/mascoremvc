﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using MASCore.ModelReference;

namespace MASCore.Models
{
    public class MantenimientoModel
    {

    }
    public class MantenimientoBusquedaModel
    {
        #region "PARAMETROS TOTALES BARCOS / FOLIOS"
        public int TotalBarcos { get; set; }
        public int TotalFolios { get; set; }
        #endregion

        #region "PARAMETROS PARA LA BUSQUEDA RESUMEN"
        //  VARIABLES PARA SELECCION
        [DisplayName("Servicio")]
        public Boolean ServicioSel		{ get; set; }
        [DisplayName("Puerto Base")]
	    public Boolean PuertoBaseSel	{ get; set; }
        [DisplayName("Razón Social")]
	    public Boolean RazonSocialSel	{ get; set; }
        [DisplayName("Supervisor")]
	    public Boolean SupervisorSel	{ get; set; }
        [DisplayName("Zona")]
	    public Boolean ZonaSel			{ get; set; }
        [DisplayName("Barco")]
        public Boolean BarcoSel         { get; set; }
        //  VARIABLES PARA CONTAR
        [DisplayName("Contar")]
        public Boolean Contar { get; set; }
        [DisplayName("Contar Barcos")]
        public Boolean NoBarcos	        { get; set; }
        [DisplayName("Contar Servicios")]
        public Boolean NoServicios	    { get; set; }
        [DisplayName("Contar Ambos")]
        public Boolean NoAmbos          { get; set; }
        #endregion

        #region "PARAMETROS PARA LA BUSQUEDA"
        [DisplayName("Rango de Fecha")]
        public Boolean UsaFechas { get; set; }
        
        [DisplayName("Fecha Inicial:")]
        public DateTime FechaInicio { get; set; }
        [DisplayName("Fecha Final:")]
        public DateTime FechaFin { get; set; }        

        [DisplayName("Servicios:")]
        public string Servicios { get; set; }

        [DisplayName("Embarcación:")]
        public string Embarcacion { get; set; }

        [DisplayName("Nombre Barco:")]
        public string NombreBarco { get; set; }
        

        [DisplayName("Antena:")]
        public string Antena { get; set; }
        [DisplayName("Puerto Base:")]
        public string PuertoBase { get; set; }
        [DisplayName("Razón Social:")]
        public string RazonSocial { get; set; }
        [DisplayName("Permiso:")]
        public string Permiso { get; set; }
        [DisplayName("Zona:")]
        public string Zona { get; set; }


        [DisplayName("Muelle:")]
        public string Muelle { get; set; }
        [DisplayName("Localidad:")]
        public string Localidad { get; set; }
        [DisplayName("Supervisor:")]
        public string Supervisor { get; set; }

        [DisplayName("No. Folio:")]
        public string Folio { get; set; }


        #endregion

        #region "USADOS PARA MOSTRAR DIVS"
        public Boolean MostrandoResumen { get; set; }
        public Boolean MostrandoBusquedaInformacionEmbarcacion { get; set; }
        #endregion


        public List<BuscaInformacionDeEmbarcacion_Result> InformacionDeEmbarcacion { get; set; }    // USADO PARA OBTENER LA INFORMACION DE EMBARCACIONES

        public List<BuscaMantenimientos_Result> ResultadoListaMantenimientos { get; set; }

        public List<MantenimientoResumen> ResultadoListaMantenimientosResumen { get; set; }
        public List<BuscaMantenimientosResumenNegacion_Result> ResultadoListaMantenimientosResumenNegacion { get; set; }

    }
    public class MantenimientoResumen
    {
        public string Id_Mantto { get; set; }
        public DateTime Fecha_Mantto { get; set; }
        public string Servicio { get; set; }
        public string NombreBarco { get; set; }
        public string Antena { get; set; }
        public string MatBarco { get; set; }
        public string RNP { get; set; }
        public string Localidad { get; set; }
        public string RazonSocial { get; set; }
        public string Zona { get; set; }
        public string Muelle { get; set; }
        public string Supervisor { get; set; }
        public string Recibe { get; set; }

        public int NoBarcos { get; set; }
        public int NoServicios { get; set; }

    }

  
}